## PEPSSBI Software Manual

### Table of Content

[TOC]

### Parameter Estimation Pipeline for Systems and Synthetic Biology

The Parameter Estimation Pipeline for Systems and Synthetic Biology (PEPSSBI) is a computer program for the automation of the parameter estimation work flow. It includes:

- data normalisation;
- model definition in ordinary differential equations with multi-condition experiments support;
- construction of objective functions based on data-driven normalisation of the simulations;
- generation of code compatible with High Performance Computing (HPC) infrastructures with Portable Batch System (PBS) job scheduler;
- plotting of the results with gnuplot

![](http://andreadegasperi.x10.mx/files/PEPSSBI_web.jpg)

PEPSSBI's main application filed is the dynamic modelling of cellular signalling pathways using Ordinary Differential Equations (ODEs). Available parameter estimation algorithms are:

- GLSDC, genetic local search algorithm with distance independent diversity control *(Kimura and Konagaya, 2003)*;
- LSQNONLIN SE, Levenberg-Marquardt optimization algorithm for non linear least squares with Latin Hypercube sampling starting points. Gradient computed using sensitivity equations in CVODES (SE) *(Raue et al. 2013)*;
- LSQNONLIN FD, Levenberg-Marquardt optimization algorithm for non linear least squares with Latin Hypercube sampling starting points. Gradient computed using finite difference approximation in CVODES (FD).

Our implementation of LSQNONLIN SE and LSQNONLIN FD uses an open source version of the Levenberg-Marquardt nonlinear least squares algorithm *(Lourakis 2009)*.

A central component of PEPSSBI is the Systems Biology Compiler (SBC).  A compiler is a program that converts a computer language to a lower level computer language. For example, a C compiler, converts a human readable C code into machine readable instructions. The SBC converts an input file written in the SBC/PEPSSBI input language described in this document into C code and scripts that compose the PEPSSBI program. 

![](http://andreadegasperi.x10.mx/files/PEPSSBI_arch_web.jpg)

The compile/code generation step using SBC allows the definition of a simplified and minimal input language, which in turns allows the user to reduce its interactions with the software, thus saving time and reducing possible sources of erros. For example, the user can copy/paste the raw data from an excel file and with a single line of SBC input language instruct the compiler to normalise the biological replicates with respect to the average (normalisation by average) of their data points or with respect to a specific control data point (normalisation by fixed point). For more details about data normalisation see the dedicated sections below or *(Degasperi et al. 2014)*. The use of SBC also allows for several errors to be identified *at compilation time*, that is before running the optimisation algorithms. Some examples are missing variables or conditions for simulation, normalisation or parameter estimation or consistency tests for the data format.

One of the most important features in PEPSSBI is the normalisation of the simulations. In order to compare the data, which are in arbitrary units, with the model simulations, which are in concentration or number of molecules, we apply to the simulations the same normalisation that was applied to the data. The alternative, would be to estimate scaling factors, thus incresing the dimensionality of the parameter search space and increasing the work necessary by the user to edit the model. In PEPSSBI, one can simply write the model, add the data and specify how to normalise the data. **By giving the data the same identifier as the corresponding model variable, the SBC is instructed to apply the same normalisation to the simulation of that variable.** For more details about DNS and the objective function definitions see *(Degasperi et al. 2017)*.

To use the program download a zipped build from the ```download``` page of the bitbucket website (https://bitbucket.org/andreadega/systems-biology-compiler/downloads).
A manual can be found in the bitbucket wiki page (https://bitbucket.org/andreadega/systems-biology-compiler/wiki/Home). 

#### References

*(Kimura and Konagaya, 2003)* Kimura, S. and A. Konagaya, **High dimensional function optimization using a new genetic local search suitable for parallel computers.** *2003 Ieee International Conference on Systems, Man and Cybernetics*, Vols 1-5, Conference Proceedings, 2003: p. 335-342.

*(Raue et al. 2013)* Raue A., et al. **Lessons Learned from Quantitative Dynamical Modeling in Systems Biology.** *PLOS ONE*, 8(9), e74335, 2013.

*(Lourakis 2009)* Lourakis, M. (2009). **Levmar: Levenberg-marquardt nonlinear least squares algorithms in C/C++(2004).** Software available at http://www.ics.forth.gr/~lourakis/levmar.

*(Degasperi et al. 2014)* A. Degasperi, M. R. Birtwistle, N. Volinsky, J. Rauch, W. Kolch, B. N. Kholodenko. **Evaluating Strategies to Normalise Biological Replicates of Western Blot Data.** *PLoS ONE*, 9(1): e87293. doi:10.1371/journal.pone.0087293, 2014.

*(Degasperi et al. 2017)* A. Degasperi, D. Fey, B. N. Kholodenko. **Performance of objective functions and optimisation procedures for parameter estimation in system biology models.** npj Systems Biology and Applications, doi:10.1038/s41540-017-0023-2, 2017.


### SBML to SBC converter

A small java program that converts SBML to the SBC input language is in development here: 

https://bitbucket.org/andreadega/sbml2sbc/

### Geany syntax highlighter

A simple SBC syntax highlighter for the Geany IDE is available here (filetypes.sbc.conf): 

https://bitbucket.org/andreadega/systems-biology-compiler/downloads

### Versions

- Version 2.2
    - This build version comes with pre-compiled PEPSSBI libraries for Linux 64 bits
    - added summaryTable.txt which is a more human readable table of the optimal parameters obtained from the parameter estimation
- Version 2.1
    - Recovery of best fits reached even if the algorithm stops unexpectedly (e.g. PBS wall time)
    - optimisation of Automated Differentiation
    - tested on OSX (using Homebrew) and Windows (using MSYS2 and MinGW-w64)
- Version 2.0
    - New parameter estimation algorithms added: LSQNONLIN SE and LSQNONLIN FD
    - New objective function added: LogLikelihood with linear model of noise
    - Reorganisation of libraries. CVODES 2.8.2 added to substitute old CVODE library, GSL added once again, Lapack and Levmar libraries also added
    - Added tracking of function evaluation values versus time and number of function evaluations. This is useful to examine convergence of algorithms and to compare their performance
    - Automated differentiation of df/dx Jacobian matrix (all ODE simulations use it now) and df/dp sensitivity right hand side (LSQNONLIN SE)
    - Normalisation by sum changed to normalisation by average
- Version 1.3
    - files reorganisation
    - initialisation of differential variable (init) now supports multiplication by a factor
- Version 1.2:
    - removed gsl (Gnu Scientific Library) from this build, gsl is now a
      system requirement
    - tested on OSX 10.8.5 (Mountain Lion)
- Version 1.1:
    - added function definitions and events
    - added dose response plots and two variables plots
    - small bug fixes and improvements
- Version 1.0:
    - SBC generates files for parameter estimation with GLSDC (genetic local search algorithm with distance independent diversity control) in the folder KimuraGLSDC
    - SBC generates files for the optimal parameter sets analysis in the folder paramterAnalysis.
    - SBC generates files for simulating and plotting in the KimuraODE folder

### Setting things up

#### Using the latest Linux version with pre-compiled libraries

##### Requirements of version 2.2

    java >= 1.6
    g++ >= 4.4.7
    R >= 2.14.1 with "ellipse" package for correlation matrix of paramter sets
    gnuplot >= 4.6
    gfortran
    gcc

##### Download the latest build

Download the build version 2.2 from the bitbucket download page:

[https://bitbucket.org/andreadega/systems-biology-compiler/downloads](https://bitbucket.org/andreadega/systems-biology-compiler/downloads)

The file name of the latest build is ```2017_11_25_SBC_PEPSSBI_build_v2_2.zip```. This zip file contains:

- the ```build``` folder with the PEPSSBI software already compiled and ready to run
- the ```docs``` folder with the manual
- the ```examples``` folder with example input files
- the ```examples_from_manual``` folder with example input files from this manual

Please note that if you want to run PEPSSBI on OSX or Windows you should either use a Docker image of PEPSSBI or download and compile the latest source code. Instructions on how to use a PEPSSBI Docker image and how to set up PEPSSBI on OSX (using Homebrew) or Windows (using MSYS2 and MinGW-w64) are given below.

- [*Running a Docker image of PEPSSBI (operative system independent)*](#running-a-docker-image-of-pepssbi-operative-system-independent)
- [*Compiling and running the latest source code on Mac OSX*](#compiling-and-running-the-latest-source-code-on-mac-osx)
- [*Compiling and running the latest source code on Windows*](#compiling-and-running-the-latest-source-code-on-windows)

##### Running the SBC 

Enter the build directory, the java jar file ```sbc.jar``` contains the SBC. Double click on ```sbc.jar``` or run from terminal:


    ./runSBC.sh


and an input dialog will appear where you can select an input file. Alternatively, you can run:


    ./runSBC.sh [inputfile]


to give the ```[inputfile]``` directly without using the input dialog. 

*IMPORTANT*: check for errors in the file ```results.log```. When using ```./runSBC.sh``` the content of ```results.log``` will be displayed automatically.

##### Java issues

If the ```sbc.jar``` fails to run when double clicking or when running ```./runSBC.sh``` on its own, this could be a problem with the Java user interface. Try to bypass this by running SBC with an input file from the command line:

    ./runSBC.sh [inputfile]

If the above command still gives a Java error, this is usually due to incompatibility of Java compilers and Java virtual machines. Unfortunately, this means you will have to compile the latest source code on your own, or use a Docker image of PEPSSBI.

- [*Running a Docker image of PEPSSBI (operative system independent)*](#running-a-docker-image-of-pepssbi-operative-system-independent)
- [*Compiling and running the latest source code on Linux*](#compiling-and-running-the-latest-source-code-on-linux)

##### Running the PEPSSBI automatically generated scripts

If ```./runSBC.sh [inputfile]``` executed succesfully, you will find one or more of the following scripts in the ```build``` directory:

- ```runSimulations.sh```
- ```runParameterEstimationOnUnixCluster.sh```
- ```runParameterEstimationOnLinuxDesktop.sh```
- ```runParameterAnalysis.sh```

Each of these files is explained in the *Running the Analysis* section below. Note that to run the simulations or parameter estimation scripts above, you need to have gcc and gfortran (for LSQNONLIN/LevMar) installed. This is because the C/C++ code generate by ```./runSBC.sh``` needs to be compiled. If there are errors running these scripts, it may be because the compilation of the generated files fails to link with the precompiled library. If this is the case you will have to compile the latest source code on your own, or use a Docker image of PEPSSBI.

- [*Running a Docker image of PEPSSBI (operative system independent)*](#running-a-docker-image-of-pepssbi-operative-system-independent)
- [*Compiling and running the latest source code on Linux*](#compiling-and-running-the-latest-source-code-on-linux)

#### Running a Docker image of PEPSSBI (operative system independent)

##### Installing Docker

Docker is a virtual machine technology that allows software code and all necessary libraries to be wrapped into a *Docker image*. This image can be run on any computer and operative system that has Docker installed. You can install Docker on Windows, OSX or Linux ([https://www.docker.com/](https://www.docker.com/)).

##### The PEPSSBI Docker image

The PEPSSBI Docker image is  available at the url [https://hub.docker.com/r/andreadega/pepssbi/](https://hub.docker.com/r/andreadega/pepssbi/). It contains all libraies and software necessary to run PEPSSBI, including a linux environment, R, gnuplot, Java, gcc and gfortran. It is approximately 500MB of compressed download and about 1.2 GB once extracted.

##### Pull and run the PEPSSBI Docker image

Assuming Docker is installed on your system, you should be able to pull a copy of the PEPSSBI image from the public repository as follows:

    docker pull andreadega/pepssbi

After the image has been downloaded, you can run it in interactive mode (```-it``` option) and enter in the PEPSSBI working directory inside the image (```-w /home/PEPSSBI```, where ```/home/PEPSSBI``` is a path inside the PEPSSBI Docker image):

    docker run -w /home/PEPSSBI -it andreadega/pepssbi

You should now be in a Docker container, with the following directories available:

- the ```build``` folder with the PEPSSBI software already compiled and ready to run
- the ```docs``` folder with the manual
- the ```examples``` folder with example input files
- the ```examples_from_manual``` folder with example input files from this manual

You can also tell that you are in a container because the command promt of the terminal has changed into something like the following:

    root@d27d510fac0b:/home/PEPSSBI#

The above command prompt indicates that you are currenly the root user (full administrator access) of the computer ```d27d510fac0b```, which is a randomly generated identifier for the *virtual* computer that is in fact the new Docker container.

When you are inside a PEPSSBI Docker container, you are in practice in a Docker version of Ubuntu Linux, with R, Java, gnuplot, gcc and gfortran already installed for you. A Docker image is a read only file, while when the image is run, a Docker container is created which is writable. To copy your PEPSSBI input files into the Docker container and to copy the results of the analysis back from the container to your folders, you need to use the command ```docker cp```.

##### Copying files to and from a PEPSSBI Docker container

After running the the PEPSSBI Docker image as explained above, a container is created. To see the name of the running container, you need to open another terminal and use the ```docker``` command. You will notice that the ```docker``` command is not available while inside the container, which you accessed with the interactive mode above. This is normal, because the contained is an instance of the PEPSSBI image run in isolation, and has only the software necessary to run PEPSSBI.

While the container is running, open another terminal and run the following ```docker``` command:

    docker ps

You should see the active container listed in a table like the following:

    CONTAINER ID	IMAGE	COMMAND	CREATED	STATUS	PORTS	NAMES
    d27d510fac0b	andreadega/pepssbi	"/bin/bash"	26 seconds ago	Up 25seconds	sad_edison

The last column gives the randomly generated name of the container that can be used to access it, in our case ```sad_edison```. To copy a file, such as a PEPSSBI input file, into the container use the following command:

    docker cp fileTocopy sad_edison:/home/PEPSSBI

The file should now be available inside the running container. After running the parameter estimation pipeline, you can copy the results back, for example copying the ```build``` folder from the container to the current directory:

    docker cp sad_edison:/home/PEPSSBI/build .

Where the ```.``` is used to indicate the current directory outside the container.

##### Going back to a previously used Docker container

As we have seen above, it is possible to run a new container, which is a writable instance of a Docker image, using the following command:

    docker run -w /home/PEPSSBI -it andreadega/pepssbi

To exit the container just use the command ```exit```. After leaving the container, the container is stopped and becomes inactive. To visualise the inactive containers use the following command:

    docker ps -a

The above command visualises all containers, whether they are active or not. To go back into an inactive container, use the ```docker start``` command:

    docker start -i sad_edison

It is also possible to remove unused containers:

    docker rm sad_edison


#### Compiling and running the latest source code on Linux

##### Download the latest source code

Clone a copy of the git repository:

    git clone --depth=1 https://bitbucket.org/andreadega/systems-biology-compiler.git

The option --depth=1 is used to copy only the latest version of the file, reducing the time of download.

##### Compile the systems biology compiler (SBC)

The SBC is written in Java. To compile it, you need to have a Java Development Kit (JDK) installed, which contains the command ```javac```.

To compile the SBC enter the directory ```systems-biology-compiler``` and run the ```compile.sh``` script:

    cd systems-biology-compiler
    ./compile.sh

You should find the compiled Java jar file ```sbc.jar``` in the ```build``` directory.

##### Building libraries

It is now possible to run the *compile* part of PEPSSBI, that is the ```./runSBC.sh``` file. However, before the generated C/C++ files can be run, the libraries used by the PEPSSBI simulation and optimisation algorithms need to be compiled. The source code of the libraries is located in the ```build/common``` folder. The libraries that need to be compiled are CVODES, GA, GSL, LEVMAR and LAPACK.

To compile the libraries the following command in the ```build``` directory:

    ./buildLibraries.sh

If you do not run the above script to build the libraries, the scripts generated by ```./runSBC.sh [inputfile]``` will run it for you. Building libraries can take some time, but needs to be done only once.


#### Compiling and running the latest source code on Mac OSX

##### Set-up instructions for PEPSSBI version 1.3 (tested on OSX 10.8.5 (Mountain Lion)) - Deprecated

The following instructions have only been tested for PEPSSBI version 1.3. Although these instructions are deprecated, they still may be useful for some.

The SBC is written in java, so it is possible to compile SBC files and generate source code on any machine that has java installed. The generated code can be copied on a linux cluster and run there if the above requirements are satisfied. It is also possible to run the generated code on a Mac in the OSX operative system. To do so, follow the instructions below.

- Install macports with Xcode and the Xcode Command Line Tools (https://www.macports.org/install.php). After installation open a terminal and use macport to install gnuplot and gsl:

        sudo port install gnuplot

- Install java. Depending on your version of OSX, it may be sufficient to type ```java``` in a terminal and if java is not present you will be asked whether you want to install it. After installation, the ```java``` command should be available in the terminal.
- Install R (http://cran.r-project.org/bin/macosx/). After installation, the commands ```R``` and ```Rscript``` should be available in the terminal. You may be asked to install X11 or XQuartz. X11 is not supported any more, so install XQuartz (http://xquartz.macosforge.org/landing/). You also need to install the additional ```ellipse``` package in R. To do so, open a terminal and type ```R``` to enter the R environment and type:
  
        install.packages("ellipse")
  you will be asked to select a server from which to download the package.

##### Set-up instructions for the latest PEPSSBI source code (tested on OSX 10.11.6 (El Capitan))

Download the latest source code of PEPSSBI:

    git clone --depth=1 https://bitbucket.org/andreadega/systems-biology-compiler.git

Then enter the ```systems-biology-compiler``` directory and run the ```compile.sh``` script:

    cd systems-biology-compiler
    ./compile.sh

The above step will produce the ```sbc.jar``` Java jar file that contains the SBC compiler in the ```build``` directory.

PEPSSBI was tested on OSX 10 using Homebrew ([https://brew.sh/](https://brew.sh/)). After installing Homebrew, run the following commands:

    brew update
    brew install cmake
    brew install gcc
    brew install gnuplot --with-cairo

We found that this installation can take some time. In particular, installing gcc can take about one hour. The installation of ```gcc``` should also install ```gfortran```.

In our installation of ```gcc```, the ```gfortran``` libraries were not added correcly to the library path. If this happens, running the LSQNONLIN FD and LSQNONLIN SE optimisation algorithms will give the following error:

    ld: library not found for -lgfortran

The library is installed, but ```ld``` cannot find it. To fix this, you need to locate the library files in your homebrew installation and create a symbolic link to your library path. You can use PEPSSBI library path, which is ```build/common/lib/```. Our libgfortran paths were:

    ~/homebrew/Cellar/gcc/7.2.0/lib/gcc/7/libgfortran.4.dylib
    ~/homebrew/Cellar/gcc/7.2.0/lib/gcc/7/libgfortran.a
    ~/homebrew/Cellar/gcc/7.2.0/lib/gcc/7/libgfortran.dylib
    ~/homebrew/Cellar/gcc/7.2.0/lib/gcc/7/libgfortran.spec

Note that the location of the files above may be different for you, for example, you may have a different ```gcc``` version. In our case, we created the symbolic links in the PEPSSBI library folder as follows:

    cd build/common/lib/
    ln -s ~/homebrew/Cellar/gcc/7.2.0/lib/gcc/7/libgfortran.4.dylib
    ln -s ~/homebrew/Cellar/gcc/7.2.0/lib/gcc/7/libgfortran.a
    ln -s ~/homebrew/Cellar/gcc/7.2.0/lib/gcc/7/libgfortran.dylib
    ln -s ~/homebrew/Cellar/gcc/7.2.0/lib/gcc/7/libgfortran.spec

You also need to install R for OSX.

Everything should now be ready to run the PEPSSBI scripts.

#### Compiling and running the latest source code on Windows

##### Set-up instructions for the latest PEPSSBI source code (tested on Windows 10 Home)

PEPSSBI was tested on Windows 10 64bit using MinGW-w64 ([https://mingw-w64.org/doku.php](https://mingw-w64.org/doku.php)) and MSYS2 ([http://www.msys2.org/](http://www.msys2.org/)). The former provides ```gcc```, ```gfortran``` and other libraries necessary to compile the PEPSSBI libraries, while the latter is a terminal that will be used to run the scripts.

**Install MinGW-w64.** We installed the MinGW-w64 files from [https://sourceforge.net/projects/mingw-w64/files](https://sourceforge.net/projects/mingw-w64/files), in particular the 64 bits version that can be found [https://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win64/Personal%20Builds/mingw-builds/7.2.0/threads-posix/sjlj/](https://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win64/Personal%20Builds/mingw-builds/7.2.0/threads-posix/sjlj/). Download and extract the files in a local folder. Take note of the location of the ```mingw64/bin``` subfolder, which contains the binary files you need.

**Install MSYS2.** Download and installation instructions can be found on the MSYS2 website ([http://www.msys2.org/](http://www.msys2.org/)). Download the x86_64 version.

You should now have a working MSYS2, which you can run by double clicking on the ```msys2_shell.cmd``` file in the MSYS2 main directory. Using the ```pacman``` package manager, install the necessary software:

    pacman -s git
    pacman -s make
    pacman -s cmake
    pacman -s tar

Also, install Java ([http://www.oracle.com/technetwork/java/javase/downloads/jdk9-downloads-3848520.html](http://www.oracle.com/technetwork/java/javase/downloads/jdk9-downloads-3848520.html)), R ([https://cran.r-project.org/bin/windows/base/](https://cran.r-project.org/bin/windows/base/)) and  gnuplot ([https://sourceforge.net/projects/gnuplot/files/gnuplot/5.2.0/](https://sourceforge.net/projects/gnuplot/files/gnuplot/5.2.0/)).

In order to use the installed software in the MSYS2 terminal, you need to add all the directories that contain the binary files to the ```PATH``` variable. Bear in mind that in MSYS2 folders structure is defined using the back slash (```/```), like in linux, instead of the forward slash (```\```) used in Windows. Also, instead of writing ```C:\```, one should write ```/c/```. For example, our path to the MinGW-w64 binaries is ```C:\mingw64\bin```, so in MSYS2 we can add it to the ```PATH``` variable as follows:

    PATH=$PATH:/c/mingw64/bin

You can add multiple paths, separated by ```:```, and you can add this to your ```.bashrc``` file so it is executed every time you open the MSYS2 terminal. The ```.bashrc``` file should be located in your home directory, that you can access by typing ```cd ~```. If the ```.bashrc``` is not present you can create it.

    PATH=$PATH:/c/mingw64/bin:/path/to/java/binaries:/path/to/gnuplot/binaries:/path/to/R/binaries

Download the latest source code of PEPSSBI:

    git clone --depth=1 https://bitbucket.org/andreadega/systems-biology-compiler.git

Then enter the ```systems-biology-compiler``` directory and run the ```compile.sh``` script:

    cd systems-biology-compiler
    ./compile.sh

The above step will produce the ```sbc.jar``` Java jar file that contains the PEPSSBI compiler in the ```build``` directory.

You should now be able to build the PEPSSBI libraries:

    cd build
    ./buildLibraries.sh

Because MSYS2 is not using the ```LD_LIBRARY_PATH``` variable, we need to add the PEPSSBI library path to the ```PATH``` variable. This is necessary to tell the compiled code where to find the PEPSSBI libraries that we built. In MSYS2, our location of the PEPSSBI libaries is ```~/systems-biology-compiler/build/common/lib```. We can add it to PATH as we did above for the binary folders:

     PATH=$PATH:~/systems-biology-compiler/build/common/lib

Everything should now be ready to run the PEPSSBI scripts.

### Quick start Example

In this section we illustrate an example of PEPSSBI input file and how to run this example using PEPSSBI.

	data aK =
	"time" "condition 1" "condition 2"
	0 25.5418871571589 58.1780900055649
	5 49.1576803942669 107.782379718123
	15 91.7768588016566 167.280391296763
	30 103.624540419953 301.601872011214
	-
	0 29.7413496797732 51.9152866925858
	5 61.2130925964036 100.377131943546
	15 89.042042929915 192.521825341902
	30 97.9498138785651 313.379258299853
	-
	0 27.9125670975692 57.7040749043549
	5 51.7476556812738 102.497696231552
	15 83.421634478456 216.951141156244
	30 95.0241068941944 347.87061205011
	-
	0 26.4822423516201 45.3598594746283
	5 50.5067484629506 113.33728331398
	15 94.8504232172411 194.80714603506
	30 106.69395435998 340.587797759041

	normaliseByAverage aK "condition 1" "condition 2"

	data S =
	"time" "condition 1" "condition 2"
	0      10.0 10.0
	5      9.5  9.5
	15     8.0  8.0
	30     7.0  7.0

	par kcat1 = [0.1,10]
	par Km1 = [1,100]
	par kback = 0.0001
	par kbasal = 0.001
	par init_aKc1 = [1,100]
	par init_aKc2 = [1,100]

	Ktot =
	"condition 1" 100
	"condition 2" 1000

	S = dataLinear

	init aK = 
	"condition 1" init_aKc1
	"condition 2" init_aKc2

	aK' = (S*kcat1*(Ktot-aK))/(Km1+(Ktot-aK)) - kback*aK + kbasal*(Ktot-aK)

	fittingData =
	aK "condition 1"  "condition 2"

	simulateOutput =
	aK "condition 1"  "condition 2"

	simulationOption simulationTime 30
	simulationOption numberOfTimePoints 1000
	simulationOption abstol 1e-6
	simulationOption reltol 1e-6

	fittingOption numberOfProcessorsPerNode 4
	fittingOption numberOfJobsPerProcessor 5

The above example is located in the ```examples_from_manual``` folder, file name ```kinaseExample.sbc```. Full details about the PEPSSBI input language are given in the section [*Input File Specifications*](#input-file-specifications).

In the above example, parameters ```kcat1```, ```Km1```, ```init_aKc1``` and ```init_aKc2``` are defined as ranges, indicated with the syntax ```[lower_bound,upper_bound]```. This implies that these parameters are unknown and need to be estimated as values within these ranges.

The variable that will be simulated by this model is the amount of active kinase ```aK```. The ordinary differential equation (ODE) for the active kinase is given by the line starting with ```aK'```. The hyphen ```'``` indicates time derivative.

There are two experimental conditions: ```condition 1``` and ```condition 2```. The two conditions differ in terms of total amount of kinase, and the intial amount of active kinase. The total amount of kinase is defined by the line starting with ```Ktot = ``` and the successive two lines. When simulating ```condition 1```, ```Ktot``` is equal to ```100```, while when simulating ```condition 2```, ```Ktot``` is equal to ```1000```. The initial amount of active kinase is defined by the line starting with ```init aK = ``` and the successive two lines. When simulating ```condition 1```, the initial amount of active kinase is equal to the value of the parameter ```init_aKc1```, while when simulating ```condition 2```, the initial amount of active kinase is equal to the value of the parameter ```init_aKc2```.

The line beginning with ```data aK``` specifies the time course data for the active kinase. These data are assumed to be organised in blocks, with blocks separated by the ```-``` symbol, and each block representing an independent biological replicate with its own arbitrary units. Thus, we assume that data points are comparable within a data block, but not across data blocks. 

The line ```normaliseByAverage aK "condition 1" "condition 2"```, indicates that we request a normalisation by average for the data blocks of the active kinase data. For each data block, the data points in the block are divided by the average of all data points in the ```condition 1``` and ```condition 2``` columns in that block.

The option ```fittingData``` indicates that we request to generate files for running a parameter estimation problem. The successive line, indicates that we request the simulated time course of the active kinase, i.e. variable ```aK```, to be fitted to the corresponding time course data. Because, there are both an ODE entry with identifier ```aK``` and a data entry with the same identifier ```aK```, then the objective function for the parameter estimation problem can be constructed. Moreoever, because we have requested normalisation by average for the ```aK``` data blocks, then the simulations will also be normalised in the same way, thus making normalised simulations and normalised data naturally comparable (we call this procedure *data-driven normalisation of the simulation*). By default, the objective function used is Least Squares. Alternatively, log-likelihood with linear model of noise can also be employed (see section [*Fitting options*](#fitting-options)).

The option ```simulateOutput``` indicates that we request time course plots to be generated using the optimal parameter sets obtained from the simulations.

We assume this example is run on a desktop or laptop computer, which implies that only one computing node is used. We also assume that such computer has at least four processors (such as an Intel i5). The options ```fittingOption numberOfProcessorsPerNode 4``` and ```fittingOption numberOfJobsPerProcessor 5``` indicate that we request to use four processors in parallel and that we want to run five parameter estimation runs on each processor. This means that we request a total of 4x5=20 parameter estimation runs.

PEPSSBI is composed by two main steps: compile and run. In the *compile* step, we compile the PEPSSBI input language using a java compiler (the systems biology compiler - SBC). This step will generate C/C++ code and scripts to run the optimisation problem. The *compile* step for our example is executed as follows in the ```build``` directory:

    ./runSBC.sh ../examples_from_manual/kinaseExample.sh

The above command, generates the following files:

- ```runSimulations.sh```
- ```runParameterEstimationOnUnixCluster.sh```
- ```runParameterEstimationOnLinuxDesktop.sh```
- ```runParameterAnalysis.sh```

In our case, the script that we are interested in is ```runParameterEstimationOnLinuxDesktop.sh```, which will run the parameter estimation locally. We can run this file as follows:

    ./runParameterEstimationOnLinuxDesktop.sh

When the above script is executed, the default parameter estimation algorithm (GLSDC) is used. To run a different algorithm, specify the algorithm as input to the script:

    ./runParameterEstimationOnLinuxDesktop.sh LSQNONLIN_SE

Default parameters parameters for these algorithms are given in the section [*Fitting options*](#fitting-options).

In addition, after the parameter estimation has completed successfully, the scripts ```runParameterAnalysis.sh``` and ```runSimulations.sh``` are also executed, for basic plots of optimal parameter sets (available in the directory ```build/parameterAnalysis```) and model simulations (available in the directory ```KimuraODEsim/plots```).

Finally, the optimal parameter sets, along with the corresponding fit value (the lower the better) and random seed used, are available in the ```summaryTable.txt``` file in the ```build``` directory.


### Running the Analysis

A PEPSSBI input file needs to be compiled using the SBC script. For example:

    ./runSBC.sh [inputfile]

*IMPORTANT*: check for errors in the file ```results.log```. When using ```./runSBC.sh``` the content of ```results.log``` will be displayed automatically.

If no errors are found, SBC generates files depending on the commands specified in the input file.

- If the commands ```simulateOutput```, ```doseResponse``` or ```twoVariablesPlot``` have been specified, then the files necessary to run and display simulations are generated in the ```KimuraODEsim``` directory. The executable file ```runSimulations.sh``` will be generated and will be available in the main directory.
- If the command ```fittingData``` has been specified, then the files necessary for parameter estimation and parameter analysis are generated in the ```KimuraGLSDC``` and ```parameterAnalysis``` directories respectively. The executable files ```runParameterEstimationOnUnixCluster.sh```, ```runParameterEstimationOnLinuxDesktop.sh``` and ```runParameterAnalysis.sh``` will be available in the main directory.

#### Parameter estimation

After running the SBC, the directories ```KimuraGLSDC```, ```Lsqnonlin_LevMar```, ```parameterAnalysis``` and ```KimuraODEsim``` are populated according to the input file specifications.

To perform the pipeline of parameter estimation, optimal parameter sets analysis and simulations plots, copy the ```build``` folder on a computer cluster and run:


    ./runParameterEstimationOnUnixCluster.sh [algorithm]


where [algorithm] is one of ```GLSDC```, ```LSQNONLIN_FD``` or ```LSQNONLIN_SE```. Alternatively, to run on a linux desktop (or OSX terminal) run:


    ./runParameterEstimationOnLinuxDesktop.sh [algorithm]


If there are no errors, the file ```bestFits.txt``` that contains the requested best parameter sets will be available in the main directory. This file contains an optimal parameter set in each line. The ```postprocess.sh``` script in the ```KimuraGLSDC/glsdc/v1``` or in the ```Lsqnonlin_LevMar``` directories takes care of generating the ```bestFits.txt``` file by estracting all succesful parameter estimation results. This script also immediately runs the ```runParameterAnalysis.sh``` script and, if available, the ```runSimulations.sh``` script. Additionally, the script ```plotNEvalVsBest.sh``` is run in the directory ```nEvalVsBest``` to generate the plots that show the convergence of the best value of the fit with respect to the number of function evaluations or the time.

#### Parameter Analysis

The parameter analysis is run automatically in pipeline after the parameter estimation procedure. It is also possible to run it again also using another file with the same format as ```bestFits.txt```, using the following command:


    ./runParameterAnalysis.sh [parameterSets]
    
    
The parameter analysis takes place in the ```parameterAnalysis``` directory. It consists of:

- correlation matrix of the parameter sets
- visualisation of the parameter sets (lines, boxplot, average and standard deviation, coefficient of variation)
- simple k-means clustering.

Notice that the default number of clusters for the k-means clustering is ```2```. It is possible to use up to ```7``` clusters by entering the ```parameterAnalysis``` directory and running:


    ./visualiseBestFitsClusters.sh [parameterSets] N

The k-means clustering will generate ```N``` files, which partition the input file ```[parameterSets]```. The generated files will be available in the main directory, with file names of the form ```[parameterSets]A```, ```[parameterSets]B```, ```[parameterSets]C```...

#### Simulations

The simulations and plotting are run automatically in pipeline after the parameter estimation and parameter analysis procedures, if output files have been requested using the commands ```simulateOutput```, ```doseResponse``` or ```twoVariablesPlot```. To re-run only the simulator and plotting after running the parameter optimisation, run:


    ./runSimulations.sh [parameterSets]


Where ```[parameterSets]``` is the file containing the parameter sets obtained from the parameter estimation procedure (this file is called ```bestFits.txt``` by the parameter estimation procedure).

It is also possible to re-run only the plotting. This is often the case when adjustments to the gnuplot files are made. To re-run the gnuplot scripts alone, enter the ```KimuraODEsim/plots/``` directory and use the ```plots.sh``` script.


To only simulate models, without using a parameter sets file (such as ```bestFits.txt```) as input, make sure that only constant parameters are specified (no parameter ranges) and after running the SBC then run:


    ./runSimulations.sh


### Input File Specifications

#### Mathematical expressions

Mathematical expressions are used on the right hand side of algebraic equation definitions and differential equation definitions. Assuming ```expr``` is an arithmetic expression and ```bexp``` is a boolean expression, the full list of available operations is given in the table below:


|	|Operator|	name|	Operator|	name|
|---|---|---|---|---|
| ```expr```|	```(expr)``` |	parenthesis|	```if bexp then expr else expr``` |	If then else construct|
| |	```- expr``` |	Negative expression |	```sin(expr)```	|Sine|
| |	```expr+expr```	| Sum |	```cos(expr)```	|Cosine|
| |	```expr-expr```	|Difference|	```ln(expr)```|	Logarithm base e|
| |	```expr * expr```|	Multiplication	|```exp(expr)```|	Exponential base e|
| |	```expr/expr```	|division|	```sqrt(expr)```	|Square root|
| |	```expr^expr```	|power		| | |
| ```bexp``` |	```(bexp)``` |	parenthesis |	```expr>=expr``` |	Greater or equal than check|
| |	```expr==expr```	|Equality check	|```expr!=expr```|	Not equal check|
| |	```expr<expr```	| Less than check|	```!bexp``` |	Boolean expression negation|
| |	```expr>expr```|	Greater than check|	```bexp and bexp```	|And operator|
| |	```expr<=expr```	|Less or equal then check	 |```bexp or bexp```	|Or operator|

Arithmetic expressions ```expr``` can also be numbers or variable IDs. Boolean expressions ```bexp``` can be the keywords ```true``` and ```false```.

**Numbers.** Numbers can be written using common standards, for example, the number ```100``` can be written ```100```, ```100.0```, ```1e2``` or ```1E2```.

**Variable IDs.** Variable IDs can be names of parameters, algebraic variables or differential variables. Variable IDs can only be composed by letters, numbers and the character ```_```, and have to begin with a letter or the character ```_```. For example, ```MST2``` or ```NF_kappaB``` are valid variable names, but ```9prot``` is not.

**Simulation time.** Additionally, the reserved keyword time can be used, and will automatically refer to model simulation time.

#### Model parameters

Parameters are defined by a constant numeric value or by a range, as in the following examples:


    par var1 = 1.0
    par var2 = [0.0001,10]


Notice that in the above example par is a keyword to be used at the beginning of a new line, ```var1``` and ```var2``` are variable IDs, and the parameter definition ends with the end of the line. The parameter range is used as parameter search range. This implies that the software automatically understands that ```var2``` is one of the parameters that need to be optimised.

**Important: the optimisation algorithms explore the parameters in the natural logarithm space, and so the left bound needs to be greater than zero.**


#### Algebraic equation definitions

Algebraic equation definitions are assignments where on the left hand side there is simply the variable ID, and on the right hand side there is an arithmetic expression or a data interpolation command. A data interpolation command can be one of two keywords: ```dataLinear``` and ```dataSpline```. A data interpolation command instructs the software to look-up a data entry with the same variable ID and for a suitable experimental condition. If such data entry exists, the interpolation of the average of the data replicate is used. This also means that data needs to be available to cover the requested simulation time. For each algebraic equation definition, it is possible to specify different right hand sides for different conditions. 

**Data interpolation.** The average of the corresponding data entry is used for interpolation. The command ```dataLinear```, simply connects the average data points with lines. The ```dataSpline``` command uses the Akima interpolation from the GNU Scientific Library.

Example:


    par RafTotal = 20.0 
    inactiveRaf = RafTotal - activeRaf
    activeRaf = 
    "Starving" 0.0
    "EGF 1nM" dataSpline


In the above example, we defined the parameter ```RafTotal``` to be equal to ```20.0 nM```. We then defined the variable ```inactiveRaf``` to be equal to the total of Raf minus the amount of active Raf. Finally we defined ```activeRaf``` to be either ```0.0``` in the case of cell starving experimental condition (```Starving```), or a ```dataSpline``` in the case of the ```"EGF 1nM"``` experimental condition.

**String format.** Strings are delimited by the double quote ```"``` and can include any character, including spaces.

#### Initial values of ODE variables

An initial value needs to be specified for each differential equation definition using the same variable ID. Initial values have to be specified as parameters. It is possible to specify a different initial value for different experimental conditions. If no experimental condition is specified, the same initialisation is used for all experimental conditions. For example:


    par ppERK_0 = 0.0 
    par pAkt_E1 = 0.5
    par pAkt_E2 = [0.0001,10]

    init ppERK = ppERK_0
    init pAkt = 
    "EGF 1nM" pAkt_E1
    "EGF 10nM" pAkt_E2


In the above example, there are two experimental conditions, identified by the strings ```"EGF 1nM"``` and ```"EGF 10nM"```. The variable ```ppERK``` is initialised to the value of the parameter ```ppERK_0```, which is ```0.0```, in both experimental conditions, because no condition is specified. The variable ```pAkt``` in the experimental condition ```"EGF 1nM"``` is initialised to the value of ```pAkt_E1```, which is ```0.5```, while ```pAkt``` in the experimental condition ```"EGF 10nM"``` is initialised to the value of ```pAkt_E2```, which is unknown and will be searched within the range ```[0.0001,10]```. Notice that the initialisation definition for ```pAkt``` spans multiple lines, where the first line begins with init and ends with the ```=``` character, and the following lines are pairs of conditions (in string format) and parameter IDs.

It is also possible to use a multiplicative factor as follows:


    par initTotRaf = [100,1000]
    init totRaf = 
    "Transfect Raf" initTotRaf
    "Double Transfect Raf" 2*initTotRaf
    
    
In the above example, the amount of transfected Raf is unknown, thus we set a search range for ```initTotRaf``` between ```100``` and ```1000``` ```nM```. However, we know that in a second exprimental condition (```Double Transfect Raf```) we expect to have twice as much Raf than in the first condition.


#### Differential equation definitions

Differential equation definitions are assignments where the left hand side is a variable ID followed by the single quote ```‘``` character (to indicate it is a time derivative) and the right hand side is an arithmetic expression. Different expression can be used for different conditions. An initialisation for each variable defined as differential equation needs to be provided.

Example:


    par E = 0.001
    par initialS = 1.0
    S = initialS - P
    init P = 0.0
    v = 1.0*E*S/(0.5 + S)
    P’ = 
    "fast" 2*v
    "normal" v
    "slow" 0.5*v


In the above example, we consider a Michaelis-Menten approximation of a generic enzymatic reaction. Enzyme ```E``` is considered constant and it is initialised as a parameter. We also define the initial amount of the substrate as the parameter ```initialS```. The variable ```S``` is defined as an algebraic equation. Using the conservation law ```P+S=intialS```, we define ```S``` as ```initalS – P```. The product ```P``` is initialised to ```0.0```. The algebraic equation ```v``` is the velocity of the conversion of ```S``` into ```P```, expressed as a Michaelis-Menten kinetic law. The time derivative of ```P``` is given for three different conditions, ```"fast"```, ```"normal"``` and ```"slow"```.

#### Data entries

Data entries are defined using the keyword data. A variable ID needs to be supplied. Data need to be organised in data blocks (or data matrices), where each data block is a biological replicate. Each line of data in a data block corresponds to a time point, and each column in a data block corresponds to an experimental condition, where the first column is always time. Conditions are specified as a list of strings right before the data blocks, and the data blocks are separated by a line containing the character ```-```.

For example:


    data ppERK =
    "Time" "EGF 1nM" "EGF 10nM"
    0	0.1	0.2
    5	1.5	2.2
    20	0.2	0.3
    -
    0	0.1	0.4
    5	2.4	3.5
    20	0.4	N/A


In the above example, we have defined a data entry for ```ppERK``` with conditions ```"EGF 1nM"``` and ```"EGF 10nM"```. Data points are obtained at time points ```0```, ```5``` and ```20``` time units. Two data blocks, i.e. data replicates, are provided. In the second data block there is a missing data point at time ```20``` for condition ```"EGF 10nM"```.

**Data blocks.** Our idea of a data block is that a data block contains data quantified from the same image, for example a Western blot. This implies that we always assume that data points in a data block are comparable quantitatively. Data points in different data blocks may be not comparable, for example if the replicates are from different Western blots. If two data blocks in a data entry are not comparable, normalisation of the data blocks (i.e. of the replicates) can be used.
 
**Missing data.** Missing data can be specified using the text ```N/A```, ```N/a```, ```n/a``` or ```n/A```.

Multiple data entry with the same variable ID. It is possible to specify more than one data entry for the same variable (e.g. ```ppERK```), as long as the conditions are not overlapping, that is as long as this refers to different sets of experiments. For example, one could perform two sets of Western blots, where different conditions are tested. Because data are not comparable across different western blots, then the data entries for the two sets of Western blots need also to be separated entries.

#### Data normalisation

Normalisation is applied before data are used for interpolation or fitting. If data is used for fitting, the simulation of the corresponding model variable is normalised in the same way before it is used in the objective function. Normalisation should be specified if the data blocks are in relative units. If the data are in absolute units (e.g. concentration or number of molecules), then data normalisation is not necessary.

We consider two types of data normalisation: *by fixed point* or *by average*. In the normalisation by fixed point, a specific data point in the data block is used as a reference. Such reference is identified by a condition and a time point as follows: 


    normaliseByFixedPoint ppERK "EGF 1nM" 5


In the above example we have specified to use the value at time ```5``` in the condition ```"EGF 1nM"``` as reference for the ppERK data. Considering our example of ppERK data entry in the previous section, the above normalisation implies that all data points in the first data block are divided by ```1.5``` while the data points in the second data block are divided by ```2.4```.

In the normalisation by average, one or more conditions are specified, and the average of all the time points in those conditions is used as reference for each data block. If there is missing data in some of the data blocks, an automatic procedure ensures that exactly the same data points are summed in all the data blocks. For example:


    normaliseByAverage ppERK "EGF 1nM" "EGF 10nM"


In the above example we specified to use the average of time courses ```"EGF 1nM"``` and ```"EGF 10nM"``` as reference for the normalisation of ```ppERK```. Considering our example of ppERK data entry in the previous section, the above normalisation implies that all data points in the first data block are divided by the average of all points in the block with the exception of the data point at time ```20``` for condition ```"EGF 10nM"``` (i.e. ```(0.1 + 0.2 + 1.5 + 2.2 + 0.2)/5 = 4.2/5 = 0.84```), while the data points in the second data block are divided by ```(0.1 + 0.4 + 2.4 + 3.5 + 0.4)/5 = 6.8/5 = 1.36```.

#### Simulation options

It is possible to specify simulation options using the keyword ```simulationOption```, followed by one of the following options:

| Option | Description |
|---|---|
| ```simulationInitialTime X```	|Specify the model initial time of the simulation. For example one can set an initial time as a negative number and then add an event at time zero|
| ```simulationTime X```	|Specify the model time of the simulation, that is what is the final model time reached. The total simulated time is equal to simulationTime - simulationInitialTime|
| ```numberOfTimePoints X```|	Specify how many time points need to be retrieved for plotting (this is actually the number of time point intervals)|
| ```abstol X```	|Specify the absolute tolerance of the simulator|
|```reltol X```|	Specify the relative tolerance of the simulator|

For the simulations we use an implicit ODE integrator from the CVODE Sundials library.

Example:


    simulationOption simulationInitialTime -100
    simulationOption simulationTime 120
    simulationOption numberOfTimePoints 1000
    simulationOption abstol 1e-6
    simulationOption reltol 1e-6


#### Fitting options

The SBC software will automatically detect which parameters need to be optimised based on the parameters defined as parameter ranges. To specify which species and conditions should be used for parameter estimation, use the ```fittingData``` keyword:


    fittingData = 
    ppERK "EGF 1nM" "EGF 10nM"
    pAkt "EGF 1nM"


In the example above, each line after the first one begins with a variable ID and continues with a list of one or more conditions. The SBC software will look-up the variable IDs and conditions in both data entries and model variables (algebraic or differential equations). The objective function to be minimised during parameter estimation is given by the sum of the squared differences between data points and corresponding simulation values. If data normalisations are used, the differences between normalised data points and normalised simulations will be used, such that both data and simulations are expressed in the same relative units. The exploration of the parameter search space is computed using the GLSDC algorithm.

The SBC software is designed to run parameter estimation multiple times in a parallelised way, to obtain multiple sets of optimal parameters. Visualisation of optimal parameter sets will be available in the ```parameterAnalysis``` directory.

The files generated by the SBC software allow for parameter estimation on a Linux Desktop or on a Unix Cluster with Portable Batch System (PBS) software for job scheduling. It is possible to specify fitting/PBS options using the keyword ```fittingOption```, followed by one of the following options:

| Option | Description |
|---|---|
|```numberOfNodes X```|	Indicates how many clusters nodes should be used on a Unix cluster (used only on the cluster)|
|```numberOfProcessorsPerNode Y```|	Indicates how many processors per cluster node should be used. If on a Linux desktop, this indicates how many concurrent optimisation runs are used. Typically for each optimisation run a processor will be used.|
|```numberOfJobsPerProcessor W```|	Indicates how many serial jobs are queued to be run on one processor. The total number of runs (jobs) of the GLSDC optimisation algorithm are X\*Y\*W on a Unix cluster and Y\*W on a Linux desktop.|
|```clusterDirectory S```|	Indicates the cluster directory where the script runOnCluster.sh is located. It is important to specify this directory, so that the PBS software can locate the files once they are sent to the cluster nodes|
|```PBSminusA S```|	Optional. Used to specify the PBS "-A" option, which is requested by some administrators to allocate resources on a cluster|
|```PBSemail S```|	Optional. Specify your email address to receive notifications about start, end and failure of the jobs sent on the cluster nodes.|
|```PBSwallTime I```|	Optional. Specify the PBS wall time option in hours as an integer ```I```.|
|```LogLikelihood_LinearModelNoise```|	Optional. This option instruct the compiler to use the the negative LogLikelihood function as objective function to be minimised. The measurement noise is modelled as a linear model, ```sigma_i = A + B*y_i```, where ```sigma_i``` is the measurement noise at datapoint ```i```, while ```y_i``` is the simulation of the model at data point ```i```. Parameters ```A``` and ```B``` will be estimated. Without this option, SBC will use the least squares objective function.|
|```Lsqnonlin_maxiter I```|	Optional. Specify the maximum number of internal iteration steps ```I``` for LSQNONLIN FD and LSQNONLIN SE. The defailt value is 50.|
|```Lsqnonlin_numOfRestarts I```|	Optional. Specify the number of latin hypercube restarts ```I``` for LSQNONLIN FD and LSQNONLIN SE. The defailt value is 100.|

For example:


    fittingOption numberOfNodes 1
    fittingOption numberOfProcessorsPerNode 8
    fittingOption numberOfJobsPerProcessor 13
    fittingOption PBSwallTime 2
    fittingOption clusterDirectory "/home/people/adegaspe/build/"


#### Comments 

Comments beginning and end are delimited by the double backslash ```//```. Notice that comments need to be always closed by the delimiter. That is, it is not possible to use C/Java style comments that begin with ```//``` and finish at the end of the line. Here, the comment will continue until a second ```//``` is found, possibly spanning multiple lines.

Example:


    //observables//
    obsActRaf = ActRaf + MEKActRaf //active Raf can be bound or unbound//
    obsppMEK = ppMEK + ERKppMEK
    obsppERK = ppERK


#### Simulation plots

Simulation plots can be requested by using the keyword ```simulateOutput```, followed by a list of lines with the requested variables (algebraic or differential equation variables) and conditions. For example:


    simulateOutput = 
    ppERK "EGF 1nM" "EGF 10nM"
    pAkt "EGF 1nM"


The above example will plot in the same graph the time courses for ```ppERK "EGF 1nM"```, ```ppERK "EGF 10nM"``` and ```pAkt "EGF 1nM"```. A maximum of ```6``` time courses can be requested in a single plot. If the requested variables have been normalised, the normalised values will be plotted. If data is available, data points will be shown together with the simulations. Typically, the parameter estimation procedure is performed multiple times, generating multiple optimal parameter sets. The simulations obtained using all the optimal parameter sets are visualised together in one graph, while in a second graph, only the average and the standard deviation of such simulations are visualised. These results will be available in the ```KimuraODE/plots``` directory.

The SBC software will generate a plot for each ```simulateOutput``` entry specified.
If no conditions have been specified by the user, SBC will use a default condition called ```"default"```. In this case, the condition ```"default"``` should be specified for plotting. For example:


    par initS = 1
    par initP = 0
    init P = initP
    mm(Vmax,Km,S) = (Vmax*S)/(Km + S)
    S = initS - P
    P' = mm(1,0.5,S)
    simulateOutput =
    P "default"
    S "default"


#### Additional normalisation of simulations

Normalisation commands refer to data entry and only if necessary they are applied to simulations (i.e. for fitting). In some cases it might be useful to indicate additional simulations normalisations. This is when simulations are requested for species or conditions that do not have a corresponding data entry. For example:


    data ppERK =
    "Time" "EGF 1nM" "EGF 10nM" 
    5	1.5	2.2
    20	0.2	0.3
    -
    5	2.4	3.5
    20	0.4	N/A

    normaliseByFixedPoint ppERK "EGF 1nM" 5

    init ppERK = …
    v =
    "EGF 1nM" …
    "EGF 10nM" …
    "EGF 100nM" …
    ppERK’ = v

    simulateOutput = 
    ppERK "EGF 1nM" "EGF 10nM" "EGF 100nM"
    simulateOutput = 
    v "EGF 1nM" "EGF 10nM" "EGF 100nM"


In the above example, we normalise the ```ppERK``` data blocks by ```"EGF 1nM"``` at time ```5```. This implies that the corresponding model variables are also normalised. However, this normalisation is based on the data entry, and because the data entry only provides data for conditions ```"EGF 1nM"``` and ```"EGF 10nM"``` these are the only simulated conditions that will be normalised. This means that the simulated condition ```"EGF 100nM"``` will not be normalised, though it is requested as part of a plot by the first ```simulationOutput``` entry. In order to scale the simulation of ```ppERK``` in condition ```"EGF 100nM"``` we need to explicitly tell the SBC to use the same normalisation constant used for the simulated ```ppERK``` in ```"EGF 1nM"``` and ```"EGF 10nM"```. This is obtained by indicating that we want to normalise ```ppERK``` in condition ```"EGF 100nM"``` using the same normalisation of the data entry for ```ppERK```:


    simulationOption normSimulations ppERK "EGF 100nM" withData ppERK "Time" "EGF 1nM" "EGF 10nM" 


Notice that to identify uniquely which data entry we are referring to we need to specify variable ID and list of conditions of the data entry. The SBC software will automatically look-up which normalisation strategy has been used for the data entry and apply the normalisation constant to the time course for ```ppERK "EGF 100nM"```.

Additionally, notice that the velocity variable ```v``` is not normalised and there is no data entry for ```v```. In some cases it might be interesting to visualise the time evolution of ```v```, as requested by the last ```simulateOutput``` entry. If we want the time evolution of ```v``` to be visualised as relative to a specific time and condition, we can do so by using the following option:


    simulationOption normSimulations v "EGF 1nM" "EGF 10nM" "EGF 100nM" by "EGF 1nM" 5


In the above example the time courses of the variable ```v``` in the three conditions ```"EGF 1nM"```, ```"EGF 10nM"``` and ```"EGF 100nM"``` are normalised by the value of ```v``` at time ```5``` in condition ```"EGF 1nM"```.


#### Function definitions

Functions can be defined for mathematical formulas that are used multiple times in a model. For example:


    par initS = 1
    par initP = 0
    init P = initP
    mm(Vmax,Km,S) = (Vmax*S)/(Km + S)
    S = initS - P
    P' = mm(1,0.5,S)


In this case a function ```mm``` is defined as the Michaelis-Menten enzyme kinetic law. On the left hand side of the assignment, the local parameters (or arguments) of the function are listed, in this case ```Vmax```, ```Km``` and ```S```. On the right hand side an expression is given, that is evaluated every time the function is called with specific instances of its arguments. The expression cannot contain differential and algebraic global variables, thus they need to be passed as arguments of the function. Global parameters, either constant or ranges, can instead be used in the function definition, even without passing them as arguments.

#### Events

Events are assignments to differential variables or constant parameters that are triggered when a given boolean expression turns ```true```. An event is triggered each time its boolean expression turns from ```false``` to ```true```.

Example:


    event e = [P>0.7]{
      P = 0.1
    }


In the above example, an event is defined with name ```e``` and with trigger boolean expression ```p>0.7```. Every time the value of ```P``` becomes greater than ```0.7``` the value of ```P``` is reset to ```0.1```. For each event it is possible to have a list of assignments of the form:


    event eventID = [bexp]{
      var1 = expr1
      var2 = expr2
      ...
    }


IMPORTANT: the implementation of the events in SBC requires that the simulation is stopped and restarted every time an event is fired. In order to do so, the simulator needs to check the output and thus the simulation options ```numberOfTimePoints``` becomes very important. The larger the number of time points requested for the output, the more frequently the trigger is checked and the more accurate the simulation of the event is.

#### Dose Response Plots

Dose response plots can be specified using the keyword ```doseResponse``` and specifying species ID (either algebraic or differential equation ID) and the time to consider. The time should be less or equal the simulation time. Each dose corresponds to an experimental condition. Thus, it is necessary to specify a list of conditions and their corresponding value of the dose (actual number). Finally, the label for the x axis needs to be specified right after the ```doseResponse``` keyword.

Example:


    doseResponse "DDR (+Inhib)" p38_star_star 16 =
    "+Inhib, DDR=0" 0
    "+Inhib, DDR=0.024" 0.024
    "+Inhib, DDR=0.073" 0.073
    "+Inhib, DDR=0.333" 0.333
    "+Inhib, DDR=0.5" 0.5
    "+Inhib, DDR=1" 1


In general:


    doseResponse xlabel speciesID time =
    condition1 = number1
    condtion2 = number2
    ...


#### Two Variables Plot

Two variables plots are analogous to the ```simulateOutput``` plots. The only difference is that an additional species ID is specified, which goes to the x axis in place of time. In analogy with ```simulateOutput```, up to six combinations of variables and conditions can be specified  for the y axis. Two specify a two variables plot use the keyword ```twoVariablesPlot```.

Example:


    twoVariablesPlot R =
    production "k3=1" "k3=1.1"
    degradation "k3=1" "k3=1.1"


In the above example we plot ```production``` and ```degradation``` variables for conditions ```"k3=1"``` and ```"k3=1.1"``` against the value of variable ```R```. Notice that it is not necessary to specify a condition for ```R``` because this is given by the corresponding variable on the y axis.

In general:


    twoVariablesPlot speciesID =
    speciesID condition1 condition2...
    speciesID condition1 condition2...
    ...


### Basic Unix/Linux terminal commands

In this section we cover some basic commands necessary to use a Unix/Linux or Mac OS terminal. After opening the terminal, you should see the command promt, which looks something like this:

    user@computername:~$

The command prompt above, tells you the user name you are currently using and the name of the computer you are on. After the ```:```, the prompt indicates the current directory, which in this case is ```~```. The symbol ```~``` is a placeholder for the current user home directory, which is usually ```/home/user/```.


### Acknowledgements

The research leading to these results has received funding from the 
European Union Seventh Framework Programme (FP7/2007-2013) under grant
agreement no 613879.
