#!/bin/bash

printf "<!DOCTYPE html>\n<html lang=\"en\">\n<head>\n<meta charset=\"utf-8\">\n" > manual.html
printf "<link rel=\"stylesheet\" type=\"text/css\" href=\"markdown-alt.css\">\n" >> manual.html
printf "</head>\n<body>\n" >> manual.html

python -m markdown -x markdown.extensions.sane_lists -x markdown.extensions.smarty -x markdown.extensions.toc -x markdown.extensions.tables -x markdown.extensions.nl2br -x markdown.extensions.nl2br manual.md >> manual.html

printf "</body>\n" >> manual.html
printf "</html>\n" >> manual.html
