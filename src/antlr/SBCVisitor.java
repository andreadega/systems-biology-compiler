// Generated from SBC.g4 by ANTLR 4.2.2
package antlr;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link SBCParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface SBCVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link SBCParser#numEntryDouble}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumEntryDouble(@NotNull SBCParser.NumEntryDoubleContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#normAverageLine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNormAverageLine(@NotNull SBCParser.NormAverageLineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#algebraicEquationCondExprList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlgebraicEquationCondExprList(@NotNull SBCParser.AlgebraicEquationCondExprListContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#differentialEquationCondList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDifferentialEquationCondList(@NotNull SBCParser.DifferentialEquationCondListContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#exprEntryPar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprEntryPar(@NotNull SBCParser.ExprEntryParContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#fittingOptionWallTime}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFittingOptionWallTime(@NotNull SBCParser.FittingOptionWallTimeContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#numEntryNegInt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumEntryNegInt(@NotNull SBCParser.NumEntryNegIntContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#dataListEntry}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDataListEntry(@NotNull SBCParser.DataListEntryContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#parEntryLine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParEntryLine(@NotNull SBCParser.ParEntryLineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#fittingOptionNumberOfProcessorsPerNode}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFittingOptionNumberOfProcessorsPerNode(@NotNull SBCParser.FittingOptionNumberOfProcessorsPerNodeContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#stringListEntryFinal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringListEntryFinal(@NotNull SBCParser.StringListEntryFinalContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#bexprEntryLT}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBexprEntryLT(@NotNull SBCParser.BexprEntryLTContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#initEntryInitialConditionParamDOUBLE}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInitEntryInitialConditionParamDOUBLE(@NotNull SBCParser.InitEntryInitialConditionParamDOUBLEContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#doseResponseListFinal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDoseResponseListFinal(@NotNull SBCParser.DoseResponseListFinalContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#parEntryParameterRange}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParEntryParameterRange(@NotNull SBCParser.ParEntryParameterRangeContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#exprEntryDiv}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprEntryDiv(@NotNull SBCParser.ExprEntryDivContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#exprEntryPlus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprEntryPlus(@NotNull SBCParser.ExprEntryPlusContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#bexprEntryFalseToken}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBexprEntryFalseToken(@NotNull SBCParser.BexprEntryFalseTokenContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#dataPointNAToken}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDataPointNAToken(@NotNull SBCParser.DataPointNATokenContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#dataTypeEntry}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDataTypeEntry(@NotNull SBCParser.DataTypeEntryContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#fittingOptionLogLikelihoodIndepNoise}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFittingOptionLogLikelihoodIndepNoise(@NotNull SBCParser.FittingOptionLogLikelihoodIndepNoiseContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#simulationOptionNormSimWithoutData}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimulationOptionNormSimWithoutData(@NotNull SBCParser.SimulationOptionNormSimWithoutDataContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#bexprEntryEqual}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBexprEntryEqual(@NotNull SBCParser.BexprEntryEqualContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#bexprEntryLE}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBexprEntryLE(@NotNull SBCParser.BexprEntryLEContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#algebraicEquationCondExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlgebraicEquationCondExpr(@NotNull SBCParser.AlgebraicEquationCondExprContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#exprEntryDouble}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprEntryDouble(@NotNull SBCParser.ExprEntryDoubleContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#dataEntryLine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDataEntryLine(@NotNull SBCParser.DataEntryLineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#fittingOptionMinusA}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFittingOptionMinusA(@NotNull SBCParser.FittingOptionMinusAContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#exprEntryNegation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprEntryNegation(@NotNull SBCParser.ExprEntryNegationContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#simulateEntryLine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimulateEntryLine(@NotNull SBCParser.SimulateEntryLineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#fittingOptionLsqnonlinMaxiter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFittingOptionLsqnonlinMaxiter(@NotNull SBCParser.FittingOptionLsqnonlinMaxiterContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#exprEntryCos}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprEntryCos(@NotNull SBCParser.ExprEntryCosContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#stringListEntry}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringListEntry(@NotNull SBCParser.StringListEntryContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#initEntryInitialConditionParamINT}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInitEntryInitialConditionParamINT(@NotNull SBCParser.InitEntryInitialConditionParamINTContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#fittingEntryLine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFittingEntryLine(@NotNull SBCParser.FittingEntryLineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#recordListEntryFinal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRecordListEntryFinal(@NotNull SBCParser.RecordListEntryFinalContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#fittingOptionNumberOfJobsPerProcessor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFittingOptionNumberOfJobsPerProcessor(@NotNull SBCParser.FittingOptionNumberOfJobsPerProcessorContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#bexprEntryGE}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBexprEntryGE(@NotNull SBCParser.BexprEntryGEContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#dataPointNum}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDataPointNum(@NotNull SBCParser.DataPointNumContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#bexprEntryOR}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBexprEntryOR(@NotNull SBCParser.BexprEntryORContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#diffEntryLine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDiffEntryLine(@NotNull SBCParser.DiffEntryLineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#datablocksList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDatablocksList(@NotNull SBCParser.DatablocksListContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#initialConditionCondListINT}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInitialConditionCondListINT(@NotNull SBCParser.InitialConditionCondListINTContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#differentialEquationCondFinal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDifferentialEquationCondFinal(@NotNull SBCParser.DifferentialEquationCondFinalContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#dataListEntryFinal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDataListEntryFinal(@NotNull SBCParser.DataListEntryFinalContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#interpolLinearToken}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInterpolLinearToken(@NotNull SBCParser.InterpolLinearTokenContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#initEntryLine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInitEntryLine(@NotNull SBCParser.InitEntryLineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#initialConditionCondFinal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInitialConditionCondFinal(@NotNull SBCParser.InitialConditionCondFinalContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#recordListEntry}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRecordListEntry(@NotNull SBCParser.RecordListEntryContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#initialConditionCondFinalDOUBLE}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInitialConditionCondFinalDOUBLE(@NotNull SBCParser.InitialConditionCondFinalDOUBLEContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#bexprEntryPar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBexprEntryPar(@NotNull SBCParser.BexprEntryParContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#diffEntryDifferentialEquationExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDiffEntryDifferentialEquationExpr(@NotNull SBCParser.DiffEntryDifferentialEquationExprContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#assignment_listList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment_listList(@NotNull SBCParser.Assignment_listListContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#bexprEntryAND}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBexprEntryAND(@NotNull SBCParser.BexprEntryANDContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#exprEntryID}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprEntryID(@NotNull SBCParser.ExprEntryIDContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#idcondListFinal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdcondListFinal(@NotNull SBCParser.IdcondListFinalContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#idcondListEntry}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdcondListEntry(@NotNull SBCParser.IdcondListEntryContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#bexprEntryNot}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBexprEntryNot(@NotNull SBCParser.BexprEntryNotContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#fittingOptionLsqnonlinNumOfRestarts}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFittingOptionLsqnonlinNumOfRestarts(@NotNull SBCParser.FittingOptionLsqnonlinNumOfRestartsContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#normEntryLine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNormEntryLine(@NotNull SBCParser.NormEntryLineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#simulationOptionInitialTime}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimulationOptionInitialTime(@NotNull SBCParser.SimulationOptionInitialTimeContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#codeList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCodeList(@NotNull SBCParser.CodeListContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#fittingOptionLogLikelihoodLinearModelNoise}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFittingOptionLogLikelihoodLinearModelNoise(@NotNull SBCParser.FittingOptionLogLikelihoodLinearModelNoiseContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#eventEntryLine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEventEntryLine(@NotNull SBCParser.EventEntryLineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#simulationOptionTime}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimulationOptionTime(@NotNull SBCParser.SimulationOptionTimeContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#exprEntryTime}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprEntryTime(@NotNull SBCParser.ExprEntryTimeContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#bexprEntryDiff}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBexprEntryDiff(@NotNull SBCParser.BexprEntryDiffContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#algebraicEquationCondInterpolationList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlgebraicEquationCondInterpolationList(@NotNull SBCParser.AlgebraicEquationCondInterpolationListContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#fittingOptionNumberOfNodes}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFittingOptionNumberOfNodes(@NotNull SBCParser.FittingOptionNumberOfNodesContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#fittingOptionEmail}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFittingOptionEmail(@NotNull SBCParser.FittingOptionEmailContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#assignment_listFinal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment_listFinal(@NotNull SBCParser.Assignment_listFinalContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#fittingOptionEntryLine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFittingOptionEntryLine(@NotNull SBCParser.FittingOptionEntryLineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#initEntryInitialConditionCondList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInitEntryInitialConditionCondList(@NotNull SBCParser.InitEntryInitialConditionCondListContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#exprEntryExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprEntryExp(@NotNull SBCParser.ExprEntryExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#normFixedPointLine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNormFixedPointLine(@NotNull SBCParser.NormFixedPointLineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#datablocksFinal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDatablocksFinal(@NotNull SBCParser.DatablocksFinalContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#exprEntryIfThenElse}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprEntryIfThenElse(@NotNull SBCParser.ExprEntryIfThenElseContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#simulationOptionAbsTol}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimulationOptionAbsTol(@NotNull SBCParser.SimulationOptionAbsTolContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#exprEntryFunCall}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprEntryFunCall(@NotNull SBCParser.ExprEntryFunCallContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#exprEntrySqrt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprEntrySqrt(@NotNull SBCParser.ExprEntrySqrtContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#doseResponsePlotEntry}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDoseResponsePlotEntry(@NotNull SBCParser.DoseResponsePlotEntryContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#simulationOptionNormSimWithData}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimulationOptionNormSimWithData(@NotNull SBCParser.SimulationOptionNormSimWithDataContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#initialConditionCondList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInitialConditionCondList(@NotNull SBCParser.InitialConditionCondListContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#algebraicEntryExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlgebraicEntryExpr(@NotNull SBCParser.AlgebraicEntryExprContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#bexprEntryGT}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBexprEntryGT(@NotNull SBCParser.BexprEntryGTContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#algebraicEntryLine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlgebraicEntryLine(@NotNull SBCParser.AlgebraicEntryLineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#numEntryNegDouble}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumEntryNegDouble(@NotNull SBCParser.NumEntryNegDoubleContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#diffEntryDifferentialEquationCondList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDiffEntryDifferentialEquationCondList(@NotNull SBCParser.DiffEntryDifferentialEquationCondListContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#listOfExprList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListOfExprList(@NotNull SBCParser.ListOfExprListContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#functionEntryLine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionEntryLine(@NotNull SBCParser.FunctionEntryLineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#EventEntryDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEventEntryDef(@NotNull SBCParser.EventEntryDefContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#interpolSplineToken}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInterpolSplineToken(@NotNull SBCParser.InterpolSplineTokenContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#exprEntryInt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprEntryInt(@NotNull SBCParser.ExprEntryIntContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#codeRule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCodeRule(@NotNull SBCParser.CodeRuleContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#numEntryInt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumEntryInt(@NotNull SBCParser.NumEntryIntContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#algebraicEquationCondInterpolation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlgebraicEquationCondInterpolation(@NotNull SBCParser.AlgebraicEquationCondInterpolationContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#skipLine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSkipLine(@NotNull SBCParser.SkipLineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#initEntryInitialConditionParam}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInitEntryInitialConditionParam(@NotNull SBCParser.InitEntryInitialConditionParamContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#algebraicEntryAlgebraicEquationCondList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlgebraicEntryAlgebraicEquationCondList(@NotNull SBCParser.AlgebraicEntryAlgebraicEquationCondListContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#simulationOptionEntryLine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimulationOptionEntryLine(@NotNull SBCParser.SimulationOptionEntryLineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#functionEntryDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionEntryDef(@NotNull SBCParser.FunctionEntryDefContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#fittingOptionClusterDirectory}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFittingOptionClusterDirectory(@NotNull SBCParser.FittingOptionClusterDirectoryContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#exprEntryMinus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprEntryMinus(@NotNull SBCParser.ExprEntryMinusContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#bexprEntryTrueToken}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBexprEntryTrueToken(@NotNull SBCParser.BexprEntryTrueTokenContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#doseResponseEntryLine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDoseResponseEntryLine(@NotNull SBCParser.DoseResponseEntryLineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#exprEntryCap}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprEntryCap(@NotNull SBCParser.ExprEntryCapContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#listOfIDsFinal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListOfIDsFinal(@NotNull SBCParser.ListOfIDsFinalContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#algebraicEntryAlgebraicEquationInterpolation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlgebraicEntryAlgebraicEquationInterpolation(@NotNull SBCParser.AlgebraicEntryAlgebraicEquationInterpolationContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment(@NotNull SBCParser.AssignmentContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#exprEntryLn}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprEntryLn(@NotNull SBCParser.ExprEntryLnContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#listOfIDsList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListOfIDsList(@NotNull SBCParser.ListOfIDsListContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#listOfExprFinal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListOfExprFinal(@NotNull SBCParser.ListOfExprFinalContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#parEntryParameterValue}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParEntryParameterValue(@NotNull SBCParser.ParEntryParameterValueContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#doseResponseListEntry}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDoseResponseListEntry(@NotNull SBCParser.DoseResponseListEntryContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#exprEntrySin}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprEntrySin(@NotNull SBCParser.ExprEntrySinContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#initialConditionCondFinalINT}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInitialConditionCondFinalINT(@NotNull SBCParser.InitialConditionCondFinalINTContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#initialConditionCondListDOUBLE}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInitialConditionCondListDOUBLE(@NotNull SBCParser.InitialConditionCondListDOUBLEContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#simulationOptionRelTol}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimulationOptionRelTol(@NotNull SBCParser.SimulationOptionRelTolContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#exprEntryMul}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprEntryMul(@NotNull SBCParser.ExprEntryMulContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#twoVariablesPlotEntryLine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTwoVariablesPlotEntryLine(@NotNull SBCParser.TwoVariablesPlotEntryLineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SBCParser#simulationOptionNumberOfTimePoints}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimulationOptionNumberOfTimePoints(@NotNull SBCParser.SimulationOptionNumberOfTimePointsContext ctx);
}