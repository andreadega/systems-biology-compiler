
grammar SBC;

//return void
code: line                           # codeRule
    | line code   EOF?                      # codeList
    ;

//return void
line: SPACETABS? NEWLINE                # skipLine
    | SPACETABS? normEntry SPACETABS? NEWLINE                 # normEntryLine
    | SPACETABS? dataEntry SPACETABS? NEWLINE                 # dataEntryLine
    | SPACETABS? parEntry SPACETABS? NEWLINE                  # parEntryLine
    | SPACETABS? initEntry SPACETABS? NEWLINE                 # initEntryLine
    | SPACETABS? diffEntry SPACETABS? NEWLINE                 # diffEntryLine
    | SPACETABS? algebraicEntry SPACETABS? NEWLINE            # algebraicEntryLine
    | SPACETABS? functionEntry SPACETABS? NEWLINE            # functionEntryLine
    | SPACETABS? eventEntry SPACETABS? NEWLINE            # eventEntryLine
    | SPACETABS? FITTINGDATA assignment NEWLINE idcondlist SPACETABS? NEWLINE              # fittingEntryLine
    | SPACETABS? SIMULATEOUTPUT assignment NEWLINE idcondlist SPACETABS? NEWLINE             # simulateEntryLine
    | SPACETABS? doseResponseEntry SPACETABS? NEWLINE             # doseResponseEntryLine
    | SPACETABS? 'twoVariablesPlot' SPACETABS ID assignment NEWLINE idcondlist SPACETABS? NEWLINE             # twoVariablesPlotEntryLine
    | SPACETABS? SIMULATIONOPTION SPACETABS simoption SPACETABS? NEWLINE                    # simulationOptionEntryLine
    | SPACETABS? FITTINGOPTION SPACETABS fitoption SPACETABS? NEWLINE                    # fittingOptionEntryLine
    ;
    
//return DoseResponsePlot
doseResponseEntry: 'doseResponse' SPACETABS STRING SPACETABS ID SPACETABS numEntry assignment NEWLINE doseResponseList  # doseResponsePlotEntry
                 ;
      
//return HashMap<Integer,Double>          
doseResponseList: STRING SPACETABS numEntry                                     # doseResponseListFinal
                | STRING SPACETABS numEntry SPACETABS? NEWLINE doseResponseList # doseResponseListEntry
                ;

//return HashMap<Integer,LinkedList<Integer>>
idcondlist: ID SPACETABS stringList                               # idcondListFinal
          | ID SPACETABS stringList SPACETABS? NEWLINE idcondlist # idcondListEntry
          ;

//return void
simoption: 'simulationTime' SPACETABS numEntry                # simulationOptionTime
         | 'simulationInitialTime' SPACETABS numEntry                # simulationOptionInitialTime
         | 'numberOfTimePoints' SPACETABS INT       # simulationOptionNumberOfTimePoints
         | 'abstol' SPACETABS numEntry              # simulationOptionAbsTol
         | 'reltol' SPACETABS numEntry              # simulationOptionRelTol
         | 'normSimulations' SPACETABS ID SPACETABS stringList SPACETABS 'withData' SPACETABS ID SPACETABS stringList   # simulationOptionNormSimWithData
         | 'normSimulations' SPACETABS ID SPACETABS stringList SPACETABS 'by' SPACETABS STRING SPACETABS numEntry   # simulationOptionNormSimWithoutData
         ;

//return void
fitoption: 'numberOfNodes' SPACETABS INT                # fittingOptionNumberOfNodes
         | 'numberOfProcessorsPerNode' SPACETABS INT    # fittingOptionNumberOfProcessorsPerNode
         | 'numberOfJobsPerProcessor' SPACETABS INT     # fittingOptionNumberOfJobsPerProcessor
         | 'clusterDirectory' SPACETABS STRING         # fittingOptionClusterDirectory
         | 'PBSminusA' SPACETABS STRING         # fittingOptionMinusA
         | 'PBSemail' SPACETABS STRING         # fittingOptionEmail
         | 'PBSwallTime' SPACETABS INT         # fittingOptionWallTime
         | 'Lsqnonlin_maxiter' SPACETABS INT         # fittingOptionLsqnonlinMaxiter
         | 'Lsqnonlin_numOfRestarts' SPACETABS INT         # fittingOptionLsqnonlinNumOfRestarts
         | 'LogLikelihood_IndependentNoise'              # fittingOptionLogLikelihoodIndepNoise
         | 'LogLikelihood_LinearModelNoise'              # fittingOptionLogLikelihoodLinearModelNoise
         ;

//return NormalisationNode
normEntry: NORMALISEBYFIXEDPOINT SPACETABS ID SPACETABS STRING SPACETABS numEntry  # normFixedPointLine
         | NORMALISEBYAVERAGE SPACETABS ID SPACETABS stringList                   # normAverageLine
         ;

//return DataNode
dataEntry: DATA SPACETABS ID assignment NEWLINE stringList SPACETABS? NEWLINE datablocks # dataTypeEntry
         ;

//nothing
assignment: '='
          | SPACETABS '='
          | '=' SPACETABS
          | SPACETABS '=' SPACETABS
          ;

//return LinkedList<Integer>
stringList: STRING                          # stringListEntryFinal
          | STRING SPACETABS stringList     # stringListEntry
          ;

//return LinkedList<Double[][]>
datablocks: records                         # datablocksFinal
          | records SPACETABS? NEWLINE '-' SPACETABS? NEWLINE datablocks # datablocksList
          ;

//return LinkedList<LinkedList<Double>>
records: record                             # recordListEntryFinal
       | record SPACETABS? NEWLINE records  # recordListEntry
       ;

//return LinkedList<Double>       
record: dataPoint                           # dataListEntryFinal
      | dataPoint SPACETABS record          # dataListEntry
      ;

//return Double
dataPoint: numEntry                         # dataPointNum
         | NADATAPOINT                      # dataPointNAToken
         ;

//return Number
numEntry: DOUBLE                            # numEntryDouble
         | '-' DOUBLE                       # numEntryNegDouble
         | INT                              # numEntryInt
         | '-' INT                          # numEntryNegInt
         ;

//return ParameterNode
parEntry: PAR SPACETABS ID assignment numEntry                        # parEntryParameterValue
        | PAR SPACETABS ID assignment '[' SPACETABS? numEntry SPACETABS? ',' SPACETABS? numEntry SPACETABS? ']' # parEntryParameterRange
        ;

//return InitialConditionNode
initEntry: INIT SPACETABS ID assignment ID                    # initEntryInitialConditionParam
         | INIT SPACETABS ID assignment INT SPACETABS? '*' SPACETABS? ID   # initEntryInitialConditionParamINT
         | INIT SPACETABS ID assignment DOUBLE SPACETABS? '*' SPACETABS? ID   # initEntryInitialConditionParamDOUBLE
         | INIT SPACETABS ID assignment NEWLINE initcondlist  # initEntryInitialConditionCondList
         ;

//return HashMap<Integer,Integer>
initcondlist: STRING SPACETABS ID                                 # initialConditionCondFinal
            | STRING SPACETABS INT SPACETABS? '*' SPACETABS? ID   # initialConditionCondFinalINT
            | STRING SPACETABS DOUBLE SPACETABS? '*' SPACETABS? ID   # initialConditionCondFinalDOUBLE
            | STRING SPACETABS ID SPACETABS? NEWLINE initcondlist # initialConditionCondList
            | STRING SPACETABS INT SPACETABS? '*' SPACETABS? ID SPACETABS? NEWLINE initcondlist   # initialConditionCondListINT
            | STRING SPACETABS DOUBLE SPACETABS? '*' SPACETABS? ID SPACETABS? NEWLINE initcondlist   # initialConditionCondListDOUBLE
            ;

//return DifferentialEquationNode
diffEntry: ID '\'' assignment expr                             # diffEntryDifferentialEquationExpr
         | ID '\'' assignment NEWLINE condlist                 # diffEntryDifferentialEquationCondList
         ;
      
//return HashMap<Integer,Expression> 
condlist: STRING SPACETABS expr                                     # differentialEquationCondFinal
        | STRING SPACETABS expr SPACETABS? NEWLINE condlist         # differentialEquationCondList
        ;

//return AlgebraicEquationNode
algebraicEntry: ID assignment expr                    # algebraicEntryExpr
              | ID assignment interpolation           # algebraicEntryAlgebraicEquationInterpolation
              | ID assignment NEWLINE condlist2       # algebraicEntryAlgebraicEquationCondList
              ;
              
//return FunctionNode
functionEntry: ID '(' SPACETABS? list_of_ids SPACETABS? ')' assignment expr # functionEntryDef
             ;

//return EventNode
eventEntry: 'event' SPACETABS ID assignment '[' SPACETABS? bexp SPACETABS? ']' SPACETABS? '{' SPACETABS? NEWLINE assignment_list SPACETABS? NEWLINE '}' #EventEntryDef
          ;

//return HashMap<Integer,Expression>
assignment_list: SPACETABS? ID assignment expr                                     #assignment_listFinal
               | SPACETABS? ID assignment expr SPACETABS? NEWLINE assignment_list  #assignment_listList
               ;

//return LinkedList<String>
list_of_ids: ID                                         # listOfIDsFinal
           | ID SPACETABS? ',' SPACETABS? list_of_ids   # listOfIDsList
           ;

//return LinkedList<AlgebraicEquationCond> 
condlist2: STRING SPACETABS expr                                        # algebraicEquationCondExpr
         | STRING SPACETABS interpolation                               # algebraicEquationCondInterpolation
         | STRING SPACETABS expr SPACETABS? NEWLINE condlist2           # algebraicEquationCondExprList
         | STRING SPACETABS interpolation SPACETABS? NEWLINE condlist2  # algebraicEquationCondInterpolationList
         ;

//return Integer
interpolation: 'dataSpline'           # interpolSplineToken
             | 'dataLinear'           # interpolLinearToken
             ;

//return Expression
expr: '(' SPACETABS? expr SPACETABS? ')'                                            # exprEntryPar
    | IF SPACETABS bexp SPACETABS THEN SPACETABS expr SPACETABS ELSE SPACETABS expr # exprEntryIfThenElse
    | ID                                                # exprEntryID
    | ID '(' SPACETABS? list_of_expr SPACETABS? ')'     # exprEntryFunCall
    | TIME              # exprEntryTime
    | DOUBLE            # exprEntryDouble
    | INT               # exprEntryInt
    | <assoc=right> expr SPACETABS? '^' SPACETABS? expr  # exprEntryCap
    | '-' expr          # exprEntryNegation
    | expr SPACETABS? '/' SPACETABS? expr               # exprEntryDiv
    | expr SPACETABS? '*' SPACETABS? expr               # exprEntryMul
    | expr SPACETABS? '-' SPACETABS? expr               # exprEntryMinus
    | expr SPACETABS? '+' SPACETABS? expr               # exprEntryPlus
    | 'sin(' SPACETABS? expr SPACETABS? ')'             # exprEntrySin
    | 'cos(' SPACETABS? expr SPACETABS? ')'             # exprEntryCos
    | 'ln(' SPACETABS? expr SPACETABS? ')'              # exprEntryLn
    | 'exp(' SPACETABS? expr SPACETABS? ')'             # exprEntryExp
    | 'sqrt(' SPACETABS? expr SPACETABS? ')'            # exprEntrySqrt
    ;
    
//return LinkedList<String>
list_of_expr: expr                                            # listOfExprFinal
            | expr SPACETABS? ',' SPACETABS? list_of_expr     # listOfExprList
            ;

//return BExpression    
bexp: '(' bexp ')'                          # bexprEntryPar
    | TRUE                                  # bexprEntryTrueToken
    | FALSE                                 # bexprEntryFalseToken
    | expr SPACETABS? '==' SPACETABS? expr  # bexprEntryEqual
    | expr SPACETABS? '<' SPACETABS? expr   # bexprEntryLT
    | expr SPACETABS? '>' SPACETABS? expr   # bexprEntryGT
    | expr SPACETABS? '<=' SPACETABS? expr  # bexprEntryLE
    | expr SPACETABS? '>=' SPACETABS? expr  # bexprEntryGE
    | expr SPACETABS? '!=' SPACETABS? expr  # bexprEntryDiff
    | '!' SPACETABS? bexp                  # bexprEntryNot
    | bexp SPACETABS 'and' SPACETABS bexp   # bexprEntryAND
    | bexp SPACETABS 'or' SPACETABS bexp    # bexprEntryOR
    ;


INT                   : DIGIT+ ;
DOUBLE                : DIGIT+ '.' DIGIT* (('e'|'E') '-'? DIGIT+)?
                      | DIGIT+ ('e'|'E') '-'? DIGIT+ ;
DIGIT                 : [0-9] ;
NEWLINE               : '\r'? '\n';
SPACETABS             : [ \t]+ ;
NORMALISEBYFIXEDPOINT : 'normaliseByFixedPoint' ;
NORMALISEBYAVERAGE    : 'normaliseByAverage' ;
DATA                  : 'data' ;
FITTINGDATA           : 'fittingData' ;
SIMULATEOUTPUT        : 'simulateOutput' ;
SIMULATIONOPTION      : 'simulationOption' ;
FITTINGOPTION         : 'fittingOption' ;
PAR                   : 'par' ;
INIT                  : 'init' ;
IF                    : 'if' ;
THEN                  : 'then' ;
ELSE                  : 'else' ;
TIME                  : 'time' ;
TRUE                  : 'true' ;
FALSE                 : 'false' ;
NADATAPOINT           : [Nn][/][Aa] ;
ID                    : [a-zA-Z\_][a-zA-Z0-9\_]* ;
STRING                : '"' .*? '"' ;
COMMENT               : '//' .*? '//' -> skip ;

