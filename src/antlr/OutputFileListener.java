/***
 * Excerpted from "The Definitive ANTLR 4 Reference",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/tpantlr2 for more book information.
***/

package antlr;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.Nullable;

import java.util.*;
import java.io.*;

public class OutputFileListener extends BaseErrorListener {
  
    BufferedWriter bw = null;
    
    public OutputFileListener(BufferedWriter bw){
      this.bw = bw;
    }
  
    @Override
    public void syntaxError(Recognizer<?, ?> recognizer,
                            Object offendingSymbol,
                            int line, int charPositionInLine,
                            String msg,
                            RecognitionException e)
    {
        List<String> stack = ((Parser)recognizer).getRuleInvocationStack();
        Collections.reverse(stack);
        //System.err.println("rule stack: "+stack);
        try{
          bw.write("[syntax error] check your input file for errors: line "+line+":"+charPositionInLine+" at "+
                           offendingSymbol+": "+msg+"\n");
        }catch(IOException ioe){
          ioe.printStackTrace();
        }
    }

}

