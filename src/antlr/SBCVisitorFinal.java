// Generated from SBC.g4 by ANTLR 4.2.2
package antlr;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import sbc.*;
import sbc.expr.*;
import sbc.util.Pair;
import java.util.*;

/**
 * This class provides an empty implementation of {@link SBCVisitor},
 * which can be extended to create a visitor which only needs to handle a subset
 * of the available methods.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public class SBCVisitorFinal extends SBCBaseVisitor<Object> {
  
  private EntryLists entryLists = null;
  private FittingOptions fittingOptions = null;
  private SimulateOptions simulateOptions = null;
  private LookUpTable lookUpTable = null;
  private LinkedList<String> localParameters = null;
  
  public SBCVisitorFinal(EntryLists entryLists,FittingOptions fittingOptions,SimulateOptions simulateOptions,LookUpTable lookUpTable) throws NullPointerException {
    if (entryLists == null) throw new NullPointerException();
    if (fittingOptions == null) throw new NullPointerException();
    if (simulateOptions == null) throw new NullPointerException();
    if (lookUpTable == null) throw new NullPointerException();
    this.entryLists = entryLists;
    this.fittingOptions = fittingOptions;
    this.simulateOptions = simulateOptions;
    this.lookUpTable = lookUpTable;
  }
  
	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitExprEntryCos(@NotNull SBCParser.ExprEntryCosContext ctx) { 
    
    return new ExprCos((Expression) visit(ctx.expr()));
  
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitInterpolSplineToken(@NotNull SBCParser.InterpolSplineTokenContext ctx) { 
    return new Integer(AlgebraicEquationInterpolation.SPLINE); 
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitExprEntryExp(@NotNull SBCParser.ExprEntryExpContext ctx) { 
    
    return new ExprExp((Expression) visit(ctx.expr()));
  
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitNormEntryLine(@NotNull SBCParser.NormEntryLineContext ctx) { 
    
    NormalisationNode nd = (NormalisationNode)visit(ctx.normEntry());
    
    try{
      entryLists.add(nd);
    }catch(Exception e){
      throw new ParseCancellationException(e.getMessage());
    }
    return null;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitNumEntryNegDouble(@NotNull SBCParser.NumEntryNegDoubleContext ctx) { 
    return new Double("-" + ctx.DOUBLE().getText());
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitParEntryParameterRange(@NotNull SBCParser.ParEntryParameterRangeContext ctx) { 
    
    Integer id = lookUpTable.getKey(ctx.ID().getText());
    Number bottom = (Number) visit(ctx.numEntry(0));
    Number top = (Number) visit(ctx.numEntry(1));
    //~ Number start = (Number) visit(ctx.numEntry(2));
    
    if (bottom.doubleValue()<=0) throw new ParseCancellationException("[error] Parameter range for fitting is incorrect for parameter " 
                              + lookUpTable.lookUpString(id) + ": bottom should be greater than zero because we explore the logarithm "
                              + "of the parameter space but you specified bottom " + bottom.doubleValue());
    
    if (top.doubleValue()<=0) throw new ParseCancellationException("[error] Parameter range for fitting is incorrect for parameter " 
                              + lookUpTable.lookUpString(id) + ": top should be greater than zero because we explore the logarithm "
                              + "of the parameter space but you specified top " + top.doubleValue());
    
    //~ if (start.doubleValue()<=0) throw new ParseCancellationException("[error] Parameter range for fitting is incorrect for parameter " 
                              //~ + lookUpTable.lookUpString(id) + ": start should be greater than zero because we explore the logarithm "
                              //~ + "of the parameter space but you specified start " + start.doubleValue());
    
    if (bottom.doubleValue()>top.doubleValue()) throw new ParseCancellationException("[error] Parameter range for fitting is incorrect for parameter " 
                              + lookUpTable.lookUpString(id) + ": bottom should be lower than top but you specified bottom " + bottom.doubleValue()
                              + " and top " + top.doubleValue());
    
    //~ if (bottom.doubleValue()>start.doubleValue()) throw new ParseCancellationException("[error] Parameter range for fitting is incorrect for parameter " 
                              //~ + lookUpTable.lookUpString(id) + ": bottom should be lower than start but you specified bottom " + bottom.doubleValue()
                              //~ + " and start " + start.doubleValue());
                              //~ 
    //~ if (start.doubleValue()>top.doubleValue()) throw new ParseCancellationException("[error] Parameter range for fitting is incorrect for parameter " 
                              //~ + lookUpTable.lookUpString(id) + ": start should be lower than top but you specified start " + start.doubleValue()
                              //~ + " and top " + top.doubleValue());
    
    return new ParameterRange(id,bottom.doubleValue(),top.doubleValue());
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitInitEntryLine(@NotNull SBCParser.InitEntryLineContext ctx) { 
    InitialConditionNode nd = (InitialConditionNode)visit(ctx.initEntry());
    
    try{
      entryLists.add(nd);
    }catch(Exception e){
      throw new ParseCancellationException(e.getMessage());
    }
    return null;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitNormFixedPointLine(@NotNull SBCParser.NormFixedPointLineContext ctx) { 
    Integer id = lookUpTable.getKey(ctx.ID().getText());
    Integer condition = lookUpTable.getKey(ctx.STRING().getText());
    Number n = (Number) visit(ctx.numEntry());
    return new NormalisationNodeFixedPoint(id, condition,new Double(n.doubleValue()));
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitExprEntrySin(@NotNull SBCParser.ExprEntrySinContext ctx) { 
    return new ExprSin((Expression) visit(ctx.expr()));
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitNumEntryInt(@NotNull SBCParser.NumEntryIntContext ctx) { 
    return new Integer(ctx.INT().getText());
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitBexprEntryTrueToken(@NotNull SBCParser.BexprEntryTrueTokenContext ctx) { 
    return new BExprTRUE();
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitAlgebraicEquationCondExprList(@NotNull SBCParser.AlgebraicEquationCondExprListContext ctx) { 
    @SuppressWarnings("unchecked")
    LinkedList<AlgebraicEquationCond> list = (LinkedList<AlgebraicEquationCond>) visit(ctx.condlist2());
    Expression expr = (Expression) visit(ctx.expr());
    Integer condition = lookUpTable.getKey(ctx.STRING().getText());
    list.add(new AlgebraicEquationCondExpr(condition,expr));
    return list;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitBexprEntryOR(@NotNull SBCParser.BexprEntryORContext ctx) { 
    return new BExprOR((BExpression) visit(ctx.bexp(0)),(BExpression) visit(ctx.bexp(1))); 
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitCodeList(@NotNull SBCParser.CodeListContext ctx) { 
    visit(ctx.line()); //visit this line
    visit(ctx.code()); //go on to next lines
    return null;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitAlgebraicEntryLine(@NotNull SBCParser.AlgebraicEntryLineContext ctx) {
    AlgebraicEquationNode nd = (AlgebraicEquationNode)visit(ctx.algebraicEntry());
    
    try{
      entryLists.add(nd);
    }catch(Exception e){
      throw new ParseCancellationException(e.getMessage());
    }
    return null;    
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitInitEntryInitialConditionCondList(@NotNull SBCParser.InitEntryInitialConditionCondListContext ctx) { 
    Integer id = lookUpTable.getKey(ctx.ID().getText());
    @SuppressWarnings("unchecked")
    HashMap<Integer,Pair<Integer,Double>> map = (HashMap<Integer,Pair<Integer,Double>>) visit(ctx.initcondlist());
    return new InitialConditionCondList(id,map);
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitDifferentialEquationCondList(@NotNull SBCParser.DifferentialEquationCondListContext ctx) { 
    Integer condition = lookUpTable.getKey(ctx.STRING().getText());
    Expression expr = (Expression) visit(ctx.expr());
    @SuppressWarnings("unchecked")
    HashMap<Integer,Expression> map = (HashMap<Integer,Expression>) visit(ctx.condlist());
    map.put(condition,expr);
    return map;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitDataListEntry(@NotNull SBCParser.DataListEntryContext ctx) { 
    @SuppressWarnings("unchecked")
    LinkedList<Double> list = (LinkedList<Double>) visit(ctx.record());
    list.addFirst((Double) visit(ctx.dataPoint()));
    return list;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitExprEntryNegation(@NotNull SBCParser.ExprEntryNegationContext ctx) { 
    return new ExprNegation((Expression) visit(ctx.expr()));
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitBexprEntryGT(@NotNull SBCParser.BexprEntryGTContext ctx) { 
    return new BExprGT((Expression) visit(ctx.expr(0)),(Expression) visit(ctx.expr(1))); 
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitCodeRule(@NotNull SBCParser.CodeRuleContext ctx) { 
    visit(ctx.line()); //visit this line
    return null;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitBexprEntryDiff(@NotNull SBCParser.BexprEntryDiffContext ctx) { 
    return new BExprDiff((Expression) visit(ctx.expr(0)),(Expression) visit(ctx.expr(1))); 
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitBexprEntryEqual(@NotNull SBCParser.BexprEntryEqualContext ctx) { 
    return new BExprEqual((Expression) visit(ctx.expr(0)),(Expression) visit(ctx.expr(1))); 
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitDataTypeEntry(@NotNull SBCParser.DataTypeEntryContext ctx) { 
    Integer id = lookUpTable.getKey(ctx.ID().getText());
    @SuppressWarnings("unchecked")
    LinkedList<Integer> conditions = (LinkedList<Integer>) visit(ctx.stringList());
    @SuppressWarnings("unchecked")
    LinkedList<Double[][]> dataBlocks = (LinkedList<Double[][]>) visit(ctx.datablocks());
    //Check that the length of conditions list is the same as the width of the matrices in 
    //the dataBlocks list. Here we know, because of other checks, that all the matrices have
    //the same dimensions, so pick the first.
    if (conditions.size() != (dataBlocks.getFirst())[0].length){
      throw new ParseCancellationException("[error] Number of declared conditions is different from the number of " 
                              + "columns in the data blocks for " + lookUpTable.lookUpString(id) + " data entry");
    }
    return new DataNodeReplicates(id,conditions,dataBlocks);
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitFittingOptionEntryLine(@NotNull SBCParser.FittingOptionEntryLineContext ctx) { 
    visit(ctx.fitoption());
    return null;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitDiffEntryLine(@NotNull SBCParser.DiffEntryLineContext ctx) { 
    DifferentialEquationNode nd = (DifferentialEquationNode)visit(ctx.diffEntry());
    
    try{
      entryLists.add(nd);
    }catch(Exception e){
      throw new ParseCancellationException(e.getMessage());
    }
    return null;      
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitExprEntryDiv(@NotNull SBCParser.ExprEntryDivContext ctx) { 
    return new ExprDiv((Expression) visit(ctx.expr(0)),(Expression) visit(ctx.expr(1)));
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitAlgebraicEquationCondExpr(@NotNull SBCParser.AlgebraicEquationCondExprContext ctx) { 
    LinkedList<AlgebraicEquationCond> list = new LinkedList<AlgebraicEquationCond>();
    Expression expr = (Expression) visit(ctx.expr());
    Integer condition = lookUpTable.getKey(ctx.STRING().getText());
    list.add(new AlgebraicEquationCondExpr(condition,expr));
    return list;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitRecordListEntry(@NotNull SBCParser.RecordListEntryContext ctx) { 
    @SuppressWarnings("unchecked")
    LinkedList<LinkedList<Double>> list = (LinkedList<LinkedList<Double>>) visit(ctx.records());
    //Important check, the number of elements in a record should always be the same. Just compare
    //with the first element in the list (by recursion all the elements in the list have the same
    //length. Also by recursion, the list cannot be empty.
    @SuppressWarnings("unchecked")
    LinkedList<Double> addition = (LinkedList<Double>) visit(ctx.record());
    if (list.getFirst().size() != addition.size()) throw new ParseCancellationException("[error] Two records (data lines), " 
                                                      + "within the same data block have different number of elements (columns).");
    list.addFirst(addition);
    return list;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitDataEntryLine(@NotNull SBCParser.DataEntryLineContext ctx) { 
    DataNode nd = (DataNode) visit(ctx.dataEntry());
    
    try{
      entryLists.add(nd);
    }catch(Exception e){
      throw new ParseCancellationException(e.getMessage());
    }
    return null;
    
  }

	/**
	 * Visit a parse tree produced by {@link SBCParser#eventEntryLine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	@Override public Object visitEventEntryLine(@NotNull SBCParser.EventEntryLineContext ctx){
    EventNode nd = (EventNode) visit(ctx.eventEntry());
    
    try{
      entryLists.add(nd);
    }catch(Exception e){
      throw new ParseCancellationException(e.getMessage());
    }
    return null;    
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitExprEntryCap(@NotNull SBCParser.ExprEntryCapContext ctx) { 
    return new ExprCap((Expression) visit(ctx.expr(0)),(Expression) visit(ctx.expr(1)));  
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitSimulationOptionTime(@NotNull SBCParser.SimulationOptionTimeContext ctx) { 
    simulateOptions.setFinalTime(((Number)visit(ctx.numEntry())).doubleValue());
    return null;
  }
  
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public Object visitSimulationOptionInitialTime(@NotNull SBCParser.SimulationOptionInitialTimeContext ctx) { 
    simulateOptions.setInitialTime(((Number)visit(ctx.numEntry())).doubleValue());
    return null;    
  }


	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitInitialConditionCondList(@NotNull SBCParser.InitialConditionCondListContext ctx) { 
    @SuppressWarnings("unchecked")
    HashMap<Integer,Pair<Integer,Double>> map = (HashMap<Integer,Pair<Integer,Double>>) visit(ctx.initcondlist());
    Integer condition = lookUpTable.getKey(ctx.STRING().getText());
    Integer paramID = lookUpTable.getKey(ctx.ID().getText());
    map.put(condition,new Pair<Integer,Double>(paramID,new Double(1.0)));
    return map;
  }
  
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public Object visitInitialConditionCondListINT(@NotNull SBCParser.InitialConditionCondListINTContext ctx) { 
    @SuppressWarnings("unchecked")
    HashMap<Integer,Pair<Integer,Double>> map = (HashMap<Integer,Pair<Integer,Double>>) visit(ctx.initcondlist());
    Integer condition = lookUpTable.getKey(ctx.STRING().getText());
    Integer paramID = lookUpTable.getKey(ctx.ID().getText());
    map.put(condition,new Pair<Integer,Double>(paramID,new Double(ctx.INT().getText())));
    return map;
  }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public Object visitInitialConditionCondListDOUBLE(@NotNull SBCParser.InitialConditionCondListDOUBLEContext ctx) {
    @SuppressWarnings("unchecked")
    HashMap<Integer,Pair<Integer,Double>> map = (HashMap<Integer,Pair<Integer,Double>>) visit(ctx.initcondlist());
    Integer condition = lookUpTable.getKey(ctx.STRING().getText());
    Integer paramID = lookUpTable.getKey(ctx.ID().getText());
    map.put(condition,new Pair<Integer,Double>(paramID,new Double(ctx.DOUBLE().getText())));
    return map;
  }


	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitSkipLine(@NotNull SBCParser.SkipLineContext ctx) { return visitChildren(ctx); }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitStringListEntryFinal(@NotNull SBCParser.StringListEntryFinalContext ctx) { 
    LinkedList<Integer> list = new LinkedList<Integer>();
    Integer condition = lookUpTable.getKey(ctx.STRING().getText());
    list.add(condition);
    return list;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitParEntryLine(@NotNull SBCParser.ParEntryLineContext ctx) { 
      ParameterNode nd = (ParameterNode) visit(ctx.parEntry());
    
    try{
      entryLists.add(nd);
    }catch(Exception e){
      throw new ParseCancellationException(e.getMessage());
    }
    return null;  
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitNumEntryDouble(@NotNull SBCParser.NumEntryDoubleContext ctx) {  
    return new Double(ctx.DOUBLE().getText());
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitBexprEntryPar(@NotNull SBCParser.BexprEntryParContext ctx) { 
    return new BExprPar((BExpression) visit(ctx.bexp()));
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitDatablocksList(@NotNull SBCParser.DatablocksListContext ctx) { 
    @SuppressWarnings("unchecked")
    LinkedList<Double[][]> list = (LinkedList<Double[][]>) visit(ctx.datablocks());
    @SuppressWarnings("unchecked")
    LinkedList<LinkedList<Double>> dll = (LinkedList<LinkedList<Double>>) visit(ctx.records());
    //convert function below already checks that it is a matrix (every row has the same number of elements)
    Double [][] addition = convertListOfListOfDoubleToDoubleMatrix(dll);
    //Now we need to check that the new addition has the same number of row and columns
    //as the other matrices in the list. By recursion, just check using the first
    //matrix in the list.
    Double [][] firstElement = list.getFirst();
    if (addition.length != firstElement.length) throw new ParseCancellationException("[error] Two data blocks for the same species" 
                                                      + " have different number of time points. You can use N/A for missing entries.");
    if (addition[0].length != firstElement[0].length) throw new ParseCancellationException("[error] Two data blocks for the same species" 
                                                      + " have different number of conditions. You can use N/A for missing entries.");
    //Also, we need to check that the list of time points (column 0, matrix[i][0]) is the same in all matrices
    //By recursion, just check with the first (we know by checks above that the length is the same
    for (int i = 0; i< addition.length; i++){
      if (addition[i][0].doubleValue() != firstElement[i][0].doubleValue()){
        //time points are different!
        throw new ParseCancellationException("[error] Two data blocks for the same species" 
                + " have different time points. Same time points need to be used, you can use N/A for missing data. [" + addition[i][0] + "!=" + firstElement[i][0] + "]");
      }
    }
    
    list.addFirst(addition);
    return list;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitDataPointNum(@NotNull SBCParser.DataPointNumContext ctx) { 
    Number n = (Number) visit(ctx.numEntry());
    if (n instanceof Integer) {
      return new Double(n.doubleValue());
    } else if (n instanceof Double) {
      return n;
    }
    throw new ParseCancellationException("[error] Probably a coding error. " 
        + "Unknown Number type in parser (visitDataPointNum), not Integer nor Double.");
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitExprEntryPar(@NotNull SBCParser.ExprEntryParContext ctx) { 
    return new ExprPar((Expression) visit(ctx.expr()));
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitExprEntryIfThenElse(@NotNull SBCParser.ExprEntryIfThenElseContext ctx) { 
    BExpression bexp = (BExpression) visit(ctx.bexp());
    Expression expr1 = (Expression) visit(ctx.expr(0));
    Expression expr2 = (Expression) visit(ctx.expr(1));
    return new ExprIfThenElse(bexp,expr1,expr2);
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitBexprEntryNot(@NotNull SBCParser.BexprEntryNotContext ctx) { 
    return new BExprNot((BExpression) visit(ctx.bexp()));
  }
	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitRecordListEntryFinal(@NotNull SBCParser.RecordListEntryFinalContext ctx) { 
    LinkedList<LinkedList<Double>> list = new LinkedList<LinkedList<Double>>();
    @SuppressWarnings("unchecked")
    LinkedList<Double> addition = (LinkedList<Double>) visit(ctx.record());
    list.add(addition);
    return list;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitExprEntryTime(@NotNull SBCParser.ExprEntryTimeContext ctx) { 
    return new ExprTime();
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitIdcondListEntry(@NotNull SBCParser.IdcondListEntryContext ctx) {    
    @SuppressWarnings("unchecked") 
    HashMap<Integer,LinkedList<Integer>> map = (HashMap<Integer,LinkedList<Integer>>) visit(ctx.idcondlist());
    Integer id = lookUpTable.getKey(ctx.ID().getText());
    @SuppressWarnings("unchecked")
    LinkedList<Integer> condlist = (LinkedList<Integer>) visit(ctx.stringList());
    map.put(id,condlist);
    return map; 
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitExprEntrySqrt(@NotNull SBCParser.ExprEntrySqrtContext ctx) { 
    return new ExprSqrt((Expression) visit(ctx.expr()));
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitExprEntryDouble(@NotNull SBCParser.ExprEntryDoubleContext ctx) { 
    return new ExprDOUBLE(new Double(ctx.DOUBLE().getText()));
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitBexprEntryLT(@NotNull SBCParser.BexprEntryLTContext ctx) { 
    return new BExprLT((Expression) visit(ctx.expr(0)),(Expression) visit(ctx.expr(1))); 
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitSimulationOptionAbsTol(@NotNull SBCParser.SimulationOptionAbsTolContext ctx) { 
    simulateOptions.setAbstol(((Number)visit(ctx.numEntry())).doubleValue());
    return null;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitAlgebraicEntryAlgebraicEquationInterpolation(@NotNull SBCParser.AlgebraicEntryAlgebraicEquationInterpolationContext ctx) {
    Integer id = lookUpTable.getKey(ctx.ID().getText()); 
    Integer type = (Integer) visit(ctx.interpolation());
    return new AlgebraicEquationInterpolation(id,type.intValue());
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitExprEntryMul(@NotNull SBCParser.ExprEntryMulContext ctx) { 
    return new ExprMul((Expression) visit(ctx.expr(0)),(Expression) visit(ctx.expr(1)));
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitNormAverageLine(@NotNull SBCParser.NormAverageLineContext ctx) { 
    Integer id = lookUpTable.getKey(ctx.ID().getText());
    @SuppressWarnings("unchecked")
    LinkedList<Integer> condList = (LinkedList<Integer>) visit(ctx.stringList());
    return new NormalisationNodeAverage(id,condList);
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitParEntryParameterValue(@NotNull SBCParser.ParEntryParameterValueContext ctx) { 
    Number n = (Number) visit(ctx.numEntry());
    Integer id = lookUpTable.getKey(ctx.ID().getText());
    if (n instanceof Double) return  new ParameterValue(id,(Double)n);
    return new ParameterValue(id, new Double(n.doubleValue()));
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitDifferentialEquationCondFinal(@NotNull SBCParser.DifferentialEquationCondFinalContext ctx) { 
    HashMap<Integer,Expression> map = new HashMap<Integer,Expression>();
    Integer condition = lookUpTable.getKey(ctx.STRING().getText());
    Expression expr = (Expression) visit(ctx.expr());
    map.put(condition,expr);
    return map;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitDataListEntryFinal(@NotNull SBCParser.DataListEntryFinalContext ctx) { 
    LinkedList<Double> list = new LinkedList<Double>();
    list.add((Double) visit(ctx.dataPoint()));
    return list;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitNumEntryNegInt(@NotNull SBCParser.NumEntryNegIntContext ctx) { 
    return new Integer("-" + ctx.INT().getText());
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitExprEntryPlus(@NotNull SBCParser.ExprEntryPlusContext ctx) { 
    return new ExprPlus((Expression) visit(ctx.expr(0)),(Expression) visit(ctx.expr(1)));
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitFittingEntryLine(@NotNull SBCParser.FittingEntryLineContext ctx) { 
    @SuppressWarnings("unchecked")
    HashMap<Integer,LinkedList<Integer>> map = (HashMap<Integer,LinkedList<Integer>>) visit(ctx.idcondlist());
    fittingOptions.setIDCondList(map);
    return null;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitFittingOptionNumberOfProcessorsPerNode(@NotNull SBCParser.FittingOptionNumberOfProcessorsPerNodeContext ctx) { 
    fittingOptions.setNumberOfProcessorsPerNode(Integer.parseInt(ctx.INT().getText()));
    return null;
  }
  
  /**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public Object visitFittingOptionLogLikelihoodIndepNoise(@NotNull SBCParser.FittingOptionLogLikelihoodIndepNoiseContext ctx) {
    //we are in a token which indicates that the logLikelihood function should be used in place of the least squares difference
    // 1 means to use the Independent Noise
    fittingOptions.setLogLikelihood(LogLikelihoodTool.IndependentNoise);
    return null;
  }

  /**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public Object visitFittingOptionLogLikelihoodLinearModelNoise(@NotNull SBCParser.FittingOptionLogLikelihoodLinearModelNoiseContext ctx) {
    //we are in a token which indicates that the logLikelihood function should be used in place of the least squares difference
    // 2 means to use the Linear Model Noise
    fittingOptions.setLogLikelihood(LogLikelihoodTool.LinearModelOfNoise);
    return null;
  }
  
	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitDatablocksFinal(@NotNull SBCParser.DatablocksFinalContext ctx) { 
    LinkedList<Double[][]> list = new LinkedList<Double[][]>();
    @SuppressWarnings("unchecked")
    LinkedList<LinkedList<Double>> dll = (LinkedList<LinkedList<Double>>) visit(ctx.records());
    list.add(convertListOfListOfDoubleToDoubleMatrix(dll));
    return list;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitInterpolLinearToken(@NotNull SBCParser.InterpolLinearTokenContext ctx) { 
    return new Integer(AlgebraicEquationInterpolation.LINEAR); 
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitSimulationOptionEntryLine(@NotNull SBCParser.SimulationOptionEntryLineContext ctx) { 
    visit(ctx.simoption());
    return null;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitFittingOptionNumberOfNodes(@NotNull SBCParser.FittingOptionNumberOfNodesContext ctx) { 
    fittingOptions.setNumberOfNodes(Integer.parseInt(ctx.INT().getText()));
    return null;
  }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public Object visitFittingOptionWallTime(@NotNull SBCParser.FittingOptionWallTimeContext ctx) {
    fittingOptions.setWallTime(Integer.parseInt(ctx.INT().getText()));
    return null; 
  }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public Object visitFittingOptionLsqnonlinMaxiter(@NotNull SBCParser.FittingOptionLsqnonlinMaxiterContext ctx) {
    fittingOptions.setLsqnonlin_maxiter(Integer.parseInt(ctx.INT().getText()));
    return null; 
  }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public Object visitFittingOptionLsqnonlinNumOfRestarts(@NotNull SBCParser.FittingOptionLsqnonlinNumOfRestartsContext ctx) {
    fittingOptions.setLsqnonlin_numOfRestarts(Integer.parseInt(ctx.INT().getText()));
    return null; 
  }


	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitDiffEntryDifferentialEquationExpr(@NotNull SBCParser.DiffEntryDifferentialEquationExprContext ctx) { 
    Integer id = lookUpTable.getKey(ctx.ID().getText());
    Expression expr = (Expression) visit(ctx.expr());
    return new DifferentialEquationExpr(id,expr);
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitExprEntryInt(@NotNull SBCParser.ExprEntryIntContext ctx) { 
    return new ExprINT(new Integer(ctx.INT().getText()));
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitAlgebraicEquationCondInterpolationList(@NotNull SBCParser.AlgebraicEquationCondInterpolationListContext ctx) { 
    @SuppressWarnings("unchecked")
    LinkedList<AlgebraicEquationCond> list = (LinkedList<AlgebraicEquationCond>) visit(ctx.condlist2());
    Integer type = (Integer) visit(ctx.interpolation());
    Integer condition = lookUpTable.getKey(ctx.STRING().getText());
    list.add(new AlgebraicEquationCondInterpolation(condition,type.intValue()));
    return list;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitBexprEntryGE(@NotNull SBCParser.BexprEntryGEContext ctx) { 
    return new BExprGE((Expression) visit(ctx.expr(0)),(Expression) visit(ctx.expr(1))); 
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitExprEntryMinus(@NotNull SBCParser.ExprEntryMinusContext ctx) { 
    return new ExprMinus((Expression) visit(ctx.expr(0)),(Expression) visit(ctx.expr(1)));
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitExprEntryID(@NotNull SBCParser.ExprEntryIDContext ctx) {
    Expression expr = null;
    String id = ctx.ID().getText();
    if (localParameters != null){  // we are in a local function definition
      if (localParameters.contains(id)) {
        // the current ID is a local parameter! Do not add to lookUpTable
        expr = new ExprLocalPar(id);
      } else {
        // the current ID is not a local parameter, could be a global parameter so add
        expr = new ExprID(lookUpTable.getKey(id));
      }
    }else {
      expr = new ExprID(lookUpTable.getKey(id));
    }
    return expr;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitInitEntryInitialConditionParam(@NotNull SBCParser.InitEntryInitialConditionParamContext ctx) { 
    Integer initID = lookUpTable.getKey(ctx.ID(0).getText());
    Integer paramID = lookUpTable.getKey(ctx.ID(1).getText());
    return new InitialConditionParam(initID,paramID,new Double(1.0));
  }
  
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public Object visitInitEntryInitialConditionParamINT(@NotNull SBCParser.InitEntryInitialConditionParamINTContext ctx) { 
    Integer initID = lookUpTable.getKey(ctx.ID(0).getText());
    Integer paramID = lookUpTable.getKey(ctx.ID(1).getText());
    return new InitialConditionParam(initID,paramID,new Double(ctx.INT().getText()));
  }
  
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public Object visitInitEntryInitialConditionParamDOUBLE(@NotNull SBCParser.InitEntryInitialConditionParamDOUBLEContext ctx) {
    Integer initID = lookUpTable.getKey(ctx.ID(0).getText());
    Integer paramID = lookUpTable.getKey(ctx.ID(1).getText());
    return new InitialConditionParam(initID,paramID,new Double(ctx.DOUBLE().getText()));    
  }
  


	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitDiffEntryDifferentialEquationCondList(@NotNull SBCParser.DiffEntryDifferentialEquationCondListContext ctx) { 
    @SuppressWarnings("unchecked")
    HashMap<Integer,Expression> map = (HashMap<Integer,Expression>) visit(ctx.condlist());
    Integer id = lookUpTable.getKey(ctx.ID().getText());
    return new DifferentialEquationCondList(id,map);
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitExprEntryLn(@NotNull SBCParser.ExprEntryLnContext ctx) { 
    return new ExprLn((Expression) visit(ctx.expr()));
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitStringListEntry(@NotNull SBCParser.StringListEntryContext ctx) {
    @SuppressWarnings("unchecked") 
    LinkedList<Integer> list = (LinkedList<Integer>) visit(ctx.stringList());
    Integer condition = lookUpTable.getKey(ctx.STRING().getText());
    list.addFirst(condition);
    return list;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitInitialConditionCondFinal(@NotNull SBCParser.InitialConditionCondFinalContext ctx) { 
    HashMap<Integer,Pair<Integer,Double>> map = new HashMap<Integer,Pair<Integer,Double>>();
    Integer condition = lookUpTable.getKey(ctx.STRING().getText());
    Integer paramID = lookUpTable.getKey(ctx.ID().getText());
    map.put(condition,new Pair<Integer,Double>(paramID,new Double(1.0)));
    return map;
  }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public Object visitInitialConditionCondFinalINT(@NotNull SBCParser.InitialConditionCondFinalINTContext ctx) { 
    HashMap<Integer,Pair<Integer,Double>> map = new HashMap<Integer,Pair<Integer,Double>>();
    Integer condition = lookUpTable.getKey(ctx.STRING().getText());
    Integer paramID = lookUpTable.getKey(ctx.ID().getText());
    map.put(condition,new Pair<Integer,Double>(paramID,new Double(ctx.INT().getText())));
    return map;
  }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public Object visitInitialConditionCondFinalDOUBLE(@NotNull SBCParser.InitialConditionCondFinalDOUBLEContext ctx) { 
    HashMap<Integer,Pair<Integer,Double>> map = new HashMap<Integer,Pair<Integer,Double>>();
    Integer condition = lookUpTable.getKey(ctx.STRING().getText());
    Integer paramID = lookUpTable.getKey(ctx.ID().getText());
    map.put(condition,new Pair<Integer,Double>(paramID,new Double(ctx.DOUBLE().getText())));
    return map;
  }


	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitDataPointNAToken(@NotNull SBCParser.DataPointNATokenContext ctx) { 
    return new Double(Double.NaN);
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitFittingOptionNumberOfJobsPerProcessor(@NotNull SBCParser.FittingOptionNumberOfJobsPerProcessorContext ctx) { 
    fittingOptions.setNumberOfJobsPerProcessor(Integer.parseInt(ctx.INT().getText()));
    return null;
  }
  
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public Object visitFittingOptionClusterDirectory(@NotNull SBCParser.FittingOptionClusterDirectoryContext ctx) { 
    fittingOptions.setClusterDirectory(ctx.STRING().getText().replace("\"",""));
    return null;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitSimulateEntryLine(@NotNull SBCParser.SimulateEntryLineContext ctx) { 
    @SuppressWarnings("unchecked")
    HashMap<Integer,LinkedList<Integer>> map = (HashMap<Integer,LinkedList<Integer>>) visit(ctx.idcondlist());
    simulateOptions.addIDCondList(map);
    return null;
  }
  
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public Object visitTwoVariablesPlotEntryLine(@NotNull SBCParser.TwoVariablesPlotEntryLineContext ctx) { 
    @SuppressWarnings("unchecked")
    HashMap<Integer,LinkedList<Integer>> map = (HashMap<Integer,LinkedList<Integer>>) visit(ctx.idcondlist());
    Integer id = lookUpTable.getKey(ctx.ID().getText());
    simulateOptions.addTwoVariablesPlot(new TwoVariablesPlot(id,map));    
    return null; 
  }


	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitAlgebraicEquationCondInterpolation(@NotNull SBCParser.AlgebraicEquationCondInterpolationContext ctx) { 
    LinkedList<AlgebraicEquationCond> list = new LinkedList<AlgebraicEquationCond>();
    Integer type = (Integer) visit(ctx.interpolation());
    Integer condition = lookUpTable.getKey(ctx.STRING().getText());
    list.add(new AlgebraicEquationCondInterpolation(condition,type.intValue()));
    return list;  
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitBexprEntryAND(@NotNull SBCParser.BexprEntryANDContext ctx) { 
    return new BExprAND((BExpression) visit(ctx.bexp(0)),(BExpression) visit(ctx.bexp(1))); 
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitAssignment(@NotNull SBCParser.AssignmentContext ctx) { return visitChildren(ctx); }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitSimulationOptionNumberOfTimePoints(@NotNull SBCParser.SimulationOptionNumberOfTimePointsContext ctx) { 
    simulateOptions.setTimePoints(Integer.parseInt(ctx.INT().getText()));
    return null;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitBexprEntryLE(@NotNull SBCParser.BexprEntryLEContext ctx) { 
    return new BExprLE((Expression) visit(ctx.expr(0)),(Expression) visit(ctx.expr(1))); 
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitIdcondListFinal(@NotNull SBCParser.IdcondListFinalContext ctx) { 
    HashMap<Integer,LinkedList<Integer>> map = new HashMap<Integer,LinkedList<Integer>>();
    Integer id = lookUpTable.getKey(ctx.ID().getText());
    @SuppressWarnings("unchecked")
    LinkedList<Integer> condlist = (LinkedList<Integer>) visit(ctx.stringList());
    map.put(id,condlist);
    return map;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitSimulationOptionRelTol(@NotNull SBCParser.SimulationOptionRelTolContext ctx) { 
    simulateOptions.setReltol(((Number)visit(ctx.numEntry())).doubleValue());
    return null;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitAlgebraicEntryExpr(@NotNull SBCParser.AlgebraicEntryExprContext ctx) { 
    Integer id = lookUpTable.getKey(ctx.ID().getText());
    Expression expr = (Expression) visit(ctx.expr());
    return new AlgebraicEquationExpr(id,expr);
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitBexprEntryFalseToken(@NotNull SBCParser.BexprEntryFalseTokenContext ctx) { 
    return new BExprFALSE();
  }
  
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public Object visitSimulationOptionNormSimWithData(@NotNull SBCParser.SimulationOptionNormSimWithDataContext ctx) { 
    Integer id = lookUpTable.getKey(ctx.ID(0).getText());
    Integer idData = lookUpTable.getKey(ctx.ID(1).getText());    
    @SuppressWarnings("unchecked")
    LinkedList<Integer> conditionsToNormalise = (LinkedList<Integer>) visit(ctx.stringList(0));    
    @SuppressWarnings("unchecked")
    LinkedList<Integer> conditionsData = (LinkedList<Integer>) visit(ctx.stringList(1));
    simulateOptions.addNormSimulationsNode(new NormSimulationsWithData(id,conditionsToNormalise,idData,conditionsData));
    return null;   
  }
  
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public Object visitSimulationOptionNormSimWithoutData(@NotNull SBCParser.SimulationOptionNormSimWithoutDataContext ctx) { 
    Integer id = lookUpTable.getKey(ctx.ID().getText());   
    @SuppressWarnings("unchecked")
    LinkedList<Integer> conditionsToNormalise = (LinkedList<Integer>) visit(ctx.stringList());
    Integer condition = lookUpTable.getKey(ctx.STRING().getText());
    Number n = (Number) visit(ctx.numEntry());  
    
    simulateOptions.addNormSimulationsNode(new NormSimulationsWithoutData(id,conditionsToNormalise,condition,new Double(n.doubleValue())));
    return null; 
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitAlgebraicEntryAlgebraicEquationCondList(@NotNull SBCParser.AlgebraicEntryAlgebraicEquationCondListContext ctx) { 
    Integer id = lookUpTable.getKey(ctx.ID().getText());
    @SuppressWarnings("unchecked")
    LinkedList<AlgebraicEquationCond> list = (LinkedList<AlgebraicEquationCond>) visit(ctx.condlist2());
    return new AlgebraicEquationCondList(id,list);
  }
  
  
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public Object visitFittingOptionMinusA(@NotNull SBCParser.FittingOptionMinusAContext ctx) { 
    fittingOptions.setMinusA(ctx.STRING().getText().replace("\"",""));
    return null;
  }  

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public Object visitFittingOptionEmail(@NotNull SBCParser.FittingOptionEmailContext ctx) { 
    fittingOptions.setEmail(ctx.STRING().getText().replace("\"",""));
    return null;
  }
  
  /**
	 * Visit a parse tree produced by {@link SBCParser#listOfExprFinal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	@Override public Object visitListOfExprFinal(@NotNull SBCParser.ListOfExprFinalContext ctx){
    LinkedList<Expression> list = new LinkedList<Expression>();
    Expression expr = (Expression) visit(ctx.expr());
    list.add(expr);
    return list;
  };

	/**
	 * Visit a parse tree produced by {@link SBCParser#listOfExprList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	@Override public Object visitListOfExprList(@NotNull SBCParser.ListOfExprListContext ctx){
    @SuppressWarnings("unchecked")
    LinkedList<Expression> list = (LinkedList<Expression>) visit(ctx.list_of_expr());
    Expression expr = (Expression) visit(ctx.expr());
    list.addFirst(expr);
    return list;
  }
  
	/**
	 * Visit a parse tree produced by {@link SBCParser#exprEntryFunCall}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	@Override public Object visitExprEntryFunCall(@NotNull SBCParser.ExprEntryFunCallContext ctx){
    String sID = ctx.ID().getText();
    if (localParameters != null){  // we are in a local function definition
      if (localParameters.contains(sID)) {
        //function name clashes with local parameter name!
        throw new ParseCancellationException("[error] In a function definition"
        + ", one of the local parameters (" + sID + ") has been used as a function!");
      }
    }
    Integer id = lookUpTable.getKey(sID);
    @SuppressWarnings("unchecked")
    LinkedList<Expression> list = (LinkedList<Expression>) visit(ctx.list_of_expr());
    return new ExprFunCall(id, list);
  }
  
	/**
	 * Visit a parse tree produced by {@link SBCParser#EventEntryDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	@Override public Object visitEventEntryDef(@NotNull SBCParser.EventEntryDefContext ctx){
    Integer id = lookUpTable.getKey(ctx.ID().getText());
    BExpression bexpr = (BExpression) visit(ctx.bexp());
    @SuppressWarnings("unchecked")
    HashMap<Integer,Expression> list = (HashMap<Integer,Expression>) visit(ctx.assignment_list());
    return new EventNode(id,bexpr,list);
  }
  
	/**
	 * Visit a parse tree produced by {@link SBCParser#assignment_listFinal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	@Override public Object visitAssignment_listFinal(@NotNull SBCParser.Assignment_listFinalContext ctx){
    HashMap<Integer,Expression> map = new HashMap<Integer,Expression>();
    Integer id = lookUpTable.getKey(ctx.ID().getText());
    Expression exp = (Expression) visit(ctx.expr());
    map.put(id,exp);
    return map;
  }
  
	/**
	 * Visit a parse tree produced by {@link SBCParser#assignment_listList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	@Override public Object visitAssignment_listList(@NotNull SBCParser.Assignment_listListContext ctx){
    @SuppressWarnings("unchecked")
    HashMap<Integer,Expression> map = (HashMap<Integer,Expression>) visit(ctx.assignment_list());
    Integer id = lookUpTable.getKey(ctx.ID().getText());
    Expression exp = (Expression) visit(ctx.expr());
    map.put(id,exp);
    return map;    
  }
  
  /**
	 * Visit a parse tree produced by {@link SBCParser#listOfIDsFinal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	@Override public Object visitListOfIDsFinal(@NotNull SBCParser.ListOfIDsFinalContext ctx){
    LinkedList<String> list = new LinkedList<String>();
    String id = ctx.ID().getText(); //local def. IDs, leave String
    list.add(id);
    return list;    
  }
  
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public Object visitDoseResponseListFinal(@NotNull SBCParser.DoseResponseListFinalContext ctx) {
    TreeMap<Double,Integer> doses = new TreeMap<Double,Integer>();
    Number n = (Number) visit(ctx.numEntry());
    Double d;
    if (n instanceof Integer) {
      d = new Double(n.doubleValue());
    } else if (n instanceof Double) {
      d = (Double) n;
    } else {
    throw new ParseCancellationException("[error] Probably a coding error. " 
        + "Unknown Number type in parser (visitDoseResponseListFinal), not Integer nor Double.");
    }
    String s = ctx.STRING().getText();
    doses.put(d,lookUpTable.getKey(s));
    return doses;
  }
  
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public Object visitDoseResponseListEntry(@NotNull SBCParser.DoseResponseListEntryContext ctx) { 
    @SuppressWarnings("unchecked")
    TreeMap<Double,Integer> doses = (TreeMap<Double,Integer>) visit(ctx.doseResponseList());
    Number n = (Number) visit(ctx.numEntry());
    Double d;
    if (n instanceof Integer) {
      d = new Double(n.doubleValue());
    } else if (n instanceof Double) {
      d = (Double) n;
    } else {
    throw new ParseCancellationException("[error] Probably a coding error. " 
        + "Unknown Number type in parser (visitDoseResponseListFinal), not Integer nor Double.");
    }
    String s = ctx.STRING().getText();
    doses.put(d,lookUpTable.getKey(s));
    return doses;
  }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public Object visitDoseResponsePlotEntry(@NotNull SBCParser.DoseResponsePlotEntryContext ctx) { 
    @SuppressWarnings("unchecked")
    TreeMap<Double,Integer> doses = (TreeMap<Double,Integer>) visit(ctx.doseResponseList());
    Number n = (Number) visit(ctx.numEntry());
    Double d;
    if (n instanceof Integer) {
      d = new Double(n.doubleValue());
    } else if (n instanceof Double) {
      d = (Double) n;
    } else {
    throw new ParseCancellationException("[error] Probably a coding error. " 
        + "Unknown Number type in parser (visitDoseResponseListFinal), not Integer nor Double.");
    }
    String xlabel = ctx.STRING().getText();
    Integer speciesID = lookUpTable.getKey(ctx.ID().getText());
    simulateOptions.addDoseResponsePlot(new DoseResponsePlot(xlabel,speciesID,d,doses));
    return null;
  }
  
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public Object visitDoseResponseEntryLine(@NotNull SBCParser.DoseResponseEntryLineContext ctx) { return visitChildren(ctx); }

	/**
	 * Visit a parse tree produced by {@link SBCParser#listOfIDsList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	@Override public Object visitListOfIDsList(@NotNull SBCParser.ListOfIDsListContext ctx){
    @SuppressWarnings("unchecked")
    LinkedList<String> list = (LinkedList<String>) visit(ctx.list_of_ids());
    String id = ctx.ID().getText(); //local def. IDs, leave String
    list.addFirst(id);
    return list;    
  }

	/**
	 * Visit a parse tree produced by {@link SBCParser#functionEntryDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	@Override public Object visitFunctionEntryDef(@NotNull SBCParser.FunctionEntryDefContext ctx){
    Integer id = lookUpTable.getKey(ctx.ID().getText());
    @SuppressWarnings("unchecked")
    LinkedList<String> list = (LinkedList<String>) visit(ctx.list_of_ids());
    localParameters = list; // assign local parameters before visiting expr
    Expression expr = (Expression) visit(ctx.expr());
    localParameters = null; // release local parameters
    return new FunctionNode(id,list,expr);
  }
  
	/**
	 * Visit a parse tree produced by {@link SBCParser#functionEntryLine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	@Override public Object visitFunctionEntryLine(@NotNull SBCParser.FunctionEntryLineContext ctx){
    FunctionNode nd = (FunctionNode)visit(ctx.functionEntry());
    
    try{
      entryLists.add(nd);
    }catch(Exception e){
      throw new ParseCancellationException(e.getMessage());
    }
    return null;  
  }

  private Double[][] convertListOfListOfDoubleToDoubleMatrix(LinkedList<LinkedList<Double>> dll){
    //preliminary checks
    if (dll == null){
      throw new ParseCancellationException("[error] Probably coding error: trying to convert a null list of list of Double to Double[][].");
    }else if (dll.size() == 0){
      throw new ParseCancellationException("[error] Probably coding error: trying to convert an empty list of list of Double to Double[][].");
    } else if (dll.size() > 1){
      //check all lists have the same size
      boolean sameSize = true;
      Iterator<LinkedList<Double>> ite = dll.iterator();
      int firstSize = ite.next().size(); //size of first element
      while (ite.hasNext()){
        LinkedList<Double> tmp = ite.next();
        sameSize = sameSize && (firstSize == tmp.size());
      }
      if (!sameSize){
        throw new ParseCancellationException("[error] Two lines in the same data block have different number of elements (columns).");
      }
    }
    //after checks, convert
    Double [][] matrix = new Double[dll.size()][dll.getFirst().size()];
    //copy elements' reference
    int i = 0;
    Iterator<LinkedList<Double>> ite = dll.iterator();
    while (ite.hasNext()){
      LinkedList<Double> tmp = ite.next();
      int j = 0;
      Iterator<Double> ite2 = tmp.iterator();
      while (ite2.hasNext()){
        Double tmp2 = ite2.next();
        matrix[i][j] = tmp2;
        j++;
      }
      i++;
    }
    
    //---------------- debugging code below:
    //~ String debugPrint = "";
    //~ for (i = 0; i< matrix.length; i++){
      //~ for (int j = 0; j< matrix[i].length; j++){
        //~ debugPrint += matrix[i][j] + " ";
      //~ }
      //~ debugPrint += "\n";
    //~ }
    //~ 
    //~ if (true) throw new ParseCancellationException(debugPrint);
    //---------------- end of debugging code
    
    return matrix;
  }
  
}
