
package sbc.expr;

public class BExprAND extends BExpression {
  
  private BExpression bexpr1;
  private BExpression bexpr2;

  public BExprAND(BExpression bexpr1, BExpression bexpr2){
    this.bexpr1 = bexpr1;
    this.bexpr2 = bexpr2;
  }
  
  public BExpression getBExpression1(){
    return bexpr1;
  }

  public BExpression getBExpression2(){
    return bexpr2;
  }
}
