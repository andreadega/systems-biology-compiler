
package sbc.expr;

public class ExprExp extends Expression {
  
  private Expression expr;

  public ExprExp(Expression expr){
    this.expr = expr;
  }
  
  public Expression getExpression(){
    return expr;
  }

}
