
package sbc.expr;

public class ExprPar extends Expression {
  
  private Expression expr;

  public ExprPar(Expression expr){
    this.expr = expr;
  }
  
  public Expression getExpression(){
    return expr;
  }
}
