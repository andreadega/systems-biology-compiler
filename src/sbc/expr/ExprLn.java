
package sbc.expr;

public class ExprLn extends Expression {
  
  private Expression expr;

  public ExprLn(Expression expr){
    this.expr = expr;
  }
  
  public Expression getExpression(){
    return expr;
  }

}
