
package sbc.expr;

public class ExprIfThenElse extends Expression {
  
  private BExpression bexpr;
  private Expression expr1;
  private Expression expr2;

  public ExprIfThenElse(BExpression bexpr, Expression expr1, Expression expr2){
    this.bexpr = bexpr;
    this.expr1 = expr1;
    this.expr2 = expr2;
  }
  
  public BExpression getBExpression(){
    return bexpr;
  }
  
  public Expression getExpression1(){
    return expr1;
  }

  public Expression getExpression2(){
    return expr2;
  }
}
