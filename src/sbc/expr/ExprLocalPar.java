
package sbc.expr;

public class ExprLocalPar extends Expression {
  
  private String id;

  public ExprLocalPar(String id){
    this.id = id;
  }
  
  public String getID(){
    return id;
  }
}
