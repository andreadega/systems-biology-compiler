
package sbc.expr;

public class ExprNegation extends Expression {
  
  private Expression expr;

  public ExprNegation(Expression expr){
    this.expr = expr;
  }
  
  public Expression getExpression(){
    return expr;
  }
}
