
package sbc.expr;

public class ExprCos extends Expression {
  
  private Expression expr;

  public ExprCos(Expression expr){
    this.expr = expr;
  }
  
  public Expression getExpression(){
    return expr;
  }

}
