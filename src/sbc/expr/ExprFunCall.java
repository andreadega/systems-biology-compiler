
package sbc.expr;

import java.util.*;

public class ExprFunCall extends Expression {
  
  private Integer id;
  private LinkedList<Expression> args;

  public ExprFunCall(Integer id, LinkedList<Expression> args){
    this.id = id;
    this.args = args;
  }
  
  public Integer getID(){
    return id;
  }
  
  public LinkedList<Expression> getArguments(){
    return args;
  }
}
