
package sbc.expr;

public class BExprPar extends BExpression {
  
  private BExpression bexpr;

  public BExprPar(BExpression bexpr){
    this.bexpr = bexpr;
  }
  
  public BExpression getBExpression(){
    return bexpr;
  }

}
