
package sbc.expr;

public class BExprNot extends BExpression {
  
  private BExpression bexpr;

  public BExprNot(BExpression bexpr){
    this.bexpr = bexpr;
  }
  
  public BExpression getBExpression(){
    return bexpr;
  }

}
