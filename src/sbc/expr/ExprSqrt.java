
package sbc.expr;

public class ExprSqrt extends Expression {
  
  private Expression expr;

  public ExprSqrt(Expression expr){
    this.expr = expr;
  }
  
  public Expression getExpression(){
    return expr;
  }

}
