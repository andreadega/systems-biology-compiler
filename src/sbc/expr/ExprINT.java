
package sbc.expr;

public class ExprINT extends Expression {
  
  private Integer i;

  public ExprINT(Integer i){
    this.i = i;
  }
  
  public Integer getInteger(){
    return i;
  }
}
