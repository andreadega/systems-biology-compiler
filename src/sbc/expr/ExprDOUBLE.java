
package sbc.expr;

public class ExprDOUBLE extends Expression {
  
  private Double d;

  public ExprDOUBLE(Double d){
    this.d = d;
  }
  
  public Double getDouble(){
    return d;
  }
}
