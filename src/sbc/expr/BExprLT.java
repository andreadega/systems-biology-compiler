
package sbc.expr;

public class BExprLT extends BExpression {
  
  private Expression expr1;
  private Expression expr2;

  public BExprLT(Expression expr1, Expression expr2){
    this.expr1 = expr1;
    this.expr2 = expr2;
  }
  
  public Expression getExpression1(){
    return expr1;
  }

  public Expression getExpression2(){
    return expr2;
  }
}
