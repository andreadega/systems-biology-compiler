
package sbc.expr;

public class ExprSin extends Expression {
  
  private Expression expr;

  public ExprSin(Expression expr){
    this.expr = expr;
  }
  
  public Expression getExpression(){
    return expr;
  }

}
