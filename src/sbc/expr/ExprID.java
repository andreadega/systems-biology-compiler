
package sbc.expr;

public class ExprID extends Expression {
  
  private Integer id;

  public ExprID(Integer id){
    this.id = id;
  }
  
  public Integer getID(){
    return id;
  }
}
