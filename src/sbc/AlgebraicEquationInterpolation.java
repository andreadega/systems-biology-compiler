package sbc;

import java.util.*;

public class AlgebraicEquationInterpolation extends AlgebraicEquationNode {
  
  public static int LINEAR = 0;
  public static int SPLINE = 1;
  
  private int typeOfInterpolation;

  public AlgebraicEquationInterpolation(Integer identifier, int typeOfInterpolation){
    super(identifier);
    this.typeOfInterpolation = typeOfInterpolation;
  }
  
  public int getTypeOfInterpolation(){
    return typeOfInterpolation;
  }
  
}
