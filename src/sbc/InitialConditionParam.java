package sbc;

import java.util.*;

public class InitialConditionParam extends InitialConditionNode {
  
  private Integer paramID;
  //factor is a number that multiplies the specified parameter.
  //it is 1 if no factor is specified. This is to give a little more
  //flexibility to intialisation of differential variables, expcially for
  //sensitivity plots where one wants to try different initialisations,
  //by multiplying a parameter by different values. This also allows
  //to specify that S is twice as much as P and varying only the parameter
  //of initialisation of P
  private Double factor;

  public InitialConditionParam(Integer identifier, Integer paramID, Double factor){
    super(identifier);
    this.factor = factor;
    this.paramID = paramID;
  }
  
  public Integer getParamID(){
    return paramID;
  }
  public Double getFactor(){
    return factor;
  }
  
}
