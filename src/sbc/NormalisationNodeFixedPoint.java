
package sbc;

import java.util.*;

public class NormalisationNodeFixedPoint extends NormalisationNode {
  
  private Integer condition;
  private Double time;
  
  /**
   * After the applciaiton of this normalisation, we know that the normalisation
   * is applicable and what the time point position is in the data matrix relative
   * to this normalisation
   * */
  
  private int timePointPosition = -1;
  
  public NormalisationNodeFixedPoint(Integer identifier, Integer condition, Double time){
    super(identifier);
    this.condition = condition;
    this.time = time;
  }
  
  public Integer getCondition(){
    return condition;
  }
  
  public Double getTime(){
    return time;
  }
  
  public void setTimePointPosition(int timePointPosition){
    this.timePointPosition = timePointPosition;
  }

  public int getTimePointPosition(){
    return this.timePointPosition;
  }
}
