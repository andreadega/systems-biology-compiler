
package sbc;

import java.util.*;

public class NormalisationNodeAverage extends NormalisationNode {
  
  private LinkedList<Integer> conditions;
  
  /**
   * this list contains an array of time points, one for each condition, in the
   * same order of the conditions. Each list of booleans indicates the time
   * points that need to be summed for that condition for the current identifier
   * This list is used to normalise simulations, and it is instantiated
   * after the data has been normalised, which is when we figure out whether
   * the normalisation is applicable and what time points are summed
   * */

  private LinkedList<Boolean[]> timePointsToSum;
  private int numberOfDataPointsInTheSum;
  
  public NormalisationNodeAverage(Integer identifier, LinkedList<Integer> conditions){
    super(identifier);
    this.conditions = conditions;
    this.timePointsToSum = null;
    this.numberOfDataPointsInTheSum = 0; //If not set this will rise a div by zero error when computing average
  }
  
  public LinkedList<Integer> getConditions(){
    return conditions;
  }
  
  public void setTimePointsToSum(LinkedList<Boolean[]> timePointsToSum){
    this.timePointsToSum = timePointsToSum;
  }
  
  public LinkedList<Boolean[]> getTimePointsToSum(){
    return timePointsToSum;
  }

  public void setNumberOfDataPointsInTheSum(int n){
    this.numberOfDataPointsInTheSum = n;
  }
  
  public int getNumberOfDataPointsInTheSum(){
    return numberOfDataPointsInTheSum;
  }
}
