
package sbc;

import java.util.*;

public class DataNodeReplicates extends DataNode {
  
  private LinkedList<Double [][]> listOfDataMatrices;
  
  public DataNodeReplicates(Integer identifier, 
                                LinkedList<Integer> conditions,
                                LinkedList<Double [][]> listOfDataMatrices){
    super(identifier,conditions);
    this.listOfDataMatrices = listOfDataMatrices;
  }
  
  public LinkedList<Double [][]> getListOfDataMatrices(){
    return this.listOfDataMatrices;
  }
  
  @Override
  public boolean equals(Object other){
    if (other == null) return false;
    if (other == this) return true;
    if (!(other instanceof DataNodeReplicates))return false;
    DataNodeReplicates c = (DataNodeReplicates)other;  //downcast
    
    //identifier
    if (!this.getIdentifier().equals(c.getIdentifier())) return false;
    
    //check condtions
    if (!this.getConditions().equals(c.getConditions())) return false;
    
    //check data
    if (!this.listOfDataMatrices.equals(c.listOfDataMatrices)) return false;

    //no differences found
    return true;
  }
  
  @Override
  public int hashCode(){
    int hash = 0;
    hash += this.getIdentifier().hashCode();
    hash += this.getConditions().hashCode();
    hash += this.listOfDataMatrices.hashCode();
    return hash;
  }  
  
}
