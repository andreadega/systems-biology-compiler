package sbc;

import java.util.*;
import sbc.expr.Expression;

public class DifferentialEquationExpr extends DifferentialEquationNode {
  
  private Expression expr;

  public DifferentialEquationExpr(Integer identifier, Expression expr){
    super(identifier);
    this.expr = expr;
  }
  
  public Expression getExpression(){
    return expr;
  }
  
}
