
package sbc;

import java.util.*;

public class NormSimulationsWithoutData extends NormSimulationsNode {
  
  private Integer normCondition;
  private Double normTime;
  
  public NormSimulationsWithoutData(Integer identifier, LinkedList<Integer> conditions, Integer normCondition, Double normTime){
    super(identifier,conditions);
    this.normCondition = normCondition;
    this.normTime = normTime;
  }
  
  public Double getNormTime(){
    return this.normTime;
  }
  
  public Integer getNormCondition(){
    return this.normCondition;
  }
}
