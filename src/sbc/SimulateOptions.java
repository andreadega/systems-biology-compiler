
package sbc;

import java.util.*;

//probably need a funtion to run some checks on the Simulate Options

public class SimulateOptions {
  
  //map Integer species ID to a list of Integer condition IDs
  //each map in the list should be a separate plot 
  private LinkedList<HashMap<Integer, LinkedList<Integer>>> listOfIdCondList;
  
  //list of normalisations for the simulations
  private LinkedList<NormSimulationsNode> listOfNormalisations;
  
  //list of of DoseResponsePlot
  private LinkedList<DoseResponsePlot> listOfDoseResponsePlots;
  
  //list of of TwoVariablesPlot
  private LinkedList<TwoVariablesPlot> listOfTwoVariablesPlots;
  
  private double abstol = 1e-3;
  private double reltol = 1e-6;
  private double initialTime = 0;
  private double finalTime = 10;
  private int timePoints = 1000;
  
  public SimulateOptions(){
    listOfIdCondList = new LinkedList<HashMap<Integer, LinkedList<Integer>>>();
    listOfNormalisations = new LinkedList<NormSimulationsNode>();
    listOfDoseResponsePlots = new LinkedList<DoseResponsePlot>();
    listOfTwoVariablesPlots = new LinkedList<TwoVariablesPlot>();
  }

  /**
   * each idcondlist added should result in an output graph, where different
   * variables (species) and conditions are plotted at the same time
   * */

  public void addIDCondList(HashMap<Integer, LinkedList<Integer>> idCondList){
    listOfIdCondList.add(idCondList);
  }

  public LinkedList<HashMap<Integer, LinkedList<Integer>>> getListOfIdCondList(){
    return listOfIdCondList;
  }
  
  public void addDoseResponsePlot(DoseResponsePlot drp){
    listOfDoseResponsePlots.add(drp);
  }
  
  public LinkedList<DoseResponsePlot> getDoseResponsePlots(){
    return listOfDoseResponsePlots;
  }
  
  public void addTwoVariablesPlot(TwoVariablesPlot tvp){
    listOfTwoVariablesPlots.add(tvp);
  }
  
  public LinkedList<TwoVariablesPlot> getTwoVariablesPlots(){
    return listOfTwoVariablesPlots;
  }
  
  public void addNormSimulationsNode(NormSimulationsNode node){
    listOfNormalisations.add(node);
  }

  public LinkedList<NormSimulationsNode> getListOfNormalisations(){
    return listOfNormalisations;
  }
  
  public void setAbstol(double abstol){
    this.abstol = abstol;
  }
  
  public double getAbstol(){
    return abstol;
  }
  
  public void setReltol(double reltol){
    this.reltol = reltol;
  }
  
  public double getReltol(){
    return reltol;
  }

  public void setInitialTime(double initialTime){
    this.initialTime = initialTime;
  }
  
  public double getInitialTime(){
    return initialTime;
  }
  
  public void setFinalTime(double finalTime){
    this.finalTime = finalTime;
  }
  
  public double getFinalTime(){
    return finalTime;
  }
  
  public void setTimePoints(int timePoints){
    this.timePoints = timePoints;
  }
  
  public int getTimePoints(){
    return timePoints;
  }

  
}
