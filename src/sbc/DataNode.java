
package sbc;

import java.util.*;

public abstract class DataNode implements GenericEntry {
  
  private Integer identifier;
  private boolean isNormalised = false;
  private LinkedList<Integer> conditions;
  
  protected DataNode(Integer identifier, LinkedList<Integer> conditions){
    this.identifier = identifier;
    this.conditions = conditions;
  }
  
  public LinkedList<Integer> getConditions(){
    return this.conditions;
  }
  
  public Integer getIdentifier(){
    return this.identifier;
  }
  
  public boolean isNormalised(){
    return this.isNormalised;
  }
  public void setIsNormalised(boolean b){
    this.isNormalised = b;
  }
  
  public void checkCompatibleWith(GenericEntry entry) throws Exception{
    if (entry==null) return;
    //really something wrong if you check compatibility with yourself (this)
    //I don't see any reason why this should happen
    if (entry==this) throw new Exception("[error] Probably programming error,"+
                          " as two DataNode objects tested for compatibility" + 
                          " (isCompatibleWith) are the same pointer."); 
    if (entry instanceof DataNode){
      //if the identifier is different then return
      //else
      //if there is no overlapping of elements in the two conditions lists
      //then return
      
      DataNode dn = (DataNode)entry;  //downcast
      
      if (dn.getIdentifier().equals(this.getIdentifier())){
        Iterator<Integer> ite = this.conditions.iterator();
        if (ite.hasNext()) ite.next(); //skip time
        while (ite.hasNext()){
          Integer tmp = ite.next();
          //return false if any element of this.conditions is in dn.conditions
          if (dn.conditions.contains(tmp)) {
            throw new Exception("[error] Two data entries have the same identifier"+
                          " (Species), AND have overlapping set of conditions." + 
                          " Sets of conditions should not overlap.");
          }
        }
        //if the identifier is the same but the conditions are different
        //then we are talking about different western blots, so it's ok
      }
      return;
    } else if (entry instanceof ParameterNode){
      ParameterNode p = (ParameterNode)entry;  //downcast
      if (p.getIdentifier().equals(this.getIdentifier())){
        throw new Exception("[error] A data entry and a parameter entry "+
                      "have the same identifier.");
      }
      return;
    }else if (entry instanceof FunctionNode){
      //if the identifier is different then return 
      
      FunctionNode dn = (FunctionNode)entry;  //downcast
      
      if (dn.getIdentifier().equals(this.getIdentifier())){
        throw new Exception("[error] A data entry and a function entry "+
                      "have the same identifier.");
      }
      return;
    } else if (entry instanceof EventNode){
      //if the identifier is different then return 
      
      EventNode dn = (EventNode)entry;  //downcast
      
      if (dn.getIdentifier().equals(this.getIdentifier())){
        throw new Exception("[error] A data node entry and an event entry "+
                      "have the same identifier.");
      }
      return;
    }
    //no known problems with other types implementing GenericEntry
    return;
  }
}
