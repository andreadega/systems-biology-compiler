
package sbc;
import java.util.*;

public class NormalisationTool{

  /**
   * for all normalisations in the normalisations list, apply the normalisation
   * to the data list
   * */

  public static void apply(LinkedList<GenericEntry> dataList,LinkedList<GenericEntry> normList, LookUpTable lookUpTable) throws Exception{
    Iterator<GenericEntry> ite = normList.iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (tmp instanceof NormalisationNodeFixedPoint){
        applyNormalisationByFixedPoint(dataList,(NormalisationNodeFixedPoint)tmp,lookUpTable);
      } else if (tmp instanceof NormalisationNodeAverage){
        applyNormalisationByAverage(dataList,(NormalisationNodeAverage)tmp,lookUpTable);
      } else if (tmp instanceof NormalisationNode){
        throw new Exception("[error] Probably a coding error (NormalisationTool class). Trying to apply an unknown normalisation.");
      } else{
        throw new Exception("[error] Probably a coding error (NormalisationTool class). The GenericEntry object is not a NormalisationNode object.");
      }
    }
    
  }
  
  private static void applyNormalisationByFixedPoint(LinkedList<GenericEntry> dataList, NormalisationNodeFixedPoint normNode, LookUpTable lookUpTable) throws Exception{
    /*
     * first of all, iterate all the dataList to find the correct identifier
     * we know, from previous checks that there could be more than one data node
     * with the identifier we want, but only one will have the condition we
     * want to use as normalisation point
     * */
     
     Integer speciesID = normNode.getIdentifier();
     Integer condition = normNode.getCondition();
     Double time = normNode.getTime();
     
     
     Iterator<GenericEntry> ite = dataList.iterator();
     while (ite.hasNext()){
       GenericEntry tmp = ite.next();
       if (! (tmp instanceof DataNodeReplicates)) throw new Exception("[error] Probably a "
              + "coding error (NormalisationTool class). The GenericEntry object is not a DataNodeReplicates object.");
       DataNodeReplicates dnr = (DataNodeReplicates) tmp; //downcast
       
       if(dnr.getIdentifier().equals(speciesID)){
          //species identifier found now look for the condition
          int positionOfCondition = dnr.getConditions().indexOf(condition); //LinkedList method, -1 if not found
          if(positionOfCondition != -1){
            //condition found, so we need to normalise this data node
            
            //check that the condition is not time
            if (positionOfCondition == 0) throw new Exception("[error] could not apply normalisation by fixed point (" + lookUpTable.lookUpString(speciesID) 
                  +","+ lookUpTable.lookUpString(condition) +"): cannot use TIME (first column) as normalisation condition.");
        
            //check that the time point is valid
            int positionOfTP = positionOfTimePoint(time,dnr.getListOfDataMatrices().getFirst()); //use the first data block, from previous checks all time points are the same
            if (positionOfTP != -1){
              //time point found
              //check for NAN or Zero at normalisation point in all replicates (data blocks)
              
              if(existsNANorZeroAtPosition(dnr.getListOfDataMatrices(),positionOfCondition,positionOfTP)){
                throw new Exception("[error] could not apply normalisation by fixed point (" + lookUpTable.lookUpString(speciesID) 
                  +","+ lookUpTable.lookUpString(condition) +"," + time.toString() + "): N/A or zero at normalisation point in at least one of the data blocks.");
              }else{
                //The normalisation by fixed point specified is valid:
                //1) check if the entry has already been normalised
                //2) apply normalisation by fixed point to all data replicates (data blocks)
                    
                if (dnr.isNormalised()) throw new Exception("[error] could not apply normalisation by fixed point (" + lookUpTable.lookUpString(speciesID) 
                  +","+ lookUpTable.lookUpString(condition) +"," + time.toString() + "): data entry is already normalised.");
                
                normaliseByFixedPoint(dnr.getListOfDataMatrices(),positionOfCondition,positionOfTP);
                normNode.setTimePointPosition(positionOfTP);
                dnr.setIsNormalised(true);
                
                return;
              }
              
            }else{
              //the specified time point could not be found, in this case we
              //are already sure that the normalisation is invalid
              throw new Exception("[error] could not apply normalisation by fixed point (" + lookUpTable.lookUpString(speciesID) 
                  +","+ lookUpTable.lookUpString(condition) +"): time point " + time.toString() + " of normalisation not found.");
            }
          }
       }
       
     }
     
     //if you reach this point then the normalisation was not applied!
     throw new Exception("[error] could not apply normalisation by fixed point: identifier " + lookUpTable.lookUpString(speciesID)
              + " and/or condition "+ lookUpTable.lookUpString(condition) +" target of normalisation not found.");
     
  }
  
  private static void applyNormalisationByAverage(LinkedList<GenericEntry> dataList, NormalisationNodeAverage normNode, LookUpTable lookUpTable) throws Exception{
  /*
    * first of all, iterate all the dataList to find the correct identifier
    * we know, from previous checks that there could be more than one data node
    * with the identifier we want, but only one can have the conditions we
    * want to use for the normalisation by sum
    * */
     
    Integer speciesID = normNode.getIdentifier();
    LinkedList<Integer> conditionsToSum = normNode.getConditions();
     
    Iterator<GenericEntry> ite = dataList.iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof DataNodeReplicates)) throw new Exception("[error] Probably a "
            + "coding error (NormalisationTool class). The GenericEntry object is not a DataNodeReplicates object.");
      DataNodeReplicates dnr = (DataNodeReplicates) tmp; //downcast
       
      if(dnr.getIdentifier().equals(speciesID)){
        //species identifier found now look for the conditions
        int[] conditionsPositions = new int[conditionsToSum.size()];
        boolean conditionsAllFound = true;
        boolean conditionIsTime = false;
        boolean conditionsAllMissing = true;
        
        //check if all the necessary conditions are in this data node
        {
          int i = 0;
          Iterator<Integer> ite2 = conditionsToSum.iterator();
          while (ite2.hasNext()){
            Integer tmp2 = ite2.next();
            conditionsPositions[i] = dnr.getConditions().indexOf(tmp2);
            if (conditionsPositions[i]==-1){
              //at least one condition was not found
              conditionsAllFound = false;
            }else if (conditionsPositions[i]==0){
              //one condition is TIME!
              conditionIsTime = true;
            }else{
              //at least one condition was found
              conditionsAllMissing = false;
            }
            i++;
          }
        }
        //check if one fo the conditions is TIME
        if (conditionIsTime){
          throw new Exception("[error] could not apply normalisation by average (" + lookUpTable.lookUpString(speciesID)
              + "): cannot use TIME (first column) as normalisation condition.");
        }
        //now, if all specified conditions are found then none of the
        //positions is -1. If all positions are -1, it might just be the
        //wrong blot, so move next in the dataList. If a few are -1, but
        //not all then it is an error and the whole procedure should return
        //false
        if (conditionsAllFound){
          //we are in the right blot, i.e. right identifier and all necessary
          //conditions for normalisation by sum are present. Now we need
          //to check that at least one time point for each condition is a
          //valid number, i.e. not NAN or zero
                 
          //Use function existsNANorZeroAtPosition which checks for at least a NAN or zero
          //in all replicates, for specific positions (cond/time). We have 
          //list of conditions to check in conditionsPositions array        
          boolean dataFound = true;
          boolean[] existsValidDataAtCond = new boolean[conditionsToSum.size()];
          {
            int i = 0;
            Iterator<Integer> ite3 = conditionsToSum.iterator();
            while (ite3.hasNext()){
              Integer tmp3 = ite3.next();
              existsValidDataAtCond[i] = false;
              for (int j=0;j<dnr.getListOfDataMatrices().getFirst().length;j++){
                if (!existsNANorZeroAtPosition(dnr.getListOfDataMatrices(),conditionsPositions[i],j)) existsValidDataAtCond[i] = true;
              }
              dataFound = dataFound && existsValidDataAtCond[i];
              i++;
            }
          }
          
          if (!dataFound){

            throw new Exception("[error] could not apply normalisation by average (" + lookUpTable.lookUpString(speciesID)
              + "): could not find a valid or non-zero data point in at least one of the conditions specified.");
          }
          
          //here we know we can apply the normalisation by sum. Check that
          //this data node is not already normalised:
          if (dnr.isNormalised()) throw new Exception("[error] could not apply normalisation by average (" + lookUpTable.lookUpString(speciesID) 
                  +"): data entry is already normalised.");
                
          
          //RETRIEVE SUITABLE DATA POINTS TO SUM
          //This could be separate function, but I would need to pass many
          //parameters, so I just keep it here
          //--------------------------------------------------------------
          
          //need a sum for each replicate (will be just one if average)
          double[] sums = new double[dnr.getListOfDataMatrices().size()];  
          for (int i = 0; i<dnr.getListOfDataMatrices().size();i++ ) sums[i] = 0;  //initialise to neutral 0
          //also need to store how many data points are in the sum, in order to compute the average
          int numberOfDataPointsInSum = 0;
          
          //initialise pointers to be used to store time points (for storing normalisation after application)
          LinkedList<Boolean[]> tmpIntListList = new LinkedList<Boolean[]>();
          Boolean[] tmpIntList = null;
          
          {
            int i = 0;
            //for all conditions to sum
            Iterator<Integer> ite4 = conditionsToSum.iterator();
            while (ite4.hasNext()){
              Integer tmp4 = ite4.next();
              tmpIntList = new Boolean[dnr.getListOfDataMatrices().getFirst().length]; //start a new list
             
              //for all time points
              for (int j=0;j<dnr.getListOfDataMatrices().getFirst().length;j++){
                if (!existsNANorZeroAtPosition(dnr.getListOfDataMatrices(),conditionsPositions[i],j)){
                  //at the current position, no replicate has NAN or Zero, so is part of the sum
                  //count the data point
                  numberOfDataPointsInSum++;
                  //collect the data points across the replicates
                  Iterator<Double[][]> ite5 = dnr.getListOfDataMatrices().iterator();
                  int k = 0;
                  while (ite5.hasNext()){
                    Double[][] matrix = ite5.next();
                    sums[k] += matrix[j][conditionsPositions[i]].doubleValue(); //we know the position of the condition to sum i
                    k++;
                  }
                  //set true at the current time point for this condition to sum
                  tmpIntList[j] = true;
                  
                } else {
                  //this time point is either zero or NAN, so exclude
                  tmpIntList[j] = false;
                }
              }
              //done with this condition, move to the next condition to sum i++
              //add the boolean array for this condition to the list (at the end)
              tmpIntListList.add(tmpIntList);
              i++;
            } 
          }
          
          //--------------------------------------------------------------
          //END OF RETRIEVING DATA POINTS TO SUM
          //at this point we have array sums to use for actual normalisation
          //also we have tmpIntListList containing actual time points used
          //for calculating the sums. Time points can be different in different conditions
          //The conditions, i.e. the lists, are in the same order as
          //in the list conditionsToSum
          
          
          //TODO CONTINUE FROM HERE!!! apply normalisation
          //1) normalise replicates (by Average)
          for (int k=0;k<sums.length;k++) sums[k]/=((double)numberOfDataPointsInSum);
          normaliseByGivenValues(dnr.getListOfDataMatrices(),sums);
          
          //2) store normalisation time points in the normalisation node
          normNode.setTimePointsToSum(tmpIntListList);
          normNode.setNumberOfDataPointsInTheSum(numberOfDataPointsInSum);
          
          return ; //DONE! Applied and Added!
        
        
        }else if (conditionsAllMissing){
          //the identifier is correct but none of the conditions were
          //found in this blot. There could be another data entry in dataList
          //with the requested conditions. Search down the list
        }else{
          //some of the requested conditions are in this blot but not all of them
          //we deduce that the normalisation cannot take place because we
          //assume that only conditions on the same data entry are directly
          //comparable, e.g. multi-strip western blot
          throw new Exception("[error] could not apply normalisation by average(" + lookUpTable.lookUpString(speciesID) 
                  +"): some of the requested conditions are in this blot but not all of them.");
        }  

      
      }
    }
     
    //if you reach this point then the normalisation was not applied!
    throw new Exception("[error] could not apply normalisation by average: identifier " + lookUpTable.lookUpString(speciesID)
              + ", could not find target of normalisation.");
  }

  /**
   * find time point position in a data block. If the time point is not
   * in the data block then return -1
   * */
  private static int positionOfTimePoint(Double time, Double[][] matrix){
    int pos = -1;
    for (int i=0;i<matrix.length;i++) if (time.equals(matrix[i][0])) pos = i;
    
    return pos;
  }
  
  /**
   * return true if at the specified position there is a NAN or the number 0 in one of the
   * matrices in the list, 
   */
  private static boolean existsNANorZeroAtPosition(LinkedList<Double[][]> list, int posCondition, int posTimePoint){
    
    Iterator<Double[][]> ite = list.iterator();
    while (ite.hasNext()){
      Double[][] tmp = ite.next();
      if (tmp[posTimePoint][posCondition].isNaN() || tmp[posTimePoint][posCondition].doubleValue() == 0.0) return true;
    }
    
    return false;
  }
    /**
   * when using this function, you should have already checked that the normalisation is valid and
   * no NAN or 0 are at the normalisation point in the data blocks 
   */
  private static void normaliseByFixedPoint(LinkedList<Double[][]> listOfDataMatrices,int positionOfCondition,int positionOfTP){
    Iterator<Double[][]> ite = listOfDataMatrices.iterator();
    while (ite.hasNext()){
      Double[][] tmp = ite.next();
      double scalingFactor = tmp[positionOfTP][positionOfCondition].doubleValue();
      for(int i=0;i<tmp.length;i++){
        for (int j=1;j<tmp[i].length;j++){ //skip time condition (time is always the first column, start from j=1)
          if(!tmp[i][j].isNaN() && tmp[i][j].doubleValue() != 0.0) tmp[i][j] = new Double(tmp[i][j].doubleValue()/scalingFactor);
        }
      }
    }    
  }
  
  
      /**
   * when using this function, you should have already checked that the normalisation is valid and
   * that the scalingFactors are not NAN or 0 
   */
  
  private static void normaliseByGivenValues(LinkedList<Double[][]> listOfDataMatrices,double[] scalingFactors){
    Iterator<Double[][]> ite = listOfDataMatrices.iterator();
    int k = 0;
    while (ite.hasNext()){
      Double[][] tmp = ite.next();
      
      for(int i=0;i<tmp.length;i++){
        for (int j=1;j<tmp[i].length;j++){ //skip time condition (time is always the first column, start from j=1)
          if(!tmp[i][j].isNaN() && tmp[i][j].doubleValue() != 0.0) tmp[i][j] = new Double(tmp[i][j].doubleValue()/scalingFactors[k]);
        }
      }
      
      k++;
    }  
  }
  
}
