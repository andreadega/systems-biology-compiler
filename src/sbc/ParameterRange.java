package sbc;

import java.util.*;

public class ParameterRange extends ParameterNode {
  
  private Double top;
  private Double bottom;

  public ParameterRange(Integer identifier, Double bottom, Double top){
    super(identifier);
    this.top = top;
    this.bottom = bottom;
  }
  
  public Double getTop(){
    return top;
  }
   
  public Double getBottom(){
    return bottom;
  } 
  
}
