
package sbc;
import java.util.*;
import sbc.util.*;
import java.lang.Math;

public class LogLikelihoodTool{
  
  public static final int IndependentNoise = 1;
  public static final int LinearModelOfNoise = 2;
  
  //C is a parameter that does not alter the log likelihood results
  //but makes the loglikelihood positive if chosen correctly.
  //we choose it as -2ln(min_sigma), that is negative 2 times the natural
  //logarithm of the minimum value of sigma, when the minimum value of
  // sigma is negative. when modelling sigma with sa+sb*y, we consider
  //the minimum value of sa instead
  private double logLikelihood_paramC = 0;
  
  private LinkedList<ParameterRangeOfAdditionalParam> listOfAdditionalParams = null;
  private LinkedList<String> c_initAdditionalParams = null;
  
  /**
   * The constructor will find the data that need to be fitted and 
   * identify which data points (protein, condition, time) have at least
   * one replicate  with a valid data point (not null, but could be 0).
   * These data points have an associated measurement error that needs
   * to be estimated in the log likelihood approach
   * */

  public LogLikelihoodTool(EntryLists entryLists,FittingOptions fittingOptions,LookUpTable lookUpTable) throws Exception{
    
    listOfAdditionalParams = new LinkedList<ParameterRangeOfAdditionalParam>();
    c_initAdditionalParams = new LinkedList<String>();
    
    /*
     * OK, now we need to know which type of LogLikelihood approach we are using:
     * 1. Independent Noise, estimate every Sigma_ik
     * 2. Linear Model Noise, estimate Sa and Sb, with linear model: 
     *    Sigma_ik = Sa + Sb*y_ik, where y_ik is the simulated observable, 
     *    either scaled or normalised
     * */
    
    int typeOfLL = fittingOptions.getLogLikelihood();
    double lowest_lowerbound = 0.001;
     
    if (typeOfLL == LogLikelihoodTool.IndependentNoise){
      
      /*
       * first of all, iterate all the dataList to find the correct identifier
       * we know, from previous checks that there could be more than one data node
       * with the identifier we want, but only one can have the conditions we
       * want 
       * */
      
      //for all species to fit
      Iterator<Integer> ite20 = fittingOptions.getIDCondList().keySet().iterator();
      while (ite20.hasNext()){
        Integer speciesID = ite20.next();
        LinkedList<Integer> listOfConditions = fittingOptions.getIDCondList().get(speciesID);
        
        //for all conditions to fit of speciesID
        Iterator<Integer> ite21 = listOfConditions.iterator();
        while (ite21.hasNext()){
          Integer cond = ite21.next();
          
          //retrieve dataNode for this speciesID and condition and build uniqueID
          DataNodeReplicates dnr = QueryModelAndData.getDataNodeReplicatesOf(entryLists,speciesID,cond);
          //retrieve position in dnr matrix of condition cond, to pull out time series
          int posOfCond = dnr.getConditions().indexOf(cond);
          
          // need to iterate through time, and for each position in the 
          // data matrix where the average is not nan (undefined), then
          // we have a sigma parameter that we need to fit for the log
          // likelihood function
          
          //array name in C++
          String c_array = lookUpTable.lookUpString(speciesID) + "c_" 
                            + cond.intValue() + "sigma";
          int nTimePoints = dnr.getListOfDataMatrices().getFirst().length;
          //add C++ intialisation line to the list
          c_initAdditionalParams.add(c_array_init(c_array,nTimePoints,(DataNode)dnr,lookUpTable));     
          
          //retrieve sigma parameters to search and their range
          //if average sigma across replicates is nan, then ignore
          //if average sigma across replicates is 0, then use wide range [0.001,1000]
          //if average sigma across replicates is >0, then use range [0.001*ave,2*ave]
          //if average sigma across replicates is <0, then use range [0.001*-ave,2*-ave]
          for (int j=0;j<nTimePoints;j++){
            Double average = averageAtPosition(dnr.getListOfDataMatrices(),posOfCond,j);
            if (!average.isNaN()){
              double lower = 0.001;
              double upper = 1000;
              if (average.doubleValue()>0){
                lower = average*0.001;
                upper = average*2;
              }else if (average.doubleValue()<0){
                lower = (-average)*0.001;
                upper = (-average)*2;            
              }
              String sigmaID = "sigma_" + lookUpTable.lookUpString(speciesID) + "c_" 
                            + cond.intValue() + "tpos_" + String.valueOf(j);
              listOfAdditionalParams.add(new ParameterRangeOfAdditionalParam(lookUpTable.getKey(sigmaID),
                                                          new Double(lower),
                                                          new Double(upper),
                                                          c_array + "[" + String.valueOf(j) + "]"));
              if (lower<lowest_lowerbound)lowest_lowerbound=lower;
            }
          }
        }
      }
      this.logLikelihood_paramC = -2*Math.log(lowest_lowerbound);
    } else if (typeOfLL == LogLikelihoodTool.LinearModelOfNoise){
      
      //names of parameters Sa and Sb to be used in c files
      String c_name_Sa = "_logLikelihood_param_Sa";
      String c_name_Sb = "_logLikelihood_param_Sb";
      
      //add initialisation of Sa and Sb to list of param init
      c_initAdditionalParams.add("realtype " + c_name_Sa + " = 1;");
      c_initAdditionalParams.add("realtype " + c_name_Sb + " = 1;");
      
      //add the params to the search algorithm
      double lower = 0.001;
      double upper = 2;
      listOfAdditionalParams.add(new ParameterRangeOfAdditionalParam(lookUpTable.getKey(c_name_Sa),
                                                          new Double(lower),
                                                          new Double(upper),
                                                          c_name_Sa));
      listOfAdditionalParams.add(new ParameterRangeOfAdditionalParam(lookUpTable.getKey(c_name_Sb),
                                                          new Double(lower),
                                                          new Double(upper),
                                                          c_name_Sb));
      this.logLikelihood_paramC = -2*Math.log(lower);
    } else{
      throw new Exception("[error] Probably a "
        + "coding error (LogLikelihoodTool() constructor). Unknown type of LogLikelihood");
    }
  }  
  /**
   * procedure that returns a string with the C++ array initialisation
   * that will contain sigmas for a given (species,condition) pair
   * */
  
  private String c_array_init(String arrayname, int arraylength, DataNode dn, LookUpTable lookUpTable){
    String uniqueID = QueryModelAndData.buildUniqueIDFrom(lookUpTable,dn);
    String initline = "static double " + arrayname + "[" + uniqueID + "ntimepoints] = { NAN";
    //with one NAN already added, add (arraylength-1) other NAN
    for (int i = 1; i < arraylength;i++) initline += ",NAN";
    initline += " };";
    return initline;
  }

  /**
   * return the average across replicates of a position in the data matrix
   * with coordinate (condition,time). If all replicates are nan at that
   * point, return nan  
   */
  private static Double averageAtPosition(LinkedList<Double[][]> list, int posCondition, int posTimePoint){
    boolean allNAN = true;
    double average = 0;
    int counter = 0;
    Iterator<Double[][]> ite = list.iterator();
    while (ite.hasNext()){
      Double[][] tmp = ite.next();
      if (!tmp[posTimePoint][posCondition].isNaN()){
        average += tmp[posTimePoint][posCondition].doubleValue();
        counter++;
        allNAN = false;
      }
    }
    //if we found something at the given position, then return its average
    if (!allNAN) return new Double(average/((double)counter));
    //if all the replicates are nan, then return nan
    return Double.NaN;
  }
  
  public LinkedList<ParameterRangeOfAdditionalParam> getListOfAdditionalParams(){
    return this.listOfAdditionalParams;
  }

  public LinkedList<String> getC_initAdditionalParams(){
    return this.c_initAdditionalParams;
  }
  
  public double getLogLikelihood_paramC(){
    return this.logLikelihood_paramC;
  }
}
