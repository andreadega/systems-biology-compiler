package sbc;

import java.util.*;
import sbc.expr.Expression;

public class DifferentialEquationCondList extends DifferentialEquationNode {
  
  private HashMap<Integer,Expression> mapConditionToExpression;

  public DifferentialEquationCondList(Integer identifier, HashMap<Integer,Expression> mapConditionToExpression){
    super(identifier);
    this.mapConditionToExpression = mapConditionToExpression;
  }
  
  public HashMap<Integer,Expression> getMapConditionToExpression(){
    return mapConditionToExpression;
  }
  
}
