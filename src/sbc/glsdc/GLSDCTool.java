
package sbc.glsdc;

import java.util.*;
import java.text.*;
import java.io.*;
import java.lang.*;
import sbc.*;
import sbc.expr.*;
import sbc.util.*;

public class GLSDCTool{
  
  public static int ALGEBRAIC = 0;
  public static int DIFFERENTIAL = 1;
  public static int PARAMETER = 2;
  
  private int GLSDC = 0;
  private int LEVMAR_FD = 1;
  private int LEVMAR_SE = 2;
  private int LEVMAR_SE_JAC = 3;

  private EntryLists entryLists = null;
  private FittingOptions fittingOptions = null;
  private SimulateOptions simulateOptions = null;
  private LookUpTable lookUpTable = null;
  private LogLikelihoodTool logLikelihoodTool = null;

  public GLSDCTool(EntryLists entryLists,FittingOptions fittingOptions,SimulateOptions simulateOptions,
                    LogLikelihoodTool logLikelihoodTool,LookUpTable lookUpTable) throws NullPointerException{
    if (entryLists == null) throw new NullPointerException();
    if (fittingOptions == null) throw new NullPointerException();
    if (simulateOptions == null) throw new NullPointerException();
    if (lookUpTable == null) throw new NullPointerException();
    this.entryLists = entryLists;
    this.fittingOptions = fittingOptions;
    this.simulateOptions = simulateOptions;
    this.lookUpTable = lookUpTable;    
    this.logLikelihoodTool = logLikelihoodTool; //could be null, use this as a check for logLikelihood
  }
  
  public void generateFiles(BufferedWriter log) throws Exception{
    
    //simulator first, in case someone just wants to simulate without fitting
    
    if(!simulateOptions.getListOfIdCondList().isEmpty() || !simulateOptions.getTwoVariablesPlots().isEmpty() || !simulateOptions.getDoseResponsePlots().isEmpty()){
      generateFile_ODEsimulatorC(log);
      generateDirectories_ODEsimulator(log);
      generateDataFiles_ODEsimulator(log);
      generatePlots_ODEsimulator(log);
      generateSimulationsExFiles(log);
      log.write("[info] Simulation output plots requested. Files generated in KimuraODEsim directory.\n");
    }else{
      log.write("[info] No simulation output plots requested. No files generated in KimuraODEsim directory.\n");
    }
    
    if(! (fittingOptions.getIDCondList() == null)){
      generateFileRunallSH(log);
      //generateFilePostprocessSH(log); //now this file already exists
      generateFilePBSPostprocessSH(log);
      generateFilePBSRunallSH(log);
      generateFilePBSRunallnodesSH(log);
      generateFile_exp_sbcmodel_org_1dat_top(log);
      generateFile_pset_sbcmodel_orgdat(log);
      generateFile_fitness_sbcmodel_orgC(log);
      generateFileCorrelationMatrixR(log);
      generateFileVisualiseBestFitsPLT(log);
      generateFileVisualiseBestFitsClustersPLT(log);
      generateParameterEstimationExFiles(log);
      generateFileLevMarRunallSH(log);
      generateFile_levmarOptimiserC_FD(log);
      generateFile_levmarOptimiserC_SE(log);
      generateFileLevMarPBSPostprocessSH(log);
      generateFileLevMarPBSRunallSH(log);
      generateFileLevMarPBSRunallnodesSH(log);
      generateListOfUnknownParameters(log);

      log.write("[info] Parameter estimation requested. Files generated in KimuraGLSDC and parameterAnalysis directories.\n");
    }else{
      log.write("[info] No parameter estimation requested. No files generated in KimuraGLSDC and parameterAnalysis directories.\n");
    }    
  }
  
  private void generateSimulationsExFiles(BufferedWriter log) throws Exception{
    BufferedWriter bw = null; 
    String fileName = "runSimulations.sh";
    
    try{
          //Output log file
      File outputFile = new File(fileName);
      Runtime.getRuntime().exec("chmod 755 " + fileName);          
      if (!outputFile.exists()) {
        outputFile.createNewFile();
      }
      FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
      bw = new BufferedWriter(fw);
      
      //--------------- WRITE FILE!
      bw.write("#!/bin/bash\n\n");

      bw.write("cd KimuraODEsim\n" + "./runSimulations.sh ../$1\n\n");

            
      log.write("[info] file written: " + fileName + "\n");
      
      //--------------- DONE WRITING!
      
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }finally{ //this is always executed, even if a return is called
      bw.flush();
      bw.close();
    }        
  }
  
  private void generateParameterEstimationExFiles(BufferedWriter log) throws Exception{
    BufferedWriter bw = null; 
    String fileName = "runParameterEstimationOnUnixCluster.sh";
    try{
          //Output log file
      File outputFile = new File(fileName);
      Runtime.getRuntime().exec("chmod 755 " + fileName);        
      if (!outputFile.exists()) {
        outputFile.createNewFile();
      }
      FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
      bw = new BufferedWriter(fw);
      
      //--------------- WRITE FILE!

      //bw.write("#!/bin/bash\n\n");

      //bw.write("module load gsl\n\n");

      bw.write("#!/bin/bash\n\n"
          + "PEMETHOD=$1\n\n"
          + "if [ \"$PEMETHOD\" = \'\' ]; then\n"
          + "  PEMETHOD=\"GLSDC\"\n"
          + "fi\n\n"
					+ "./buildLibraries.sh\n"
					+ "if [ $? != 0 ]; then\n"
					+ "  echo \"Failed to compile all libraries, quitting the pipeline\";\n"
					+ "  exit 1\n"
					+ "fi\n"
          + "echo \"###############################################################\"\n"
          + "echo \"### Running parameter estimation script                     ###\"\n"
          + "echo \"###############################################################\"\n"
          + "echo \"### Algorithm ${PEMETHOD} requested\"\n"
          + "echo \"###############################################################\"\n\n"
          + "if [ \"$PEMETHOD\" = \'GLSDC\' ]; then\n"
          + "  cd KimuraGLSDC/glsdc/v1/\n"
          + "  chmod 755 pbsrunallnodes.sh\n"
          + "  ./pbsrunallnodes.sh\n"
          + "elif [ \"$PEMETHOD\" = \'LSQNONLIN_FD\' ]; then\n"
          + "  cd Lsqnonlin_LevMar\n"
          + "  chmod 755 pbsrunallnodes.sh\n"
          + "  ./pbsrunallnodes.sh \"FD\"\n"
          + "elif [ \"$PEMETHOD\" = \'LSQNONLIN_SE\' ]; then\n"
          + "  cd Lsqnonlin_LevMar\n"
          + "  chmod 755 pbsrunallnodes.sh\n"
          + "  ./pbsrunallnodes.sh \"SE\"\n"
          + "fi\n\n");
                  
      log.write("[info] file written: " + fileName + "\n");
   
      //--------------- DONE WRITING!
      
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }finally{ //this is always executed, even if a return is called
      bw.flush();
      bw.close();
    }        
    
    fileName = "runParameterEstimationOnLinuxDesktop.sh";
    try{
          //Output log file
      File outputFile = new File(fileName);
      Runtime.getRuntime().exec("chmod 755 " + fileName);           
      if (!outputFile.exists()) {
        outputFile.createNewFile();
      }
      FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
      bw = new BufferedWriter(fw);
      
      //--------------- WRITE FILE!
      
      bw.write("#!/bin/bash\n\n"
          + "PEMETHOD=$1\n\n"
          + "if [ \"$PEMETHOD\" = \'\' ]; then\n"
          + "  PEMETHOD=\"GLSDC\"\n"
          + "fi\n\n"
					+ "./buildLibraries.sh\n"
					+ "if [ $? != 0 ]; then\n"
					+ "  echo \"Failed to compile all libraries, quitting the pipeline\";\n"
					+ "  exit 1\n"
					+ "fi\n"
          + "echo \"###############################################################\"\n"
          + "echo \"### Running parameter estimation script                     ###\"\n"
          + "echo \"###############################################################\"\n"
          + "echo \"### Algorithm ${PEMETHOD} requested\"\n"
          + "echo \"###############################################################\"\n\n"
          + "if [ \"$PEMETHOD\" = \'GLSDC\' ]; then\n"
          + "  cd KimuraGLSDC/glsdc/v1/\n"
          + "  chmod 755 runall.sh\n"
          + "  ./runall.sh\n"
          + "elif [ \"$PEMETHOD\" = \'LSQNONLIN_FD\' ]; then\n"
          + "  cd Lsqnonlin_LevMar\n"
          + "  chmod 755 runall.sh\n"
          + "  ./runall.sh \"FD\"\n"
          + "elif [ \"$PEMETHOD\" = \'LSQNONLIN_SE\' ]; then\n"
          + "  cd Lsqnonlin_LevMar\n"
          + "  chmod 755 runall.sh\n"
          + "  ./runall.sh \"SE\"\n"
          + "fi\n\n");
          
      log.write("[info] file written: " + fileName + "\n");
      
      //--------------- DONE WRITING!
      
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }finally{ //this is always executed, even if a return is called
      bw.flush();
      bw.close();
    }     
    
    fileName = "retrieveBestFits.sh";
    try{
          //Output log file
      File outputFile = new File(fileName);
      Runtime.getRuntime().exec("chmod 755 " + fileName);    
      if (!outputFile.exists()) {
        outputFile.createNewFile();
      }
      FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
      bw = new BufferedWriter(fw);
      
      //--------------- WRITE FILE!

      bw.write("#!/bin/bash\n\n"
      + "cd KimuraGLSDC/glsdc/v1/\n\n"
      + "chmod 755 retrieveBestFits.sh\n"
      + "./retrieveBestFits.sh\n\n");
                  
      log.write("[info] file written: " + fileName + "\n");
      
      //--------------- DONE WRITING!
      
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }finally{ //this is always executed, even if a return is called
      bw.flush();
      bw.close();
    }     

    fileName = "runParameterAnalysis.sh";
    try{
          //Output log file
      File outputFile = new File(fileName);
      Runtime.getRuntime().exec("chmod 755 " + fileName);     
      if (!outputFile.exists()) {
        outputFile.createNewFile();
      }
      FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
      bw = new BufferedWriter(fw);
      
      //--------------- WRITE FILE!

      bw.write("#!/bin/bash\n\n"
      + "cp $1 parameterAnalysis/\n"
      + "cd parameterAnalysis\n"
      + "./parameterAnalysis.sh $1\n\n");
                  
      log.write("[info] file written: " + fileName + "\n");
      
      //--------------- DONE WRITING!
      
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }finally{ //this is always executed, even if a return is called
      bw.flush();
      bw.close();
    } 

    
  }
  
  private void generateFileVisualiseBestFitsClustersPLT(BufferedWriter log) throws Exception{
    
    BufferedWriter bw = null; 
    String fileName = "parameterAnalysis/visualiseBestFitsClusters.plt";
    
    try{
          //Output log file
      File outputFile = new File(fileName);        
      if (!outputFile.exists()) {
        outputFile.createNewFile();
      }
      FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
      bw = new BufferedWriter(fw);
      
      //--------------- WRITE FILE!
      
      bw.write("#!/usr/bin/gnuplot -persist\n\n"

            + "set term pngcairo dashed dl 5 enhanced size 3072, 2304 font \"Arial, 64\"\n"
            + "set border lw 8\n"
            + "unset xtics\n\n");

            //get IDs of parameters to fit
            LinkedList<Integer> params = QueryModelAndData.getParametersToFit(entryLists);
            
            bw.write("set xtics 0,1," + params.size() + "\n");

            int n = 0;
            Iterator<Integer> ite2 = params.iterator();
            while (ite2.hasNext()){
              Integer tmp = ite2.next();
              bw.write("set xtics add (\'" + lookUpTable.lookUpString(tmp).replace("_","\\_") + "\' " + n + ")\n"); 
              n++; 
            }
            
            //if LogLikelihood is requested, I need to fit some additional parameters as well
            int typeOfLL = fittingOptions.getLogLikelihood();
            if (typeOfLL == LogLikelihoodTool.IndependentNoise || typeOfLL == LogLikelihoodTool.LinearModelOfNoise){
              Iterator<ParameterRangeOfAdditionalParam> ite_sigma = logLikelihoodTool.getListOfAdditionalParams().iterator();
              while (ite_sigma.hasNext()){
                ParameterRangeOfAdditionalParam pr = ite_sigma.next();
                bw.write("set xtics add (\'" + lookUpTable.lookUpString(pr.getIdentifier()).replace("_","\\_") + "\' " + n + ")\n"); 
                n++;
              }
            }
            
            bw.write("set xtics add (\"\" " + n + ")\n");
            bw.write("\nset xtics nomirror rotate by -45\n\n");

            bw.write("lastParameter = " + (n - 1) + "\n\n");

            bw.write("set grid xtics noytics lw 5\n\n");

            bw.write("set title \"Visualisation of \".numberOfBestFitsCluster.\" best fits (Cluster \".cluster.\")\"\n\n");

            bw.write("set lmargin at screen 0.15\n"
                  + "set bmargin at screen 0.18\n"
                  + "set rmargin at screen 0.8\n"
                  + "set tmargin at screen 0.9\n"
                  + "set size 1,1. \n"
                  + "set pointsize 1.5\n"
                  + "set xrange [-1:lastParameter+1]\n"
                  + "top = numberOfBestFitsCluster\n"
                  + "set cbrange [0:top]\n"
                  + "set palette defined ( 0 0.05 0.05 0.2, 0.1 0 0 1, 0.25 0.7 0.85 0.9,\\\n"
                  + "     0.4 0 0.75 0, 0.5 1 1 0, 0.7 1 0 0, 0.9 0.6 0.6 0.6,\\\n"
                  + "     1 0.7 0.07 0.7 ) \n"
                  + "#set xtics 1\n"
                  + "#set cbtics 1\n"
                  + "set log y\n"
                  + "set ylabel \"value of parameters\"\n"
                  + "set xlabel \"parameters\"\n"
                  + "set cblabel \"parameter sets\"\n\n");

            bw.write("set output \"visualiseBestFitsCluster\".cluster.\".png\"\n"
                  + "plot for [i=1:numberOfBestFitsCluster] fileWithPars.cluster.\"T\" using 0:(column(numberOfBestFitsCluster+1-i)):(numberOfBestFitsCluster+1-i) w l lw 8 lt 1 palette notitle, \\\n"
                  + "\"pset.dat\" using 1:(exp($2)) with points pt 2 ps 5 lc 1 lw 8 t \"search range lower bound\", \\\n"
                  + "\"pset.dat\" using 1:(exp($3)) with points pt 3 ps 5 lc 2 lw 8 t \"search range upper bound\"\n\n");

            bw.write("#unset log y\n\n");

            bw.write("set title \"Average and standard deviation of \".numberOfBestFitsCluster.\" best fits (Cluster \".cluster.\")\"\n"
                  + "set output \"visualiseBestFitsCluster\".cluster.\"AveStd.png\"\n"
                  + "plot fileWithPars.cluster.\"AveStdT\" using 0:1 w l lt 1 lc 3 lw 8 notitle, fileWithPars.cluster.\"AveStdT\" using 0:1:2 w yerrorbars lt 1 lc 3 lw 8 notitle\n\n"
                  + "set title \"Coefficient of variation of \".numberOfBestFitsCluster.\" best fits (Cluster \".cluster.\")\"\n"
                  + "set output \"visualiseBestFitsCluster\".cluster.\"CV.png\"\n"
                  + "plot fileWithPars.cluster.\"AveStdT\" using 0:($2/$1) w l lt 1 lc 3 lw 8 notitle\n\n"
                  + "set style data boxplot\n"
                  + "set style boxplot fraction 0.95 #whiskers contain 95% of points, rest is treated as outliars\n"
                  + "set title \"Quartiles boxplots (\".numberOfBestFitsCluster.\" best fits, Cluster \".cluster.\")\"\n"
                  + "set output \"visualiseBestFitsCluster\".cluster.\"Boxplot.png\"\n"
                  + "set xrange [-1:lastParameter+1]\n"
                  + "plot for [i=1:lastParameter+1] fileWithPars.cluster using (i-1):i lt 1 lc 3 pt 6 lw 8 notitle, \\\n"
                  + "    \"pset.dat\" using 1:(exp($2)) with points pt 2 ps 5 lc 1 lw 8 t \"search range lower bound\", \\\n"
                  + "    \"pset.dat\" using 1:(exp($3)) with points pt 3 ps 5 lc 2 lw 8 t \"search range upper bound\"\n\n");
            
      log.write("[info] file written: " + fileName + "\n");
      
      //--------------- DONE WRITING!
      
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }finally{ //this is always executed, even if a return is called
      bw.flush();
      bw.close();
    }
  } 


  private void generateFileVisualiseBestFitsPLT(BufferedWriter log) throws Exception{
    
    BufferedWriter bw = null; 
    String fileName = "parameterAnalysis/visualiseBestFits.plt";
    
    try{
          //Output log file
      File outputFile = new File(fileName);        
      if (!outputFile.exists()) {
        outputFile.createNewFile();
      }
      FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
      bw = new BufferedWriter(fw);
      
      //--------------- WRITE FILE!
      
      bw.write("#!/usr/bin/gnuplot -persist\n\n"

            + "set term pngcairo dashed dl 5 enhanced size 3072, 2304 font \"Arial, 64\"\n"
            + "set output \"visualiseBestFits\".numberOfBestFits.\".png\"\n"
            + "set border lw 8\n"
            + "unset xtics\n\n");

            //get IDs of parameters to fit
            LinkedList<Integer> params = QueryModelAndData.getParametersToFit(entryLists);
            
            bw.write("set xtics 0,1," + params.size() + "\n");

            int n = 0;
            Iterator<Integer> ite2 = params.iterator();
            while (ite2.hasNext()){
              Integer tmp = ite2.next();
              bw.write("set xtics add (\'" + lookUpTable.lookUpString(tmp).replace("_","\\_") + "\' " + n + ")\n"); 
              n++; 
            }
            
            //if LogLikelihood is requested, I need to fit some additional parameters as well
            int typeOfLL = fittingOptions.getLogLikelihood();
            if (typeOfLL == LogLikelihoodTool.IndependentNoise || typeOfLL == LogLikelihoodTool.LinearModelOfNoise){
              Iterator<ParameterRangeOfAdditionalParam> ite_sigma = logLikelihoodTool.getListOfAdditionalParams().iterator();
              while (ite_sigma.hasNext()){
                ParameterRangeOfAdditionalParam pr = ite_sigma.next();
                bw.write("set xtics add (\'" + lookUpTable.lookUpString(pr.getIdentifier()).replace("_","\\_") + "\' " + n + ")\n"); 
                n++;
              }
            }
            
            bw.write("set xtics add (\"\" " + n + ")\n");
            bw.write("\nset xtics nomirror rotate by -45\n\n");

            bw.write("lastParameter = " + (n - 1) + "\n\n");

            bw.write("set grid xtics noytics lw 5\n\n");

            bw.write("set title \"Visualisation of \".numberOfBestFits.\" best fits\"\n\n");

            bw.write("set lmargin at screen 0.15\n"
                  + "set bmargin at screen 0.18\n"
                  + "set rmargin at screen 0.8\n"
                  + "set tmargin at screen 0.9\n"
                  + "set size 1,1. \n"
                  + "set pointsize 1.5\n"
                  + "set xrange [-1:lastParameter+1]\n"
                  + "top = numberOfBestFits\n"
                  + "set cbrange [0:top]\n"
                  + "set palette defined ( 0 0.05 0.05 0.2, 0.1 0 0 1, 0.25 0.7 0.85 0.9,\\\n"
                  + "     0.4 0 0.75 0, 0.5 1 1 0, 0.7 1 0 0, 0.9 0.6 0.6 0.6,\\\n"
                  + "     1 0.7 0.07 0.7 ) \n"
                  + "#set xtics 1\n"
                  + "#set cbtics 1\n"
                  + "set log y\n"
                  + "set ylabel \"value of parameters\"\n"
                  + "set xlabel \"parameters\"\n"
                  + "set cblabel \"parameter sets\"\n\n");

            bw.write("plot for [i=1:numberOfBestFits] fileWithPars.\"T\" using 0:(column(numberOfBestFits+1-i)):(numberOfBestFits+1-i) w l lw 8 lt 1 palette notitle, \\\n"
                  + "\"pset.dat\" using 1:(exp($2)) with points pt 2 ps 5 lc 1 lw 8 t \"search range lower bound\", \\\n"
                  + "\"pset.dat\" using 1:(exp($3)) with points pt 3 ps 5 lc 2 lw 8 t \"search range upper bound\"\n\n");

            bw.write("#unset log y\n\n");

            bw.write("set title \"Average and standard deviation of \".numberOfBestFits.\" best fits\"\n"
                  + "set output \"visualiseBestFits\".numberOfBestFits.\"AveStd.png\"\n\n"
                  + "#plot \"bestFits\".numberOfBestFits.\"AveStd.txt\" using 0:(log($1)) w l lt 1 lc 3 notitle, \"bestFits\".numberOfBestFits.\"AveStd.txt\" using 0:(log($1)):(log($1+$2)-log($1)):(log($1-$2)+log($1)) w yerrorbars lt 1 lc 3 notitle\n\n"
                  + "plot fileWithPars.\"AveStdT\" using 0:1 w l lt 1 lc 3 lw 8 notitle, fileWithPars.\"AveStdT\" using 0:1:2 w yerrorbars lt 1 lc 3 lw 8 notitle\n\n"
                  + "#unset log y\n\n"
                  + "set title \"Coefficient of variation of \".numberOfBestFits.\" best fits\"\n"
                  + "set output \"visualiseBestFits\".numberOfBestFits.\"CV.png\"\n\n"
                  + "plot fileWithPars.\"AveStdT\" using 0:($2/$1) w l lt 1 lc 3 lw 8 notitle\n\n"
                  + "set style data boxplot\n"
                  + "set style boxplot fraction 0.95 #whiskers contain 95% of points, rest is treated as outliars\n\n"
                  + "set title \"Quartiles boxplots where circles are farthest 5% of points (\".numberOfBestFits.\" best fits)\"\n"
                  + "set output \"visualiseBestFits\".numberOfBestFits.\"Boxplot.png\"\n\n"
                  + "set xrange [-1:lastParameter+1]\n\n"
                  + "plot for [i=1:lastParameter+1] fileWithPars using (i-1):i lt 1 lc 3 pt 6 lw 8 notitle, \\\n"
                  + "    \"pset.dat\" using 1:(exp($2)) with points pt 2 ps 5 lc 1 lw 8 t \"search range lower bound\", \\\n"
                  + "    \"pset.dat\" using 1:(exp($3)) with points pt 3 ps 5 lc 2 lw 8 t \"search range upper bound\" \n\n");
            
      log.write("[info] file written: " + fileName + "\n");
      
      //--------------- DONE WRITING!
      
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }finally{ //this is always executed, even if a return is called
      bw.flush();
      bw.close();
    }
  } 
  
  private void generateFileCorrelationMatrixR(BufferedWriter log) throws Exception{
    
    BufferedWriter bw = null; 
    String fileName = "parameterAnalysis/correlationMatrix.R";
    
    try{
          //Output log file
      File outputFile = new File(fileName);        
      if (!outputFile.exists()) {
        outputFile.createNewFile();
      }
      FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
      bw = new BufferedWriter(fw);
      
      //--------------- WRITE FILE!
      
      bw.write("#!/usr/bin/Rscript\n\n"
            + "my.plotcorr <- function (corr, outline = FALSE, col = \"grey\", upper.panel = c(\"ellipse\", \"number\", \"none\"), "
            + "lower.panel = c(\"ellipse\", \"number\", \"none\"), diag = c(\"none\", \"ellipse\", \"number\"), digits = 2, bty = \"n\", "
            + "axes = FALSE, xlab = \"\", ylab = \"\", asp = 1, cex.lab = par(\"cex.lab\"), cex = 0.75 * par(\"cex\"), mar = 0.1 + c(2, 2, 4, 2), ...)\n"
            + "{\n"
            + "# this is a modified version of the plotcorr function from the ellipse package\n"
            + "# this prints numbers and ellipses on the same plot but upper.panel and lower.panel changes what is displayed\n"
            + "# diag now specifies what to put in the diagonal (numbers, ellipses, nothing)\n"
            + "# digits specifies the number of digits after the . to round to\n"
            + "# unlike the original, this function will always print x_i by x_i correlation rather than being able to drop it\n"
            + "# modified by Esteban Buz\n"
            + "  if (!require('ellipse', quietly = TRUE, character = TRUE)) {\n"
            + "    stop(\"Need the ellipse library\")\n"
            + "  }\n"
            + "  savepar <- par(pty = \"s\", mar = mar)\n"
            + "  on.exit(par(savepar))\n"
            + "  if (is.null(corr))\n"
            + "    return(invisible())\n"
            + "  if ((!is.matrix(corr)) || (round(min(corr, na.rm = TRUE), 6) < -1) || (round(max(corr, na.rm = TRUE), 6) > 1))\n"
            + "    stop(\"Need a correlation matrix\")\n"
            + "  plot.new()\n"
            + "  par(new = TRUE)\n"
            + "  rowdim <- dim(corr)[1]\n"
            + "  coldim <- dim(corr)[2]\n"
            + "  rowlabs <- dimnames(corr)[[1]]\n"
            + "  collabs <- dimnames(corr)[[2]]\n"
            + "  if (is.null(rowlabs))\n"
            + "    rowlabs <- 1:rowdim\n"
            + "  if (is.null(collabs))\n"
            + "    collabs <- 1:coldim\n"
            + "  rowlabs <- as.character(rowlabs)\n"
            + "  collabs <- as.character(collabs)\n"
            + "  col <- rep(col, length = length(corr))\n"
            + "  dim(col) <- dim(corr)\n"
            + "  upper.panel <- match.arg(upper.panel)\n"
            + "  lower.panel <- match.arg(lower.panel)\n"
            + "  diag <- match.arg(diag)\n"
            + "  cols <- 1:coldim\n"
            + "  rows <- 1:rowdim\n"
            + "  maxdim <- max(length(rows), length(cols))\n"
            + "  plt <- par(\"plt\")\n"
            + "  xlabwidth <- max(strwidth(rowlabs[rows], units = \"figure\", cex = cex.lab))/(plt[2] - plt[1])\n"
            + "  xlabwidth <- xlabwidth * maxdim/(1 - xlabwidth)\n"
            + "  ylabwidth <- max(strwidth(collabs[cols], units = \"figure\", cex = cex.lab))/(plt[4] - plt[3])\n"
            + "  ylabwidth <- ylabwidth * maxdim/(1 - ylabwidth)\n"
            + "  plot(c(-xlabwidth - 0.5, maxdim + 0.5), c(0.5, maxdim + 1 + ylabwidth), type = \"n\", bty = bty, axes = axes, xlab = \"\", ylab = \"\", asp = asp, cex.lab = cex.lab, ...)\n"
            + "  text(rep(0, length(rows)), length(rows):1, labels = rowlabs, adj = 1, cex = cex.lab)\n"
            + "  text(cols, rep(length(rows) + 1, length(cols)), labels = collabs, srt = 90, adj = 0, cex = cex.lab)\n"
            + "  mtext(xlab, 1, 0)\n"
            + "  mtext(ylab, 2, 0)\n"
            + "  mat <- diag(c(1, 1))\n"
            + "  plotcorrInternal <- function() {\n"
            + "    if (i == j){ #diag behavior\n"
            + "      if (diag == 'none'){\n"
            + "        return()\n"
            + "      } else if (diag == 'number'){\n"
            + "        text(j + 0.3, length(rows) + 1 - i, round(corr[i, j], digits=digits), adj = 1, cex = cex)\n"
            + "      } else if (diag == 'ellipse') {\n"
            + "        mat[1, 2] <- corr[i, j]\n"
            + "        mat[2, 1] <- mat[1, 2]\n"
            + "        ell <- ellipse(mat, t = 0.43)\n"
            + "        ell[, 1] <- ell[, 1] + j\n"
            + "        ell[, 2] <- ell[, 2] + length(rows) + 1 - i\n"
            + "        polygon(ell, col = col[i, j])\n"
            + "        if (outline)\n"
            + "          lines(ell)\n"
            + "      }\n"
            + "    } else if (i >= j){ #lower half of plot\n"
            + "      if (lower.panel == 'ellipse') { #check if ellipses should go here\n"
            + "        mat[1, 2] <- corr[i, j]\n"
            + "        mat[2, 1] <- mat[1, 2]\n"
            + "        ell <- ellipse(mat, t = 0.43)\n"
            + "        ell[, 1] <- ell[, 1] + j\n"
            + "        ell[, 2] <- ell[, 2] + length(rows) + 1 - i\n"
            + "        polygon(ell, col = col[i, j])\n"
            + "        if (outline)\n"
            + "          lines(ell)\n"
            + "      } else if (lower.panel == 'number') { #check if ellipses should go here\n"
            + "        text(j + 0.3, length(rows) + 1 - i, round(corr[i, j], digits=digits), adj = 1, cex = cex)\n"
            + "      } else {\n"
            + "        return()\n"
            + "      }\n"
            + "    } else { #upper half of plot\n"
            + "      if (upper.panel == 'ellipse') { #check if ellipses should go here\n"
            + "        mat[1, 2] <- corr[i, j]\n"
            + "        mat[2, 1] <- mat[1, 2]\n"
            + "        ell <- ellipse(mat, t = 0.43)\n"
            + "        ell[, 1] <- ell[, 1] + j\n"
            + "        ell[, 2] <- ell[, 2] + length(rows) + 1 - i\n"
            + "        polygon(ell, col = col[i, j])\n"
            + "        if (outline)\n"
            + "          lines(ell)\n"
            + "      } else if (upper.panel == 'number') { #check if ellipses should go here\n"
            + "        text(j + 0.3, length(rows) + 1 - i, round(corr[i, j], digits=digits), adj = 1, cex = cex)\n"
            + "      } else {\n"
            + "        return()\n"
            + "      }\n"
            + "    }\n"
            + "  }\n"
            + "  for (i in 1:dim(corr)[1]) {\n"
            + "    for (j in 1:dim(corr)[2]) {\n"
            + "      plotcorrInternal()\n"
            + "    }\n"
            + "  }\n"
            + "  invisible()\n"
            + "}\n\n\n"
            + "args <- commandArgs(TRUE)\n"
            + "bestfits <- args[1]\n"
            + "cluster <- args[2]\n"
            + "if (is.na(bestfits)) bestfits <- \"../bestFits.txt\"\n"
            + "if (is.na(cluster)) cluster <- \"\"\n\n"
            + "readfrom <- paste(bestfits,cluster,sep=\"\")\n"
            + "matrixOfParams <- read.table(readfrom)\n\n"
            + "#colnames(matrixOfParams) <- c(\"K_{qss1}\",\"Km_{1}\",\"Km_{2}\",\"K_{qss3}\",\"Km_{3}\",\"Km_{4}\",\"K_{d11}\",\"k_5\",\"a_1\",\"a_2\",\"{/Symbol w}_{1}\",\"{/Symbol w}_{2}\",\"K_S\",\"K_{CE}\",\"k_6\",\"k_7\",\"kcat_9\",\"V_{10}\",\"Km_9\",\"Km_{10}\",\"K_{pF}\",\"k_{13}\",\"k_{14}\")\n"
            + "#colnames(matrixOfParams) <- c(\"k[5]\",\"a\",\"omega\",\"K[S]\",\"K[EC]\",\"k[6]\",\"k[7]\",\"k[8]\",\"K[qss9]\",\"Km[9]\",\"Km[10]\",\"K[pF]\",\"k[13]\",\"k[14]\")\n");
            
            bw.write("colnames(matrixOfParams) <- c(");
            
            //get IDs of parameters to fit
            LinkedList<Integer> params = QueryModelAndData.getParametersToFit(entryLists);
            
            Iterator<Integer> ite2 = params.iterator();
            while (ite2.hasNext()){
              Integer tmp = ite2.next();
              bw.write("\"" + lookUpTable.lookUpString(tmp) + "\"");  
              if (ite2.hasNext()) bw.write(",");
            }
            
            //if LogLikelihood is requested, I need to fit some additional parameters as well
            int typeOfLL = fittingOptions.getLogLikelihood();
            if (typeOfLL == LogLikelihoodTool.IndependentNoise || typeOfLL == LogLikelihoodTool.LinearModelOfNoise){
              Iterator<ParameterRangeOfAdditionalParam> ite_sigma = logLikelihoodTool.getListOfAdditionalParams().iterator();
              while (ite_sigma.hasNext()){
                ParameterRangeOfAdditionalParam pr = ite_sigma.next();
                bw.write(",\"" + lookUpTable.lookUpString(pr.getIdentifier()) + "\"");  
              }
            }
              
            bw.write(")\n\n");
            
            bw.write("library(package=\"ellipse\")\n\n"
            + "corr.mtcars <- cor(matrixOfParams)\n"
            + "ord <- order(corr.mtcars[1,])\n"
            + "xc <- corr.mtcars[ord, ord]\n"
            + "colors <- c(\"#A50F15\",\"#DE2D26\",\"#FB6A4A\",\"#FCAE91\",\"#FEE5D9\",\"white\",\n"
            + "  \"#EFF3FF\",\"#BDD7E7\",\"#6BAED6\",\"#3182BD\",\"#08519C\")\n"

            + "jpeg(filename = \"correlationMatrix.jpg\", width = 2000, height = 2000, \n"
            + "     units = \"px\", pointsize = 48, quality = 85, bg = \"white\")\n"
            + "my.plotcorr(xc, col=colors[5*xc + 6])\n"
            + "dev.off()\n\n");

      
            
      log.write("[info] file written: " + fileName + "\n");
      
      //--------------- DONE WRITING!
      
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }finally{ //this is always executed, even if a return is called
      bw.flush();
      bw.close();
    }
  }
  
  private void generateListOfUnknownParameters(BufferedWriter log) throws Exception{
    
    BufferedWriter bw = null; 
    String fileName = "listOfUnknownParameters.txt";
    
    try{
          //Output log file
      File outputFile = new File(fileName);        
      if (!outputFile.exists()) {
        outputFile.createNewFile();
      }
      FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
      bw = new BufferedWriter(fw);
      
      //--------------- WRITE FILE!
            
            //get IDs of parameters to fit
            LinkedList<Integer> params = QueryModelAndData.getParametersToFit(entryLists);
            
            Iterator<Integer> ite2 = params.iterator();
            while (ite2.hasNext()){
              Integer tmp = ite2.next();
              bw.write(lookUpTable.lookUpString(tmp));  
              if (ite2.hasNext()) bw.write(" ");
            }
            
            //if LogLikelihood is requested, I need to fit some additional parameters as well
            int typeOfLL = fittingOptions.getLogLikelihood();
            if (typeOfLL == LogLikelihoodTool.IndependentNoise || typeOfLL == LogLikelihoodTool.LinearModelOfNoise){
              Iterator<ParameterRangeOfAdditionalParam> ite_sigma = logLikelihoodTool.getListOfAdditionalParams().iterator();
              while (ite_sigma.hasNext()){
                ParameterRangeOfAdditionalParam pr = ite_sigma.next();
                bw.write(" " + lookUpTable.lookUpString(pr.getIdentifier()));  
              }
            }
              
            bw.write("\n");      
            
      log.write("[info] file written: " + fileName + "\n");
      
      //--------------- DONE WRITING!
      
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }finally{ //this is always executed, even if a return is called
      bw.flush();
      bw.close();
    }
  }
  
  private void generateFileRunallSH(BufferedWriter log) throws Exception{
    
    BufferedWriter bw = null; 
    String fileName = "KimuraGLSDC/glsdc/v1/runall.sh";
    
    try{
          //Output log file
      File outputFile = new File(fileName);        
      if (!outputFile.exists()) {
        outputFile.createNewFile();
      }
      FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
      bw = new BufferedWriter(fw);
      
      //--------------- WRITE FILE!
      
      bw.write("#!/bin/bash\n\n"
            + "#Additional fancy notification system\n"
            + "echo 0 > work/sbcmodel_all/1/tmp\n\n"
            + "#INIT is the first random seed to use\n"
            + "INIT=1\n\n"
            + "#PRS is the number of processors to use\n"
            + "PRS=" + fittingOptions.getNumberOfProcessorsPerNode() + "\n\n"
            + "#Z is the number of seeds to be used by each processors\n"
            + "#so Z seeds are tested in sequence\n"
            + "Z=" + fittingOptions.getNumberOfJobsPerProcessor() + "\n\n"
            + "#total number of seeds tested is PRS*Z starting from INIT\n\n");
      
      bw.write("#Y is a just a counter, do not touch\n"
            + "Y=1\n\n"
            + "cd work/sbcmodel_all/1\n"
            + "rm -f 000_bestValues.txt\n"
            + "rm -f *.log\n"
            + "cd ../../..\n"
            + "make\n"
            + "chmod 755 bin/a.out\n"
            + "mv bin/a.out work/sbcmodel_all/sbcmodel\n\n");
            
      bw.write("echo \"###############################################################\"\n"
            + "echo \"### Commencing parameter estimation\"\n"
            + "echo \"### Using " + fittingOptions.getNumberOfProcessorsPerNode() + " processor(s)\"\n"
            + "echo \"### Performing " + fittingOptions.getNumberOfJobsPerProcessor() + " optimisation(s) per processor\"\n"
            + "echo \"###############################################################\"\n\n");
            
      bw.write("for i in `seq $INIT $Z $(($Z*$PRS+$INIT-1))`;\n"
            + "do\n"
            + "  ./run.sh $i $Y $Z &\n"
            + "  let Y=Y+1\n"
            + "done\n\n");
      
      bw.write("wait\n"
            + "#rm work/sbcmodel_all/1/lock\n"
            + "rm work/sbcmodel_all/1/tmp\n"
            + "chmod 755 postprocess.sh\n"
            + "./postprocess.sh " + (fittingOptions.getNumberOfProcessorsPerNode()*fittingOptions.getNumberOfJobsPerProcessor()) + "\n");
      
            
      log.write("[info] file written: " + fileName + "\n");
      
      //--------------- DONE WRITING!
      
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }finally{ //this is always executed, even if a return is called
      bw.flush();
      bw.close();
    }
  }
  
  private void generateFileLevMarRunallSH(BufferedWriter log) throws Exception{
    
    BufferedWriter bw = null; 
    String fileName = "Lsqnonlin_LevMar/runall.sh";
    
    try{
          //Output log file
      File outputFile = new File(fileName);        
      if (!outputFile.exists()) {
        outputFile.createNewFile();
      }
      FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
      bw = new BufferedWriter(fw);
      
      //--------------- WRITE FILE!
      
      bw.write("#!/bin/bash\n\n"
            + "JACOBIANMETHOD=$1\n\n"
            + "if [ \"$JACOBIANMETHOD\" = \'\' ]; then\n"
            + "  JACOBIANMETHOD=\"SE\"\n"
            + "fi\n\n"
            + "#INIT is the first random seed to use\n"
            + "INIT=1\n\n"
            + "#PRS is the number of processors to use\n"
            + "PRS=" + fittingOptions.getNumberOfProcessorsPerNode() + "\n\n"
            + "#Z is the number of seeds to be used by each processors\n"
            + "#so Z seeds are tested in sequence\n"
            + "Z=" + fittingOptions.getNumberOfJobsPerProcessor() + "\n\n"
            + "#total number of seeds tested is PRS*Z starting from INIT\n\n");
      
      bw.write("#Y is a just a counter, do not touch\n"
            + "Y=1\n\n"
            + "chmod 755 clean.sh\n"
            + "./clean.sh\n"
            + "cd ..\n"
            + "./buildLibraries.sh\n"
            + "if [ $? != 0 ]; then\n"
            + "  echo \"Failed to compile all libraries, quitting the pipeline\";\n"
            + "  exit 1\n"
            + "fi\n"
            + "cd Lsqnonlin_LevMar\n"
            + "make\n"
            + "chmod 755 levmarOptimiser_se\n"
            + "chmod 755 levmarOptimiser_fd\n\n");
            
      bw.write("echo \"###############################################################\"\n"
            + "echo \"### Commencing parameter estimation\"\n"
            + "echo \"### Using " + fittingOptions.getNumberOfProcessorsPerNode() + " processor(s)\"\n"
            + "echo \"### Performing " + fittingOptions.getNumberOfJobsPerProcessor() + " optimisation(s) per processor\"\n"
            + "echo \"###############################################################\"\n\n");
            
      bw.write("for i in `seq $INIT $Z $(($Z*$PRS+$INIT-1))`;\n"
            + "do\n"
            + "  ./run.sh $i $Y $Z $JACOBIANMETHOD &\n"
            + "  let Y=Y+1\n"
            + "done\n\n");
      
      bw.write("wait\n"
            + "chmod 755 postprocess.sh\n"
            + "./postprocess.sh " + (fittingOptions.getNumberOfProcessorsPerNode()*fittingOptions.getNumberOfJobsPerProcessor()) + "\n");
      
            
      log.write("[info] file written: " + fileName + "\n");
      
      //--------------- DONE WRITING!
      
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }finally{ //this is always executed, even if a return is called
      bw.flush();
      bw.close();
    }
  }


  private void generateFilePostprocessSH(BufferedWriter log) throws Exception{
    
    BufferedWriter bw = null; 
    String fileName = "KimuraGLSDC/glsdc/v1/postprocess.sh";
    
    try{
          //Output log file
      File outputFile = new File(fileName);        
      if (!outputFile.exists()) {
        outputFile.createNewFile();
      }
      FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
      bw = new BufferedWriter(fw);
      
      //--------------- WRITE FILE!
      
      bw.write("#!/bin/bash\n\n"
            + "BESTFITS=$1\n\n"
            + "if [ \"$BESTFITS\" = '' ]; then\n"
            + "  let BESTFITS=50\n"
            + "fi\n\n"
            + "cd work/sbcmodel_all/1/\n"
            + "g++ -o extractBestFits extractBestFits.cpp\n"
            + "chmod 755 extractBestFits\n"
            + "grep \"infeasible= 0\" 000_bestValues.txt | cut -d' ' -f1,9 > tmp2\n"
            + "FITSFOUND=`wc -l tmp2 | awk {'print $1'}`\n"
            + "./extractBestFits ${FITSFOUND} tmp2\n"
            + "rm tmp2\n"
            + "echo \"Found ${FITSFOUND} best fits out of ${BESTFITS} attempts\"\n"
            + "cd ../../..\n"
            + "cd ../../..\n"
            + "cd parameterAnalysis/\n"
            + "./parameterAnalysis.sh ${FITSFOUND}\n"
            + "cd ..\n"
            + "cd KimuraODEsim\n"
            + "./runSimulations.sh ${FITSFOUND}\n\n");
      
            
      log.write("[info] file written: " + fileName + "\n");
      
      //--------------- DONE WRITING!
      
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }finally{ //this is always executed, even if a return is called
      bw.flush();
      bw.close();
    }
  }  
  
  private void generateFilePBSPostprocessSH(BufferedWriter log) throws Exception{
    
    BufferedWriter bw = null; 
    String fileName = "KimuraGLSDC/glsdc/v1/pbspostprocess.sh";
    
    try{
          //Output log file
      File outputFile = new File(fileName);        
      if (!outputFile.exists()) {
        outputFile.createNewFile();
      }
      FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
      bw = new BufferedWriter(fw);
      
      //--------------- WRITE FILE!
      
      bw.write("#!/bin/bash\n\n"
            + "#PBS -l nodes=1:ppn=1\n"
            + "#PBS -l walltime=2:00:00\n"
            + "#PBS -N sbcfitting\n"
            + "#PBS -m bea\n"
            + (fittingOptions.getEmail()!=null ? "#PBS -M " + fittingOptions.getEmail() + "\n\n" : "")
            + (fittingOptions.getMinusA()!=null ? "#PBS -A " + fittingOptions.getMinusA() + "\n\n" : "")
            + "cd " + fittingOptions.getClusterDirectory() + "\n\n"
            + "./loadmodulesUnixCluster.sh\n"
            + "cd KimuraGLSDC/glsdc/v1\n"
            + "chmod 755 postprocess.sh\n"
            + "./postprocess.sh " + (fittingOptions.getNumberOfNodes()*fittingOptions.getNumberOfProcessorsPerNode()*fittingOptions.getNumberOfJobsPerProcessor()) + "\n");
      
            
      log.write("[info] file written: " + fileName + "\n");
      
      //--------------- DONE WRITING!
      
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }finally{ //this is always executed, even if a return is called
      bw.flush();
      bw.close();
    }
  }  

  
  private void generateFileLevMarPBSPostprocessSH(BufferedWriter log) throws Exception{
    
    BufferedWriter bw = null; 
    String fileName = "Lsqnonlin_LevMar/pbspostprocess.sh";
    
    try{
          //Output log file
      File outputFile = new File(fileName);        
      if (!outputFile.exists()) {
        outputFile.createNewFile();
      }
      FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
      bw = new BufferedWriter(fw);
      
      //--------------- WRITE FILE!
      
      bw.write("#!/bin/bash\n\n"
            + "#PBS -l nodes=1:ppn=1\n"
            + "#PBS -l walltime=2:00:00\n"
            + "#PBS -N sbcfitting\n"
            + "#PBS -m bea\n"
            + (fittingOptions.getEmail()!=null ? "#PBS -M " + fittingOptions.getEmail() + "\n\n" : "")
            + (fittingOptions.getMinusA()!=null ? "#PBS -A " + fittingOptions.getMinusA() + "\n\n" : "")
            + "cd " + fittingOptions.getClusterDirectory() + "\n\n"
            + "./loadmodulesUnixCluster.sh\n"
            + "cd Lsqnonlin_LevMar\n"
            + "chmod 755 postprocess.sh\n"
            + "./postprocess.sh " + (fittingOptions.getNumberOfNodes()*fittingOptions.getNumberOfProcessorsPerNode()*fittingOptions.getNumberOfJobsPerProcessor()) + "\n");
      
            
      log.write("[info] file written: " + fileName + "\n");
      
      //--------------- DONE WRITING!
      
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }finally{ //this is always executed, even if a return is called
      bw.flush();
      bw.close();
    }
  }  

  
  private void generateFilePBSRunallSH(BufferedWriter log) throws Exception{
    
    BufferedWriter bw = null; 
    String fileName = "KimuraGLSDC/glsdc/v1/pbsrunall.sh";
    
    try{
          //Output log file
      File outputFile = new File(fileName);        
      if (!outputFile.exists()) {
        outputFile.createNewFile();
      }
      FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
      bw = new BufferedWriter(fw);
      
      //--------------- WRITE FILE!
      
      bw.write("#!/bin/bash\n\n"
            + "#PBS -l nodes=1:ppn=" + fittingOptions.getNumberOfProcessorsPerNode() + "\n"
            + "#PBS -l walltime=" + fittingOptions.getWallTime() + ":00:00\n"
            + "#PBS -N sbcfitting\n"
            + "#PBS -m bea\n"
            + (fittingOptions.getEmail()!=null ? "#PBS -M " + fittingOptions.getEmail() + "\n\n" : "")
            + (fittingOptions.getMinusA()!=null ? "#PBS -A " + fittingOptions.getMinusA() + "\n\n" : "")
            + "cd " + fittingOptions.getClusterDirectory() + "\n\n"
            + "./loadmodulesUnixCluster.sh\n"
            + "cd KimuraGLSDC/glsdc/v1\n"
            + "#PRS is the number of processors to use\n"
            + "PRS=" + fittingOptions.getNumberOfProcessorsPerNode() + "\n\n"
            + "#Z is the number of seeds to be used by each processors\n"
            + "#so Z seeds are tested in sequence\n"
            + "Z=" + fittingOptions.getNumberOfJobsPerProcessor() + "\n\n"
            + "#INIT is the first random seed to use\n"
            + "INIT=$(($PRS*$Z*($NODE-1)+1))\n\n"
            + "#total number of seeds tested is PRS*Z starting from INIT\n\n");
      
      bw.write("#Y is the counter for single processes.\n"
            + "#Run \"qsub -v NODE=1 pbsrunall.sh\"\n"
            + "Y=$(($PRS*($NODE-1)+1))\n\n"
            + "for i in `seq $INIT $Z $(($Z*$PRS+$INIT-1))`;\n"
            + "do\n"
            + "  ./run.sh $i $Y $Z &\n"
            + "  let Y=Y+1\n"
            + "done\n\n");
      
      bw.write("wait\n");
      
            
      log.write("[info] file written: " + fileName + "\n");
      
      //--------------- DONE WRITING!
      
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }finally{ //this is always executed, even if a return is called
      bw.flush();
      bw.close();
    }
  }  

  
  private void generateFileLevMarPBSRunallSH(BufferedWriter log) throws Exception{
    
    BufferedWriter bw = null; 
    String fileName = "Lsqnonlin_LevMar/pbsrunall.sh";
    
    try{
          //Output log file
      File outputFile = new File(fileName);        
      if (!outputFile.exists()) {
        outputFile.createNewFile();
      }
      FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
      bw = new BufferedWriter(fw);
      
      //--------------- WRITE FILE!
      
      bw.write("#!/bin/bash\n\n"
            + "#PBS -l nodes=1:ppn=" + fittingOptions.getNumberOfProcessorsPerNode() + "\n"
            + "#PBS -l walltime=" + fittingOptions.getWallTime() + ":00:00\n"
            + "#PBS -N sbcfitting\n"
            + "#PBS -m bea\n"
            + (fittingOptions.getEmail()!=null ? "#PBS -M " + fittingOptions.getEmail() + "\n\n" : "")
            + (fittingOptions.getMinusA()!=null ? "#PBS -A " + fittingOptions.getMinusA() + "\n\n" : "")
            + "cd " + fittingOptions.getClusterDirectory() + "\n\n"
            + "./loadmodulesUnixCluster.sh\n"
            + "cd Lsqnonlin_LevMar\n"
            + "#PRS is the number of processors to use\n"
            + "PRS=" + fittingOptions.getNumberOfProcessorsPerNode() + "\n\n"
            + "#Z is the number of seeds to be used by each processors\n"
            + "#so Z seeds are tested in sequence\n"
            + "Z=" + fittingOptions.getNumberOfJobsPerProcessor() + "\n\n"
            + "#INIT is the first random seed to use\n"
            + "INIT=$(($PRS*$Z*($NODE-1)+1))\n\n"
            + "#total number of seeds tested is PRS*Z starting from INIT\n\n");
      
      bw.write("#Y is the counter for single processes.\n"
            + "#Run \"qsub -v NODE=1 pbsrunall.sh\"\n"
            + "Y=$(($PRS*($NODE-1)+1))\n\n"
            + "for i in `seq $INIT $Z $(($Z*$PRS+$INIT-1))`;\n"
            + "do\n"
            + "  ./run.sh $i $Y $Z $JACOBIANMETHOD &\n"
            + "  let Y=Y+1\n"
            + "done\n\n");
      
      bw.write("wait\n");
      
            
      log.write("[info] file written: " + fileName + "\n");
      
      //--------------- DONE WRITING!
      
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }finally{ //this is always executed, even if a return is called
      bw.flush();
      bw.close();
    }
  }  

  
  private void generateFilePBSRunallnodesSH(BufferedWriter log) throws Exception{
    
    BufferedWriter bw = null; 
    String fileName = "KimuraGLSDC/glsdc/v1/pbsrunallnodes.sh";
    
    try{
          //Output log file
      File outputFile = new File(fileName);        
      if (!outputFile.exists()) {
        outputFile.createNewFile();
      }
      FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
      bw = new BufferedWriter(fw);
      
      //--------------- WRITE FILE!
      String afterOK = "";
      
      bw.write("#!/bin/bash\n\n"
          + "#this script sends serial programs to multiple nodes with multiple processors.\n\n"
          + "cd work/sbcmodel_all/1\n"
          + "rm -f 000_bestValues.txt\n"
          + "rm -f *.log\n"
          + "cd ../../..\n"
          + "make\n"
          + "chmod 755 bin/a.out\n"
          + "mv bin/a.out work/sbcmodel_all/sbcmodel\n\n"
          + "chmod 755 pbspostprocess.sh\n"
          + "chmod 755 pbsrunall.sh\n\n");
      for (int i=1; i<= fittingOptions.getNumberOfNodes(); i++){
        bw.write("JOBN" + i + "=`qsub -v NODE=" + i + " pbsrunall.sh`\n");
        bw.write("echo $JOBN" + i + "\n");
        afterOK = afterOK + ":$JOBN" + i;
      }
      bw.write("\n");
         
      bw.write("qsub -W depend=afterok" + afterOK + " pbspostprocess.sh\n");
      log.write("[info] file written: " + fileName + "\n");
      
      //--------------- DONE WRITING!
      
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }finally{ //this is always executed, even if a return is called
      bw.flush();
      bw.close();
    }
  }   
  
  
  private void generateFileLevMarPBSRunallnodesSH(BufferedWriter log) throws Exception{
    
    BufferedWriter bw = null; 
    String fileName = "Lsqnonlin_LevMar/pbsrunallnodes.sh";
    
    try{
          //Output log file
      File outputFile = new File(fileName);        
      if (!outputFile.exists()) {
        outputFile.createNewFile();
      }
      FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
      bw = new BufferedWriter(fw);
      
      //--------------- WRITE FILE!
      String afterOK = "";
      
      bw.write("#!/bin/bash\n\n"
          + "JACOBIANMETHOD=$1\n\n"
          + "if [ \"$JACOBIANMETHOD\" = \'\' ]; then\n"
          + "  JACOBIANMETHOD=\"SE\"\n"
          + "fi\n\n"
          + "#this script sends serial programs to multiple nodes with multiple processors.\n\n"
          + "chmod 755 clean.sh\n"
          + "./clean.sh\n"
          + "cd ..\n"
          + "./buildLibraries.sh\n"
					+ "if [ $? != 0 ]; then\n"
					+ "  echo \"Failed to compile all libraries, quitting the pipeline\";\n"
					+ "  exit 1\n"
					+ "fi\n"
          + "cd Lsqnonlin_LevMar\n"
          + "make\n"
          + "chmod 755 levmarOptimiser_se\n"
          + "chmod 755 levmarOptimiser_fd\n\n"
          + "chmod 755 pbspostprocess.sh\n"
          + "chmod 755 pbsrunall.sh\n\n");
      for (int i=1; i<= fittingOptions.getNumberOfNodes(); i++){
        bw.write("JOBN" + i + "=`qsub -v NODE=" + i + ",JACOBIANMETHOD=$JACOBIANMETHOD pbsrunall.sh`\n");
        bw.write("echo $JOBN" + i + "\n");
        afterOK = afterOK + ":$JOBN" + i;
      }
      bw.write("\n");
         
      bw.write("qsub -W depend=afterok" + afterOK + " pbspostprocess.sh\n");
      log.write("[info] file written: " + fileName + "\n");
      
      //--------------- DONE WRITING!
      
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }finally{ //this is always executed, even if a return is called
      bw.flush();
      bw.close();
    }
  }   

    
  private void generateFile_exp_sbcmodel_org_1dat_top(BufferedWriter log) throws Exception{
    
    BufferedWriter bw = null; 
    String fileName = "KimuraGLSDC/glsdc/v1/work/sbcmodel_all/1/exp_sbcmodel_org_1.dat_top";
    
    try{
          //Output log file
      File outputFile = new File(fileName);        
      if (!outputFile.exists()) {
        outputFile.createNewFile();
      }
      FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
      bw = new BufferedWriter(fw);
      
      //--------------- WRITE FILE!
      
      // check for loglikelihood sigma params, in case loglikelihood was requested
      int llparams = 0;
      int typeOfLL = fittingOptions.getLogLikelihood();
      if (typeOfLL == LogLikelihoodTool.IndependentNoise || typeOfLL == LogLikelihoodTool.LinearModelOfNoise){
        llparams = logLikelihoodTool.getListOfAdditionalParams().size();
      }
      int dimension = numberOfParametersToFit() + llparams;
      
      bw.write("Dimension= " + dimension + "\n"
              + "MaxGeneration= 50\n"
              + "MaxNumOfEval= -1\n"
              + "PopSizeRatio= 3\n"
              + "NumOfParents= ADAPT\n"
              + "NumOfChildren= 10\n"
              + "Step_normal= 1\n"
              + "Step_backup= 10\n"
              + "Step_debug= 10\n");
            
      log.write("[info] file written: " + fileName + "\n");
      
      //--------------- DONE WRITING!
      
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }finally{ //this is always executed, even if a return is called
      bw.flush();
      bw.close();
    }
  }   


    
  private void generateFile_pset_sbcmodel_orgdat(BufferedWriter log) throws Exception{
    
    BufferedWriter bw = null; 
    String fileName = "KimuraGLSDC/glsdc/v1/work/sbcmodel_all/pset_sbcmodel_org.dat";
    
    try{
          //Output log file
      File outputFile = new File(fileName);        
      if (!outputFile.exists()) {
        outputFile.createNewFile();
      }
      FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
      bw = new BufferedWriter(fw);
      
      //--------------- WRITE FILE!
      
      int n =0;
      
      Iterator<GenericEntry> ite = entryLists.getParamList().iterator();
      while (ite.hasNext()){
        GenericEntry tmp = ite.next();
        if (! (tmp instanceof ParameterNode)) throw new Exception("[error] coding error! I should have ParameterNode objects in GLSDCTool.generateFile_pset_sbcmodel_orgdat()");
        if (tmp instanceof ParameterRange){
          ParameterRange pr = (ParameterRange) tmp; //downcast
          
          bw.write(n + "\t" + Math.log(pr.getBottom().doubleValue()) + "\t" + Math.log(pr.getTop().doubleValue()) + "\t" + Math.log(pr.getBottom().doubleValue()) + "\t" + Math.log(pr.getTop().doubleValue()) + "\n");
          
          
          n++;
        }
      }
      
      //if LogLikelihood is requested, I need to fit some additional parameters as well
      int typeOfLL = fittingOptions.getLogLikelihood();
      if (typeOfLL == LogLikelihoodTool.IndependentNoise || typeOfLL == LogLikelihoodTool.LinearModelOfNoise){
        Iterator<ParameterRangeOfAdditionalParam> ite_sigma = logLikelihoodTool.getListOfAdditionalParams().iterator();
        while (ite_sigma.hasNext()){
          ParameterRangeOfAdditionalParam pr = ite_sigma.next();
          bw.write(n + "\t" + Math.log(pr.getBottom().doubleValue()) + "\t" + Math.log(pr.getTop().doubleValue()) + "\t" + Math.log(pr.getBottom().doubleValue()) + "\t" + Math.log(pr.getTop().doubleValue()) + "\n");
          n++;
        }
      }
      
      log.write("[info] file written: " + fileName + "\n");
      
      //--------------- DONE WRITING!
      
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }finally{ //this is always executed, even if a return is called
      bw.flush();
      bw.close();
    }
  }  
  
  /**
   * Generate C++ file with the model, the data and the objective function
   * to be minimised
   * */

  private void generateFile_fitness_sbcmodel_orgC(BufferedWriter log) throws Exception{
    
    BufferedWriter bw = null; 
    String fileName = "KimuraGLSDC/glsdc/v1/src/fitness_sbcmodel_org.C";
    //fileName = "fitness_sbcmodel_org.C"; //temporary for debug
    
    //check function definitions
    checkFunctionDefinitions();
    
    try{
          //Output log file
      File outputFile = new File(fileName);        
      if (!outputFile.exists()) {
        outputFile.createNewFile();
      }
      FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
      bw = new BufferedWriter(fw);
      
      //--------------- WRITE FILE!
      
      Set<Integer> conditionsToSimulate = getConditionsToSimulate_FittingFile();
      Set<Integer> allConditions = QueryModelAndData.getAllConditions(entryLists,lookUpTable);
      
      bw.write("///////////////////////////////////////////////////////\n"
               + "// File " + fileName + " generated automatically by the Systems Biology Compiler (SBC)\n"
               + "// SBC from Andrea Degasperi, Systems Biology Ireland, 2014\n"
               + "// This file is to be used with GLSDC optimisation algorithm from Shuhei Kimura, 2003\n"
               + "///////////////////////////////////////////////////////\n\n");
      
      bw.write("#include <stdio.h>\n"
              + "#include <sstream>\n"
              + "#include <math.h>\n"
              + "#include <time.h>\n"
              + "#include <string.h>\n"
              + "#include <cvodes/cvodes.h>\n"
              + "#include <cvodes/cvodes_dense.h>\n"
              + "#include <nvector/nvector_serial.h>\n"
              + "#include <sundials/sundials_math.h>\n"
              + "#include \"main.h\"\n"
              + "#include <iostream>\n"
              + "#include <gsl/gsl_errno.h>\n"
              + "#include <gsl/gsl_spline.h>\n\n");

      bw.write("#ifdef __E4model_origianl__\n"
              + "#define RTOL  " + simulateOptions.getReltol() + "\n"
              + "#define ATOL  " + simulateOptions.getAbstol() + "\n"
              + "#define Y(i) NV_Ith_S(y, (i))\n"
              + "#define dY(i) NV_Ith_S(ydot, (i))\n"
              + "#define INITIALTIME " + simulateOptions.getInitialTime() + "\n"
              + "#define FINALTIME " + simulateOptions.getFinalTime() + "\n"
              + "#define TIMESTEPS " + simulateOptions.getTimePoints() + "\n\n");
      
      int typeOfLL = fittingOptions.getLogLikelihood();
      //~ if (typeOfLL == LogLikelihoodTool.IndependentNoise || typeOfLL == LogLikelihoodTool.LinearModelOfNoise){           
        //~ bw.write("#define LOGLIKELIHOOD_C " + logLikelihoodTool.getLogLikelihood_paramC() + "\n\n");
      //~ }
      
      bw.write("extern int randomSeed;\n"
              + "static int global_neval = 0;\n"
              + "static double global_best = 1.0e10;\n"
              + "static double global_best2 = 1.0e10;\n\n");

      bw.write("static realtype *y_zero;\n"
              + "static double *X;\n\n");
      
      //--------- Enumerate conditions to fit
              
      bw.write("enum {\n");
      
      Iterator<Integer> ite = allConditions.iterator();
      while (ite.hasNext()){
        Integer tmp = ite.next();
        bw.write("  c_" + tmp.intValue() + (ite.hasNext()? "," : "") + " // " + lookUpTable.lookUpString(tmp) + "\n");
      }

      bw.write("};\n\n");

      //--------- data matrices
      printDataMatrices(bw);
      
      //--------- Additional data structures, for logLikelihood params
      if (typeOfLL == LogLikelihoodTool.IndependentNoise || typeOfLL == LogLikelihoodTool.LinearModelOfNoise){
        Iterator<String> ite_strings = logLikelihoodTool.getC_initAdditionalParams().iterator();
        while (ite_strings.hasNext()){
          String tmp = ite_strings.next();
          bw.write(tmp + "\n");
        }
        bw.write("\n");
      }
      
      //--------- Enumerate all parameters
      enumerateAllParameters(bw);
      
      //--------- Enumerate differential variables 
      enumerateDifferentialVariables(bw);
      
      //--------- Print event triggers boolean variables
      printEventTriggerBooleanVariables(bw);
      
      //--------- signatures of algebraic equations functions
      Map<Integer,Set<Integer>> signatures = AlgebraicEquationsFunctionSignatures.retrieveSignatures(entryLists,lookUpTable);
      printSignatures(true,signatures,bw);
      
      //--------- signatures of function definitions
      printCFunctionSignatures(true,bw);
      
      //--------- print F
      printF(GLSDC,conditionsToSimulate,signatures,bw);
      printJac(GLSDC,conditionsToSimulate,signatures,bw);
      
      //--------- some functions...
      
      //bw.write("static CVDenseJacFn dFdy = NULL;\n\n");
      
      bw.write("static N_Vector make_atol(int neq, realtype tol)\n"
        + "{\n"
        + "  N_Vector atol = N_VNew_Serial(neq);\n"
        + "  int i;\n"
        + "  for (i = 0; i < neq; ++i) NV_Ith_S(atol, i) = tol;\n"
        + "  return atol;\n"
        + "}\n\n");
      
      bw.write("static N_Vector make_vector(int neq, realtype val[])\n"
        + "{\n"
        + "  N_Vector vec = N_VNew_Serial(neq);\n"
        + "  int i;\n"
        + "  for (i = 0; i < neq; ++i) NV_Ith_S(vec, i) = val[i];\n"
        + "  return vec;\n"
        + "}\n\n");

      bw.write("static int isproper(int neq, N_Vector y)\n"
        + "{\n"
        + "  for (int i = 0; i < neq; ++i)\n"
        + "    if (NV_Ith_S(y, i) < -ATOL)\n"
        + "      return 0;\n"
        + "  return 1;\n"
        + "}\n\n");
      
      //---------
      
      
      //--------- my_fitness function HERE
      
      printMYFitnessFunction(GLSDC,conditionsToSimulate,signatures,bw);
      
      //---------
      
      bw.write("#undef Y\n");
      bw.write("#undef dY\n\n");
      bw.write("extern PROBLEMSETTING *setting_problems;\n\n");
      
      //--------- fitness function HERE
      
      printFitnessFunction(bw);   
           
      printAlgebraicEquationsFunctions(true,conditionsToSimulate,signatures,bw);
      printCFunctionDefinitions(true,bw);
            

      bw.write("#endif\n\n"); //last line!
            
      log.write("[info] file written: " + fileName + "\n");
      
      //--------------- DONE WRITING!
      
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }finally{ //this is always executed, even if a return is called
      bw.flush();
      bw.close();
    }
  }  

  /**
   * Generate C++ file with the model, the data and the objective function
   * to be minimised
   * */

  private void generateFile_levmarOptimiserC_FD(BufferedWriter log) throws Exception{
    
    BufferedWriter bw = null; 
    String fileName = "Lsqnonlin_LevMar/levmarOptimiser_fd.c";
    
    //check function definitions
    checkFunctionDefinitions();
    
    try{
          //Output log file
      File outputFile = new File(fileName);        
      if (!outputFile.exists()) {
        outputFile.createNewFile();
      }
      FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
      bw = new BufferedWriter(fw);
      
      //--------------- WRITE FILE!
      
      Set<Integer> conditionsToSimulate = getConditionsToSimulate_FittingFile();
      Set<Integer> allConditions = QueryModelAndData.getAllConditions(entryLists,lookUpTable);
      
      bw.write("///////////////////////////////////////////////////////\n"
               + "// File " + fileName + " generated automatically by the Systems Biology Compiler (SBC)\n"
               + "// SBC from Andrea Degasperi, Systems Biology Ireland, 2016\n"
               + "// This file is to be used with LevMar optimisation algorithm\n"
               + "///////////////////////////////////////////////////////\n\n");
      
      bw.write("#include <stdio.h>\n"
              + "#include <sstream>\n"
              + "#include <gsl/gsl_rng.h>\n"
              + "#include <gsl/gsl_randist.h>\n"
              + "#include <math.h>\n"
              + "#include <time.h>\n"
              + "#include <string.h>\n"
              + "#include <cvodes/cvodes.h>\n"
              + "#include <cvodes/cvodes_dense.h>\n"
              + "#include <nvector/nvector_serial.h>\n"
              + "#include <sundials/sundials_math.h>\n"
              + "#include \"levmar.h\"\n"
              + "#include <iostream>\n"
              + "#include <gsl/gsl_errno.h>\n"
              + "#include <gsl/gsl_spline.h>\n\n");

      bw.write("#define RTOL  " + simulateOptions.getReltol() + "\n"
              + "#define ATOL  " + simulateOptions.getAbstol() + "\n"
              + "#define Y(i) NV_Ith_S(y, (i))\n"
              + "#define dY(i) NV_Ith_S(ydot, (i))\n"
              + "#define INITIALTIME " + simulateOptions.getInitialTime() + "\n"
              + "#define FINALTIME " + simulateOptions.getFinalTime() + "\n"
              + "#define TIMESTEPS " + simulateOptions.getTimePoints() + "\n"
              + "#define MAXITERATIONS " + fittingOptions.getLsqnonlin_maxiter() + "\n"
              + "#define MAXRESTARTS " + fittingOptions.getLsqnonlin_numOfRestarts() + "\n\n");
      
      int typeOfLL = fittingOptions.getLogLikelihood();        
      if (typeOfLL == LogLikelihoodTool.IndependentNoise || typeOfLL == LogLikelihoodTool.LinearModelOfNoise){           
        bw.write("#define LOGLIKELIHOOD_C " + logLikelihoodTool.getLogLikelihood_paramC() + "\n\n");
      }

      bw.write("static int randomSeed;\n"
              + "static int global_neval = 0;\n"
              + "static double global_best = 1.0e10;\n"
              + "static double global_best2 = 1.0e10;\n\n");

      bw.write("static realtype *y_zero;\n"
              + "static double *X;\n\n");
      
      //--------- Enumerate conditions to fit
              
      bw.write("enum {\n");
      
      Iterator<Integer> ite = allConditions.iterator();
      while (ite.hasNext()){
        Integer tmp = ite.next();
        bw.write("  c_" + tmp.intValue() + (ite.hasNext()? "," : "") + " // " + lookUpTable.lookUpString(tmp) + "\n");
      }

      bw.write("};\n\n");

      //--------- data matrices
      printDataMatrices(bw);
      
      //--------- Additional data structures, for logLikelihood params
      if (typeOfLL == LogLikelihoodTool.IndependentNoise || typeOfLL == LogLikelihoodTool.LinearModelOfNoise){
        Iterator<String> ite_strings = logLikelihoodTool.getC_initAdditionalParams().iterator();
        while (ite_strings.hasNext()){
          String tmp = ite_strings.next();
          bw.write(tmp + "\n");
        }
        bw.write("\n");
      }
      
      //--------- Enumerate all parameters
      enumerateAllParameters(bw);
      
      //--------- Enumerate differential variables 
      enumerateDifferentialVariables(bw);
      
      //--------- Print event triggers boolean variables
      printEventTriggerBooleanVariables(bw);
      
      //--------- signatures of algebraic equations functions
      Map<Integer,Set<Integer>> signatures = AlgebraicEquationsFunctionSignatures.retrieveSignatures(entryLists,lookUpTable);
      printSignatures(true,signatures,bw);
      
      //--------- signatures of function definitions
      printCFunctionSignatures(true,bw);
      
      //--------- print F
      printF(LEVMAR_FD,conditionsToSimulate,signatures,bw);
      printJac(LEVMAR_FD,conditionsToSimulate,signatures,bw);
      
      //--------- some functions...
      
      bw.write("static N_Vector make_atol(int neq, realtype tol)\n"
        + "{\n"
        + "  N_Vector atol = N_VNew_Serial(neq);\n"
        + "  int i;\n"
        + "  for (i = 0; i < neq; ++i) NV_Ith_S(atol, i) = tol;\n"
        + "  return atol;\n"
        + "}\n\n");
      
      bw.write("static N_Vector make_vector(int neq, realtype val[])\n"
        + "{\n"
        + "  N_Vector vec = N_VNew_Serial(neq);\n"
        + "  int i;\n"
        + "  for (i = 0; i < neq; ++i) NV_Ith_S(vec, i) = val[i];\n"
        + "  return vec;\n"
        + "}\n\n");

      bw.write("static int isproper(int neq, N_Vector y)\n"
        + "{\n"
        + "  for (int i = 0; i < neq; ++i)\n"
        + "    if (NV_Ith_S(y, i) < -ATOL)\n"
        + "      return 0;\n"
        + "  return 1;\n"
        + "}\n\n");
      
      //---------
      
      
      //--------- my_fitness function HERE
      
      printMYFitnessFunction(LEVMAR_FD,conditionsToSimulate,signatures,bw);
      
      //---------
      
      bw.write("#undef Y\n");
      bw.write("#undef dY\n\n");
      
      //--------- fitness function HERE
      
      printFitnessFunctionLevMar(LEVMAR_FD,bw);   
      printMainLevMar(LEVMAR_FD,bw); 
           
      printAlgebraicEquationsFunctions(true,conditionsToSimulate,signatures,bw);
      printCFunctionDefinitions(true,bw);
            
      bw.write("\n"); //last line!
            
      log.write("[info] file written: " + fileName + "\n");
      
      //--------------- DONE WRITING!
      
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }finally{ //this is always executed, even if a return is called
      bw.flush();
      bw.close();
    }
  }  


  /**
   * Generate C++ file with the model, the data and the objective function
   * to be minimised
   * */

  private void generateFile_levmarOptimiserC_SE(BufferedWriter log) throws Exception{
    
    BufferedWriter bw = null; 
    String fileName = "Lsqnonlin_LevMar/levmarOptimiser_se.c";
    
    //check function definitions
    checkFunctionDefinitions();
    
    try{
          //Output log file
      File outputFile = new File(fileName);        
      if (!outputFile.exists()) {
        outputFile.createNewFile();
      }
      FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
      bw = new BufferedWriter(fw);
      
      //--------------- WRITE FILE!
      
      Set<Integer> conditionsToSimulate = getConditionsToSimulate_FittingFile();
      Set<Integer> allConditions = QueryModelAndData.getAllConditions(entryLists,lookUpTable);
      
      bw.write("///////////////////////////////////////////////////////\n"
               + "// File " + fileName + " generated automatically by the Systems Biology Compiler (SBC)\n"
               + "// SBC from Andrea Degasperi, Systems Biology Ireland, 2016\n"
               + "// This file is to be used with LevMar optimisation algorithm\n"
               + "///////////////////////////////////////////////////////\n\n");
      
      bw.write("#include <stdio.h>\n"
              + "#include <sstream>\n"
              + "#include <gsl/gsl_rng.h>\n"
              + "#include <gsl/gsl_randist.h>\n"
              + "#include <math.h>\n"
              + "#include <time.h>\n"
              + "#include <string.h>\n"
              + "#include <cvodes/cvodes.h>\n"
              + "#include <cvodes/cvodes_dense.h>\n"
              + "#include <nvector/nvector_serial.h>\n"
              + "#include <sundials/sundials_math.h>\n"
              + "#include \"levmar.h\"\n"
              + "#include <iostream>\n"
              + "#include <gsl/gsl_errno.h>\n"
              + "#include <gsl/gsl_spline.h>\n\n");

      bw.write("#define RTOL  " + simulateOptions.getReltol() + "\n"
              + "#define ATOL  " + simulateOptions.getAbstol() + "\n"
              + "#define Y(i) NV_Ith_S(y, (i))\n"
              + "#define dY(i) NV_Ith_S(ydot, (i))\n"
              + "#define INITIALTIME " + simulateOptions.getInitialTime() + "\n"
              + "#define FINALTIME " + simulateOptions.getFinalTime() + "\n"
              + "#define TIMESTEPS " + simulateOptions.getTimePoints() + "\n"
              + "#define MAXITERATIONS " + fittingOptions.getLsqnonlin_maxiter() + "\n"
              + "#define MAXRESTARTS " + fittingOptions.getLsqnonlin_numOfRestarts() + "\n\n");
      
      int typeOfLL = fittingOptions.getLogLikelihood();        
      if (typeOfLL == LogLikelihoodTool.IndependentNoise || typeOfLL == LogLikelihoodTool.LinearModelOfNoise){           
        bw.write("#define LOGLIKELIHOOD_C " + logLikelihoodTool.getLogLikelihood_paramC() + "\n\n");
      }

      bw.write("static int randomSeed;\n"
              + "static int global_neval = 0;\n"
              + "static double global_best = 1.0e10;\n"
              + "static double global_best2 = 1.0e10;\n\n");

      bw.write("static realtype *y_zero;\n"
              + "static double *X;\n\n");
              
      bw.write("typedef struct {\n"
      + "  realtype *p;\n"
      + "  int condition;\n"
      + "} *UserData;\n\n");
      
      //--------- Enumerate conditions to fit
              
      bw.write("enum {\n");
      
      Iterator<Integer> ite = allConditions.iterator();
      while (ite.hasNext()){
        Integer tmp = ite.next();
        bw.write("  c_" + tmp.intValue() + (ite.hasNext()? "," : "") + " // " + lookUpTable.lookUpString(tmp) + "\n");
      }

      bw.write("};\n\n");

      //--------- data matrices
      printDataMatrices(bw);
      
      //--------- Additional data structures, for logLikelihood params
      if (typeOfLL == LogLikelihoodTool.IndependentNoise || typeOfLL == LogLikelihoodTool.LinearModelOfNoise){
        Iterator<String> ite_strings = logLikelihoodTool.getC_initAdditionalParams().iterator();
        while (ite_strings.hasNext()){
          String tmp = ite_strings.next();
          bw.write(tmp + "\n");
        }
        bw.write("\n");
      }
      
      //--------- Enumerate all parameters
      enumerateAllParameters(bw);
      
      //--------- Enumerate differential variables 
      enumerateDifferentialVariables(bw);
      
      //--------- Print event triggers boolean variables
      printEventTriggerBooleanVariables(bw);
      
      //--------- signatures of algebraic equations functions
      Map<Integer,Set<Integer>> signatures = AlgebraicEquationsFunctionSignatures.retrieveSignatures(entryLists,lookUpTable);
      printSignatures(true,signatures,bw);
      
      //--------- signatures of function definitions
      printCFunctionSignatures(true,bw);
      
      //--------- print F
      printF(LEVMAR_SE,conditionsToSimulate,signatures,bw);
      printJac(LEVMAR_SE,conditionsToSimulate,signatures,bw);
      printSensRHS(LEVMAR_SE,conditionsToSimulate,signatures,bw);
      
      //--------- some functions...
      
      bw.write("static N_Vector make_atol(int neq, realtype tol)\n"
        + "{\n"
        + "  N_Vector atol = N_VNew_Serial(neq);\n"
        + "  int i;\n"
        + "  for (i = 0; i < neq; ++i) NV_Ith_S(atol, i) = tol;\n"
        + "  return atol;\n"
        + "}\n\n");
      
      bw.write("static N_Vector make_vector(int neq, realtype val[])\n"
        + "{\n"
        + "  N_Vector vec = N_VNew_Serial(neq);\n"
        + "  int i;\n"
        + "  for (i = 0; i < neq; ++i) NV_Ith_S(vec, i) = val[i];\n"
        + "  return vec;\n"
        + "}\n\n");

      bw.write("static int isproper(int neq, N_Vector y)\n"
        + "{\n"
        + "  for (int i = 0; i < neq; ++i)\n"
        + "    if (NV_Ith_S(y, i) < -ATOL)\n"
        + "      return 0;\n"
        + "  return 1;\n"
        + "}\n\n");
      
      //---------
      
      
      //--------- my_fitness function HERE
      
      printMYFitnessFunction(LEVMAR_SE,conditionsToSimulate,signatures,bw);
      printMYFitnessFunction(LEVMAR_SE_JAC,conditionsToSimulate,signatures,bw);
      
      //---------
      
      bw.write("#undef Y\n");
      bw.write("#undef dY\n\n");
      
      //--------- fitness function HERE
      
      printFitnessFunctionLevMar(LEVMAR_SE,bw); 
      printFitnessFunctionLevMar(LEVMAR_SE_JAC,bw);   
      printMainLevMar(LEVMAR_SE,bw); 
           
      printAlgebraicEquationsFunctions(true,conditionsToSimulate,signatures,bw);
      printCFunctionDefinitions(true,bw);
            
      bw.write("\n"); //last line!
            
      log.write("[info] file written: " + fileName + "\n");
      
      //--------------- DONE WRITING!
      
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }finally{ //this is always executed, even if a return is called
      bw.flush();
      bw.close();
    }
  }  

  
  private void generateDirectories_ODEsimulator(BufferedWriter log) throws Exception{
    
    String fileName = "KimuraODEsim/data";
    try{
      
      File theDir1 = new File(fileName);

      // if the directory does not exist, create it
      if (!theDir1.exists()) {
        //System.out.println("creating directory: " + directoryName);
        boolean result = theDir1.mkdir();  

         if(result) {    
           log.write("[info] created empty directory " + fileName + "\n"); 
         }
      }
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }
    
    fileName = "KimuraODEsim/plots";
    try{
      
      File theDir1 = new File(fileName);

      // if the directory does not exist, create it
      if (!theDir1.exists()) {
        //System.out.println("creating directory: " + directoryName);
        boolean result = theDir1.mkdir();  

         if(result) {    
           log.write("[info] created empty directory " + fileName + "\n"); 
         }
      }
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }    
    
    fileName = "KimuraODEsim/sims";
    try{
      
      File theDir1 = new File(fileName);

      // if the directory does not exist, create it
      if (!theDir1.exists()) {
        //System.out.println("creating directory: " + directoryName);
        boolean result = theDir1.mkdir();  

         if(result) {    
           log.write("[info] created empty directory " + fileName + "\n"); 
         }
      }
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }

  }

  private void generateFile_ODEsimulatorC(BufferedWriter log) throws Exception{
    
    BufferedWriter bw = null; 
    String fileName = "KimuraODEsim/ODEsimulator.c";
    //fileName = "fitness_sbcmodel_org.C"; //temporary for debug
    
    //check function definitions
    checkFunctionDefinitions();
    
    try{
          //Output log file
      File outputFile = new File(fileName);        
      if (!outputFile.exists()) {
        outputFile.createNewFile();
      }
      FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
      bw = new BufferedWriter(fw);
      
      //--------------- WRITE FILE!
      
      Set<Integer> conditionsToSimulate = getConditionsToSimulate_SimulationsFile();
      Set<Integer> allConditions = QueryModelAndData.getAllConditions(entryLists,lookUpTable);
      
      bw.write("///////////////////////////////////////////////////////\n"
               + "// File " + fileName + " generated automatically by the Systems Biology Compiler (SBC)\n"
               + "// SBC from Andrea Degasperi, Systems Biology Ireland, 2014\n"
               + "// This file is to be used to run simulations of optimal parameter sets obtained\n"
               + "// with GLSDC optimisation algorithm from Shuhei Kimura, 2003\n"
               + "///////////////////////////////////////////////////////\n\n");
      
      bw.write("#include <stdio.h>\n"
              + "#include <math.h>\n"
              + "#include <string.h>\n"
              + "#include <cvodes/cvodes.h>\n"
              + "#include <cvodes/cvodes_dense.h>\n"
              + "#include <nvector/nvector_serial.h>\n"
              + "#include <sundials/sundials_math.h>\n"
              + "#include <iostream>\n"
              + "#include <gsl/gsl_errno.h>\n"
              + "#include <gsl/gsl_spline.h>\n\n");

      bw.write("#define RTOL  " + simulateOptions.getReltol() + "\n"
              + "#define ATOL  " + simulateOptions.getAbstol() + "\n"
              + "#define Y(i) NV_Ith_S(y, (i))\n"
              + "#define dY(i) NV_Ith_S(ydot, (i))\n"
              + "#define INITIALTIME " + simulateOptions.getInitialTime() + "\n"
              + "#define FINALTIME " + simulateOptions.getFinalTime() + "\n"
              + "#define TIMESTEPS " + simulateOptions.getTimePoints() + "\n\n");
              
      bw.write("int numberOfFits = 10; //default value overwritten by program argument\n\n");

      bw.write("static realtype *y_zero;\n"
              + "static double *X;\n\n");
      
      //--------- Enumerate all conditions 
              
      bw.write("//enumerate all conditions\n");   
      bw.write("enum {\n");
      
      Iterator<Integer> ite = allConditions.iterator();
      while (ite.hasNext()){
        Integer tmp = ite.next();
        bw.write("  c_" + tmp.intValue() + (ite.hasNext()? "," : "") + " // " + lookUpTable.lookUpString(tmp) + "\n");
      }

      bw.write("};\n\n");

      //--------- Enumerate all conditions 
              
      bw.write("//enumerate conditions to simulate (to be used with simulationMatrix)\n");
      bw.write("enum {\n");
      
      ite = conditionsToSimulate.iterator();
      while (ite.hasNext()){
        Integer tmp = ite.next();
        bw.write("  c_sims_" + tmp.intValue() + (ite.hasNext()? "," : "") + " // " + lookUpTable.lookUpString(tmp) + "\n");
      }

      bw.write("};\n\n");

      //--------- data matrices
      printDataMatrices(bw);
      
      //--------- Enumerate all parameters
      enumerateAllParameters(bw);
      
      //--------- Enumerate differential variables 
      enumerateDifferentialVariables(bw);

      //--------- Enumerate species variables (to be used with simulation matrix)
      enumerateSpeciesVariables(bw);
      
      //--------- Print event triggers boolean variables
      printEventTriggerBooleanVariables(bw);
      
      //--------- signatures of algebraic equations functions
      Map<Integer,Set<Integer>> signatures = AlgebraicEquationsFunctionSignatures.retrieveSignatures(entryLists,lookUpTable);
      printSignatures(true,signatures,bw);
      
      //--------- signatures of function definitions
      printCFunctionSignatures(true,bw);
      
      //signatures for simulations normalisation and statistics
      printAdditionalODEsimulatorSignatures(bw);
      
      //--------- print F
      printF(GLSDC,conditionsToSimulate,signatures,bw);
      printJac(GLSDC,conditionsToSimulate,signatures,bw);
      
      //--------- some functions...
      
      //bw.write("static CVDenseJacFn dFdy = NULL;\n\n");
      
      bw.write("static N_Vector make_atol(int neq, realtype tol)\n"
        + "{\n"
        + "  N_Vector atol = N_VNew_Serial(neq);\n"
        + "  int i;\n"
        + "  for (i = 0; i < neq; ++i) NV_Ith_S(atol, i) = tol;\n"
        + "  return atol;\n"
        + "}\n\n");
      
      bw.write("static N_Vector make_vector(int neq, realtype val[])\n"
        + "{\n"
        + "  N_Vector vec = N_VNew_Serial(neq);\n"
        + "  int i;\n"
        + "  for (i = 0; i < neq; ++i) NV_Ith_S(vec, i) = val[i];\n"
        + "  return vec;\n"
        + "}\n\n");

      bw.write("static int isproper(int neq, N_Vector y)\n"
        + "{\n"
        + "  for (int i = 0; i < neq; ++i)\n"
        + "    if (NV_Ith_S(y, i) < -ATOL)\n"
        + "      return 0;\n"
        + "  return 1;\n"
        + "}\n\n");
      
      //---------
      
      
      //--------- my_fitness function HERE
      
      printMainODEsimulator(conditionsToSimulate,signatures,bw);
      
      //---------
      
      bw.write("#undef Y\n");
      bw.write("#undef dY\n\n");
      
      
      
                
           
      printAlgebraicEquationsFunctions(true,conditionsToSimulate,signatures,bw);
      printCFunctionDefinitions(true,bw);
      printAdditionalODEsimulatorFunctions(bw);     
            

      bw.write("\n"); //last line!
            
      log.write("[info] file written: " + fileName + "\n");
      
      //--------------- DONE WRITING!
      
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }finally{ //this is always executed, even if a return is called
      bw.flush();
      bw.close();
    }
  }  

  
  
  /**
   * this private function computes the number of parameters to fit. 
   * This is the number of ParameterRange objects in the paramList,
   * which in turn is in the entryList object
   * throws Exception if no ParameterRange object is found (i.e. there 
   * is no parameter to fit)
   * */
  private int numberOfParametersToFit() throws Exception{
    int n = 0;
    
    Iterator<GenericEntry> ite = entryLists.getParamList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof ParameterNode)) throw new Exception("[error] coding error! I should have ParameterNode objects in GLSDCTool.numberOfParametersToFit()");
      if (tmp instanceof ParameterRange) n++;
    }
    if (n==0) throw new Exception("[error] No parameters to fit! Please specify at least one parameter to fit using the range syntax: par ID = [num,num]");
    return n;
  }
  
  /**
   * return a set with all the conditions requested for fitting and for
   * normalisation. These together are the conditions I need to simulate
   * so that the fitting can take place. Consider conditions for normalisation
   * only if they are useful to normalise conditions requested for fitting
   * */
  
  private Set<Integer> getConditionsToSimulate_FittingFile() throws Exception{
    Set<Integer> set = new HashSet<Integer>();
    
    Iterator<Integer> ite = fittingOptions.getIDCondList().keySet().iterator();
    while (ite.hasNext()){
      Integer speciesID = ite.next(); //species to fit
      LinkedList<Integer> condsToFit = fittingOptions.getIDCondList().get(speciesID); //conditions requested for fitting (might be in multiple data blocks)
      
      //retrieve data blocks
      Set<DataNodeReplicates> dataNodesSet = new HashSet<DataNodeReplicates>();
      Iterator<Integer> ite2 = condsToFit.iterator();
      while (ite2.hasNext()){
        Integer cond = ite2.next();
        //add this condition to the return set!!!
        set.add(cond);
        //~ System.out.println("added to conditions to simulate: " + lookUpTable.lookUpString(cond));
        //retrieve data block (DataNodesReplicates) that contains the requested speciesID,cond pair
        DataNodeReplicates dnr1 = QueryModelAndData.getDataNodeReplicatesOf(entryLists,speciesID,cond);
        if (dnr1 == null) {
          throw new Exception("[error] No available data for fitting variable " + lookUpTable.lookUpString(speciesID) + ", condition " + lookUpTable.lookUpString(cond));  
        }
        dataNodesSet.add(dnr1);
      }
      
      Iterator<DataNodeReplicates> ite3 = dataNodesSet.iterator();
      while (ite3.hasNext()){
        DataNodeReplicates dnr = ite3.next();
        NormalisationNode nnode = getNormalisationNodeOf(dnr);
        
        if (nnode != null){  //only if there is a corresponding normalisation node
          if (nnode instanceof NormalisationNodeFixedPoint){
            NormalisationNodeFixedPoint nn = (NormalisationNodeFixedPoint) nnode; //downcast

            set.add(nn.getCondition());
            //~ System.out.println("added to conditions to simulate: " + lookUpTable.lookUpString(nn.getCondition()));
            
          }else if (nnode instanceof NormalisationNodeAverage){   
            NormalisationNodeAverage nn = (NormalisationNodeAverage) nnode; //downcast   
            
            set.addAll(nn.getConditions());
            //~ Iterator<Integer> ite4 = nn.getConditions().iterator();
            //~ while (ite4.hasNext()){
              //~ Integer tmp = ite4.next();
              //~ System.out.println("added to conditions to simulate: " + lookUpTable.lookUpString(tmp));
            //~ }
            
            
          }else {
            throw new Exception("[error] Probably a "
              + "coding error (GLSDCTool.getConditionsToSimulate_FittingFile()). Unknown NormalisationNode object");  
          }
        }
      }
      
    }    
    
    return set;
  }

  /**
   * return a set with all the conditions requested for simulations (time course plots) and for
   * normalisation. These together are the conditions I need to simulate
   * so that the requested plots can be made. Consider conditions for normalisation
   * only if they are useful to normalise conditions requested for plotting
   * */
  
  private Set<Integer> getConditionsToSimulate_SimulationsFile() throws Exception{
    //find all simulations requested for plots
    Set<Integer> allConds = getConditionsRequestedForPlots();
     
    
    // if no conditions have been specified you can still simulate the default condition of the equations
    if (allConds.isEmpty()) allConds.add(lookUpTable.getKey("\"default\""));
    return allConds;
  }

  private void printDataMatrices(BufferedWriter bw) throws Exception{
    //get list of data nodes
    Iterator<GenericEntry> ite = entryLists.getDataList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof DataNodeReplicates)) throw new Exception("[error] Probably a "
      + "coding error (GLSDCTool.printDataMatrices()). The GenericEntry object is not a DataNodeReplicates object");
      DataNodeReplicates dnr = (DataNodeReplicates) tmp; //downcast
      
      LinkedList<Double[][]> matrixList = dnr.getListOfDataMatrices();
      
      //---- first I need a unique id for this data struct (species name + conditionIDs)
      
      String uniqueID = QueryModelAndData.buildUniqueIDFrom(lookUpTable,(DataNode)dnr);
      
      //---- done building unique id
      
      bw.write("// Species name: " + lookUpTable.lookUpString(dnr.getIdentifier()) + "\n\n");
      bw.write("const int " + uniqueID + "nreplicates = " + matrixList.size() + ";\n");
      bw.write("const int " + uniqueID + "ntimepoints = " + matrixList.getFirst().length + ";\n\n");
      
      //struct definition
      
      bw.write("struct " + uniqueID + "struct {\n"
              + "  realtype time;\n");
      
      Iterator<Integer> ite2 = dnr.getConditions().iterator();
      if (ite2.hasNext()) ite2.next(); //skip time
      while (ite2.hasNext()){
        Integer tmp2 = ite2.next();
        bw.write("  realtype c_" + tmp2.intValue() + "; // " + lookUpTable.lookUpString(tmp2) + "\n");
      }      
      bw.write("};\n\n");
      
      //data matrix
      
      bw.write("static " + uniqueID + "struct " + uniqueID 
              + "data[" + uniqueID + "nreplicates][" + uniqueID + "ntimepoints] = {\n");
      
      Iterator<Double[][]> ite3 = matrixList.iterator();
      while (ite3.hasNext()){
        Double[][] matrix = ite3.next();
        
        String s = "  {\n";
        for (int i = 0; i< matrix.length; i++){
          s += "    { ";
          for (int j = 0; j< matrix[i].length; j++){
            
            if (j+1<matrix[i].length) s += (matrix[i][j].isNaN()? "NAN" : matrix[i][j].toString()) + ", ";
            else s += (matrix[i][j].isNaN()? "NAN" : matrix[i][j].toString());
          }
          if (i+1<matrix.length) s += " },\n";
          else s += " }\n";
        }
        
        if (ite3.hasNext()) bw.write(s + "  },\n");
        else bw.write(s + "  }\n");
      }
      
      bw.write("};\n\n");
      
      
      //data average
      
      double[][] dataAverage = computeMatrixAverage(dnr.getListOfDataMatrices());
      
      bw.write("static " + uniqueID + "struct " + uniqueID 
              + "dataAverage[" + uniqueID + "ntimepoints] = {\n");
      
      String s = "";
      for (int i = 0; i< dataAverage.length; i++){
        s += "  { ";
        for (int j = 0; j< dataAverage[i].length; j++){
          
          if (j+1<dataAverage[i].length) s += (Double.isNaN(dataAverage[i][j])? "NAN" : dataAverage[i][j]) + ", ";
          else s += (Double.isNaN(dataAverage[i][j])? "NAN" : dataAverage[i][j]);
        }
        if (i+1<dataAverage.length) s += " },\n";
        else s += " }\n";
      }      
      
      bw.write(s + "};\n\n");      
    }
    
  }
  
  
  private double[][] computeMatrixAverage(LinkedList<Double[][]> list){
    
    int ntimepoints = list.getFirst().length;
    int nconditions = list.getFirst()[0].length;
    
    double[][] matrixAverage = new double[ntimepoints][nconditions];
    int[][] nAtPos = new int[ntimepoints][nconditions];
    boolean[][] atLeastOneNotNaN = new boolean[ntimepoints][nconditions];
    
    for (int i=0; i<ntimepoints;i++ ){
      //set time points
      matrixAverage[i][0] = list.getFirst()[i][0].doubleValue();
    }
    
    
    for (int i=0; i<ntimepoints;i++ ){
      for (int j=1; j<nconditions;j++ ){ //skip time
        matrixAverage[i][j] = 0.0;
        nAtPos[i][j] = 0;
        atLeastOneNotNaN[i][j] = false;
      }
    }
    
    //iterate across replicates
    Iterator<Double[][]> ite = list.iterator();
    while (ite.hasNext()){
      Double[][] tmp = ite.next();

      for (int i=0; i<ntimepoints;i++ ){
        for (int j=1; j<nconditions;j++ ){ //skip time
          if (!tmp[i][j].isNaN()) {
            //found not NaN data point
            matrixAverage[i][j] += tmp[i][j].doubleValue();
            nAtPos[i][j]++;
            atLeastOneNotNaN[i][j] = true;
          }
        }
      }   
      
    }
    
    for (int i=0; i<ntimepoints;i++ ){
      for (int j=1; j<nconditions;j++ ){ //skip time
        if (atLeastOneNotNaN[i][j]) {
          matrixAverage[i][j] /= nAtPos[i][j];
        }else{
          matrixAverage[i][j] = Double.NaN;
        }
      }
    }   
    
    return matrixAverage;
  }
  
  /**
   * Write algebraic equations c functions signatures, and their
   * derivatives if requested
   * */
  
  private void printSignatures(boolean writeDer,Map<Integer,Set<Integer>> signatures,BufferedWriter bw) throws Exception{
    
    //write a signature for each element in the algebraic equation list
    //if additional parameters are necessary, these will be in the signatures map
    
    //in case writeDer is true I will need to write the derivatives of 
    //some of the algebraic equation functions. Which? retrieve them:
    //Set<Integer> setForDerivation = QueryModelAndData.requiredAlgebraicEqDerivatives(entryLists,lookUpTable,fittingOptions);
    //above commented out, write all derivatives
    
    //now main loop
    Iterator<GenericEntry> ite = entryLists.getAlgList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof AlgebraicEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printSignatures()). The GenericEntry object is not a AlgebraicEquationNode object");
      AlgebraicEquationNode aen = (AlgebraicEquationNode) tmp;
      
      bw.write("static realtype " + lookUpTable.lookUpString(aen.getIdentifier()) + "(realtype time, int condition");
      
      Set<Integer> s = signatures.get(aen.getIdentifier());
      if (s != null){
        Iterator<Integer> ite2 = s.iterator();
        while (ite2.hasNext()){
          Integer tmp2 = ite2.next();
          bw.write(", realtype " + lookUpTable.lookUpString(tmp2)); 
        }
      } 
      bw.write(");\n");
      
      //if (writeDer && setForDerivation.contains(aen.getIdentifier())){
      if (writeDer){
          //for all parameters to fit
          LinkedList<Integer> paramsToFit = QueryModelAndData.getParametersToFit(entryLists);
          LinkedList<Integer> differentials = QueryModelAndData.getDifferentialVariablesIDs(entryLists);
          LinkedList<Integer> fullDerivativesList = new LinkedList<Integer>();
          fullDerivativesList.addAll(paramsToFit);
          //fullDerivativesList.addAll(differentials);
          fullDerivativesList.add(differentials.getFirst()); //trick: I just need to write this derivative once for all differentials
          Iterator<Integer> ite3 = fullDerivativesList.iterator();
          while (ite3.hasNext()){
            Integer paramID = ite3.next();
            //--------copy from above and adapt
            int type = QueryModelAndData.getIDType(entryLists,paramID);
            if (type == GLSDCTool.PARAMETER){
              bw.write("static realtype " + lookUpTable.lookUpString(aen.getIdentifier()) 
                  + "__der__" + lookUpTable.lookUpString(paramID) +"(realtype time, int condition");
            }else if (type == GLSDCTool.DIFFERENTIAL){
              bw.write("static realtype " + lookUpTable.lookUpString(aen.getIdentifier()) 
                  + "__der__(realtype time, int condition");
            }
            s = signatures.get(aen.getIdentifier());
            if (s != null){
              Iterator<Integer> ite2 = s.iterator();
              while (ite2.hasNext()){
                Integer tmp2 = ite2.next();
                bw.write(", realtype " + lookUpTable.lookUpString(tmp2)); 
                if (type == GLSDCTool.PARAMETER){
                  bw.write(", realtype " + lookUpTable.lookUpString(tmp2) + "__der__" + lookUpTable.lookUpString(paramID));
                }else if (type == GLSDCTool.DIFFERENTIAL){
                  bw.write(", realtype " + lookUpTable.lookUpString(tmp2) + "__der__");
                }
              }
            } 
            bw.write(");\n");
            //--------
          }
      }//--------end of if writeDer
    }
    
    bw.write("\n");
  }
  
  private void enumerateAllParameters(BufferedWriter bw) throws Exception{
    
    bw.write("enum {\n");
    int n = 0;
    Iterator<GenericEntry> ite = entryLists.getParamList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof ParameterNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.enumerateAllParameters()). The GenericEntry object is not a ParameterNode object");
      ParameterNode pn = (ParameterNode) tmp;
      
      bw.write("  " + lookUpTable.lookUpString(pn.getIdentifier()) + ",  // " + n + "\n");
      
      n++;
    }
    
    bw.write("  K_n  // " + n + "\n");
    bw.write("};\n\n");
  }
  
  private void enumerateDifferentialVariables(BufferedWriter bw) throws Exception{
    
    bw.write("enum {\n");
    int n = 0;
    Iterator<GenericEntry> ite = entryLists.getDiffList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof DifferentialEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.enumerateDifferentialVariables()). The GenericEntry object is not a DifferentialEquationNode object");
      DifferentialEquationNode den = (DifferentialEquationNode) tmp;
      
      bw.write("  " + lookUpTable.lookUpString(den.getIdentifier()) + ",  // " + n + "\n");
      
      n++;
    }
    
    bw.write("  M_n  // " + n + "\n");
    bw.write("};\n\n");
  }
  
  private void enumerateSpeciesVariables(BufferedWriter bw) throws Exception{
    bw.write("//enumerate species variables (to be used with simulationMatrix)\n");
    bw.write("enum {\n");
    int n = 0;
    Iterator<GenericEntry> ite = entryLists.getDiffList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof DifferentialEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.enumerateSpeciesVariables()). The GenericEntry object is not a DifferentialEquationNode object");
      DifferentialEquationNode den = (DifferentialEquationNode) tmp;
      
      bw.write("  s_" + den.getIdentifier() + ",  // " + lookUpTable.lookUpString(den.getIdentifier()) + " " + n + "\n");
      
      n++;
    }

    ite = entryLists.getAlgList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof AlgebraicEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.enumerateSpeciesVariables()). The GenericEntry object is not a AlgebraicEquationNode object");
      AlgebraicEquationNode den = (AlgebraicEquationNode) tmp;
      
      bw.write("  s_" + den.getIdentifier() + ",  // " + lookUpTable.lookUpString(den.getIdentifier()) + " " + n + "\n");
      
      n++;
    }
    
    bw.write("  S_n  // " + n + "\n");
    bw.write("};\n\n");
  }  
  
  private Set<Integer> getSpeciesVariables() throws Exception{
    Set<Integer> speciesVariables = new HashSet<Integer>();
    
    Iterator<GenericEntry> ite = entryLists.getDiffList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof DifferentialEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.speciesVariables()). The GenericEntry object is not a DifferentialEquationNode object");
      DifferentialEquationNode den = (DifferentialEquationNode) tmp;
      
      speciesVariables.add(den.getIdentifier());

    }

    ite = entryLists.getAlgList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof AlgebraicEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.speciesVariables()). The GenericEntry object is not a AlgebraicEquationNode object");
      AlgebraicEquationNode den = (AlgebraicEquationNode) tmp;
      
      speciesVariables.add(den.getIdentifier());
    }
    return speciesVariables;
  }
  
  
  private void printF(int optimisationAlgorithm,Set<Integer> conditionsToSimulate, Map<Integer,Set<Integer>> signatures, BufferedWriter bw) throws Exception{
    
    bw.write("static int __F__(realtype time, N_Vector y, N_Vector ydot, void *f_data){\n\n");
    
    if (optimisationAlgorithm == GLSDC || optimisationAlgorithm == LEVMAR_FD){
      bw.write("  int condition = *((int *) f_data);\n\n");
    }else if (optimisationAlgorithm == LEVMAR_SE){
      
      //for sensitivity equations (CVODES) I need to pass to F the parameters
      
      bw.write("  UserData data = (UserData) f_data;\n"
              + "  int condition = data->condition;\n");
      
      //no passing parameters any more
      //~ int n =0;
      
      //~ Iterator<GenericEntry> ite = entryLists.getParamList().iterator();
      //~ while (ite.hasNext()){
        //~ GenericEntry tmp = ite.next();
        //~ if (! (tmp instanceof ParameterNode)) throw new Exception("[error] coding error! I should have ParameterNode objects in GLSDCTool.generateFile_pset_sbcmodel_orgdat()");
        //~ if (tmp instanceof ParameterRange){
          //~ ParameterRange pr = (ParameterRange) tmp; //downcast
          //~ bw.write("  X[" + lookUpTable.lookUpString(pr.getIdentifier()) + "] = data->p[" + n + "];\n");          
          //~ n++;
        //~ }
      //~ }

    }else {
      throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printF()). Unknown optimisation algorithm");
    }
              
    //find out which algebraic equations are requested directly by the differential equations
    Set<Integer> algebraicEquationIDs = new HashSet<Integer>();
    
    Iterator<GenericEntry> ite4 = entryLists.getDiffList().iterator();
    while (ite4.hasNext()){
      GenericEntry tmp = ite4.next();
      if (! (tmp instanceof DifferentialEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printF()). The GenericEntry object is not a DifferentialEquationNode object");
      if (tmp instanceof DifferentialEquationExpr){
        
        DifferentialEquationExpr dee = (DifferentialEquationExpr)tmp; //downcast
        
        getAlgEqIDInExpression(dee.getExpression(),algebraicEquationIDs);
        
      } else if (tmp instanceof DifferentialEquationCondList){
        
        DifferentialEquationCondList dec = (DifferentialEquationCondList)tmp; //downcast
        
        HashMap<Integer,Expression> mapCondToExpr = dec.getMapConditionToExpression();
        Set<Integer> keys = mapCondToExpr.keySet();
        if (!keys.containsAll(conditionsToSimulate)){
          throw new Exception("[error] Not all conditions necessary requested for fitting or necessary for normalisation " 
            + "have been specified for differential equation entry " + lookUpTable.lookUpString(dec.getIdentifier()) + ".");    
        }
        
        Iterator<Integer> ite3 = conditionsToSimulate.iterator();
        while (ite3.hasNext()){
          Integer cond = ite3.next();
          getAlgEqIDInExpression(mapCondToExpr.get(cond),algebraicEquationIDs);          
        }
        
      } else {
        throw new Exception("[error] Probably a "
          + "coding error (GLSDCTool.printF()). Unknown type of DifferentialEquationNode.");        
      }
      
    }    

              
    //--------- call algebraic functions
    Iterator<GenericEntry> ite = entryLists.getAlgList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof AlgebraicEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printF()). The GenericEntry object is not a AlgebraicEquationNode object");
      AlgebraicEquationNode aen = (AlgebraicEquationNode) tmp;
      if (algebraicEquationIDs.contains(aen.getIdentifier())){
        bw.write("  realtype var_" + lookUpTable.lookUpString(aen.getIdentifier()) + " = " + lookUpTable.lookUpString(aen.getIdentifier()) + "(time,condition");
        
        Set<Integer> s = signatures.get(aen.getIdentifier());
        if (s != null){
          Iterator<Integer> ite2 = s.iterator();
          while (ite2.hasNext()){
            Integer tmp2 = ite2.next();
            bw.write(",Y(" + lookUpTable.lookUpString(tmp2) + ")"); 
          }
        } 
        bw.write(");\n");
      }
    }
    bw.write("\n");
    
    //--------- done calling algebraic functions
    
    
    //--------- write differential equations
    Iterator<GenericEntry> ite2 = entryLists.getDiffList().iterator();
    while (ite2.hasNext()){
      GenericEntry tmp = ite2.next();
      if (! (tmp instanceof DifferentialEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printF()). The GenericEntry object is not a DifferentialEquationNode object");
      if (tmp instanceof DifferentialEquationExpr){
        
        DifferentialEquationExpr dee = (DifferentialEquationExpr)tmp; //downcast
        
        bw.write("  dY(" + lookUpTable.lookUpString(dee.getIdentifier()) + ") =");
        
        bw.write(writeExpression(dee.getExpression(),bw,1,null));
        
        bw.write(";\n");
        
      } else if (tmp instanceof DifferentialEquationCondList){
        
        DifferentialEquationCondList dec = (DifferentialEquationCondList)tmp; //downcast
        
        HashMap<Integer,Expression> mapCondToExpr = dec.getMapConditionToExpression();
        Set<Integer> keys = mapCondToExpr.keySet();
        if (!keys.containsAll(conditionsToSimulate)){
          throw new Exception("[error] Not all conditions necessary requested for fitting or necessary for normalisation " 
            + "have been specified for differential equation entry " + lookUpTable.lookUpString(dec.getIdentifier()) + ".");    
        }
        
        bw.write("  if (condition==");
        
        Iterator<Integer> ite3 = conditionsToSimulate.iterator();
        while (ite3.hasNext()){
          Integer cond = ite3.next();
          bw.write("c_" + cond.intValue() + "){ // " + lookUpTable.lookUpString(cond) + "\n");
          
          bw.write("    dY(" + lookUpTable.lookUpString(dec.getIdentifier()) + ") =");
          bw.write(writeExpression(mapCondToExpr.get(cond),bw,1,null));
           
          if (ite3.hasNext()) bw.write(";\n  } else if (condition==");
          else bw.write(";\n  }\n");
        }
        
        
      } else {
        throw new Exception("[error] Probably a "
          + "coding error (GLSDCTool.printF()). Unknown type of DifferentialEquationNode.");        
      }
      
    }
    
    bw.write("\n");
    bw.write("  return 0;\n");
    
    //--------- done writing differential equations
    
    bw.write("}\n\n");
  }


  
  private void printJac(int optimisationAlgorithm,Set<Integer> conditionsToSimulate, Map<Integer,Set<Integer>> signatures, BufferedWriter bw) throws Exception{
    
    bw.write("static int __Jac__(long int N, realtype time, N_Vector y, N_Vector fy, DlsMat J, void *f_data, N_Vector tmp1, N_Vector tmp2, N_Vector tmp3){\n\n");
    
    if (optimisationAlgorithm == GLSDC || optimisationAlgorithm == LEVMAR_FD){
      bw.write("  int condition = *((int *) f_data);\n\n");
    }else if (optimisationAlgorithm == LEVMAR_SE){
      
      //for sensitivity equations (CVODES) I need to pass to F the parameters
      
      bw.write("  UserData data = (UserData) f_data;\n"
              + "  int condition = data->condition;\n");

      //~ int n =0;
      
      //~ Iterator<GenericEntry> ite = entryLists.getParamList().iterator();
      //~ while (ite.hasNext()){
        //~ GenericEntry tmp = ite.next();
        //~ if (! (tmp instanceof ParameterNode)) throw new Exception("[error] coding error! I should have ParameterNode objects in GLSDCTool.generateFile_pset_sbcmodel_orgdat()");
        //~ if (tmp instanceof ParameterRange){
          //~ ParameterRange pr = (ParameterRange) tmp; //downcast
          //~ bw.write("  X[" + lookUpTable.lookUpString(pr.getIdentifier()) + "] = data->p[" + n + "];\n");          
          //~ n++;
        //~ }
      //~ }

    }else {
      throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printF()). Unknown optimisation algorithm");
    }
              
    //find out which algebraic equations are requested directly by the differential equations
    Set<Integer> algebraicEquationIDs = new HashSet<Integer>();
    
    Iterator<GenericEntry> ite4 = entryLists.getDiffList().iterator();
    while (ite4.hasNext()){
      GenericEntry tmp = ite4.next();
      if (! (tmp instanceof DifferentialEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printF()). The GenericEntry object is not a DifferentialEquationNode object");
      if (tmp instanceof DifferentialEquationExpr){
        
        DifferentialEquationExpr dee = (DifferentialEquationExpr)tmp; //downcast
        
        getAlgEqIDInExpression(dee.getExpression(),algebraicEquationIDs);
        
      } else if (tmp instanceof DifferentialEquationCondList){
        
        DifferentialEquationCondList dec = (DifferentialEquationCondList)tmp; //downcast
        
        HashMap<Integer,Expression> mapCondToExpr = dec.getMapConditionToExpression();
        Set<Integer> keys = mapCondToExpr.keySet();
        if (!keys.containsAll(conditionsToSimulate)){
          throw new Exception("[error] Not all conditions necessary requested for fitting or necessary for normalisation " 
            + "have been specified for differential equation entry " + lookUpTable.lookUpString(dec.getIdentifier()) + ".");    
        }
        
        Iterator<Integer> ite3 = conditionsToSimulate.iterator();
        while (ite3.hasNext()){
          Integer cond = ite3.next();
          getAlgEqIDInExpression(mapCondToExpr.get(cond),algebraicEquationIDs);          
        }
        
      } else {
        throw new Exception("[error] Probably a "
          + "coding error (GLSDCTool.printF()). Unknown type of DifferentialEquationNode.");        
      }
      
    }    

    //IDs of variables I need to differentiate for
    LinkedList<Integer> differentialDerIDs = QueryModelAndData.getDifferentialVariablesIDs(entryLists);  
            
    //--------- call algebraic functions
    Iterator<GenericEntry> ite = entryLists.getAlgList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof AlgebraicEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printF()). The GenericEntry object is not a AlgebraicEquationNode object");
      AlgebraicEquationNode aen = (AlgebraicEquationNode) tmp;
      if (algebraicEquationIDs.contains(aen.getIdentifier())){
        bw.write("  realtype var_" + lookUpTable.lookUpString(aen.getIdentifier()) + " = " + lookUpTable.lookUpString(aen.getIdentifier()) + "(time,condition");
        
        Set<Integer> s = signatures.get(aen.getIdentifier());
        if (s != null){
          Iterator<Integer> ite2 = s.iterator();
          while (ite2.hasNext()){
            Integer tmp2 = ite2.next();
            bw.write(",Y(" + lookUpTable.lookUpString(tmp2) + ")"); 
          }
        } 
        bw.write(");\n");
        //also call derivatives:
        Iterator<Integer> iteDer = differentialDerIDs.iterator();
        while (iteDer.hasNext()){
          Integer derID = iteDer.next();

          bw.write("  realtype var_" + lookUpTable.lookUpString(aen.getIdentifier()) 
            + "__der__" + lookUpTable.lookUpString(derID)
            + " = " + lookUpTable.lookUpString(aen.getIdentifier()) 
            + "__der__(time,condition");
          
          s = signatures.get(aen.getIdentifier());
          if (s != null){
            Iterator<Integer> ite2 = s.iterator();
            while (ite2.hasNext()){
              Integer tmp2 = ite2.next();
              bw.write(",Y(" + lookUpTable.lookUpString(tmp2) + ")"); 
              if (tmp2.equals(derID)){//same id, derivative is 1
                bw.write(",1.0"); 
              }else{ //different id, derivative is 0
                bw.write(",0.0"); 
              }
            }
          } 
          bw.write(");\n");
        }
        //done calling derivatives
      }
    }
    bw.write("\n");
    
    //--------- done calling algebraic functions
    
    
    //--------- write differential equations
    Iterator<GenericEntry> ite2 = entryLists.getDiffList().iterator();
    while (ite2.hasNext()){
      GenericEntry tmp = ite2.next();
      if (! (tmp instanceof DifferentialEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printF()). The GenericEntry object is not a DifferentialEquationNode object");
      if (tmp instanceof DifferentialEquationExpr){
        
        DifferentialEquationExpr dee = (DifferentialEquationExpr)tmp; //downcast
        Iterator<Integer> iteDer = differentialDerIDs.iterator();
        while (iteDer.hasNext()){ //populate jacobian
          Integer derID = iteDer.next();
          bw.write("  DENSE_ELEM(J," + lookUpTable.lookUpString(dee.getIdentifier()) + "," + lookUpTable.lookUpString(derID) + ") =");
          
          try{
						bw.write(writeDerivative(dee.getExpression(),derID,1,bw));
          } catch (DerivativeZeroException dze){
						bw.write(" 0.0 ");
					}
          bw.write(";\n");
        }
      } else if (tmp instanceof DifferentialEquationCondList){
        
        DifferentialEquationCondList dec = (DifferentialEquationCondList)tmp; //downcast
        
        HashMap<Integer,Expression> mapCondToExpr = dec.getMapConditionToExpression();
        Set<Integer> keys = mapCondToExpr.keySet();
        if (!keys.containsAll(conditionsToSimulate)){
          throw new Exception("[error] Not all conditions necessary requested for fitting or necessary for normalisation " 
            + "have been specified for differential equation entry " + lookUpTable.lookUpString(dec.getIdentifier()) + ".");    
        }
        
        bw.write("  if (condition==");
        
        Iterator<Integer> ite3 = conditionsToSimulate.iterator();
        while (ite3.hasNext()){
          Integer cond = ite3.next();
          bw.write("c_" + cond.intValue() + "){ // " + lookUpTable.lookUpString(cond) + "\n");
          
          Iterator<Integer> iteDer = differentialDerIDs.iterator();
          while (iteDer.hasNext()){ //populate jacobian
            Integer derID = iteDer.next();
            bw.write("  DENSE_ELEM(J," + lookUpTable.lookUpString(dec.getIdentifier()) + "," + lookUpTable.lookUpString(derID) + ") =");
            
            try{
							bw.write(writeDerivative(mapCondToExpr.get(cond),derID,1,bw));
            } catch (DerivativeZeroException dze){
							bw.write(" 0.0 ");
						}
						
            bw.write(";\n");
          }
           
          if (ite3.hasNext()) bw.write(";\n  } else if (condition==");
          else bw.write(";\n  }\n");
        }
        
        
      } else {
        throw new Exception("[error] Probably a "
          + "coding error (GLSDCTool.printF()). Unknown type of DifferentialEquationNode.");        
      }
      
    }
    
    bw.write("\n");
    bw.write("  return 0;\n");
    
    //--------- done writing differential equations
    
    bw.write("}\n\n");
  }//-------------- End of printJac


  
  private void printSensRHS(int optimisationAlgorithm,Set<Integer> conditionsToSimulate, Map<Integer,Set<Integer>> signatures, BufferedWriter bw) throws Exception{
    
    bw.write("static int __fS__(int Ns, realtype time, N_Vector y, N_Vector ydot, N_Vector *yS, N_Vector *ySdot, void *f_data, N_Vector tmp1, N_Vector tmp2){\n\n");
    if (optimisationAlgorithm == GLSDC || optimisationAlgorithm == LEVMAR_FD){
      bw.write("  int condition = *((int *) f_data);\n\n");
    }else if (optimisationAlgorithm == LEVMAR_SE){
      
      //for sensitivity equations (CVODES) I need to pass to F the parameters
      
      bw.write("  UserData data = (UserData) f_data;\n"
              + "  int condition = data->condition;\n");

      //~ int n =0;
      
      //~ Iterator<GenericEntry> ite = entryLists.getParamList().iterator();
      //~ while (ite.hasNext()){
        //~ GenericEntry tmp = ite.next();
        //~ if (! (tmp instanceof ParameterNode)) throw new Exception("[error] coding error! I should have ParameterNode objects in GLSDCTool.generateFile_pset_sbcmodel_orgdat()");
        //~ if (tmp instanceof ParameterRange){
          //~ ParameterRange pr = (ParameterRange) tmp; //downcast
          //~ bw.write("  X[" + lookUpTable.lookUpString(pr.getIdentifier()) + "] = data->p[" + n + "];\n");          
          //~ n++;
        //~ }
      //~ }

    }else {
      throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printSensRHS()). Unknown optimisation algorithm");
    }
              
    //find out which algebraic equations are requested directly by the differential equations
    Set<Integer> algebraicEquationIDs = new HashSet<Integer>();
    
    Iterator<GenericEntry> ite4 = entryLists.getDiffList().iterator();
    while (ite4.hasNext()){
      GenericEntry tmp = ite4.next();
      if (! (tmp instanceof DifferentialEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printSensRHS()). The GenericEntry object is not a DifferentialEquationNode object");
      if (tmp instanceof DifferentialEquationExpr){
        
        DifferentialEquationExpr dee = (DifferentialEquationExpr)tmp; //downcast
        
        getAlgEqIDInExpression(dee.getExpression(),algebraicEquationIDs);
        
      } else if (tmp instanceof DifferentialEquationCondList){
        
        DifferentialEquationCondList dec = (DifferentialEquationCondList)tmp; //downcast
        
        HashMap<Integer,Expression> mapCondToExpr = dec.getMapConditionToExpression();
        Set<Integer> keys = mapCondToExpr.keySet();
        if (!keys.containsAll(conditionsToSimulate)){
          throw new Exception("[error] Not all conditions necessary requested for fitting or necessary for normalisation " 
            + "have been specified for differential equation entry " + lookUpTable.lookUpString(dec.getIdentifier()) + ".");    
        }
        
        Iterator<Integer> ite3 = conditionsToSimulate.iterator();
        while (ite3.hasNext()){
          Integer cond = ite3.next();
          getAlgEqIDInExpression(mapCondToExpr.get(cond),algebraicEquationIDs);          
        }
        
      } else {
        throw new Exception("[error] Probably a "
          + "coding error (GLSDCTool.printSensRHS()). Unknown type of DifferentialEquationNode.");        
      }
      
    }    

    //IDs of variables I need to differentiate for
    LinkedList<Integer> differentialDerIDs = QueryModelAndData.getDifferentialVariablesIDs(entryLists);  
    LinkedList<Integer> paramsToFit = QueryModelAndData.getParametersToFit(entryLists);
    LinkedList<Integer> allDerivativesToCompute = new LinkedList<Integer>();
    allDerivativesToCompute.addAll(differentialDerIDs);
    allDerivativesToCompute.addAll(paramsToFit);
            
    //--------- call algebraic functions
    Iterator<GenericEntry> ite = entryLists.getAlgList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof AlgebraicEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printSensRHS()). The GenericEntry object is not a AlgebraicEquationNode object");
      AlgebraicEquationNode aen = (AlgebraicEquationNode) tmp;
      if (algebraicEquationIDs.contains(aen.getIdentifier())){
        bw.write("  realtype var_" + lookUpTable.lookUpString(aen.getIdentifier()) + " = " + lookUpTable.lookUpString(aen.getIdentifier()) + "(time,condition");
        
        Set<Integer> s = signatures.get(aen.getIdentifier());
        if (s != null){
          Iterator<Integer> ite2 = s.iterator();
          while (ite2.hasNext()){
            Integer tmp2 = ite2.next();
            bw.write(",Y(" + lookUpTable.lookUpString(tmp2) + ")"); 
          }
        } 
        bw.write(");\n");
        //also call derivatives:
        Iterator<Integer> iteDer = allDerivativesToCompute.iterator();
        while (iteDer.hasNext()){
          Integer derID = iteDer.next();

          int idType = QueryModelAndData.getIDType(entryLists,derID);
          if (idType == GLSDCTool.DIFFERENTIAL){
            bw.write("  realtype var_" + lookUpTable.lookUpString(aen.getIdentifier()) 
              + "__der__" + lookUpTable.lookUpString(derID)
              + " = " + lookUpTable.lookUpString(aen.getIdentifier()) 
              + "__der__(time,condition");
          }else if (idType == GLSDCTool.PARAMETER){
            bw.write("  realtype var_" + lookUpTable.lookUpString(aen.getIdentifier()) 
              + "__der__" + lookUpTable.lookUpString(derID)
              + " = " + lookUpTable.lookUpString(aen.getIdentifier()) 
              + "__der__" + lookUpTable.lookUpString(derID) + "(time,condition");
          }
          s = signatures.get(aen.getIdentifier());
          if (s != null){
            Iterator<Integer> ite2 = s.iterator();
            while (ite2.hasNext()){
              Integer tmp2 = ite2.next();
              bw.write(",Y(" + lookUpTable.lookUpString(tmp2) + ")"); 
              if (tmp2.equals(derID)){//same id, derivative is 1
                bw.write(",1.0"); 
              }else{ //different id, derivative is 0
                bw.write(",0.0"); 
              }
            }
          } 
          bw.write(");\n");
        }
        //done calling derivatives
      }
    }
    bw.write("\n");
    
    //--------- done calling algebraic functions
    
    
    //local memory to store Jacobian
    bw.write("  int N = " + differentialDerIDs.size() + ";\n");
    bw.write("  realtype __dfdx__[N][N];\n");
    bw.write("  realtype __tmpStore__ = 0;\n\n");
    
    //--------- write differential equations
    Iterator<GenericEntry> ite2 = entryLists.getDiffList().iterator();
    while (ite2.hasNext()){
      GenericEntry tmp = ite2.next();
      if (! (tmp instanceof DifferentialEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printSensRHS()). The GenericEntry object is not a DifferentialEquationNode object");
      if (tmp instanceof DifferentialEquationExpr){
        
        DifferentialEquationExpr dee = (DifferentialEquationExpr)tmp; //downcast
        Iterator<Integer> iteDer = differentialDerIDs.iterator();
        while (iteDer.hasNext()){ //populate jacobian
          Integer derID = iteDer.next();
          bw.write("  __dfdx__[" + lookUpTable.lookUpString(dee.getIdentifier()) + "][" + lookUpTable.lookUpString(derID) + "] =");
          
          try{
						bw.write(writeDerivative(dee.getExpression(),derID,1,bw));
					} catch (DerivativeZeroException dze){
						bw.write(" 0.0 ");
					}
					
          bw.write(";\n");
        }
        
        //for all parameters to fit
        Iterator<Integer> itePar = paramsToFit.iterator();
        int parPos = 0;
        while (itePar.hasNext()){
          Integer paramID = itePar.next();
          //compute Sensitivity RHS
          bw.write("  __tmpStore__ = 0;\n");
          bw.write("  for (int k = 0; k<N; k++) __tmpStore__ += __dfdx__[" 
              + lookUpTable.lookUpString(dee.getIdentifier()) + "][k] * " 
              + "NV_Ith_S(yS[" + parPos + "],k);\n");
          bw.write("  NV_Ith_S(ySdot[" + parPos + "]," + lookUpTable.lookUpString(dee.getIdentifier()) + ") = "
              + "__tmpStore__ + "); 
          
          try{
						bw.write(writeDerivative(dee.getExpression(),paramID,1,bw));
          }catch(DerivativeZeroException dze){
						bw.write(" 0.0 ");
					}
          
          bw.write(";\n");
          parPos++;
        }
        
        
        
      } else if (tmp instanceof DifferentialEquationCondList){
        
        DifferentialEquationCondList dec = (DifferentialEquationCondList)tmp; //downcast
        
        HashMap<Integer,Expression> mapCondToExpr = dec.getMapConditionToExpression();
        Set<Integer> keys = mapCondToExpr.keySet();
        if (!keys.containsAll(conditionsToSimulate)){
          throw new Exception("[error] Not all conditions necessary requested for fitting or necessary for normalisation " 
            + "have been specified for differential equation entry " + lookUpTable.lookUpString(dec.getIdentifier()) + ".");    
        }
        
        bw.write("  if (condition==");
        
        Iterator<Integer> ite3 = conditionsToSimulate.iterator();
        while (ite3.hasNext()){
          Integer cond = ite3.next();
          bw.write("c_" + cond.intValue() + "){ // " + lookUpTable.lookUpString(cond) + "\n");
          
          Iterator<Integer> iteDer = differentialDerIDs.iterator();
          while (iteDer.hasNext()){ //populate jacobian
            Integer derID = iteDer.next();
            bw.write("    __dfdx__[" + lookUpTable.lookUpString(dec.getIdentifier()) + "][" + lookUpTable.lookUpString(derID) + "] =");
            
            try{
							bw.write(writeDerivative(mapCondToExpr.get(cond),derID,1,bw));
            }catch(DerivativeZeroException dze){
							bw.write(" 0.0 ");
						}
					
            bw.write(";\n");
          }
          
          //for all parameters to fit
          Iterator<Integer> itePar = paramsToFit.iterator();
          int parPos = 0;
          while (itePar.hasNext()){
            Integer paramID = itePar.next();
            //compute Sensitivity RHS
            bw.write("    __tmpStore__ = 0;\n");
            bw.write("    for (int k = 0; k<N; k++) __tmpStore__ += __dfdx__[" 
                + lookUpTable.lookUpString(dec.getIdentifier()) + "][k] * " 
                + "NV_Ith_S(yS[" + parPos + "],k);\n");
            bw.write("    NV_Ith_S(ySdot[" + parPos + "]," + lookUpTable.lookUpString(dec.getIdentifier()) + ") = "
                + "__tmpStore__ + "); 
                
            try{    
							bw.write(writeDerivative(mapCondToExpr.get(cond),paramID,1,bw));
            }catch(DerivativeZeroException dze){
							bw.write(" 0.0 ");
						}
            
            bw.write(";\n");
            parPos++;
          }
           
          if (ite3.hasNext()) bw.write(";\n  } else if (condition==");
          else bw.write(";\n  }\n");
        }
        
        
      } else {
        throw new Exception("[error] Probably a "
          + "coding error (GLSDCTool.printSensRHS()). Unknown type of DifferentialEquationNode.");        
      }
      
    }
    
    bw.write("\n");    
    
    bw.write("  return 0;\n");
    
    //--------- done writing differential equations
    
    bw.write("}\n\n");
  }//-------------- End of printSensRHS


  
  /**
   * Write expressions. Depending on where the expression is written, the
   * variables may be written in a different way. Specify this with the 
   * int mode parameter. Types of mode:<br/>
   * 1 = Write differental variables using Y(name) format<br/>
   * 2 = Write differental variables using only the name<br/>
   * 3 = Write differental variables using NV_Ith_S(y_c_id,P) format<br/>
   * */
  
  private String writeExpression(Expression expr, BufferedWriter bw, int mode, Integer condID) throws Exception{
    
    if (expr instanceof ExprID){
      ExprID e = (ExprID)expr; //downcast
      Integer id = e.getID();
      int type = QueryModelAndData.getIDType(entryLists,id);
      //add id to appropriate set if it is an id of an algebraic or differential or parameter entry
      if (type == ALGEBRAIC){
        return(" var_" + lookUpTable.lookUpString(id) + " ");
      }else if (type == DIFFERENTIAL){
        if (mode == 1) {
          return(" Y(" + lookUpTable.lookUpString(id) + ") ");
        } else if(mode == 2) {
          return(" " + lookUpTable.lookUpString(id) + " "); 
        } else if(mode == 3) {
          if (condID == null) throw new Exception("[error] coding error, unspecified condID in writeExpression");
          return(" NV_Ith_S(y_c_" + condID + "," + lookUpTable.lookUpString(id) + ") ");  
        } else {
          throw new Exception("[error] coding error, unknown type of mode in writeExpression");
        }
      }else if (type == PARAMETER){
        return(" X[" + lookUpTable.lookUpString(id) + "] ");             
      }else {
        throw new Exception("[error] Unknown type of ID in Expression. Cannot find: " + lookUpTable.lookUpString(id));           
      }
    }else if(expr instanceof ExprCap){
      ExprCap e = (ExprCap)expr; //downcast
      String sexp = "";
      if (e.getExpression2() instanceof ExprINT){
        sexp += " SUNRpowerI(";
      }else {
        sexp += " SUNRpowerR(";
      }
      sexp += writeExpression(e.getExpression1(),bw,mode,condID);
      sexp += ",";
      sexp += writeExpression(e.getExpression2(),bw,mode,condID);
      sexp += ") ";
      return sexp;
    }else if(expr instanceof ExprCos){
      ExprCos e = (ExprCos)expr; //downcast
      String sexp = "";
      sexp += " cos(";
      sexp += writeExpression(e.getExpression(),bw,mode,condID);
      sexp += ") ";
      return sexp;
    }else if(expr instanceof ExprDiv){
      ExprDiv e = (ExprDiv)expr; //downcast
      String sexp = "";
      sexp += " (";
      sexp += writeExpression(e.getExpression1(),bw,mode,condID);
      sexp += "/";
      sexp += writeExpression(e.getExpression2(),bw,mode,condID);
      sexp += ") ";
      return sexp;
    }else if(expr instanceof ExprDOUBLE){
      ExprDOUBLE e = (ExprDOUBLE)expr; //downcast
      return(" " + e.getDouble().doubleValue() + " ");
    }else if(expr instanceof ExprExp){
      ExprExp e = (ExprExp)expr; //downcast
      String sexp = "";
      sexp += " exp(";
      sexp += writeExpression(e.getExpression(),bw,mode,condID);
      sexp += ") ";
      return sexp;
    }else if(expr instanceof ExprIfThenElse){
      ExprIfThenElse e = (ExprIfThenElse)expr; //downcast
      String sexp = "";
      sexp += " (";
      sexp += "(";
      sexp += writeBExpression(e.getBExpression(),bw,mode,condID);
      sexp += ")?";
      sexp += writeExpression(e.getExpression1(),bw,mode,condID);
      sexp += ":";
      sexp += writeExpression(e.getExpression2(),bw,mode,condID);
      sexp += ") ";
      return sexp;
    }else if(expr instanceof ExprINT){
      ExprINT e = (ExprINT)expr; //downcast
      return(" " + e.getInteger().intValue() + " ");
    }else if(expr instanceof ExprTime){
      if (mode == 3){
        return(" t ");
      }else{
        return(" time ");
      }
    }else if(expr instanceof ExprLn){
      ExprLn e = (ExprLn)expr; //downcast
      String sexp = "";
      sexp += " log(";
      sexp += writeExpression(e.getExpression(),bw,mode,condID);
      sexp += ") ";
      return sexp;
    }else if(expr instanceof ExprNegation){
      ExprNegation e = (ExprNegation)expr; //downcast
      String sexp = "";
      sexp += " (";
      sexp += " - ";
      sexp += writeExpression(e.getExpression(),bw,mode,condID);
      sexp += ") ";
      return sexp;
    }else if(expr instanceof ExprPar){
			String sexp = "";
      sexp += " (";
      ExprPar e = (ExprPar)expr; //downcast
      sexp += writeExpression(e.getExpression(),bw,mode,condID);
      sexp += ") ";
      return sexp;
    }else if(expr instanceof ExprSin){
      ExprSin e = (ExprSin)expr; //downcast
      String sexp = "";
      sexp += " sin(";
      sexp += writeExpression(e.getExpression(),bw,mode,condID);
      sexp += ") ";
      return sexp;
    }else if(expr instanceof ExprSqrt){
      ExprSqrt e = (ExprSqrt)expr; //downcast
      String sexp = "";
      sexp += " SUNRsqrt(";
      sexp += writeExpression(e.getExpression(),bw,mode,condID);
      sexp += ") ";
      return sexp;
    }else if(expr instanceof ExprMinus){
      ExprMinus e = (ExprMinus)expr; //downcast
      String sexp = "";
      sexp += " (";
      sexp += writeExpression(e.getExpression1(),bw,mode,condID);
      sexp += "-";
      sexp += writeExpression(e.getExpression2(),bw,mode,condID);
      sexp += ") ";
      return sexp;
    }else if(expr instanceof ExprMul){
      ExprMul e = (ExprMul)expr; //downcast
      String sexp = "";
      sexp += " (";
      sexp += writeExpression(e.getExpression1(),bw,mode,condID);
      sexp += "*";
      sexp += writeExpression(e.getExpression2(),bw,mode,condID);
      sexp += ") ";
      return sexp;
    }else if(expr instanceof ExprPlus){
      ExprPlus e = (ExprPlus)expr; //downcast
      String sexp = "";
      sexp += " (";
      sexp += writeExpression(e.getExpression1(),bw,mode,condID);
      sexp += "+";
      sexp += writeExpression(e.getExpression2(),bw,mode,condID);
      sexp += ") ";
      return sexp;
    }else if(expr instanceof ExprFunCall){
      ExprFunCall e = (ExprFunCall)expr; //downcast
      String sexp = "";
      sexp += " fun_" + e.getID() + "(";
      Iterator<Expression> ite = e.getArguments().iterator();
      while (ite.hasNext()){
        Expression tmp = ite.next();      
        sexp += writeExpression(tmp,bw,mode,condID);
        if (ite.hasNext()) sexp += ",";
      }
      sexp += ") ";
      return sexp;
    }else if(expr instanceof ExprLocalPar){
      ExprLocalPar e = (ExprLocalPar)expr; //downcast
      return(" " + e.getID() + " ");
    }else{
      throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.writeExpression()). Unknown type of Expression.");
    }
  }
  
  private String writeBExpression(BExpression bexp, BufferedWriter bw, int mode, Integer condID) throws Exception{
    
    if (bexp instanceof BExprAND){
      BExprAND b = (BExprAND)bexp; //downcast
      String sexp = "";
      sexp += writeBExpression(b.getBExpression1(),bw,mode,condID);
      sexp += "&&";      
      sexp += writeBExpression(b.getBExpression2(),bw,mode,condID);
      return sexp;
    } else if (bexp instanceof BExprDiff){
      String sexp = "";
      BExprDiff b = (BExprDiff)bexp; //downcast
      sexp += writeExpression(b.getExpression1(),bw,mode,condID);
      sexp += "!=";
      sexp += writeExpression(b.getExpression2(),bw,mode,condID);
      return sexp;
    }else if (bexp instanceof BExprEqual){
      BExprEqual b = (BExprEqual)bexp; //downcast
      String sexp = "";
      sexp += writeExpression(b.getExpression1(),bw,mode,condID);
      sexp += "==";
      sexp += writeExpression(b.getExpression2(),bw,mode,condID);
      return sexp;
    }else if (bexp instanceof BExprGE){
      BExprGE b = (BExprGE)bexp; //downcast
      String sexp = "";
      sexp += writeExpression(b.getExpression1(),bw,mode,condID);
      sexp += ">=";
      sexp += writeExpression(b.getExpression2(),bw,mode,condID);
      return sexp;
    }else if (bexp instanceof BExprGT){
      BExprGT b = (BExprGT)bexp; //downcast
      String sexp = "";
      sexp += writeExpression(b.getExpression1(),bw,mode,condID);
      sexp += ">";
      sexp += writeExpression(b.getExpression2(),bw,mode,condID);
      return sexp;
    }else if (bexp instanceof BExprLE){
      BExprLE b = (BExprLE)bexp; //downcast
      String sexp = "";
      sexp += writeExpression(b.getExpression1(),bw,mode,condID);
      sexp += "<=";
      sexp += writeExpression(b.getExpression2(),bw,mode,condID);
      return sexp;
    }else if (bexp instanceof BExprLT){
      BExprLT b = (BExprLT)bexp; //downcast
      String sexp = "";
      sexp += writeExpression(b.getExpression1(),bw,mode,condID);
      sexp += "<";
      sexp += writeExpression(b.getExpression2(),bw,mode,condID);
      return sexp;
    }else if (bexp instanceof BExprOR){
      BExprOR b = (BExprOR)bexp; //downcast
      String sexp = "";
      sexp += writeBExpression(b.getBExpression1(),bw,mode,condID);
      sexp += "||";
      sexp += writeBExpression(b.getBExpression2(),bw,mode,condID);
      return sexp;
    }else if (bexp instanceof BExprPar){
      BExprPar b = (BExprPar)bexp; //downcast
      String sexp = "";
      sexp += " (";
      sexp += writeBExpression(b.getBExpression(),bw,mode,condID);
      sexp += ") ";
      return sexp;
    }else if (bexp instanceof BExprNot){
      BExprNot b = (BExprNot)bexp; //downcast
      String sexp = "";
      sexp += "!";
      sexp += writeBExpression(b.getBExpression(),bw,mode,condID);
      return sexp;
    }else if (bexp instanceof BExprFALSE){
      return(" true ");
    }else if (bexp instanceof BExprTRUE){
      return(" false ");
    }else{
      throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.writeBExpression()). Unknown type of BExpression.");
    }

  }
  
private String writeDerivative(Expression expr, Integer paramID, int mode,BufferedWriter bw) throws Exception, DerivativeZeroException{
     
    if (expr instanceof ExprID){
      ExprID e = (ExprID)expr; //downcast
      Integer id = e.getID();
      int type = QueryModelAndData.getIDType(entryLists,id);
      //add id to appropriate set if it is an id of an algebraic or differential or parameter entry
      if (type == GLSDCTool.ALGEBRAIC){
        if (mode == 1) {
          return(" var_" + lookUpTable.lookUpString(id) + "__der__" + lookUpTable.lookUpString(paramID) + " ");
        }else if (mode == 2) {
          int type2 = QueryModelAndData.getIDType(entryLists,paramID);
          if (type2 == GLSDCTool.DIFFERENTIAL){
            return(" var_" + lookUpTable.lookUpString(id) + "__der__ ");
          } else if (type2 == GLSDCTool.PARAMETER){
            return(" var_" + lookUpTable.lookUpString(id) + "__der__" + lookUpTable.lookUpString(paramID) + " ");
          }
        }
      }else if (type == GLSDCTool.DIFFERENTIAL){
        if (mode == 1) {
          if(paramID.equals(id)){  //it's a differential, let's see if it is the same
            return(" 1.0 ");    
          }else{
            int type2 = QueryModelAndData.getIDType(entryLists,paramID);
            if (type2 == GLSDCTool.DIFFERENTIAL){
              throw new DerivativeZeroException();
            } else if (type2 == GLSDCTool.PARAMETER){
              //bw.write(" Ith(yS[]," + lookUpTable.lookUpString(id) + ") ");
              throw new DerivativeZeroException();
            }
          }
        } else if(mode == 2) {
          int type2 = QueryModelAndData.getIDType(entryLists,paramID);
          if (type2 == GLSDCTool.DIFFERENTIAL){
            return(" " + lookUpTable.lookUpString(id) + "__der__ ");
          } else if (type2 == GLSDCTool.PARAMETER){
            //bw.write(" Ith(yS[]," + lookUpTable.lookUpString(id) + ") ");
            return(" " + lookUpTable.lookUpString(id) + "__der__" + lookUpTable.lookUpString(paramID) + " ");
          }
        }
      }else if (type == GLSDCTool.PARAMETER){
        if(paramID.equals(id)){  //it's a parameter, let's see if it is the same
          return(" 1.0 ");    
        }else{
          throw new DerivativeZeroException();
        }         
      }else {
        throw new Exception("[error] Unknown type of ID in Expression. Cannot find: " + lookUpTable.lookUpString(id));           
      }
    }else if(expr instanceof ExprLocalPar){
      //local parameter of a function
      ExprLocalPar e = (ExprLocalPar)expr; //downcast
      int type = QueryModelAndData.getIDType(entryLists,paramID);
      if (type == GLSDCTool.PARAMETER){
        return(" " + e.getID() + "__der__" + lookUpTable.lookUpString(paramID) + " ");
      }else if (type == GLSDCTool.DIFFERENTIAL){
        return(" " + e.getID() + "__der__ ");
      }
    }else if(expr instanceof ExprFunCall){
      //function call
      ExprFunCall e = (ExprFunCall)expr; //downcast
      String sexp = "";
      int type = QueryModelAndData.getIDType(entryLists,paramID);
      if (type == GLSDCTool.PARAMETER){
        sexp += " fun_" + e.getID() + "__der__" + lookUpTable.lookUpString(paramID) + "(";
      }else if (type == GLSDCTool.DIFFERENTIAL){
        sexp += " fun_" + e.getID() + "__der__(";
      }
      Iterator<Expression> ite = e.getArguments().iterator();
      while (ite.hasNext()){
        Expression tmp = ite.next();      
        sexp += writeExpression(tmp,bw,mode,null);
        //also the derivative
        sexp += ",";
        try{
					sexp += writeDerivative(tmp,paramID,mode,bw);
				} catch (DerivativeZeroException dze){
					sexp += " 0.0 ";
				}
        if (ite.hasNext()) sexp += ",";
      }
      sexp += ") ";
      return sexp;
    }else if(expr instanceof ExprCap){
      ExprCap e = (ExprCap)expr; //downcast
      String sexp = "";
      if (e.getExpression2() instanceof ExprINT){
				try{
					sexp += " (";
					sexp += writeExpression(e.getExpression2(),bw,mode,null);
					sexp += "* SUNRpowerI(";
					sexp += writeExpression(e.getExpression1(),bw,mode,null);
					sexp += ",";
					sexp += writeExpression(e.getExpression2(),bw,mode,null);
					sexp += "- 1) *";
					sexp += writeDerivative(e.getExpression1(),paramID,mode,bw);
					sexp += ") ";
				} catch (DerivativeZeroException dze){
					//return(" 0.0 ");
					throw new DerivativeZeroException(); //throw again, this is zero!
				}
      }else {
        // d/dx( f(x)^g(x) ) =
        // f(x)^g(x) * d/dx( g(x) ) * ln( f(x) )
        // + f(x)^( g(x)-1 ) * g(x) * d/dx( f(x) )
        // try to change as below, although it is the same function 
        // d/dx( f(x)^g(x) ) =
        // f(x)^g(x) * ( d/dx( f(x) )*g(x)/f(x) + d/dx( g(x) ) * ln( f(x) ) )
        // Check that if f is 0 then the derivative is 0
        
        int countZeros = 0;
        sexp += " (";
        sexp += "(";
        sexp += writeExpression(e.getExpression1(),bw,mode,null);
        sexp += " < ATOL)?";
        sexp += " 0.0 : ";   
         
        String tsexp = "";
        try{
					tsexp += " SUNRpowerR(";
					tsexp += writeExpression(e.getExpression1(),bw,mode,null);
					tsexp += ",";
					tsexp += writeExpression(e.getExpression2(),bw,mode,null);
					tsexp += ") *";
					tsexp += writeDerivative(e.getExpression2(),paramID,mode,bw);
					tsexp += " * log(";
					tsexp += writeExpression(e.getExpression1(),bw,mode,null);
					tsexp += ") ";
        } catch (DerivativeZeroException dze){
					//if derivative of expression2 is zero, this part is zero, ignore
					tsexp = "";
					countZeros++;
				}
				sexp += tsexp;
				
				tsexp = "";
				try{
					if (countZeros!=1) tsexp += "+";
					tsexp += " SUNRpowerR(";
					tsexp += writeExpression(e.getExpression1(),bw,mode,null);
					tsexp += ",";
					tsexp += writeExpression(e.getExpression2(),bw,mode,null);
					tsexp += "- 1) *";
					tsexp += writeExpression(e.getExpression2(),bw,mode,null);
					tsexp += "*";
					tsexp += writeDerivative(e.getExpression1(),paramID,mode,bw);
        } catch (DerivativeZeroException dze){
					//if derivative of expression1 is zero, this part is zero
					tsexp = "";
					countZeros++;
				}      
				sexp += tsexp;  
        sexp += ") ";
        //check, if both zeros then it's zero!
        if (countZeros==2) throw new DerivativeZeroException();
      }
			return sexp;
    }else if(expr instanceof ExprCos){
      //cos(f)' = -sin(f)*f'
			String sexp = "";
      try{
				ExprCos e = (ExprCos)expr; //downcast
				sexp += " (- sin(";
				sexp += writeExpression(e.getExpression(),bw,mode,null);
				sexp += ") *";
				sexp += writeDerivative(e.getExpression(),paramID,mode,bw);
				sexp += ") ";
			} catch (DerivativeZeroException dze){
				//if derivative of expression is zero, this part is zero
				throw new DerivativeZeroException();
			}
			return sexp;
    }else if(expr instanceof ExprDiv){
      //(f'*g - g'*f )/g^2
      String sexp = "";
      int countZeros = 0;
      ExprDiv e = (ExprDiv)expr; //downcast
      sexp += " ((";
      
      String tsexp = "";
      try{
				tsexp += " (";
				tsexp += writeDerivative(e.getExpression1(),paramID,mode,bw);
				tsexp += "*";
				tsexp += writeExpression(e.getExpression2(),bw,mode,null);
				tsexp += ") ";
      } catch (DerivativeZeroException dze){
				//derivative of exp1 is zero!
				tsexp = "";
				countZeros++;
			}
			sexp += tsexp;
			
      tsexp = "";
      try{			
				tsexp += "-";
				tsexp += " (";
				tsexp += writeDerivative(e.getExpression2(),paramID,mode,bw);
				tsexp += "*";
				tsexp += writeExpression(e.getExpression1(),bw,mode,null);
				tsexp += ")";
      } catch (DerivativeZeroException dze){
				//derivative of exp2 is zero!
				tsexp = "";
				countZeros++;
			}
			sexp += tsexp;
      
      sexp += ")/(SUNRpowerI(";
      sexp += writeExpression(e.getExpression2(),bw,mode,null);
      sexp += ",2))";
      
      sexp += ") ";
			//check, if both zeros then it's zero!
			if (countZeros==2) throw new DerivativeZeroException();
			return sexp;
    }else if(expr instanceof ExprDOUBLE){
      ExprDOUBLE e = (ExprDOUBLE)expr; //downcast
      throw new DerivativeZeroException();
    }else if(expr instanceof ExprExp){
      ExprExp e = (ExprExp)expr; //downcast
      String sexp = "";
      try{
				sexp += " (exp(";
				sexp += writeExpression(e.getExpression(),bw,mode,null);
				sexp += ") *";
				sexp += writeDerivative(e.getExpression(),paramID,mode,bw);
				sexp += ") ";
			} catch (DerivativeZeroException dze){
				//if derivative of expression is zero, this part is zero
				throw new DerivativeZeroException();
			}
			return sexp;
    }else if(expr instanceof ExprIfThenElse){
      ExprIfThenElse e = (ExprIfThenElse)expr; //downcast
      String sexp = "";
      int countZeros = 0;
      
      sexp += " (";
      sexp += "(";
      sexp += writeBExpression(e.getBExpression(),bw,mode,null);
      sexp += ")?";
      
      String tsexp = "";
      try{
				tsexp += writeDerivative(e.getExpression1(),paramID,mode,bw);
      }catch (DerivativeZeroException dze){
				tsexp += " 0.0 ";
				countZeros++;
			}
      sexp += tsexp;
      
      sexp += ":";
      
      tsexp = "";
      try{
				tsexp += writeDerivative(e.getExpression2(),paramID,mode,bw);
      }catch (DerivativeZeroException dze){
				tsexp += " 0.0 ";
				countZeros++;
			}
      sexp += tsexp;
      
      sexp += ") ";
      //check, if both zeros then it's zero!
			if (countZeros==2) throw new DerivativeZeroException();
			return sexp;
    }else if(expr instanceof ExprINT){
      ExprINT e = (ExprINT)expr; //downcast
      throw new DerivativeZeroException();
    }else if(expr instanceof ExprTime){
      throw new DerivativeZeroException();
    }else if(expr instanceof ExprLn){
      //ln(f)' = (1/f)*f'
      ExprLn e = (ExprLn)expr; //downcast
      String sexp = "";
      try{
				sexp += " (1/(";
				sexp += writeExpression(e.getExpression(),bw,mode,null);
				sexp += ") *";
				sexp += writeDerivative(e.getExpression(),paramID,mode,bw);
				sexp += ") ";
			}catch (DerivativeZeroException dze){
				//if derivative of expression is zero, this part is zero
				throw new DerivativeZeroException();
			}
			return sexp;
    }else if(expr instanceof ExprNegation){
      ExprNegation e = (ExprNegation)expr; //downcast
      String sexp = "";
      try{
				sexp += " (";
				sexp += " - ";
				sexp += writeDerivative(e.getExpression(),paramID,mode,bw);
				sexp += ") ";
			}catch (DerivativeZeroException dze){
				//if derivative of expression is zero, this part is zero
				throw new DerivativeZeroException();
			}
			return sexp;
    }else if(expr instanceof ExprPar){
			ExprPar e = (ExprPar)expr; //downcast
			String sexp = "";
			try{
				sexp += " (";
				sexp += writeDerivative(e.getExpression(),paramID,mode,bw);
				sexp += ") ";
			}catch (DerivativeZeroException dze){
				//if derivative of expression is zero, this part is zero
				throw new DerivativeZeroException();
			}
			return sexp;
    }else if(expr instanceof ExprSin){
      //sin(f)' = cos(f)*f'
      ExprSin e = (ExprSin)expr; //downcast
			String sexp = "";
			try{
				sexp += " (cos(";
				sexp += writeExpression(e.getExpression(),bw,mode,null);
				sexp += ") *";
				sexp += writeDerivative(e.getExpression(),paramID,mode,bw);
				sexp += ") ";
			}catch (DerivativeZeroException dze){
				//if derivative of expression is zero, this part is zero
				throw new DerivativeZeroException();
			}
			return sexp;
    }else if(expr instanceof ExprSqrt){
      //sqrt(f)' = (f')/(2*sqrt(f))
      ExprSqrt e = (ExprSqrt)expr; //downcast
			String sexp = "";
			try{
				sexp += " (";
				sexp += writeDerivative(e.getExpression(),paramID,mode,bw);
				sexp += ")/(2.0*SUNRsqrt(";
				sexp += writeExpression(e.getExpression(),bw,mode,null);
				sexp += ")) ";
			}catch (DerivativeZeroException dze){
				//if derivative of expression is zero, this part is zero
				throw new DerivativeZeroException();
			}
			return sexp;
    }else if(expr instanceof ExprMinus){
      ExprMinus e = (ExprMinus)expr; //downcast
      String sexp = "";
      int countZeros = 0;
      
      sexp += " (";
      
      String tsexp = "";
      try{
				tsexp += writeDerivative(e.getExpression1(),paramID,mode,bw);
			}catch (DerivativeZeroException dze){
				tsexp = "";
				countZeros++;
			}
			sexp += tsexp;
			
			tsexp = "";
			try{
				tsexp += "-";
				tsexp += writeDerivative(e.getExpression2(),paramID,mode,bw);
			}catch (DerivativeZeroException dze){
				tsexp = "";
				countZeros++;
			}
			sexp += tsexp;
				
      sexp += ") ";
      
      //check, if both zeros then it's zero!
			if (countZeros==2) throw new DerivativeZeroException();
			return sexp;
    }else if(expr instanceof ExprMul){
      //(f'*g + g'*f )
      ExprMul e = (ExprMul)expr; //downcast
      String sexp = "";
      int countZeros = 0;
      
      sexp += " (";
      
      String tsexp = "";
      try{
				tsexp += " (";
				tsexp += writeDerivative(e.getExpression1(),paramID,mode,bw);
				tsexp += "*";
				tsexp += writeExpression(e.getExpression2(),bw,mode,null);
				tsexp += ") ";
      }catch (DerivativeZeroException dze){
				tsexp = "";
				countZeros++;
			}
			sexp += tsexp;
			
			tsexp = "";
      try{
				if (countZeros != 1) tsexp += "+";
				tsexp += " (";
				tsexp += writeDerivative(e.getExpression2(),paramID,mode,bw);
				tsexp += "*";
				tsexp += writeExpression(e.getExpression1(),bw,mode,null);
				tsexp += ") ";
      }catch (DerivativeZeroException dze){
				tsexp = "";
				countZeros++;
			}
			sexp += tsexp;      
      
      sexp += ") ";
      
      //check, if both zeros then it's zero!
			if (countZeros==2) throw new DerivativeZeroException();
			return sexp;
    }else if(expr instanceof ExprPlus){
      ExprPlus e = (ExprPlus)expr; //downcast
      String sexp = "";
      int countZeros = 0;
      
			sexp += " (";
      
      String tsexp = "";
      try{
				tsexp += writeDerivative(e.getExpression1(),paramID,mode,bw);
			}catch (DerivativeZeroException dze){
				tsexp = "";
				countZeros++;
			}
			sexp += tsexp;
			
			tsexp = "";
      try{
				if (countZeros != 1) tsexp += "+";
				tsexp += writeDerivative(e.getExpression2(),paramID,mode,bw);
			}catch (DerivativeZeroException dze){
				tsexp = "";
				countZeros++;
			}
			sexp += tsexp;
			sexp += ") ";     
			
			//check, if both zeros then it's zero!
			if (countZeros==2) throw new DerivativeZeroException();
			return sexp;
    }else{
      throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.writeDerivative()). Unknown type of Expression.");
    }
    
    return ""; //so the compiler does not complain
  }

  
  private void printMYFitnessFunction(int optimiser,Set<Integer> conditionsToSimulate, Map<Integer,Set<Integer>> signatures, BufferedWriter bw) throws Exception{
    
    if (optimiser == GLSDC){
      bw.write("int my_fitness(int pdim, double *params, double *fit, double *fit2)\n{\n");
    } else if (optimiser == LEVMAR_FD){
      bw.write("int my_fitness(int pdim, double *params, double *fit, double *fit2, double* hx)\n{\n");
    } else if (optimiser == LEVMAR_SE){
      bw.write("int my_fitness(int pdim, double *params, double *fit, double *fit2, double* hx, double* p, int NP)\n{\n");
    } else if (optimiser == LEVMAR_SE_JAC){
      bw.write("int my_jac_fitness(int pdim, double *params, double* jac, double* p, int NP)\n{\n");
    } else {
      throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printMYFitnessFunction()). Unknown optimisation algorithm");
    }
    
    //----------- preliminaries
    
    bw.write("  int rv = 0;\n"
          + "  const int neq = M_n;\n"
          //+ "  realtype ropt[OPT_SIZE] = { 0 };\n"
          //+ "  long iopt[OPT_SIZE] = { 0 };\n"
          + "  realtype reltol = RTOL;\n"
          + "  void *cvode_mem = NULL;\n"
          + "  realtype t;\n"
          + "  int i;\n");
    if (optimiser == LEVMAR_FD || optimiser == LEVMAR_SE || optimiser == LEVMAR_SE_JAC){
      bw.write("  int hxpos;\n");
    }
    bw.write("\n");
    //chech if we are using loglikelihood and what type
    int typeOfLL = fittingOptions.getLogLikelihood();
    
    if (optimiser == LEVMAR_SE || optimiser == LEVMAR_SE_JAC){
      //overwrite NP, so that we do not compute derivative of model variables with respect to
      //parameters of the log likelihood function (the sigma parameters)
      //we know those derivatives are 0
      if (typeOfLL == LogLikelihoodTool.IndependentNoise || typeOfLL == LogLikelihoodTool.LinearModelOfNoise) {
        bw.write("  //overwrite NP, so that we do not compute derivative of model variables with respect to\n"
            + "  //parameters of the log likelihood function (the sigma parameters)\n"
            + "  //we know those derivatives are 0\n"
            + "  NP = " + QueryModelAndData.getParametersToFit(entryLists).size() + "; //number of params to fit, excluding log likelihood additional parameters\n");
      }
      bw.write("  //instantiate user data and copy parameters\n"
      + "  UserData data;\n"
      + "  data = (UserData) malloc(sizeof *data);\n"
      + "  data->p = (realtype *) malloc(NP*sizeof(realtype));\n"
      + "  for (int iNP=0;iNP<NP;iNP++) data->p[iNP]=exp(p[iNP]);\n\n");
    }
    //--------------------------
    
    //retrieve the DataNodeReplicates of the speciesID/conditions that need to be fitted
    Set<DataNodeReplicates> dataNodesSet = new HashSet<DataNodeReplicates>();
    Iterator<Integer> ite = fittingOptions.getIDCondList().keySet().iterator();
    while (ite.hasNext()){
      Integer key = ite.next();
      
      LinkedList<Integer> listOfConds = fittingOptions.getIDCondList().get(key);
      
      Iterator<Integer> ite2 = listOfConds.iterator();
      while (ite2.hasNext()){
        Integer cond = ite2.next();
        DataNodeReplicates dnr1 = QueryModelAndData.getDataNodeReplicatesOf(entryLists,key,cond);
        if (dnr1 == null) {
          throw new Exception("[error] No available data for fitting variable " + lookUpTable.lookUpString(key) + ", condition " + lookUpTable.lookUpString(cond));  
        }
        dataNodesSet.add(dnr1);
      }
      
    }
    //---------------------------
    
    //------ print simulations data structures
    bw.write("  //data structures to store simulations\n");
    bw.write("  //need also a time point counter for each data structure used to store simulations\n"
            + "  //and a scaling factor if a normalisation is necessary\n");
    
    Iterator<DataNodeReplicates> ite3 = dataNodesSet.iterator();
    while (ite3.hasNext()){
      DataNodeReplicates dnr = ite3.next();       
      String uniqueID = QueryModelAndData.buildUniqueIDFrom(lookUpTable,(DataNode)dnr);
      
      bw.write("  " + uniqueID + "struct " + uniqueID + "sims[" + uniqueID + "ntimepoints];\n"
          + "  int currentTimePoint" + uniqueID + ";\n");
      if (optimiser == LEVMAR_SE_JAC){ //jacobian, store sensitivities
        bw.write("  //struct for storing sensitivities\n");
        bw.write("  " + uniqueID + "struct " + uniqueID + "sens[NP][" + uniqueID + "ntimepoints];\n");
      }
      NormalisationNode nnode = getNormalisationNodeOf(dnr);
        
      if (nnode != null){ //there is a normalisation for the DataNodeReplicates, so also for the simulations
        bw.write("  realtype scaling" + uniqueID + " = 0;\n");
        
        //may need data structures to store the derivative of the scaling factors if computing jacobian
        if (optimiser == LEVMAR_SE_JAC){
          bw.write("  //for derivative of sim scaling\n");
          bw.write("  realtype scaling" + uniqueID + "_der[NP];\n");
          bw.write("  for(int is=0;is<NP;is++) scaling" + uniqueID + "_der[is] = 0;\n");
        }
        
        if(nnode instanceof NormalisationNodeAverage){  //need additional data structure for normalisation by average
          NormalisationNodeAverage ns = (NormalisationNodeAverage)nnode;
          
          //iterate the boolean arrays containing the indication of which 
          //time points to sum for the corresponding condition
          
          Iterator<Boolean[]> iteBoolArrays = ns.getTimePointsToSum().iterator();
          Iterator<Integer> iteConds = ns.getConditions().iterator();
          while (iteBoolArrays.hasNext() && iteConds.hasNext()){ //should have same length
            Boolean[] boolArray = iteBoolArrays.next();
            Integer cond = iteConds.next();
            
            bw.write("  bool toSum" + lookUpTable.lookUpString(dnr.getIdentifier()) + "c_" + cond.intValue() + "[" + uniqueID + "ntimepoints] = { ");
            for (int j = 0;j<boolArray.length-1;j++) bw.write(boolArray[j].toString() + ", ");
            bw.write(boolArray[boolArray.length-1].toString() + " }; // " + lookUpTable.lookUpString(cond) + "\n");
            
          }
          
        }
      }
    }
    bw.write("\n");
    //------ done printing simulations data structures
    
    
    //other initialisations
    bw.write("  //**************************************************Init\n"
        + "  y_zero = params;\n"
        + "  X = new double[K_n];\n"
        + "  realtype t0 = INITIALTIME;\n"
        + "  N_Vector abstol = make_atol(neq, ATOL);\n\n");
        
    //extra parameter for the loglikelihood function
    if (typeOfLL == LogLikelihoodTool.IndependentNoise || typeOfLL == LogLikelihoodTool.LinearModelOfNoise) {
      bw.write("  //additional parameters for the loglikelihood function \n");
      //bw.write("  double sumOfLogSigmas = 0;\n  double currentSigma = 0;\n\n"); //only need current sigma
      bw.write("  double currentSigma = 0;\n\n");
    }
    bw.write("  //vectors for CVODE solver\n");
    Iterator<Integer> ite4 = conditionsToSimulate.iterator();
    while (ite4.hasNext()){
      Integer tmp = ite4.next();
      bw.write("  N_Vector y_c_" + tmp.intValue() + " = make_vector(neq, y_zero); // " + lookUpTable.lookUpString(tmp) + "\n");
      if (optimiser == LEVMAR_SE_JAC) {
        bw.write("  N_Vector* uS_c_" + tmp.intValue() + " = N_VCloneVectorArray_Serial(NP, y_c_" + tmp.intValue() + ");\n");
        bw.write("  for(int is=0;is<NP;is++) N_VConst(0.0,uS_c_" + tmp.intValue() + "[is]);\n");
      }
    }
    bw.write("\n");
    
    //if (optimiser == LEVMAR_FD || optimiser == GLSDC){
    bw.write("  int condition;\n");
    //} 
      

    //----------- SIMULATE ALL NECESSARY CONDITIONS
    
    Iterator<Integer> ite5 = conditionsToSimulate.iterator();  //for all conditions
    while (ite5.hasNext()){
      Integer currentCondition = ite5.next();
      
      bw.write("  //****************************** Simulate " + lookUpTable.lookUpString(currentCondition) + "\n\n");
      
      //reset triggers
      printEventTriggeredReset(bw);
      
      Iterator<DataNodeReplicates> ite6 = dataNodesSet.iterator();
      while (ite6.hasNext()){
        DataNodeReplicates dnr = ite6.next();       
        String uniqueID = QueryModelAndData.buildUniqueIDFrom(lookUpTable,(DataNode)dnr);
        
        bw.write("  currentTimePoint" + uniqueID + " = 0;\n");
          
      }
      bw.write("\n");
      bw.write("  condition = c_" + currentCondition.intValue() + ";\n\n");
      if (optimiser == LEVMAR_SE || optimiser == LEVMAR_SE_JAC){
        bw.write("  data->condition = c_" + currentCondition.intValue() + ";\n\n");
      }
      bw.write("  memcpy(X,params + M_n,K_n*sizeof(double));  //copy params in X\n\n");
      
      //--- set initial conditions
      boolean initSens = (optimiser == LEVMAR_SE_JAC);
      QueryModelAndData.printInitialConditionsForCondition(entryLists,lookUpTable,initSens,currentCondition,bw);
      
      bw.write("    cvode_mem = CVodeCreate(CV_BDF,CV_NEWTON);\n");
      if (optimiser == LEVMAR_FD || optimiser == GLSDC){
        bw.write("    CVodeSetUserData(cvode_mem, &condition);\n");
      } else if (optimiser == LEVMAR_SE || optimiser == LEVMAR_SE_JAC){
        bw.write("    CVodeSetUserData(cvode_mem, data);\n");
      }
      bw.write("    CVodeSetMaxNumSteps(cvode_mem, 30000);\n"
            + "    CVodeInit(cvode_mem,__F__,t0,y_c_" + currentCondition.intValue() + ");\n"
            + "    CVodeSStolerances(cvode_mem,RTOL,ATOL);\n"
            + "    CVDense(cvode_mem,neq);\n"
            + "    CVDlsSetDenseJacFn(cvode_mem, __Jac__);\n");
      if (optimiser == LEVMAR_SE_JAC){ 
        bw.write("    //in addition, for sensitivity calculation\n"
        + "    CVodeSensInit(cvode_mem, NP, CV_SIMULTANEOUS, __fS__, uS_c_" + currentCondition.intValue() + ");\n"
        + "    //CVodeSetSensParams(cvode_mem, data->p, NULL, NULL); //commented out, we now provide sensitivity RHS\n"
        + "    CVodeSensEEtolerances(cvode_mem);\n\n");
      }
      bw.write("  if (!isproper(neq, y_c_" + currentCondition.intValue() + ")) { rv = 1; goto end; }\n\n");
      
      //before entering simulation loop, check if we need to store initial conditions
      //this in case data time point at time 0 have been specified
      Iterator<DataNodeReplicates> ite7 = dataNodesSet.iterator();
      while (ite7.hasNext()){
        DataNodeReplicates dnr = ite7.next();       
        
        if (dnr.getConditions().contains(currentCondition)){
          String uniqueID = QueryModelAndData.buildUniqueIDFrom(lookUpTable,(DataNode)dnr);
          //up to here we have identified which data structures are relevant
          //for this condition
          
          bw.write("  if (" + uniqueID + "data[0][currentTimePoint" + uniqueID + "].time <= t0){\n"
                + "    " + uniqueID + "sims[currentTimePoint" + uniqueID + "].c_" + currentCondition.intValue() + " = ");
          
          
          int type = QueryModelAndData.getIDType(entryLists,dnr.getIdentifier());
          if(type==ALGEBRAIC){
            //type is algebraic, need to make a call
            
            bw.write(lookUpTable.lookUpString(dnr.getIdentifier()) + "(t0,condition");
      
            Set<Integer> s = signatures.get(dnr.getIdentifier());
            if (s != null){
              Iterator<Integer> ite8 = s.iterator();
              while (ite8.hasNext()){
                Integer par = ite8.next();
                bw.write(",NV_Ith_S(y_c_" + currentCondition.intValue() + "," + lookUpTable.lookUpString(par) + ")"); 
              }
            } 
            bw.write(");\n");
            
          }else if (type==DIFFERENTIAL){
            bw.write("NV_Ith_S(y_c_" + currentCondition.intValue() + "," + lookUpTable.lookUpString(dnr.getIdentifier()) + ");\n");
          }else{
            throw new Exception("[error] there is no Algebraic nor Differential entry for the fitting of " + lookUpTable.lookUpString(dnr.getIdentifier())
                        + " condition " + lookUpTable.lookUpString(currentCondition));
          }
          
          //Now in case we have to retrieve the sensitivities (jacobian w.r.t. parameters):
          if (optimiser == LEVMAR_SE_JAC){ 
            int nitep = 0;
            Iterator<Integer> iteP = QueryModelAndData.getParametersToFit(entryLists).iterator();
            while (iteP.hasNext()){ //begin iteration of parameters to fit
              Integer paramID = iteP.next();
              
              bw.write("    " + uniqueID + "sens[" + nitep + "][currentTimePoint" + uniqueID + "].c_" + currentCondition.intValue() + " = ");
              
              //NOTE: if the requested object is a differential equation, no problem
              //BUT if it is an algebraic equation we need to find the derivative!
              //for example when fitting scaling factors, the derivative y' w.r.t. to
              //the scaling factor s of y=s*x will be y'=s'*x+s*x'=x
              
              if(type==ALGEBRAIC){
                //type is algebraic, need to make a call
                
                bw.write(lookUpTable.lookUpString(dnr.getIdentifier()) + "__der__" + lookUpTable.lookUpString(paramID) + "(t0,condition");
          
                Set<Integer> s = signatures.get(dnr.getIdentifier());
                if (s != null){
                  Iterator<Integer> ite8 = s.iterator();
                  while (ite8.hasNext()){
                    Integer par = ite8.next();
                    //first the non derivative and then the derivative
                    bw.write(",NV_Ith_S(y_c_" + currentCondition.intValue() + "," + lookUpTable.lookUpString(par) + ")");
                    bw.write(",NV_Ith_S(uS_c_" + currentCondition.intValue() + "[" + nitep + "]," + lookUpTable.lookUpString(par) + ")"); 
                  }
                } 
                bw.write(");\n");
                
              }else if (type==DIFFERENTIAL){
                bw.write("NV_Ith_S(uS_c_" + currentCondition.intValue() + "[" + nitep + "]," + lookUpTable.lookUpString(dnr.getIdentifier()) + ");\n");
              }else{
                throw new Exception("[error] there is no Algebraic nor Differential entry for the fitting of " + lookUpTable.lookUpString(dnr.getIdentifier())
                            + " condition " + lookUpTable.lookUpString(currentCondition));
              }
              nitep++;
            }//-------------------end of iteration of parameters to fit (and thus to compute derivative)
            
            bw.write("\n");
          }//-----------end of if (optimiser == LEVMAR_SE_JAC)
          
          bw.write("    currentTimePoint" + uniqueID + "++;\n"
                + "  }\n");
          
        }
      }      
      
      bw.write("\n");
      
      //---------- enter main simulation loop
      
      bw.write("  for (i = 1; i <= TIMESTEPS; i++) {\n"
            + "    int flag = CVode(cvode_mem, t0 + i*(FINALTIME - t0)/TIMESTEPS, y_c_" + currentCondition.intValue() + ", &t, CV_NORMAL);\n");
      //bw.write("    if (flag != CV_SUCCESS) { rv = 1; goto end; }\n\n"); 
             
      bw.write("    if (flag != CV_SUCCESS) { \n"
      + "      if (flag == CV_TSTOP_RETURN) { \n"
      + "        fprintf(stderr,\"CV_TSTOP_RETURN\\n\");\n"
      + "      }else if (flag == CV_ROOT_RETURN) { \n"
      + "        fprintf(stderr,\"CV_ROOT_RETURN\\n\");\n"
      + "      }else if (flag == CV_MEM_NULL) { \n"
      + "        fprintf(stderr,\"CV_MEM_NULL\\n\");\n"
      + "      }else if (flag == CV_NO_MALLOC) { \n"
      + "        fprintf(stderr,\"CV_NO_MALLOC\\n\");\n"
      + "      }else if (flag == CV_ILL_INPUT) { \n"
      + "        fprintf(stderr,\"CV_ILL_INPUT\\n\");\n"
      + "      }else if (flag == CV_TOO_CLOSE) { \n"
      + "        fprintf(stderr,\"CV_TOO_CLOSE\\n\");\n"
      + "      }else if (flag == CV_TOO_MUCH_WORK) { \n"
      + "        fprintf(stderr,\"CV_TOO_MUCH_WORK\\n\");\n"
      + "      }else if (flag == CV_TOO_MUCH_ACC) { \n"
      + "        fprintf(stderr,\"CV_TOO_MUCH_ACC\\n\");\n"
      + "      }else if (flag == CV_ERR_FAILURE) { \n"
      + "        fprintf(stderr,\"CV_ERR_FAILURE\\n\");\n"
      + "      }else if (flag == CV_CONV_FAILURE) { \n"
      + "        fprintf(stderr,\"CV_CONV_FAILURE\\n\");\n"
      + "      }else if (flag == CV_LINIT_FAIL) { \n"
      + "        fprintf(stderr,\"CV_LINIT_FAIL\\n\");\n"
      + "      }else if (flag == CV_LSETUP_FAIL) { \n"
      + "        fprintf(stderr,\"CV_LSETUP_FAIL\\n\");\n"
      + "      }else if (flag == CV_LSOLVE_FAIL) { \n"
      + "        fprintf(stderr,\"CV_LSOLVE_FAIL\\n\");\n"
      + "      }else if (flag == CV_RHSFUNC_FAIL) { \n"
      + "        fprintf(stderr,\"CV_RHSFUNC_FAIL\\n\");\n"
      + "      }else if (flag == CV_FIRST_RHSFUNC_ERR) { \n"
      + "        fprintf(stderr,\"CV_FIRST_RHSFUNC_ERR\\n\");\n"
      + "      }else if (flag == CV_REPTD_RHSFUNC_ERR) { \n"
      + "        fprintf(stderr,\"CV_REPTD_RHSFUNC_ERR\\n\");\n"
      + "      }else if (flag == CV_UNREC_RHSFUNC_ERR) { \n"
      + "        fprintf(stderr,\"CV_UNREC_RHSFUNC_ERR\\n\");\n"
      + "      }\n"
      + "      rv = 1; goto end; \n"
      + "    }\n\n");

      
      if (optimiser == LEVMAR_SE_JAC) bw.write("    CVodeGetSens(cvode_mem, &t, uS_c_" + currentCondition.intValue() + ");\n\n");
    
      //-------- Store simulations similarly as above (t instead of t0) and more indentation
    
      Iterator<DataNodeReplicates> ite9 = dataNodesSet.iterator();
      while (ite9.hasNext()){
        DataNodeReplicates dnr = ite9.next();       
        
        if (dnr.getConditions().contains(currentCondition)){
          String uniqueID = QueryModelAndData.buildUniqueIDFrom(lookUpTable,(DataNode)dnr);
          //up to here we have identified which data structures are relevant
          //for this condition
          
          bw.write("    if (" + uniqueID + "data[0][currentTimePoint" + uniqueID + "].time <= t && currentTimePoint" + uniqueID + " < " + uniqueID + "ntimepoints){\n"
                + "      " + uniqueID + "sims[currentTimePoint" + uniqueID + "].c_" + currentCondition.intValue() + " = ");
          
          
          int type = QueryModelAndData.getIDType(entryLists,dnr.getIdentifier());
          if(type==ALGEBRAIC){
            //type is algebraic, need to make a call
            
            bw.write(lookUpTable.lookUpString(dnr.getIdentifier()) + "(t,condition");
      
            Set<Integer> s = signatures.get(dnr.getIdentifier());
            if (s != null){
              Iterator<Integer> ite10 = s.iterator();
              while (ite10.hasNext()){
                Integer par = ite10.next();
                bw.write(",NV_Ith_S(y_c_" + currentCondition.intValue() + "," + lookUpTable.lookUpString(par) + ")"); 
              }
            } 
            bw.write(");\n");
            
          }else if (type==DIFFERENTIAL){
            bw.write("NV_Ith_S(y_c_" + currentCondition.intValue() + "," + lookUpTable.lookUpString(dnr.getIdentifier()) + ");\n");
          }else{
            throw new Exception("[error] there is no Algebraic nor Differential entry for the fitting of " + lookUpTable.lookUpString(dnr.getIdentifier())
                        + " condition " + lookUpTable.lookUpString(currentCondition));
          }
          
          //Now in case we have to retrieve the sensitivities (jacobian w.r.t. parameters):
          if (optimiser == LEVMAR_SE_JAC){ 
            int nitep = 0;
            Iterator<Integer> iteP = QueryModelAndData.getParametersToFit(entryLists).iterator();
            while (iteP.hasNext()){ //begin iteration of parameters to fit
              Integer paramID = iteP.next();
              
              bw.write("      " + uniqueID + "sens[" + nitep + "][currentTimePoint" + uniqueID + "].c_" + currentCondition.intValue() + " = ");
              
              //NOTE: if the requested object is a differential equation, no problem
              //BUT if it is an algebraic equation we need to find the derivative!
              //for example when fitting scaling factors, the derivative y' w.r.t. to
              //the scaling factor s of y=s*x will be y'=s'*x+s*x'=x
              
              if(type==ALGEBRAIC){
                //type is algebraic, need to make a call
                
                bw.write(lookUpTable.lookUpString(dnr.getIdentifier()) + "__der__" + lookUpTable.lookUpString(paramID) + "(t,condition");
          
                Set<Integer> s = signatures.get(dnr.getIdentifier());
                if (s != null){
                  Iterator<Integer> ite8 = s.iterator();
                  while (ite8.hasNext()){
                    Integer par = ite8.next();
                    //first the non derivative and then the derivative
                    bw.write(",NV_Ith_S(y_c_" + currentCondition.intValue() + "," + lookUpTable.lookUpString(par) + ")");
                    bw.write(",NV_Ith_S(uS_c_" + currentCondition.intValue() + "[" + nitep + "]," + lookUpTable.lookUpString(par) + ")"); 
                  }
                } 
                bw.write(");\n");
                
              }else if (type==DIFFERENTIAL){
                bw.write("NV_Ith_S(uS_c_" + currentCondition.intValue() + "[" + nitep + "]," + lookUpTable.lookUpString(dnr.getIdentifier()) + ");\n");
              }else{
                throw new Exception("[error] there is no Algebraic nor Differential entry for the fitting of " + lookUpTable.lookUpString(dnr.getIdentifier())
                            + " condition " + lookUpTable.lookUpString(currentCondition));
              }
              nitep++;
            }//-------------------end of iteration of parameters to fit (and thus to compute derivative)
            
            bw.write("\n");
          }//-----------end of if (optimiser == LEVMAR_SE_JAC)          
          
          bw.write("      currentTimePoint" + uniqueID + "++;\n"
                + "    }\n");
          
        }
      }      
      
      bw.write("\n");
      
      //-----------events
      //reinit sensitivities if event is triggered, only when computing sensitivities
      printEvents((optimiser == LEVMAR_SE_JAC),bw,currentCondition,signatures);
    
      //----------- closing the loop
    
      bw.write("    if (!isproper(neq, y_c_" + currentCondition.intValue() + ")) { rv = 1; goto end; }\n"
            + "    }\n");
      
      bw.write("  if (cvode_mem != NULL) CVodeFree(&cvode_mem);\n"
            + "  cvode_mem = NULL;\n");
    }
    
    
    bw.write("\n");
    
    bw.write("  delete[] X;\n\n");
    
    //----------- DONE SIMULATING
    
    
    //----------- Normalisation of the Simulations
    
    bw.write("  //--- Done with the simulations, now normalise simulations if necessary\n\n"); 
    
    Iterator<DataNodeReplicates> ite11 = dataNodesSet.iterator();
    while (ite11.hasNext()){
      DataNodeReplicates dnr = ite11.next();
      
      //check if dnr has an associated normalisation
      NormalisationNode nnode = getNormalisationNodeOf(dnr);
        
      if (nnode != null){ //there is a normalisation for the DataNodeReplicates, so also for the simulations
        //apply nnode normalisation to the corresponding simulations
        //get unique id
        String uniqueID = QueryModelAndData.buildUniqueIDFrom(lookUpTable,(DataNode)dnr);
        
        if (nnode instanceof NormalisationNodeFixedPoint){
          NormalisationNodeFixedPoint nfp = (NormalisationNodeFixedPoint)nnode;
          
          bw.write("  //compute scaling" + uniqueID + " (fixedpoint normalisation)\n");
          bw.write("  scaling" + uniqueID + " = " + uniqueID + "sims[" + nfp.getTimePointPosition() + "].c_" 
                + nfp.getCondition().intValue() + "; // " + lookUpTable.lookUpString(nfp.getCondition()) 
                + " at time " + dnr.getListOfDataMatrices().getFirst()[nfp.getTimePointPosition()][0] + "\n");
          if (optimiser == LEVMAR_SE_JAC){
            bw.write("  //also derivative of scaling\n");
            bw.write("  for (int iNS = 0;iNS<NP;iNS++){\n");
            bw.write("    scaling" + uniqueID + "_der[iNS] = " + uniqueID + "sens[iNS][" + nfp.getTimePointPosition() + "].c_" 
                + nfp.getCondition().intValue() + ";\n");
            bw.write("  }\n");
          }
          bw.write("  \n");
          
        }else if (nnode instanceof NormalisationNodeAverage){
          NormalisationNodeAverage ns = (NormalisationNodeAverage)nnode;
                  
          bw.write("  //compute scaling" + uniqueID + " (normalisation by average)\n");  
          
          //~ //iterate the boolean arrays containing the indication of which 
          //~ //time points to sum for the corresponding condition
          //~ 
          //~ Iterator<Boolean[]> iteBoolArrays = ns.getTimePointsToSum().iterator();
          //~ Iterator<Integer> iteConds = ns.getConditions().iterator();
          //~ while (iteBoolArrays.hasNext() && iteConds.hasNext()){ //should have same length
            //~ Boolean[] boolArray = iteBoolArrays.next();
            //~ Integer cond = iteConds.next();
            //~ 
            //~ bw.write("  bool toSum" + lookUpTable.lookUpString(dnr.getIdentifier()) + "c_" + cond.intValue() + "[" + uniqueID + "ntimepoints] = { ");
            //~ for (int j = 0;j<boolArray.length-1;j++) bw.write(boolArray[j].toString() + ", ");
            //~ bw.write(boolArray[boolArray.length-1].toString() + " }; // " + lookUpTable.lookUpString(cond) + "\n");
            //~ 
          //~ }
          
          
          bw.write("  for(i=0;i<" + uniqueID + "ntimepoints;i++){\n");    
          Iterator<Integer> iteConds = ns.getConditions().iterator();
          while (iteConds.hasNext()){
            Integer cond = iteConds.next();
            bw.write("    if (toSum" + lookUpTable.lookUpString(dnr.getIdentifier()) + "c_" + cond.intValue() + "[i]) {\n"
                    +"      scaling" + uniqueID + " += " + uniqueID + "sims[i].c_" + cond.intValue() + ";// " + lookUpTable.lookUpString(cond) + "\n");
            if (optimiser == LEVMAR_SE_JAC){
              bw.write("      //also derivative of scaling\n");
              bw.write("      for (int iNS = 0;iNS<NP;iNS++){\n");
              bw.write("        scaling" + uniqueID + "_der[iNS] += " + uniqueID + "sens[iNS][i].c_" 
                  + cond.intValue() + ";\n");
              bw.write("      }\n");
            }
            bw.write("    }\n");
          }
             
          bw.write("  }\n\n");
          bw.write("  scaling" + uniqueID + " /= " + ns.getNumberOfDataPointsInTheSum() +".0;// divide by number of points in the sum\n\n");
          if (optimiser == LEVMAR_SE_JAC){
              bw.write("  for (int iNS = 0;iNS<NP;iNS++){\n");
              bw.write("    scaling" + uniqueID + "_der[iNS] /= " + ns.getNumberOfDataPointsInTheSum() +".0;// divide by number of points in the sum\n");
              bw.write("  }\n\n");
          }
          
        }else{
          throw new Exception("[error] probably a coding error: Unknown type of NormalisationNode in method GLSDC.printMYFitnessFunction()");
        }
        
        //write normalisation procedure for the current simulations (normalise a condition only if it is a simulated condition
        //i.e. it belongs to conditionsToSimulate)
        bw.write("  for(i=0;i<" + uniqueID + "ntimepoints;i++){\n");
        
        Iterator<Integer> ite12 = dnr.getConditions().iterator();
        if (ite12.hasNext()) ite12.next(); //skip time
        while (ite12.hasNext()){
          Integer cond = ite12.next();
          if (conditionsToSimulate.contains(cond)){
            if (optimiser == LEVMAR_SE_JAC){
              //if we are computing the jacobian, then compute the derivative of the normalised system
              bw.write("    for (int iNS = 0;iNS<NP;iNS++){\n");
              bw.write("      " + uniqueID + "sens[iNS][i].c_"+ cond.intValue() 
                      + " = (" + uniqueID + "sens[iNS][i].c_"+ cond.intValue() 
                      + "*scaling" + uniqueID + " - " + uniqueID + "sims[i].c_"+ cond.intValue() 
                      + "*scaling" + uniqueID + "_der[iNS])/SUNRpowerI(scaling" + uniqueID + ",2); // " + lookUpTable.lookUpString(cond) + "\n");
              bw.write("    }\n");
            } 
            //else if (optimiser == GLSDC || optimiser == LEVMAR_FD || optimiser == LEVMAR_SE){
            bw.write("    " + uniqueID + "sims[i].c_"+ cond.intValue() +" /= scaling" + uniqueID + "; // " + lookUpTable.lookUpString(cond) + "\n");
            //}
          }
          
        }
        
        bw.write("  }\n\n");
        
      }
            
    }
    
    //----------- done with Normalisation of the Simulations
    
    //----------- WRITE FIT FUNCTION
 
    bw.write("  //--- Compute fit function\n\n"); 
    
    if (optimiser == LEVMAR_FD || optimiser == LEVMAR_SE || optimiser == GLSDC){
       bw.write("  *fit = 0;\n");
       bw.write("  *fit2 = 0;\n\n");
    }
    if (optimiser == LEVMAR_FD || optimiser == LEVMAR_SE || optimiser == LEVMAR_SE_JAC) bw.write("  hxpos = 0;\n");
    //for all species to fit
    Iterator<Integer> ite20 = fittingOptions.getIDCondList().keySet().iterator();
    while (ite20.hasNext()){
      Integer speciesID = ite20.next();
      LinkedList<Integer> listOfConditions = fittingOptions.getIDCondList().get(speciesID);
      
      //for all conditions to fit of speciesID
      Iterator<Integer> ite21 = listOfConditions.iterator();
      while (ite21.hasNext()){
        Integer cond = ite21.next();
        
        //retrive dataNode for this speciesID and condition and build uniqueID
        DataNodeReplicates dnr = QueryModelAndData.getDataNodeReplicatesOf(entryLists,speciesID,cond);
        
        if (dnr != null){
          
          String uniqueID = QueryModelAndData.buildUniqueIDFrom(lookUpTable,(DataNode)dnr);
          
          LinkedList<Double[][]> listOfData = dnr.getListOfDataMatrices();
          
          bw.write("  for(int j=0;j<" + uniqueID + "ntimepoints;j++){\n");
          
          //sum up log(sigma) for the loglikelihood
          if (typeOfLL == LogLikelihoodTool.IndependentNoise){
            bw.write("    currentSigma = " + lookUpTable.lookUpString(speciesID) + "c_" + cond.intValue() + "sigma[j];\n");
          }else if (typeOfLL == LogLikelihoodTool.LinearModelOfNoise) {
            bw.write("    currentSigma = _logLikelihood_param_Sa + _logLikelihood_param_Sb*" + uniqueID + "sims[j].c_" + cond.intValue() + ";\n");
          }
          
          bw.write("    for(i=0;i<" + uniqueID + "nreplicates;i++){\n");
          
          if (typeOfLL == LogLikelihoodTool.IndependentNoise || typeOfLL == LogLikelihoodTool.LinearModelOfNoise){
            if (optimiser == LEVMAR_FD || optimiser == LEVMAR_SE){
              bw.write("      if (!isnan(" + uniqueID + "data[i][j].c_" + cond.intValue() + ")) {\n");
              bw.write("        hx[hxpos] =  (" + uniqueID + "sims[j].c_" + cond.intValue() + " - " + uniqueID + "data[i][j].c_" + cond.intValue() + ")/(currentSigma); // "
                    + lookUpTable.lookUpString(cond)+ "\n"
                    + "        hx[hxpos+1] = SUNRsqrt(2*log(currentSigma) + LOGLIKELIHOOD_C);\n" //LevMar algorithm needs this to be positive
                    + "        *fit += SUNRpowerI(hx[hxpos],2) + 2*log(currentSigma);\n" //*fit report does not use the positive loglikelihood to compare with other algorithms
                    + "        hxpos += 2;\n");
              bw.write("        *fit2 += SUNRpowerI(" + uniqueID + "sims[j].c_" + cond.intValue() + " - " + uniqueID + "data[i][j].c_" + cond.intValue() + ",2); //\n");
              bw.write("      }\n");
            }else if (optimiser == GLSDC){
              bw.write("      if (!isnan(" + uniqueID + "data[i][j].c_" + cond.intValue() + ")) {\n");
              bw.write("        *fit += SUNRpowerI((" + uniqueID + "sims[j].c_" + cond.intValue() + " - " + uniqueID + "data[i][j].c_" + cond.intValue() + ")/(currentSigma),2); // "
                    + lookUpTable.lookUpString(cond)+ "\n"
                    + "        *fit += 2*log(currentSigma);\n");
              bw.write("        *fit2 += SUNRpowerI(" + uniqueID + "sims[j].c_" + cond.intValue() + " - " + uniqueID + "data[i][j].c_" + cond.intValue() + ",2); //\n");
              bw.write("      }\n");
            }else if (optimiser == LEVMAR_SE_JAC){
              if (typeOfLL == LogLikelihoodTool.LinearModelOfNoise){
                bw.write("      if (!isnan(" + uniqueID + "data[i][j].c_" + cond.intValue() + ")) {\n");
                bw.write("        //derivatives of the weighted difference\n");
                bw.write("        for (int iNS = 0;iNS<NP;iNS++){\n"
                        + "          jac[hxpos++] = (" + uniqueID + "sens[iNS][j].c_" + cond.intValue() + "*currentSigma - (" + uniqueID + "sims[j].c_" + cond.intValue() + " - " + uniqueID + "data[i][j].c_" + cond.intValue() + ")*_logLikelihood_param_Sb*" + uniqueID + "sens[iNS][j].c_" + cond.intValue() + ")/(SUNRpowerI(currentSigma,2))*data->p[iNS];\n"
                        + "        }\n");
                bw.write("        jac[hxpos++] = (" + uniqueID + "data[i][j].c_" + cond.intValue() + " - " + uniqueID + "sims[j].c_" + cond.intValue() + ")/(SUNRpowerI(currentSigma,2))*_logLikelihood_param_Sa;\n");
                bw.write("        jac[hxpos++] = (" + uniqueID + "data[i][j].c_" + cond.intValue() + " - " + uniqueID + "sims[j].c_" + cond.intValue() + ")*" + uniqueID + "sims[j].c_" + cond.intValue() + "/(SUNRpowerI(currentSigma,2))*_logLikelihood_param_Sb;\n");
                bw.write("        //derivatives of the log sigma\n");
                bw.write("        for (int iNS = 0;iNS<NP;iNS++){\n"
                        + "          jac[hxpos++] = (_logLikelihood_param_Sb*" + uniqueID + "sens[iNS][j].c_" + cond.intValue() + ")/(SUNRsqrt(2*log(currentSigma) + LOGLIKELIHOOD_C)*currentSigma)*data->p[iNS];\n"
                        + "        }\n");
                bw.write("        jac[hxpos++] = (1)/(SUNRsqrt(2*log(currentSigma) + LOGLIKELIHOOD_C)*currentSigma)*_logLikelihood_param_Sa;\n");
                bw.write("        jac[hxpos++] = (" + uniqueID + "sims[j].c_" + cond.intValue() + ")/(SUNRsqrt(2*log(currentSigma) + LOGLIKELIHOOD_C)*currentSigma)*_logLikelihood_param_Sb;\n");
                bw.write("      }\n");
              }else if (typeOfLL == LogLikelihoodTool.IndependentNoise){
                bw.write("      if (!isnan(" + uniqueID + "data[i][j].c_" + cond.intValue() + ")) {\n");
                bw.write("        //derivatives of the weighted difference\n");
                bw.write("        for (int iNS = 0;iNS<NP;iNS++){\n"
                        + "          jac[hxpos++] = (" + uniqueID + "sens[iNS][j].c_" + cond.intValue() + "*currentSigma)/(SUNRpowerI(currentSigma,2))*data->p[iNS];\n"
                        + "        }\n");
                //iterate all the sigmas
                Iterator<ParameterRangeOfAdditionalParam> ite_params = logLikelihoodTool.getListOfAdditionalParams().iterator();
                while (ite_params.hasNext()){
                  ParameterRangeOfAdditionalParam pr = ite_params.next();
                  //
                  bw.write("        if (&" + pr.getVariable() + " == &" + lookUpTable.lookUpString(speciesID) + "c_" + cond.intValue() + "sigma[j]" + ") {\n");
                  bw.write("          jac[hxpos++] = (" + uniqueID + "data[i][j].c_" + cond.intValue() + " - " + uniqueID + "sims[j].c_" + cond.intValue() + ")/(SUNRpowerI(currentSigma,2))*currentSigma;\n");
                  bw.write("        }else{\n");
                  bw.write("          jac[hxpos++] = 0.0;\n");
                  bw.write("        }\n");
                  //
                }
                bw.write("        //derivatives of the log sigma\n");
                bw.write("        for (int iNS = 0;iNS<NP;iNS++){\n"
                        + "          jac[hxpos++] = 0.0;\n"
                        + "        }\n");
                //iterate all the sigmas
                ite_params = logLikelihoodTool.getListOfAdditionalParams().iterator();
                while (ite_params.hasNext()){
                  ParameterRangeOfAdditionalParam pr = ite_params.next();
                  //
                  bw.write("        if (&" + pr.getVariable() + " == &" + lookUpTable.lookUpString(speciesID) + "c_" + cond.intValue() + "sigma[j]" + ") {\n");
                  bw.write("        jac[hxpos++] = (1)/(SUNRsqrt(2*log(currentSigma) + LOGLIKELIHOOD_C)*currentSigma)*currentSigma;\n");
                  bw.write("        }else{\n");
                  bw.write("          jac[hxpos++] = 0.0;\n");
                  bw.write("        }\n");
                  //
                }
                
                bw.write("      }\n");
              }
            }
          }else{ //not loglikelihood
            if (optimiser == LEVMAR_FD || optimiser == LEVMAR_SE){
              bw.write("      if (!isnan(" + uniqueID + "data[i][j].c_" + cond.intValue() + ")) {\n"
                  + "        hx[hxpos] = " + uniqueID + "sims[j].c_" + cond.intValue() + " - " + uniqueID + "data[i][j].c_" + cond.intValue() + ";\n"
                  + "        *fit += SUNRpowerI(hx[hxpos++],2); // "
                  + lookUpTable.lookUpString(cond)+ "\n"
                  + "      }\n");
            }else if (optimiser == GLSDC){
              bw.write("      if (!isnan(" + uniqueID + "data[i][j].c_" + cond.intValue() + ")) *fit += " 
                  + "SUNRpowerI(" + uniqueID + "sims[j].c_" + cond.intValue() + " - " + uniqueID + "data[i][j].c_" + cond.intValue() + ",2); // "
                  + lookUpTable.lookUpString(cond)+ "\n");
            }else if (optimiser == LEVMAR_SE_JAC){
              bw.write("      if (!isnan(" + uniqueID + "data[i][j].c_" + cond.intValue() + ")) {\n"
                  + "        for (int iNS = 0;iNS<NP;iNS++){\n"
                  + "          jac[hxpos++] = " + uniqueID + "sens[iNS][j].c_" + cond.intValue() + "*data->p[iNS];\n"
                  + "        }\n"
                  + "      }\n");
            }
          }
          
          bw.write("    }\n");
          bw.write("  }\n\n");
          
        }else {
          throw new Exception("[error] could not find data for the fitting of species " + lookUpTable.lookUpString(speciesID) 
                        + " and condition " + lookUpTable.lookUpString(cond));
        }
      }
      
      
    }
    
    
    //if (logLikelihoodTool != null) bw.write("  *fit += sumOfLogSigmas;\n");//comment out, add earlier
    bw.write("\n");
    
    //---------------- END OF FIT FUNCTION
    
    
    //---------------- last lines below
    
    bw.write("  //**************************************************END\n");
    bw.write("  end:\n");
    
    Iterator<Integer> iteEnd = conditionsToSimulate.iterator();
    while (iteEnd.hasNext()){
      Integer tmp = iteEnd.next();
      bw.write("  if (y_c_" + tmp.intValue() + " != NULL) N_VDestroy_Serial(y_c_" + tmp.intValue() + "); // " + lookUpTable.lookUpString(tmp) + "\n"
            + "  y_c_" + tmp.intValue() + " = NULL;\n");
      if (optimiser == LEVMAR_SE_JAC){
        bw.write("  if (uS_c_" + tmp.intValue() + " != NULL) N_VDestroyVectorArray_Serial(uS_c_" + tmp.intValue() + ",NP); // " + lookUpTable.lookUpString(tmp) + "\n"
            + "  uS_c_" + tmp.intValue() + " = NULL;\n");
      }
        //if (uS_c_3 != NULL) N_VDestroyVectorArray_Serial(uS_c_3, NP);
        //uS_c_3 = NULL;
    }
    bw.write("\n");
    
    
    bw.write("  if (abstol != NULL) N_VDestroy_Serial(abstol);\n"
          + "  abstol = NULL;\n"
          + "  if (cvode_mem != NULL) CVodeFree(&cvode_mem);\n"
          + "  cvode_mem = NULL;\n\n");
    if (optimiser == LEVMAR_SE){      
      bw.write("  //free user data\n"
            + "  if (data->p != NULL) free(data->p);\n"
            + "  data->p = NULL;\n"
            + "  if (data!=NULL) free(data);\n"
            + "  data = NULL;\n\n");
    }      
    bw.write("  return rv;\n}\n\n");
    
  }
  
  private void printFitnessFunction(BufferedWriter bw) throws Exception{
    
    int problemDim = entryLists.getDiffList().size() + entryLists.getParamList().size();
  
    bw.write("//*************************************************** problem Dim=" + problemDim + "\n");
    bw.write("void fitness(INDIVIDUAL *indiv)\n"
          + "{\n\n"  
          + "  double params[" + problemDim + "];\n\n");
          
    bw.write("  if (setting_problems != NULL) {\n"
          + "    if (setting_problems->FeasibleCheck(indiv) == _INDIVIDUAL_INFEASIBLE_) {\n"
          + "      indiv->SetInfeasible(_INDIVIDUAL_INFEASIBLE_);\n"
          + "      return;\n"
          + "    }\n"
          + "  }\n\n");
    bw.write("  //******************************************\n\n");
    
    //-------- List all paramters
    
    bw.write("  //values of initial conditions will be set later, here set to 0\n");
    
    int i = 0; //parameter counter
    
    Iterator<GenericEntry> ite = entryLists.getDiffList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof DifferentialEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printFitnessFunction()). The GenericEntry object is not a DifferentialEquationNode object");
      DifferentialEquationNode den = (DifferentialEquationNode) tmp;
      
      bw.write("  params[" + i + "] = 0; // " + lookUpTable.lookUpString(den.getIdentifier()) + "_0 \n");
      
      i++;
    } 
    
    bw.write("  //values of parameters\n");
    
    int j = 0; //parameter to fit counter
    
    Iterator<GenericEntry> ite3 = entryLists.getParamList().iterator();
    while (ite3.hasNext()){
      GenericEntry tmp = ite3.next();
      if (! (tmp instanceof ParameterNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printFitnessFunction()). The GenericEntry object is not a ParameterNode object");
      if (tmp instanceof ParameterValue){
        
        ParameterValue pv = (ParameterValue)tmp;
        bw.write("  params[" + i + "] = " + pv.getValue().doubleValue() + "; // " + lookUpTable.lookUpString(pv.getIdentifier()) + " \n");
      
      }else if (tmp instanceof ParameterRange){
        
        ParameterRange pr = (ParameterRange)tmp;
        bw.write("  params[" + i + "] = exp(indiv->GetData(" + j + ")); // " + lookUpTable.lookUpString(pr.getIdentifier()) + " \n");
        j++;
      }else {
        throw new Exception("[error] Probably a "
          + "coding error (GLSDCTool.printFitnessFunction()). Unknown type of ParameterNode");
      }
      
      i++;
    }
    
    //if LogLikelihood is requested, I need to fit some additional parameters as well
    int typeOfLL = fittingOptions.getLogLikelihood();
    boolean llIsOn = false;
    if (typeOfLL == LogLikelihoodTool.IndependentNoise || typeOfLL == LogLikelihoodTool.LinearModelOfNoise){
      llIsOn = true;
      Iterator<ParameterRangeOfAdditionalParam> ite_params = logLikelihoodTool.getListOfAdditionalParams().iterator();
      while (ite_params.hasNext()){
        ParameterRangeOfAdditionalParam pr = ite_params.next();
        bw.write("  " + pr.getVariable() + " = exp(indiv->GetData(" + j + ")); // " + lookUpTable.lookUpString(pr.getIdentifier()) + " \n");
        j++;
      }
    }          
    
    bw.write("\n  //******************************************\n\n");

    bw.write("  double fit; //either Least Squares or LogLikelihood\n"
          + "  double fit2; //contains the Least Squares when optimising the LogLikelihood\n"
          + "  int infeasible = my_fitness(" + problemDim + ", params, &fit, &fit2);\n\n");

    bw.write("  global_neval++;\n"
          + "  if (fit < global_best & infeasible == 0) {\n"
          + "    global_best=fit;\n");
    if (llIsOn) bw.write("    global_best2=fit2;\n");
    //------------------------------------------------------------------
    //here we overwrite the content of X.best, where X is the current random seed
    bw.write("    FILE * fp;\n"
      + "    //const char* outfilename;\n"
      + "    std::stringstream sstm;\n"
      + "    sstm << randomSeed << \".best\";\n"
      + "    //outfilename = sstm.str().c_str();\n"
      + "    fp = fopen (sstm.str().c_str(), \"w\");\n"
      + "    fprintf(fp,\"%d %le\\n----Normal----\\n\",randomSeed,global_best);\n");
   
    int n =0;
    
    ite = entryLists.getParamList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof ParameterNode)) throw new Exception("[error] coding error! I should have ParameterNode objects in GLSDCTool.generateFile_pset_sbcmodel_orgdat()");
      if (tmp instanceof ParameterRange){
        ParameterRange pr = (ParameterRange) tmp; //downcast
        bw.write("    fprintf(fp,\"%d\\t%.64f\\n\"," + n + ",exp(indiv->GetData(" + n + "))); // " + lookUpTable.lookUpString(pr.getIdentifier()) + "\n");
        n++;
      }
    }
    
    //if LogLikelihood is requested, I need to fit some additional parameters as well
    if (llIsOn){
      Iterator<ParameterRangeOfAdditionalParam> ite_sigma = logLikelihoodTool.getListOfAdditionalParams().iterator();
      while (ite_sigma.hasNext()){
        ParameterRangeOfAdditionalParam pr = ite_sigma.next();
        bw.write("    fprintf(fp,\"%d\\t%.64f\\n\"," + n + ",exp(indiv->GetData(" + n + "))); // " + lookUpTable.lookUpString(pr.getIdentifier()) + "\n");
        n++;
      }
    }
   
    bw.write("    fclose(fp);\n");
    
    //------------------------------------------------------------------
    bw.write("  }\n"
          + "  if (global_neval % 10 == 0){\n");
    if (llIsOn){
      bw.write("    fprintf(stdout,\"global_neval,%d,global_best,%le,time,%ld,global_best2,%le\\n\",global_neval,global_best,static_cast<long int> (time(NULL)),global_best2);\n");
    }else{
      bw.write("    fprintf(stdout,\"global_neval,%d,global_best,%le,time,%ld\\n\",global_neval,global_best,static_cast<long int> (time(NULL)));\n");
    }
    bw.write("    fflush(stdout);\n"
          + "  }\n\n");

    bw.write("  indiv->SetFitness(0, fit);\n");
    bw.write("  indiv->SetInfeasible(infeasible);\n\n");
  
    bw.write("  return;\n");

    bw.write("}\n\n");
    
  }

  
  private void printFitnessFunctionLevMar(int optimiser, BufferedWriter bw) throws Exception{
    
    int problemDim = entryLists.getDiffList().size() + entryLists.getParamList().size();
  
    bw.write("//*************************************************** problem Dim=" + problemDim + "\n");
    if (optimiser == LEVMAR_FD || optimiser == LEVMAR_SE){
      bw.write("void fitness(double* p, double* hx, int m, int n, void* adata)\n");
    } else if (optimiser == LEVMAR_SE_JAC){
      bw.write("void jac_fitness(double* p, double* jac, int m, int n, void* adata)\n");
    }
    
    bw.write("{\n\n"  
          + "  double params[" + problemDim + "];\n\n");
          
    bw.write("  //******************************************\n\n");
    
    //-------- List all paramters
    
    bw.write("  //values of initial conditions will be set later, here set to 0\n");
    
    int i = 0; //parameter counter
    
    Iterator<GenericEntry> ite = entryLists.getDiffList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof DifferentialEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printFitnessFunction()). The GenericEntry object is not a DifferentialEquationNode object");
      DifferentialEquationNode den = (DifferentialEquationNode) tmp;
      
      bw.write("  params[" + i + "] = 0; // " + lookUpTable.lookUpString(den.getIdentifier()) + "_0 \n");
      
      i++;
    } 
    
    bw.write("  //values of parameters\n");
    
    int j = 0; //parameter to fit counter
    
    Iterator<GenericEntry> ite3 = entryLists.getParamList().iterator();
    while (ite3.hasNext()){
      GenericEntry tmp = ite3.next();
      if (! (tmp instanceof ParameterNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printFitnessFunction()). The GenericEntry object is not a ParameterNode object");
      if (tmp instanceof ParameterValue){
        
        ParameterValue pv = (ParameterValue)tmp;
        bw.write("  params[" + i + "] = " + pv.getValue().doubleValue() + "; // " + lookUpTable.lookUpString(pv.getIdentifier()) + " \n");
      
      }else if (tmp instanceof ParameterRange){
        
        ParameterRange pr = (ParameterRange)tmp;
        bw.write("  params[" + i + "] = exp(p[" + j + "]); // " + lookUpTable.lookUpString(pr.getIdentifier()) + " \n");
        j++;
      }else {
        throw new Exception("[error] Probably a "
          + "coding error (GLSDCTool.printFitnessFunction()). Unknown type of ParameterNode");
      }
      
      i++;
    }
    
    
    //if LogLikelihood is requested, I need to fit some additional parameters as well
    int typeOfLL = fittingOptions.getLogLikelihood();
    boolean llIsOn = false;
    if (typeOfLL == LogLikelihoodTool.IndependentNoise || typeOfLL == LogLikelihoodTool.LinearModelOfNoise){
      llIsOn = true;
      Iterator<ParameterRangeOfAdditionalParam> ite_params = logLikelihoodTool.getListOfAdditionalParams().iterator();
      while (ite_params.hasNext()){
        ParameterRangeOfAdditionalParam pr = ite_params.next();
        bw.write("  " + pr.getVariable() + " = exp(p[" + j + "]); // " + lookUpTable.lookUpString(pr.getIdentifier()) + " \n");
        j++;
      }
    }          
    
    bw.write("\n  //******************************************\n\n");
    if (optimiser == LEVMAR_FD){
      bw.write("  double fit; //either Least Squares or LogLikelihood\n"
          + "  double fit2; //contains the Least Squares when optimising the LogLikelihood\n"
          + "  int infeasible = my_fitness(" + problemDim + ", params, &fit, &fit2, hx);\n\n");
    }else if (optimiser == LEVMAR_SE){
      bw.write("  double fit; //either Least Squares or LogLikelihood\n"
          + "  double fit2; //contains the Least Squares when optimising the LogLikelihood\n"
          + "  int infeasible = my_fitness(" + problemDim + ", params, &fit, &fit2, hx, p, m);\n\n");
    }else if (optimiser == LEVMAR_SE_JAC){
      bw.write("  int infeasible = my_jac_fitness(" + problemDim + ", params, jac, p, m);\n\n");
    }
    bw.write("  global_neval++;\n");
    if (optimiser == LEVMAR_FD || optimiser == LEVMAR_SE){
      bw.write("  if (fit < global_best & infeasible == 0) {\n"
        + "    global_best=fit;\n");
      if (llIsOn){ //using log likelihood
        bw.write("    global_best2=fit2;\n");
      }
    //------------------------------------------------------------------
    //here we overwrite the content of X.best, where X is the current random seed
    bw.write("    FILE * fp;\n"
      + "    //const char* outfilename;\n"
      + "    std::stringstream sstm;\n"
      + "    sstm << \"results/\" << randomSeed << \".best\";\n"
      + "    //outfilename = sstm.str().c_str();\n"
      + "    fp = fopen (sstm.str().c_str(), \"w\");\n"
      + "    fprintf(fp,\"%d %le\\n----Normal----\\n\",randomSeed,global_best);\n");
   
    int n =0;
    
    ite = entryLists.getParamList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof ParameterNode)) throw new Exception("[error] coding error! I should have ParameterNode objects in GLSDCTool.generateFile_pset_sbcmodel_orgdat()");
      if (tmp instanceof ParameterRange){
        ParameterRange pr = (ParameterRange) tmp; //downcast
        bw.write("    fprintf(fp,\"%d\\t%.64f\\n\"," + n + ",exp(p[" + n + "])); // " + lookUpTable.lookUpString(pr.getIdentifier()) + "\n");
        n++;
      }
    }
    
    //if LogLikelihood is requested, I need to fit some additional parameters as well
    if (llIsOn){
      Iterator<ParameterRangeOfAdditionalParam> ite_sigma = logLikelihoodTool.getListOfAdditionalParams().iterator();
      while (ite_sigma.hasNext()){
        ParameterRangeOfAdditionalParam pr = ite_sigma.next();
        bw.write("    fprintf(fp,\"%d\\t%.64f\\n\"," + n + ",exp(p[" + n + "])); // " + lookUpTable.lookUpString(pr.getIdentifier()) + "\n");
        n++;
      }
    }
   
    bw.write("    fclose(fp);\n");
    
    //------------------------------------------------------------------
      bw.write("  }\n");
    }
    bw.write("  if (global_neval % 10 == 0){\n");
      if (llIsOn){  //using log likelihood
        bw.write("    fprintf(stdout,\"global_neval,%d,global_best,%le,time,%ld,global_best2,%le\\n\",global_neval,global_best,static_cast<long int> (time(NULL)),global_best2);\n");
      }else{
        bw.write("    fprintf(stdout,\"global_neval,%d,global_best,%le,time,%ld\\n\",global_neval,global_best,static_cast<long int> (time(NULL)));\n");
      }
    bw.write("    fflush(stdout);\n"
          + "  }\n\n");

    bw.write("  //CHECK: there may be some problems if infeasible != 0\n");
    if (optimiser == LEVMAR_FD || optimiser == LEVMAR_SE){
      bw.write("  if (infeasible) for (int i=0;i<n;i++) hx[i]=NAN;\n\n");
    }else if (optimiser == LEVMAR_SE_JAC){
      //bw.write("  if (infeasible) for (int i=0;i<n*m;i++) jac[i]=NAN;\n\n");
      bw.write("  if (infeasible) {\n"
      + "    for (int i=0;i<n*m;i++) jac[i]=NAN;\n"
      + "    fprintf(stderr,\"Jacobian Infeasible, CVODES failed to compute sensitivity equations\\n\");\n"
      + "  }\n");
    }
    bw.write("  return;\n");

    bw.write("}\n\n");
    
  }

  
  private void printMainLevMar(int optimiser,BufferedWriter bw) throws Exception{
    bw.write("int main(int argc, char **argv)\n"
            + "{\n"
            + "  //set up random number generator\n"
            + "  const gsl_rng_type * T;\n"
            + "  gsl_rng * r;\n"
            + "  gsl_rng_env_setup();\n"
            + "  T = gsl_rng_default;\n"
            + "  r = gsl_rng_alloc(T);\n"
            + "  randomSeed = 0;  //global variable\n"
            + "  gsl_rng_set(r,randomSeed); //set random seed\n\n");
  
    bw.write("  if (argc == 2){\n"
            + "    //use number of fits from input line\n"
            + "    randomSeed = atoi(argv[1]);\n"            
            + "    gsl_rng_set(r,randomSeed);\n"
            + "  }\n\n");
    // check for loglikelihood sigma params, in case loglikelihood was requested
    int llparams = 0;
    int typeOfLL = fittingOptions.getLogLikelihood();
    if (typeOfLL == LogLikelihoodTool.IndependentNoise || typeOfLL == LogLikelihoodTool.LinearModelOfNoise){
      llparams = logLikelihoodTool.getListOfAdditionalParams().size();
    }
    int npartofit = QueryModelAndData.getParametersToFit(entryLists).size() + llparams;
    bw.write("  //number of parameters to estimate\n"
            + "  int m = " + npartofit + ";\n\n");
   
    bw.write("  //allocate vector for current parameter values\n"
            + "  double* p = new double[m];\n\n");

    bw.write("  //allocate vector for best parameter values\n"
            + "  double* bestp = new double[m];\n\n");
  
    bw.write("  //parameter bounds (natural logarithm)\n"
              + "  double* lb = new double[m];\n"
              + "  double* ub = new double[m];\n\n");
              
    //set bounds for the search
    
    int n =0;
      
    Iterator<GenericEntry> ite = entryLists.getParamList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof ParameterNode)) throw new Exception("[error] coding error! I should have ParameterNode objects in GLSDCTool.generateFile_pset_sbcmodel_orgdat()");
      if (tmp instanceof ParameterRange){
        ParameterRange pr = (ParameterRange) tmp; //downcast
        bw.write("  //set boundaries for " + lookUpTable.lookUpString(pr.getIdentifier()) + "\n");
        bw.write("  lb[" + n + "] = " + Math.log(pr.getBottom().doubleValue()) + ";\n");
        bw.write("  ub[" + n + "] = " + Math.log(pr.getTop().doubleValue()) + ";\n");
        
        n++;
      }
    }
      
    //if LogLikelihood is requested, I need to fit some additional parameters as well
    if (typeOfLL == LogLikelihoodTool.IndependentNoise || typeOfLL == LogLikelihoodTool.LinearModelOfNoise){
      Iterator<ParameterRangeOfAdditionalParam> ite_sigma = logLikelihoodTool.getListOfAdditionalParams().iterator();
      while (ite_sigma.hasNext()){
        ParameterRangeOfAdditionalParam pr = ite_sigma.next();
        bw.write("  lb[" + n + "] = " + Math.log(pr.getBottom().doubleValue()) + ";\n");
        bw.write("  ub[" + n + "] = " + Math.log(pr.getTop().doubleValue()) + ";\n");
        n++;
      }
    }
  
    bw.write("  //find out the number of datapoints to fit\n"
          + "  int n = 0;\n\n");
          
    //for all species to fit
    Iterator<Integer> ite20 = fittingOptions.getIDCondList().keySet().iterator();
    while (ite20.hasNext()){
      Integer speciesID = ite20.next();
      LinkedList<Integer> listOfConditions = fittingOptions.getIDCondList().get(speciesID);
      
      //for all conditions to fit of speciesID
      Iterator<Integer> ite21 = listOfConditions.iterator();
      while (ite21.hasNext()){
        Integer cond = ite21.next();
        
        //retrive dataNode for this speciesID and condition and build uniqueID
        DataNodeReplicates dnr = QueryModelAndData.getDataNodeReplicatesOf(entryLists,speciesID,cond);
        
        if (dnr != null){
          
          String uniqueID = QueryModelAndData.buildUniqueIDFrom(lookUpTable,(DataNode)dnr);
          
          LinkedList<Double[][]> listOfData = dnr.getListOfDataMatrices();
          
          bw.write("  for(int j=0;j<" + uniqueID + "ntimepoints;j++){\n");
          
          //~ //sum up log(sigma) for the loglikelihood
          //~ if (typeOfLL == LogLikelihoodTool.IndependentNoise){
            //~ bw.write("    currentSigma = " + lookUpTable.lookUpString(speciesID) + "c_" + cond.intValue() + "sigma[j];\n");
          //~ }else if (typeOfLL == LogLikelihoodTool.LinearModelOfNoise) {
            //~ bw.write("    currentSigma = _logLikelihood_param_Sa + _logLikelihood_param_Sb*" + uniqueID + "sims[j].c_" + cond.intValue() + ";\n");
          //~ }
          
          bw.write("    for(int i=0;i<" + uniqueID + "nreplicates;i++){\n");
          
          //~ if (typeOfLL == LogLikelihoodTool.IndependentNoise || typeOfLL == LogLikelihoodTool.LinearModelOfNoise){
            //~ bw.write("      if (!isnan(" + uniqueID + "data[i][j].c_" + cond.intValue() + ")) {\n"
                  //~ + "        *fit += SUNRpowerI((" + uniqueID + "sims[j].c_" + cond.intValue() + " - " + uniqueID + "data[i][j].c_" + cond.intValue() + ")/(currentSigma),2); // "
                  //~ + lookUpTable.lookUpString(cond)+ "\n"
                  //~ + "        sumOfLogSigmas += 2*log(currentSigma);\n"
                  //~ + "      }\n");
          //~ }else{ //not loglikelihood
            bw.write("      if (!isnan(" + uniqueID + "data[i][j].c_" + cond.intValue() + ")) n++;\n");
          //~ }
          
          bw.write("    }\n");
          bw.write("  }\n\n");
          
        }else {
          throw new Exception("[error] could not find data for the fitting of species " + lookUpTable.lookUpString(speciesID) 
                        + " and condition " + lookUpTable.lookUpString(cond));
        }
      } 
    }     
    
    bw.write("  //allocate memory for x and hx\n");
    if (typeOfLL == LogLikelihoodTool.IndependentNoise || typeOfLL == LogLikelihoodTool.LinearModelOfNoise){
      bw.write("  //for loglikelihood need double the space for x and hx\n"
          + "  //to accomodate the sigmas as well\n"
          + "  n = 2*n;\n");
    }
    bw.write("  double* x = new double[n];\n"
          + "  for (int i=0;i<n;i++) x[i]=0.0; //fit to zero as we return residuals\n\n");
    
    bw.write("  //set up info array\n"
          + "  double info[LM_INFO_SZ];\n"
          + "  for (int i=0;i<LM_INFO_SZ;i++){\n"
          + "    info[i]=0;\n"
          + "  }\n\n");   
    
    bw.write("  //--- Set up Latin Hypercube Sampling\n"
          + "  int** gridPositions = new int*[m];\n"
          + "  for (int i=0;i<m;i++) {\n"
          + "    gridPositions[i] = new int[MAXRESTARTS];\n"
          + "    for (int j=0;j<MAXRESTARTS;j++) gridPositions[i][j] = j;\n"
          + "  }\n"
          + "  //shuffle the grid positions\n"
          + "  //fprintf(stderr,\"Latin Hypercube grid:\\n\");\n"
          + "  for (int i=0;i<m;i++) {\n"
          + "    gsl_ran_shuffle (r,gridPositions[i],MAXRESTARTS,sizeof (int));\n"
          + "    //for (int j=0;j<MAXRESTARTS;j++) fprintf(stderr,\"%d\\t\",gridPositions[i][j]);\n"
          + "    //fprintf(stderr,\"\\n\");\n"
          + "  }\n");
    
    bw.write("  //---------MAIN LOOP\n"
          + "  double bestf = 1.0e10;\n\n"
          + "  for (int j=0;j<MAXRESTARTS;j++){\n\n"
          + "    //pick a random p to start\n"
          + "    fprintf(stderr,\"--------------------------------\\n\");\n"
          + "    fprintf(stderr,\"start values:\\n\");\n"
          + "    for (int i=0;i<m;i++){\n"
          + "      //p[i] = lb[i] + gsl_rng_uniform(r)*(ub[i]-lb[i]);\n"
          + "      //use latin hypercube\n"
          + "      p[i] = lb[i] + (gsl_rng_uniform(r) + gridPositions[i][j])*(ub[i]-lb[i])/MAXRESTARTS;\n"
          + "      fprintf(stderr,\"%le\\n\",exp(p[i]));\n"
          + "    }\n\n");
      
    bw.write("    //Now let's do the optimisation\n");
    if (optimiser == LEVMAR_FD){
      bw.write("    int res = dlevmar_bc_dif(fitness,p,x,m,n,lb,ub,NULL,MAXITERATIONS,NULL,info,NULL,NULL,NULL);\n\n");
    }else if (optimiser == LEVMAR_SE){
      bw.write("    int res = dlevmar_bc_der(fitness,jac_fitness,p,x,m,n,lb,ub,NULL,MAXITERATIONS,NULL,info,NULL,NULL,NULL);\n\n");
    }else{
      throw new Exception("[error] in GLSDCTool.printMainLevMar(). Unknown optimisaition algorithm, probably coding error ");
    }
    bw.write("    if (info[1]<bestf){\n"
          + "      bestf = info[1];\n"
          + "      for (int i=0;i<m;i++){\n"
          + "        bestp[i] = p[i];\n"
          + "      }\n"
          + "    }\n\n");
      
    bw.write("    //print best\n"
          + "    fprintf(stderr,\"--------------------------------\\n\");\n"
          + "    fprintf(stderr,\"best values:\\n\");\n"
          + "    for (int i=0;i<m;i++){\n"
          + "      fprintf(stderr,\"%le\\n\",exp(p[i]));\n"
          + "    }\n\n"
          + "    //print info\n"
          + "    fprintf(stderr,\"--------------------------------\\n\");\n"
          + "    fprintf(stderr,\"info:\\n\");\n"
          + "    for (int i=0;i<LM_INFO_SZ;i++){\n"
          + "      fprintf(stderr,\"info[%d]=%le\\n\",i,info[i]);\n"
          + "    }\n"
          + "  }\n\n");
          
    bw.write("  //print best\n"
          + "  fprintf(stderr,\"--------------------------------\\n\");\n"
          + "  fprintf(stderr,\"best f: %le\\n\",bestf);\n"
          + "  fprintf(stderr,\"overall best values:\\n\");\n"
          + "  fprintf(stderr,\"----Normal----\\n\");\n"
          + "  for (int i=0;i<m;i++){\n"
          + "    fprintf(stderr,\"%d\\t%.64f\\n\",i,exp(bestp[i]));\n"
          + "  }  \n"
          + "  fprintf(stderr,\"\\n\");\n\n"
          + "  //free random number generator\n"
          + "  gsl_rng_free (r);\n"
          + "  //free data structures\n"
          + "  for (int i=0;i<m;i++) {\n"
          + "    delete[] gridPositions[i];\n"
          + "  }\n"
          + "  delete[] gridPositions;\n"
          + "  delete[] x;\n"
          + "  delete[] p;\n"
          + "  delete[] bestp;\n"
          + "  delete[] lb;\n"
          + "  delete[] ub;\n\n"
          + "  return 0;\n"
          + "}\n\n");
  
  }
  
  /**
   * Print algebraic equation c functions and their derivatives if requested
   * */
  
  private void printAlgebraicEquationsFunctions(boolean writeDer,Set<Integer> conditionsToSimulate, Map<Integer,Set<Integer>> signatures, BufferedWriter bw) throws Exception{
    
    //in case writeDer is true I will need to write the derivatives of 
    //some of the algebraic equation functions. Which? retrieve them:
    //Set<Integer> setForDerivation = QueryModelAndData.requiredAlgebraicEqDerivatives(entryLists,lookUpTable,fittingOptions);
    //above commented out, compute all the derivatives
    
    Iterator<GenericEntry> ite = entryLists.getAlgList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof AlgebraicEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printAlgebraicEquationsFunctions()). The GenericEntry object is not a AlgebraicEquationNode object");
      AlgebraicEquationNode aen = (AlgebraicEquationNode) tmp;
      
      bw.write("static realtype " + lookUpTable.lookUpString(aen.getIdentifier()) + "(realtype time, int condition");
      
      Set<Integer> s = signatures.get(aen.getIdentifier());
      if (s != null){
        Iterator<Integer> ite2 = s.iterator();
        while (ite2.hasNext()){
          Integer tmp2 = ite2.next();
          bw.write(", realtype " + lookUpTable.lookUpString(tmp2)); 
        }
      } 
      bw.write("){\n");
      
      //--------- content of the function
      
      //find out what AlgebraicEquationNode this is
      if (tmp instanceof AlgebraicEquationExpr){
        AlgebraicEquationExpr aee = (AlgebraicEquationExpr) tmp;
        
        //retrieve which algebraic equations are mentioned in the Expression
        Set<Integer> setOfIDs = new HashSet<Integer>();
        getAlgEqIDInExpression(aee.getExpression(),setOfIDs);
        
        //--------- call algebraic functions
        Iterator<Integer> ite0 = setOfIDs.iterator();
        while (ite0.hasNext()){
          Integer tmp0 = ite0.next(); //id of algebraic eq found in the expression
          bw.write("  realtype var_" + lookUpTable.lookUpString(tmp0) + " = " + lookUpTable.lookUpString(tmp0) + "(time,condition");
        
          Set<Integer> s0 = signatures.get(tmp0);
          if (s0 != null){
            Iterator<Integer> ite2 = s0.iterator();
            while (ite2.hasNext()){
              Integer tmp2 = ite2.next();
              bw.write("," + lookUpTable.lookUpString(tmp2) + ""); 
            }
          } 
          bw.write(");\n");
        }
        if (setOfIDs.size() > 0) bw.write("\n");
      
        //--------- done calling algebraic functions

        
        bw.write("  return");
        bw.write(writeExpression(aee.getExpression(),bw,2,null));
        bw.write(";\n");
        
      }else if (tmp instanceof AlgebraicEquationInterpolation){
        
        AlgebraicEquationInterpolation aei = (AlgebraicEquationInterpolation)tmp; //downcast
        
        //check all the conditions that we need to simulate have data for this speciesID
        if (!conditionsOfSpeciesIDInData(aei.getIdentifier()).containsAll(conditionsToSimulate)){
          throw new Exception("[error] not all requested conditions (for fitting and normalisation) "
            + "can be simulated, because of missing data for some species/conditions that need to be interpolated ("+ 
            lookUpTable.lookUpString(aei.getIdentifier()) +")");
        }
        //now we know we have all conditions, write C switch
        
        String interpolationMethod = "";
        
        //interpolation method:
        if (aei.getTypeOfInterpolation() == AlgebraicEquationInterpolation.LINEAR){
          interpolationMethod = "gsl_interp_linear";
        }else if (aei.getTypeOfInterpolation() == AlgebraicEquationInterpolation.SPLINE){
          interpolationMethod = "gsl_interp_akima";          
        }else{
          throw new Exception("[error] coding error, unknown interpolation method in GLSDC.printAlgebraicEquationsFunctions()");
        }
        
        bw.write("  //ODE integrator might ask for interpolation outside considered time\n"
                + "  //Replace with finaltime\n"
                + "  if(time>FINALTIME) time = FINALTIME;\n\n"
                + "  realtype tmp = 0.0;\n\n"
                + "  switch (condition){\n");
        
        Iterator<Integer> ite2 = conditionsToSimulate.iterator();
        while (ite2.hasNext()){
          Integer tmp2 = ite2.next(); //conditionID
          
          //retrieve DataNodeReplicates corresponding to speciesID and conditionID
          DataNodeReplicates dnr = QueryModelAndData.getDataNodeReplicatesOf(entryLists,aei.getIdentifier(),tmp2);
          if (dnr == null) {
            throw new Exception("[error] No available data for interpolation of variable " + lookUpTable.lookUpString(aei.getIdentifier()) + ", condition " + lookUpTable.lookUpString(tmp2));  
          }
          //reconstruct uniqueID
          
          String uniqueID = QueryModelAndData.buildUniqueIDFrom(lookUpTable,(DataNode)dnr);
          
          //check that there is no extrapolation, that is the last data time point
          //is smaller than the simulation time
          double lastTimePoint = dnr.getListOfDataMatrices().getFirst()[dnr.getListOfDataMatrices().getFirst().length - 1][0].doubleValue();
          if (lastTimePoint < simulateOptions.getFinalTime()){
            throw new Exception("[error] cannot interpolate species "
                        + lookUpTable.lookUpString(dnr.getIdentifier()) + " and condition "
                        + lookUpTable.lookUpString(tmp2) + " because the requested simulation time ("
                        + simulateOptions.getFinalTime() + ") is greater than the latest data time point ("
                        + lastTimePoint + ")");
          }
          //also check that first data time point is not after the initial simulation time
          double firstTimePoint = dnr.getListOfDataMatrices().getFirst()[0][0].doubleValue();
          if (firstTimePoint > simulateOptions.getInitialTime()){
            throw new Exception("[error] cannot interpolate species "
                        + lookUpTable.lookUpString(dnr.getIdentifier()) + " and condition "
                        + lookUpTable.lookUpString(tmp2) + " because the requested simulation time ("
                        + simulateOptions.getInitialTime() + ") is smaller than the first data time point ("
                        + firstTimePoint + ")");
          }          
          
          bw.write("    case c_" + tmp2.intValue() + ": // " + lookUpTable.lookUpString(tmp2) + "\n"
              + "      {\n"
              + "        realtype p[" + uniqueID + "ntimepoints], q[" + uniqueID + "ntimepoints];\n"
              + "        gsl_interp_accel *acc = gsl_interp_accel_alloc ();\n"
              + "        gsl_spline *spline = gsl_spline_alloc (" + interpolationMethod + ", " + uniqueID + "ntimepoints);\n\n");
          
          bw.write("        for (int i = 0; i < " + uniqueID + "ntimepoints; i++){\n"
              + "          p[i] = " + uniqueID + "dataAverage[i].time;\n"
              + "          q[i] = " + uniqueID + "dataAverage[i].c_" + tmp2.intValue() + ";\n"
              + "        }\n\n"
              + "        gsl_spline_init (spline, p, q, " + uniqueID + "ntimepoints);\n"
              + "        tmp = gsl_spline_eval (spline, time, acc);\n\n");
          
          bw.write("        gsl_spline_free (spline);\n"
              + "        gsl_interp_accel_free (acc);\n"
              + "        spline = NULL;\n"
              + "        acc = NULL;\n" 
              + "      }\n"
              + "      break;\n");
          
        }
        
        bw.write("    default:\n"
                + "      tmp = 0;\n"
                + "  }\n  return tmp;\n");
        
      }else if (tmp instanceof AlgebraicEquationCondList){
        
        AlgebraicEquationCondList aecl = (AlgebraicEquationCondList) tmp; //downcast
        
        //check if all conditions requested are in the AlgebraicEquationCondList object
        if (!aecl.getConditions().containsAll(conditionsToSimulate)){
          throw new Exception("[error] not all conditions requested for fitting or necessary for simulation"
                  + " have been specified for " + lookUpTable.lookUpString(aecl.getIdentifier()));
        }
        
        //begin the switch
        
        bw.write("\n  realtype tmp = 0.0;\n\n"); // return variable
        
        bw.write("  switch (condition){\n");
        
        //iterate the conditions necessary to simulate
        Iterator<Integer> ite2 = conditionsToSimulate.iterator();
        while (ite2.hasNext()){
          AlgebraicEquationCond tmp2 = aecl.getNodeWithConditionID(ite2.next());
          
          //begin case
          bw.write("    case c_" + tmp2.getCondition().intValue() + ": // " + lookUpTable.lookUpString(tmp2.getCondition()) + "\n"
              + "      {\n");
          
          //find out what AlgebraicEquationCond this is
          if (tmp2 instanceof AlgebraicEquationCondExpr){
            AlgebraicEquationCondExpr aee = (AlgebraicEquationCondExpr) tmp2;
            
            //retrieve which algebraic equations are mentioned in the Expression
            Set<Integer> setOfIDs = new HashSet<Integer>();
            getAlgEqIDInExpression(aee.getExpression(),setOfIDs);
            
            //--------- call algebraic functions
            Iterator<Integer> ite0 = setOfIDs.iterator();
            while (ite0.hasNext()){
              Integer tmp0 = ite0.next(); //id of algebraic eq found in the expression
              bw.write("        realtype var_" + lookUpTable.lookUpString(tmp0) + " = " + lookUpTable.lookUpString(tmp0) + "(time,condition");
            
              Set<Integer> s0 = signatures.get(tmp0);
              if (s0 != null){
                Iterator<Integer> ite3 = s0.iterator();
                while (ite3.hasNext()){
                  Integer tmp3 = ite3.next();
                  bw.write("," + lookUpTable.lookUpString(tmp3) + ""); 
                }
              } 
              bw.write(");\n");
            }
            if (setOfIDs.size() > 0) bw.write("\n");
          
            //--------- done calling algebraic functions
    
            
            bw.write("        tmp =");
            bw.write(writeExpression(aee.getExpression(),bw,2,null));
            bw.write(";\n");
            
          }else if (tmp2 instanceof AlgebraicEquationCondInterpolation){
            AlgebraicEquationCondInterpolation aei = (AlgebraicEquationCondInterpolation) tmp2;
            
            //check that the condition that we need to simulate has data for this speciesID
            Set<Integer> conditionSet = new HashSet<Integer>();
            conditionSet.add(aei.getCondition()); //add the condition of this AlgebraicEquationCond object
            if (!conditionsOfSpeciesIDInData(aecl.getIdentifier()).containsAll(conditionSet)){
              throw new Exception("[error] the requested condition " + lookUpTable.lookUpString(aei.getCondition())
                + "cannot be simulated, because of no data for species " 
                + lookUpTable.lookUpString(aecl.getIdentifier()) + "(which needs to be interpolated) has been provided for this condition");
            }
            //now we know we have the data necessary, write C switch
        
            String interpolationMethod = "";
        
            //interpolation method:
            if (aei.getTypeOfInterpolation() == AlgebraicEquationInterpolation.LINEAR){
              interpolationMethod = "gsl_interp_linear";
            }else if (aei.getTypeOfInterpolation() == AlgebraicEquationInterpolation.SPLINE){
              interpolationMethod = "gsl_interp_akima";          
            }else{
              throw new Exception("[error] coding error, unknown interpolation method in GLSDC.printAlgebraicEquationsFunctions()");
            }
            
            bw.write("        //ODE integrator might ask for interpolation outside considered time\n"
                    + "        //Replace with finaltime\n"
                    + "        if(time>FINALTIME) time = FINALTIME;\n\n");
                    
            //-------------------------------------
            
            //retrieve DataNodeReplicates corresponding to speciesID and conditionID
            DataNodeReplicates dnr = QueryModelAndData.getDataNodeReplicatesOf(entryLists,aecl.getIdentifier(),aei.getCondition());
            if (dnr == null) {
              throw new Exception("[error] No available data for interpolation of variable " + lookUpTable.lookUpString(aecl.getIdentifier()) + ", condition " + lookUpTable.lookUpString(aei.getCondition()));  
            }
            //reconstruct uniqueID
            String uniqueID = QueryModelAndData.buildUniqueIDFrom(lookUpTable,(DataNode)dnr);
            
            //check that there is no extrapolation, that is the last time point
            //is larger than the simulation time
            double lastTimePoint = dnr.getListOfDataMatrices().getFirst()[dnr.getListOfDataMatrices().getFirst().length - 1][0].doubleValue();
            if (lastTimePoint < simulateOptions.getFinalTime()){
              throw new Exception("[error] cannot interpolate species "
                          + lookUpTable.lookUpString(dnr.getIdentifier()) + " and condition "
                          + lookUpTable.lookUpString(aei.getCondition()) + " because the requested simulation time ("
                          + simulateOptions.getFinalTime() + ") is greater than the latest data time point ("
                          + lastTimePoint + ")");
            }
            //also check that first data time point is not after the initial simulation time
            double firstTimePoint = dnr.getListOfDataMatrices().getFirst()[0][0].doubleValue();
            if (firstTimePoint > simulateOptions.getInitialTime()){
              throw new Exception("[error] cannot interpolate species "
                          + lookUpTable.lookUpString(dnr.getIdentifier()) + " and condition "
                          + lookUpTable.lookUpString(aei.getCondition()) + " because the requested simulation time ("
                          + simulateOptions.getInitialTime() + ") is smaller than the first data time point ("
                          + firstTimePoint + ")");
            } 
          
          bw.write("        realtype p[" + uniqueID + "ntimepoints], q[" + uniqueID + "ntimepoints];\n"
              + "        gsl_interp_accel *acc = gsl_interp_accel_alloc ();\n"
              + "        gsl_spline *spline = gsl_spline_alloc (" + interpolationMethod + ", " + uniqueID + "ntimepoints);\n\n");
          
          bw.write("        for (int i = 0; i < " + uniqueID + "ntimepoints; i++){\n"
              + "          p[i] = " + uniqueID + "dataAverage[i].time;\n"
              + "          q[i] = " + uniqueID + "dataAverage[i].c_" + aei.getCondition().intValue() + ";\n"
              + "        }\n\n"
              + "        gsl_spline_init (spline, p, q, " + uniqueID + "ntimepoints);\n"
              + "        tmp = gsl_spline_eval (spline, time, acc);\n\n");
          
          bw.write("        gsl_spline_free (spline);\n"
              + "        gsl_interp_accel_free (acc);\n"
              + "        spline = NULL;\n"
              + "        acc = NULL;\n"); 
           
            
            //-------------------------------------
            
          }else{
            throw new Exception("[error] Probably a "
              + "coding error (GLSDCTool.printAlgebraicEquationsFunctions()). Unknown type of AlgebraicEquationCond.");
          }
          //end case
          bw.write("      }\n"
            + "      break;\n");
          
        }
        
        //close the switch
        bw.write("    default:\n"
          + "      tmp = 0;\n"
          + "  }\n  return tmp;\n");
        
      }else{
        throw new Exception("[error] Probably a "
          + "coding error (GLSDCTool.printAlgebraicEquationsFunctions()). Unknown type of AlgebraicEquationNode.");
      }
      
      
      //---------done with the function
      
      bw.write("}\n\n");
      
      
      //------------------------------------------------
      // Write derivatives here, if requested
      //------------------------------------------------
      
      //if (writeDer && setForDerivation.contains(aen.getIdentifier())){
      if (writeDer){
          //for all parameters to fit
          LinkedList<Integer> paramsToFit = QueryModelAndData.getParametersToFit(entryLists);
          LinkedList<Integer> differentials = QueryModelAndData.getDifferentialVariablesIDs(entryLists);
          LinkedList<Integer> fullDerivativesList = new LinkedList<Integer>();
          fullDerivativesList.addAll(paramsToFit);
          //fullDerivativesList.addAll(differentials);
          fullDerivativesList.add(differentials.getFirst()); //trick: I just need to write this derivative once for all differentials
          Iterator<Integer> iteP = fullDerivativesList.iterator();
          while (iteP.hasNext()){
            Integer paramID = iteP.next();
            //--------copy from above and adapt
            //~ + "__der__" + lookUpTable.lookUpString(paramID) 
            int type = QueryModelAndData.getIDType(entryLists,paramID);
            if (type == GLSDCTool.PARAMETER){
              bw.write("static realtype " + lookUpTable.lookUpString(aen.getIdentifier())
                    + "__der__" + lookUpTable.lookUpString(paramID) + "(realtype time, int condition");
            } else if (type == GLSDCTool.DIFFERENTIAL){
              bw.write("static realtype " + lookUpTable.lookUpString(aen.getIdentifier())
                    + "__der__(realtype time, int condition");
            } 
            s = signatures.get(aen.getIdentifier());
            if (s != null){
              Iterator<Integer> ite2 = s.iterator();
              while (ite2.hasNext()){
                Integer tmp2 = ite2.next();
                bw.write(", realtype " + lookUpTable.lookUpString(tmp2)); 
                if (type == GLSDCTool.PARAMETER){
                  bw.write(", realtype " + lookUpTable.lookUpString(tmp2) + "__der__" + lookUpTable.lookUpString(paramID)); 
                }else if (type == GLSDCTool.DIFFERENTIAL){
                  bw.write(", realtype " + lookUpTable.lookUpString(tmp2) + "__der__"); 
                }
              }
            } 
            bw.write("){\n");
            
            //--------- content of the function
            
            //find out what AlgebraicEquationNode this is
            if (tmp instanceof AlgebraicEquationExpr){
              AlgebraicEquationExpr aee = (AlgebraicEquationExpr) tmp;
              
              //retrieve which algebraic equations are mentioned in the Expression
              Set<Integer> setOfIDs = new HashSet<Integer>();
              getAlgEqIDInExpression(aee.getExpression(),setOfIDs);
              
              //--------- call algebraic functions
              Iterator<Integer> ite0 = setOfIDs.iterator();
              while (ite0.hasNext()){
                Integer tmp0 = ite0.next(); //id of algebraic eq found in the expression
                bw.write("  realtype var_" + lookUpTable.lookUpString(tmp0) + " = " + lookUpTable.lookUpString(tmp0) + "(time,condition");
              
                Set<Integer> s0 = signatures.get(tmp0);
                if (s0 != null){
                  Iterator<Integer> ite2 = s0.iterator();
                  while (ite2.hasNext()){
                    Integer tmp2 = ite2.next();
                    bw.write("," + lookUpTable.lookUpString(tmp2) + ""); 
                  }
                } 
                bw.write(");\n");
                
                //also get the derivative
                if (type == GLSDCTool.PARAMETER){
                  bw.write("  realtype var_" + lookUpTable.lookUpString(tmp0) + "__der__" + lookUpTable.lookUpString(paramID) + " = " + lookUpTable.lookUpString(tmp0) + "__der__" + lookUpTable.lookUpString(paramID) + "(time,condition");
                }else if (type == GLSDCTool.DIFFERENTIAL){
                  bw.write("  realtype var_" + lookUpTable.lookUpString(tmp0) + "__der__ = " + lookUpTable.lookUpString(tmp0) + "__der__(time,condition");
                }
                s0 = signatures.get(tmp0);
                if (s0 != null){
                  Iterator<Integer> ite2 = s0.iterator();
                  while (ite2.hasNext()){
                    Integer tmp2 = ite2.next();
                    bw.write("," + lookUpTable.lookUpString(tmp2)); 
                    if (type == GLSDCTool.PARAMETER){
                      bw.write("," + lookUpTable.lookUpString(tmp2) + "__der__" + lookUpTable.lookUpString(paramID)); 
                    }else if (type == GLSDCTool.DIFFERENTIAL){
                      bw.write("," + lookUpTable.lookUpString(tmp2) + "__der__"); 
                    }
                  }
                } 
                bw.write(");\n");
              }
              if (setOfIDs.size() > 0) bw.write("\n");
            
              //--------- done calling algebraic functions

              
              bw.write("  return");
              
              try{
								bw.write(writeDerivative(aee.getExpression(),paramID,2,bw));
							}catch(DerivativeZeroException dze){
								bw.write(" 0.0 ");
							}
              
              bw.write(";\n");
              
            }else if (tmp instanceof AlgebraicEquationInterpolation){
              
              //If it is an interpolation, then the derivative is 0
              bw.write("    return 0.0;\n");
              
            }else if (tmp instanceof AlgebraicEquationCondList){
              //conditions can have interpolations (derivative = 0.0)
              //or other expressions
              
              AlgebraicEquationCondList aecl = (AlgebraicEquationCondList) tmp; //downcast
              
              //check if all conditions requested are in the AlgebraicEquationCondList object
              if (!aecl.getConditions().containsAll(conditionsToSimulate)){
                throw new Exception("[error] not all conditions requested for fitting or necessary for simulation"
                        + " have been specified for " + lookUpTable.lookUpString(aecl.getIdentifier()));
              }
              
              //begin the switch
              
              bw.write("\n  realtype tmp = 0.0;\n\n"); // return variable
              
              bw.write("  switch (condition){\n");
              
              //iterate the conditions necessary to simulate
              Iterator<Integer> ite2 = conditionsToSimulate.iterator();
              while (ite2.hasNext()){
                AlgebraicEquationCond tmp2 = aecl.getNodeWithConditionID(ite2.next());
                
                //begin case
                bw.write("    case c_" + tmp2.getCondition().intValue() + ": // " + lookUpTable.lookUpString(tmp2.getCondition()) + "\n"
                    + "      {\n");
                
                //find out what AlgebraicEquationCond this is
                if (tmp2 instanceof AlgebraicEquationCondExpr){
                  AlgebraicEquationCondExpr aee = (AlgebraicEquationCondExpr) tmp2;
                  
                  //retrieve which algebraic equations are mentioned in the Expression
                  Set<Integer> setOfIDs = new HashSet<Integer>();
                  getAlgEqIDInExpression(aee.getExpression(),setOfIDs);
                  
                  //--------- call algebraic functions
                  Iterator<Integer> ite0 = setOfIDs.iterator();
                  while (ite0.hasNext()){
                    Integer tmp0 = ite0.next(); //id of algebraic eq found in the expression
                    bw.write("        realtype var_" + lookUpTable.lookUpString(tmp0) + " = " 
                              + lookUpTable.lookUpString(tmp0) + "(time,condition");
                    
                    Set<Integer> s0 = signatures.get(tmp0);
                    if (s0 != null){
                      Iterator<Integer> ite3 = s0.iterator();
                      while (ite3.hasNext()){
                        Integer tmp3 = ite3.next();
                        bw.write("," + lookUpTable.lookUpString(tmp3) + ""); 
                      }
                    } 
                    bw.write(");\n");
                    
                    //call also the derivative
                    if (type == GLSDCTool.PARAMETER){
                      bw.write("        realtype var_" + lookUpTable.lookUpString(tmp0) 
                              + "__der__" + lookUpTable.lookUpString(paramID) + " = " 
                              + lookUpTable.lookUpString(tmp0) + "__der__" + lookUpTable.lookUpString(paramID)  + "(time,condition");
                    }else if (type == GLSDCTool.DIFFERENTIAL){
                      bw.write("        realtype var_" + lookUpTable.lookUpString(tmp0) 
                              + "__der__ = " 
                              + lookUpTable.lookUpString(tmp0) + "__der__(time,condition");
                    }
                    s0 = signatures.get(tmp0);
                    if (s0 != null){
                      Iterator<Integer> ite3 = s0.iterator();
                      while (ite3.hasNext()){
                        Integer tmp3 = ite3.next();
                        bw.write("," + lookUpTable.lookUpString(tmp3)); 
                        if (type == GLSDCTool.PARAMETER){
                          bw.write("," + lookUpTable.lookUpString(tmp3) + "__der__" + lookUpTable.lookUpString(paramID)); 
                        }else if (type == GLSDCTool.DIFFERENTIAL){
                          bw.write("," + lookUpTable.lookUpString(tmp3) + "__der__"); 
                        }
                      }
                    } 
                    bw.write(");\n");
                    
                  }
                  if (setOfIDs.size() > 0) bw.write("\n");
                
                  //--------- done calling algebraic functions
          
                  
                  bw.write("        tmp =");
                  
                  try{
										bw.write(writeDerivative(aee.getExpression(),paramID,2,bw));
                  }catch(DerivativeZeroException dze){
										bw.write(" 0.0 ");
									}
                  bw.write(";\n");
                  
                }else if (tmp2 instanceof AlgebraicEquationCondInterpolation){
                 
                  //interpolation has derivative 0
                  //-------------------------------------
                  bw.write("        tmp = 0.0;");
                  
                }else{
                  throw new Exception("[error] Probably a "
                    + "coding error (GLSDCTool.printAlgebraicEquationsFunctions()). Unknown type of AlgebraicEquationCond.");
                }
                //end case
                bw.write("      }\n"
                  + "      break;\n");
                
              }
              
              //close the switch
              bw.write("    default:\n"
                + "      tmp = 0;\n"
                + "  }\n  return tmp;\n");
              
            }else{
              throw new Exception("[error] Probably a "
                + "coding error (GLSDCTool.printAlgebraicEquationsFunctions()). Unknown type of AlgebraicEquationNode.");
            }
            
            
            //---------done with the function
            
            bw.write("}\n\n");

            //--------
          }
      }//--------end of if writeDer
 
      //------------------------------------------------
      // End of writing all derivatives
      //------------------------------------------------
  
      
    }
  
  }
  
  private void getAlgEqIDInExpression(Expression expr, Set<Integer> set) throws Exception{
    
    if (expr instanceof ExprID){
      ExprID e = (ExprID)expr; //downcast
      Integer id = e.getID();
      int type = QueryModelAndData.getIDType(entryLists,id);
      //add id to appropriate set if it is an id of an algebraic or differential or parameter entry
      if (type == ALGEBRAIC){
        set.add(id);
      }else if (type == DIFFERENTIAL){
         //do nothing     
      }else if (type == PARAMETER){
         //do nothing      
      }else {
        throw new Exception("[error] Unknown type of ID in Expression. Cannot find: " + lookUpTable.lookUpString(id));        
      }
    }else if(expr instanceof ExprCap){
      ExprCap e = (ExprCap)expr; //downcast
      getAlgEqIDInExpression(e.getExpression1(),set);
      getAlgEqIDInExpression(e.getExpression2(),set);
    }else if(expr instanceof ExprCos){
      ExprCos e = (ExprCos)expr; //downcast
      getAlgEqIDInExpression(e.getExpression(),set);
    }else if(expr instanceof ExprDiv){
      ExprDiv e = (ExprDiv)expr; //downcast
      getAlgEqIDInExpression(e.getExpression1(),set);
      getAlgEqIDInExpression(e.getExpression2(),set);
    }else if(expr instanceof ExprDOUBLE){
      ExprDOUBLE e = (ExprDOUBLE)expr; //downcast
      //do nothing
    }else if(expr instanceof ExprExp){
      ExprExp e = (ExprExp)expr; //downcast
      getAlgEqIDInExpression(e.getExpression(),set);
    }else if(expr instanceof ExprIfThenElse){
      ExprIfThenElse e = (ExprIfThenElse)expr; //downcast
      getAlgEqIDInBExpression(e.getBExpression(),set);
      getAlgEqIDInExpression(e.getExpression1(),set);
      getAlgEqIDInExpression(e.getExpression2(),set);
    }else if(expr instanceof ExprINT){
      ExprINT e = (ExprINT)expr; //downcast
      //do nothing
    }else if(expr instanceof ExprTime){
      //do nothing
    }else if(expr instanceof ExprLn){
      ExprLn e = (ExprLn)expr; //downcast
      getAlgEqIDInExpression(e.getExpression(),set);
    }else if(expr instanceof ExprNegation){
      ExprNegation e = (ExprNegation)expr; //downcast
      getAlgEqIDInExpression(e.getExpression(),set);
    }else if(expr instanceof ExprPar){
      ExprPar e = (ExprPar)expr; //downcast
      getAlgEqIDInExpression(e.getExpression(),set);
    }else if(expr instanceof ExprSin){
      ExprSin e = (ExprSin)expr; //downcast
      getAlgEqIDInExpression(e.getExpression(),set);
    }else if(expr instanceof ExprSqrt){
      ExprSqrt e = (ExprSqrt)expr; //downcast
      getAlgEqIDInExpression(e.getExpression(),set);
    }else if(expr instanceof ExprMinus){
      ExprMinus e = (ExprMinus)expr; //downcast
      getAlgEqIDInExpression(e.getExpression1(),set);
      getAlgEqIDInExpression(e.getExpression2(),set);
    }else if(expr instanceof ExprMul){
      ExprMul e = (ExprMul)expr; //downcast
      getAlgEqIDInExpression(e.getExpression1(),set);
      getAlgEqIDInExpression(e.getExpression2(),set);
    }else if(expr instanceof ExprPlus){
      ExprPlus e = (ExprPlus)expr; //downcast
      getAlgEqIDInExpression(e.getExpression1(),set);
      getAlgEqIDInExpression(e.getExpression2(),set);
    }else if(expr instanceof ExprFunCall){
      ExprFunCall e = (ExprFunCall)expr; //downcast
      Iterator<Expression> ite = e.getArguments().iterator();
      while (ite.hasNext()){
        Expression tmp = ite.next();      
        getAlgEqIDInExpression(tmp,set);
      }
    }else if(expr instanceof ExprLocalPar){
      ExprLocalPar e = (ExprLocalPar)expr; //downcast
      // do nothing
    }else{
      throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.getAlgEqIDInExpression()). Unknown type of Expression.");
    }
  }
  
  private void getAlgEqIDInBExpression(BExpression bexp, Set<Integer> set) throws Exception{
    
    if (bexp instanceof BExprAND){
      BExprAND b = (BExprAND)bexp; //downcast
      getAlgEqIDInBExpression(b.getBExpression1(),set);
      getAlgEqIDInBExpression(b.getBExpression2(),set);
    } else if (bexp instanceof BExprDiff){
      BExprDiff b = (BExprDiff)bexp; //downcast
      getAlgEqIDInExpression(b.getExpression1(),set);
      getAlgEqIDInExpression(b.getExpression2(),set);
    }else if (bexp instanceof BExprEqual){
      BExprEqual b = (BExprEqual)bexp; //downcast
      getAlgEqIDInExpression(b.getExpression1(),set);
      getAlgEqIDInExpression(b.getExpression2(),set);
    }else if (bexp instanceof BExprGE){
      BExprGE b = (BExprGE)bexp; //downcast
      getAlgEqIDInExpression(b.getExpression1(),set);
      getAlgEqIDInExpression(b.getExpression2(),set);
    }else if (bexp instanceof BExprGT){
      BExprGT b = (BExprGT)bexp; //downcast
      getAlgEqIDInExpression(b.getExpression1(),set);
      getAlgEqIDInExpression(b.getExpression2(),set);
    }else if (bexp instanceof BExprLE){
      BExprLE b = (BExprLE)bexp; //downcast
      getAlgEqIDInExpression(b.getExpression1(),set);
      getAlgEqIDInExpression(b.getExpression2(),set);
    }else if (bexp instanceof BExprLT){
      BExprLT b = (BExprLT)bexp; //downcast
      getAlgEqIDInExpression(b.getExpression1(),set);
      getAlgEqIDInExpression(b.getExpression2(),set);
    }else if (bexp instanceof BExprOR){
      BExprOR b = (BExprOR)bexp; //downcast
      getAlgEqIDInBExpression(b.getBExpression1(),set);
      getAlgEqIDInBExpression(b.getBExpression2(),set);
    }else if (bexp instanceof BExprPar){
      BExprPar b = (BExprPar)bexp; //downcast
      getAlgEqIDInBExpression(b.getBExpression(),set);
    }else if (bexp instanceof BExprNot){
      BExprNot b = (BExprNot)bexp; //downcast
      getAlgEqIDInBExpression(b.getBExpression(),set);
    }else if (bexp instanceof BExprFALSE){
      //do nothing
    }else if (bexp instanceof BExprTRUE){
      //do nothing
    }else{
      throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.writeBExpression()). Unknown type of BExpression.");
    }

  }
  
  /**
   * retrive a set of conditions IDs that belong to DataNodes with the 
   * given speciesID. It could be that more than one DataNode contains
   * the same speciesID (as long as the conditions are different)
   * */
  
  private Set<Integer> conditionsOfSpeciesIDInData(Integer speciesID) throws Exception{
    Set<Integer> s = new HashSet<Integer>();
    
    Iterator<GenericEntry> ite = entryLists.getDataList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof DataNodeReplicates)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.conditionsOfSpeciesIDInData()). The GenericEntry object is not a DataNodeReplicates object");
      DataNodeReplicates dnr = (DataNodeReplicates) tmp;
      if (dnr.getIdentifier().equals(speciesID)){
        s.addAll(dnr.getConditions());
      }
    }
    
    return s;
  }
  
  /**
   * retrieve the normalisation node corresponding to the given DataNodeReplicates object
   * there should be at most one such normalisation node, as it is forbidden
   * for two normalisaitons to take place on the same DataNodeReplicates object
   * return null if not found
   * */
  
  private NormalisationNode getNormalisationNodeOf(DataNodeReplicates dnr) throws Exception{
    
    Iterator<GenericEntry> ite3 = entryLists.getNormList().iterator();
    while (ite3.hasNext()){
      GenericEntry tmp3 = ite3.next();
      if (! (tmp3 instanceof NormalisationNode)) throw new Exception("[error] Probably a "
      + "coding error (GLSDCTool.getNormalisationNodeOf()). The GenericEntry object is not a NormalisationNode object");
      if (tmp3 instanceof NormalisationNodeFixedPoint){
        NormalisationNodeFixedPoint nn = (NormalisationNodeFixedPoint) tmp3; //downcast

        if (nn.getIdentifier().equals(dnr.getIdentifier()) && dnr.getConditions().contains(nn.getCondition())){
          
            return (NormalisationNode)nn;
        }
        
      }else if (tmp3 instanceof NormalisationNodeAverage){   
        NormalisationNodeAverage nn = (NormalisationNodeAverage) tmp3; //downcast   
        
        if (nn.getIdentifier().equals(dnr.getIdentifier()) && dnr.getConditions().containsAll(nn.getConditions())){
          
            return (NormalisationNode)nn;
        }        
        
      }else {
        throw new Exception("[error] Probably a "
          + "coding error (GLSDCTool.getNormalisationNodeOf()). Unknown NormalisationNode object");  
      }
    }
    
    return null;
  }
  

  

  
  /**
   * print additional function signatures for ODEsimulator.c
   * */
  private void printAdditionalODEsimulatorSignatures(BufferedWriter bw) throws Exception{
    bw.write("void printSimsToFileAndComputeAverageOfSims(double***sims,double **average,const char*filename);\n");
    bw.write("void computeStd(double***sims,double**average,double**std);\n");
    bw.write("void printAveStdToFile(double**average,double**std,const char*filename);\n\n");
  }
  
  private void printAdditionalODEsimulatorFunctions(BufferedWriter bw) throws Exception{  
    bw.write("void printSimsToFileAndComputeAverageOfSims(double***sims,double **average,const char*filename){\n\n");

    bw.write("  FILE *pFile = fopen(filename,\"w\");\n\n");
  
    bw.write("  //initialise average matrix\n"
          + "  for(int timePoint=0;timePoint<=TIMESTEPS;timePoint++){\n"
          + "    for(int species=0;species<S_n+1;species++){\n"
          + "      average[species][timePoint] = 0;\n"
          + "    }\n"
          + "  }\n\n");
      
    bw.write("  for(int fit=0;fit<numberOfFits;fit++){\n"
            + "    for(int timePoint=0;timePoint<=TIMESTEPS;timePoint++){\n"
            + "      for(int species=0;species<S_n+1;species++){\n"
            + "        fprintf(pFile,\"%.16f \",sims[fit][species][timePoint]);\n"
            + "        average[species][timePoint] += (sims[fit][species][timePoint]/numberOfFits);\n"
            + "      }\n"
            + "      fprintf(pFile,\"\\n\");\n"
            + "    }\n"
            + "    fprintf(pFile,\"\\n\\n\");\n"
            + "  }\n"
            + "  fclose(pFile);\n"
            + "}\n\n");
            
  bw.write("void computeStd(double***sims,double**average,double**std){\n\n");
  
  bw.write("  //initialise std matrix\n"
            + "  for(int timePoint=0;timePoint<=TIMESTEPS;timePoint++){\n"
            + "    for(int species=0;species<S_n+1;species++){\n"
            + "      std[species][timePoint] = 0;\n"
            + "    }\n"
            + "  }\n\n");
            


  bw.write("  for(int species=1;species<S_n+1;species++){ //skip time\n"
          + "    for(int timePoint=0;timePoint<=TIMESTEPS;timePoint++){\n"
          + "      for(int fit=0;fit<numberOfFits;fit++){\n"
          + "        std[species][timePoint] += SUNRpowerI(sims[fit][species][timePoint]-average[species][timePoint],2);\n"
          + "      }\n"
          + "      std[species][timePoint] /= numberOfFits;\n"
          + "      std[species][timePoint] = SUNRsqrt(std[species][timePoint]);\n"
          + "    }\n"
          + "  }\n"
          + "}\n\n");


  bw.write("void printAveStdToFile(double**average,double**std,const char*filename){\n\n"
          + "  FILE *pFile = fopen(filename,\"w\");\n\n"
          + "  for(int timePoint=0;timePoint<=TIMESTEPS;timePoint++){\n"
          + "    for(int species=0;species<S_n+1;species++){\n"
          + "      fprintf(pFile,\"%.16f %.16f \",average[species][timePoint],std[species][timePoint]);\n"
          + "    }\n"
          + "    fprintf(pFile,\"\\n\");\n"
          + "  }  \n"
          + "  fclose(pFile);\n"
          + "}\n\n");
  
  }
  
  /**
   * data nodes set that is mentioned by normalisations
   * */
  private Set<DataNodeReplicates> getDataNodesOfNormalisations() throws Exception{
    Set<DataNodeReplicates> dataNodesSet = new HashSet<DataNodeReplicates>();

    Iterator<GenericEntry> ite11 = entryLists.getDataList().iterator();
    while (ite11.hasNext()){
      GenericEntry tmp = ite11.next();
      if (! (tmp instanceof DataNodeReplicates)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.dataNodesOfNormalisations()). The GenericEntry object is not a DataNodeReplicates object");
      DataNodeReplicates dnr = (DataNodeReplicates) tmp;
      
      //check if dnr has an associated normalisation
      NormalisationNode nnode = getNormalisationNodeOf(dnr);
      
      if (nnode!=null){  
        dataNodesSet.add(dnr);
      }
            
    }
    return dataNodesSet;
  }

    
  private void printMainODEsimulator(Set<Integer> conditionsToSimulate, Map<Integer,Set<Integer>> signatures, BufferedWriter bw) throws Exception{

    
    //------------- CHECK that the normalisations of simulations are correct
    
    Iterator<NormSimulationsNode> iteNormSim = simulateOptions.getListOfNormalisations().iterator();
    while (iteNormSim.hasNext()){
      NormSimulationsNode tmp = iteNormSim.next();
      
      //check that there is no data entry with the corresponding ID and conditions to be normalised
      Iterator<Integer> iteCond = tmp.getConditions().iterator();
      while (iteCond.hasNext()){
        Integer cond = iteCond.next();
         
         
        if (!conditionsToSimulate.contains(cond)){
          throw new Exception("[error] simulationOption normSimulations for " 
                + lookUpTable.lookUpString(tmp.getIdentifier()) + " cannot be applied because condition " + lookUpTable.lookUpString(cond) 
                + " is not requested for simulation.\n");
        }
         
        DataNodeReplicates dnr = QueryModelAndData.getDataNodeReplicatesOf(entryLists,tmp.getIdentifier(),cond);
        if (dnr!=null) throw new Exception("[error] simulationOption normSimulations for " 
                + lookUpTable.lookUpString(tmp.getIdentifier()) + " cannot be applied because there exists a data entry "
                + "with the same ID that already has one (" + lookUpTable.lookUpString(cond) + ") or more conditions requested for normalisation. Simulations of conditions with "
                + "data are automatically normalised using the data normalisations normaliseByFixedPoint or normaliseByAverage.\n");
      }
      
      
      if (tmp instanceof NormSimulationsWithData){
        NormSimulationsWithData ns = (NormSimulationsWithData) tmp;
        DataNode dn = getDataNodeWith(ns.getDataID(),ns.getDataConditions());
        if (dn==null) throw new Exception("[error] simulationOption normSimulations withData for " 
                + lookUpTable.lookUpString(ns.getIdentifier()) + " cannot be applied because the requested data entry cannot be found.\n");
      }else if(tmp instanceof NormSimulationsWithoutData){
        NormSimulationsWithoutData ns = (NormSimulationsWithoutData) tmp;
        if (!conditionsToSimulate.contains(ns.getNormCondition())){
          throw new Exception("[error] simulationOption normSimulations for " 
                + lookUpTable.lookUpString(ns.getIdentifier()) + " cannot be applied because normalising condition " + lookUpTable.lookUpString(ns.getNormCondition()) 
                + " is not requested for simulation.\n");
        }
        if (!ns.getConditions().contains(ns.getNormCondition())) throw new Exception("[error] simulationOption normSimulations for "
                + lookUpTable.lookUpString(ns.getIdentifier()) + "by specified condition " + lookUpTable.lookUpString(ns.getNormCondition())
                + "cannot be applied because the condition is not in the list of conditions to be normalised.\n");
        if (simulateOptions.getInitialTime() > ns.getNormTime().doubleValue()) throw new Exception("[error] simulationOption normSimulations for "
                + lookUpTable.lookUpString(ns.getIdentifier()) + "by specified condition " + lookUpTable.lookUpString(ns.getNormCondition())
                + "cannot be applied because the specified time of normalisation " + ns.getNormTime().doubleValue() 
                + " is smaller than the initial simulation time " + simulateOptions.getInitialTime() + ".\n");
        if (simulateOptions.getFinalTime() < ns.getNormTime().doubleValue()) throw new Exception("[error] simulationOption normSimulations for "
                + lookUpTable.lookUpString(ns.getIdentifier()) + "by specified condition " + lookUpTable.lookUpString(ns.getNormCondition())
                + "cannot be applied because the specified time of normalisation " + ns.getNormTime().doubleValue() 
                + " is greater than the final simulation time " + simulateOptions.getFinalTime() + ".\n");
      }else{
        throw new Exception("[error] probably a coding error: Unknown type of NormSimulationsNode in method GLSDC.printMainODEsimulator()");
      }
    }
    
    
    //------------- DONE CHECKING that the normalisations of simulations are correct

    
    int problemDim = entryLists.getDiffList().size() + entryLists.getParamList().size();
      
    bw.write("int main(int argc, char **argv)\n"
          + "{\n");  
      
    bw.write("  if (argc == 2){\n"
            + "    //use number of fits from input line\n"
            + "    numberOfFits = atoi(argv[1]);\n"
            + "  }\n\n");
            
    //if log likelihood was used for fitting, then there are additional
    //parameters in the best fits file that need to be skipped
    //so I need a skip variable
    int typeOfLL = fittingOptions.getLogLikelihood();
    if (typeOfLL == LogLikelihoodTool.IndependentNoise || typeOfLL == LogLikelihoodTool.LinearModelOfNoise){
      bw.write("  //variable to skip additional params (e.g. logLikelihood additional params)\n  double skip = 0;\n\n");
    }
  
    bw.write("  //Read parameters of the model from standard input\n"
            + "  double params[numberOfFits][" + problemDim + "];\n\n");
  
    bw.write("  for(int j=0;j<numberOfFits;j++){\n\n");
  
    bw.write("    //values of initial conditions will be set later, here set to 0\n");
    
    int i = 0; //parameter counter
    
    Iterator<GenericEntry> ite = entryLists.getDiffList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof DifferentialEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printMainODEsimulator()). The GenericEntry object is not a DifferentialEquationNode object");
      DifferentialEquationNode den = (DifferentialEquationNode) tmp;
      
      bw.write("    params[j][" + i + "] = 0; // " + lookUpTable.lookUpString(den.getIdentifier()) + "_0 \n");
      
      i++;
    } 
    
    bw.write("    //values of parameters\n");
    
    ite = entryLists.getParamList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof ParameterNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printMainODEsimulator()). The GenericEntry object is not a ParameterNode object");
      if (tmp instanceof ParameterValue){
        
        ParameterValue pv = (ParameterValue)tmp;
        bw.write("    params[j][" + i + "] = " + pv.getValue().doubleValue() + "; // " + lookUpTable.lookUpString(pv.getIdentifier()) + " \n");
      
      }else if (tmp instanceof ParameterRange){
        
        ParameterRange pr = (ParameterRange)tmp;
        bw.write("    std::cin >> params[j][" + i + "]; // " + lookUpTable.lookUpString(pr.getIdentifier()) + " \n");

      }else {
        throw new Exception("[error] Probably a "
          + "coding error (GLSDCTool.printMainODEsimulator()). Unknown type of ParameterNode");
      }
      
      i++;
    } 
    //if log likelihood was used for fitting, then there are additional
    //parameters in the best fits file that need to be skipped
    if (typeOfLL == LogLikelihoodTool.IndependentNoise || typeOfLL == LogLikelihoodTool.LinearModelOfNoise){
      int nparams = logLikelihoodTool.getListOfAdditionalParams().size();
      bw.write("    for (int i = 0;i<" + nparams + ";i++){\n"
        + "      std::cin >> skip;\n"
        + "    }\n\n");
    }
    
    bw.write("  }\n  //done reading parameter\n\n");

  
    bw.write("  int rv = 0;\n"
          + "  const int neq = M_n;\n"
          //+ "  realtype ropt[OPT_SIZE] = { 0 };\n"
          //+ "  long iopt[OPT_SIZE] = { 0 };\n"
          + "  realtype reltol = RTOL;\n"
          + "  void *cvode_mem = NULL;\n"
          + "  realtype t;\n"
          + "  int i;\n");
    
    //obtain data nodes of normalisations
    Set<DataNodeReplicates> dataNodesSet =  getDataNodesOfNormalisations();
    
    //------ print simulations data structures
    bw.write("  //data structures to store simulations for the normalisations\n");
    bw.write("  //need also a time point counter for each data structure used to store simulations\n"
            + "  //and a scaling factor if a normalisation is necessary\n");
    
    Iterator<DataNodeReplicates> ite3 = dataNodesSet.iterator();
    while (ite3.hasNext()){
      DataNodeReplicates dnr = ite3.next();       
      String uniqueID = QueryModelAndData.buildUniqueIDFrom(lookUpTable,(DataNode)dnr);
      
      bw.write("  " + uniqueID + "struct " + uniqueID + "sims[numberOfFits][" + uniqueID + "ntimepoints];\n"
          + "  int currentTimePoint" + uniqueID + ";\n");
      NormalisationNode nnode = getNormalisationNodeOf(dnr);
        
      if (nnode != null){ //there is a normalisation for the DataNodeReplicates, so also for the simulations
        bw.write("  realtype scaling" + uniqueID + "[numberOfFits];\n");
        bw.write("  for(int i=0;i<numberOfFits;i++) scaling" + uniqueID + "[i] = 0;\n");
        if(nnode instanceof NormalisationNodeAverage){  //need additional data structure for normalisation by sum
          NormalisationNodeAverage ns = (NormalisationNodeAverage)nnode;
          
          //iterate the boolean arrays containing the indication of which 
          //time points to sum for the corresponding condition
          
          Iterator<Boolean[]> iteBoolArrays = ns.getTimePointsToSum().iterator();
          Iterator<Integer> iteConds = ns.getConditions().iterator();
          while (iteBoolArrays.hasNext() && iteConds.hasNext()){ //should have same length
            Boolean[] boolArray = iteBoolArrays.next();
            Integer cond = iteConds.next();
            
            bw.write("  bool toSum" + lookUpTable.lookUpString(dnr.getIdentifier()) + "c_" + cond.intValue() + "[" + uniqueID + "ntimepoints] = { ");
            for (int j = 0;j<boolArray.length-1;j++) bw.write(boolArray[j].toString() + ", ");
            bw.write(boolArray[boolArray.length-1].toString() + " }; // " + lookUpTable.lookUpString(cond) + "\n");
            
          }
          
        }
      }
    }
    bw.write("\n");
    
    Iterator<NormSimulationsNode> itenorm = simulateOptions.getListOfNormalisations().iterator();
    while (itenorm.hasNext()){
      NormSimulationsNode tmp = itenorm.next();
      if (tmp instanceof NormSimulationsWithoutData){
        NormSimulationsWithoutData nswd = (NormSimulationsWithoutData) tmp;
        String uniqueID = QueryModelAndData.buildUniqueIDFrom(lookUpTable,nswd.getIdentifier(),nswd.getConditions());
        bw.write("  realtype scaling" + uniqueID + "[numberOfFits];\n");
        bw.write("  for(int i=0;i<numberOfFits;i++) scaling" + uniqueID + "[i] = 0;\n");
        bw.write("  bool scalingAcquired" + uniqueID + "[numberOfFits];\n");
        bw.write("  for(int i=0;i<numberOfFits;i++) scalingAcquired" + uniqueID + "[i] = false;\n");
      } 
    }
    
    
    //------ done printing simulations data structures
    
    
    //other initialisations
    bw.write("  //**************************************************Init\n"
        + "  realtype t0 = INITIALTIME;\n"
        + "  X = new double[K_n];\n"
        + "  N_Vector abstol = make_atol(neq, ATOL);\n\n");
    
    bw.write("  int condition;\n  int currentParameterSet;\n\n");
    
    //------------------------- initialise simulation matrices
    
    bw.write("  //matrix conditions x fits x (time + species) x timepoints\n"
          + "  double ****simulationMatrix = new double***[" + conditionsToSimulate.size() + "];\n"
          + "  for(int c=0;c<" + conditionsToSimulate.size() + ";c++){\n"
          + "    simulationMatrix[c] = new double**[numberOfFits];\n"
          + "    for(int i=0;i<numberOfFits;i++){\n"
          + "      simulationMatrix[c][i] = new double*[S_n+1];\n"
          + "      for(int j=0;j<S_n+1;j++){\n"
          + "        simulationMatrix[c][i][j] = new double[TIMESTEPS+1];\n"
          + "      }\n"
          + "    }\n"
          + "  }\n\n");
          
    bw.write("  //average matrix\n"
          + "  double ***averageMatrix = new double**[" + conditionsToSimulate.size() + "];\n"
          + "  for(int c=0;c<" + conditionsToSimulate.size() + ";c++){\n"
          + "    averageMatrix[c] = new double*[S_n+1];\n"
          + "    for(int j=0;j<S_n+1;j++){\n"
          + "      averageMatrix[c][j] = new double[TIMESTEPS+1];\n"
          + "    }\n"
          + "  }\n\n");  
              
    bw.write("  //sample std matrix\n"
          + "  double ***stdMatrix = new double**[" + conditionsToSimulate.size() + "];\n"
          + "  for(int c=0;c<" + conditionsToSimulate.size() + ";c++){\n"
          + "    stdMatrix[c] = new double*[S_n+1];\n"
          + "    for(int j=0;j<S_n+1;j++){\n"
          + "      stdMatrix[c][j] = new double[TIMESTEPS+1];\n"
          + "    }\n"
          + "  }\n\n");   
    
    
    
    //--------------------------------------------
    
    //bw.write("  int condIter = 0;\n\n");

    //----------- SIMULATE ALL NECESSARY CONDITIONS
    
    //define pointers
    Iterator<Integer> ite90 = conditionsToSimulate.iterator();
    while (ite90.hasNext()){
      Integer cond = ite90.next();
      bw.write("  N_Vector y_c_" + cond.intValue() + " = NULL;\n");
    }
    
    
    Iterator<Integer> ite5 = conditionsToSimulate.iterator();  //for all conditions
    while (ite5.hasNext()){
      Integer currentCondition = ite5.next();
      
      bw.write("  //****************************** Simulate " + lookUpTable.lookUpString(currentCondition) + "\n\n");
      

      
      bw.write("  //N_Vector y_c_" + currentCondition.intValue() + ";\n"
              + "  condition = c_" + currentCondition.intValue() + ";\n\n");
              
      bw.write("  for (currentParameterSet = 0;currentParameterSet<numberOfFits;currentParameterSet++){\n\n");

      bw.write("    y_zero = params[currentParameterSet];\n"
              + "    memcpy(X,params[currentParameterSet] + M_n,K_n*sizeof(double));  //copy parameters to X\n"
              + "    y_c_" + currentCondition.intValue() + " = make_vector(neq, y_zero);\n\n");

      //reset triggers
      printEventTriggeredReset(bw);      
      
      Iterator<DataNodeReplicates> ite6 = dataNodesSet.iterator();
      while (ite6.hasNext()){
        DataNodeReplicates dnr = ite6.next();       
        String uniqueID = QueryModelAndData.buildUniqueIDFrom(lookUpTable,(DataNode)dnr);
        
        bw.write("    currentTimePoint" + uniqueID + " = 0;\n");
          
      }
      bw.write("\n");
      
      
      //--- set initial conditions
      QueryModelAndData.printInitialConditionsForCondition(entryLists,lookUpTable,false,currentCondition,bw);

      //~ bw.write("    iopt[MXSTEP] = 32767;\n"
            //~ + "    cvode_mem = CVodeMalloc(neq, F, t0, y_c_" + currentCondition.intValue() + ", BDF, NEWTON, SV, &reltol, abstol,\n"
            //~ + "                   &condition, stderr, TRUE, iopt, ropt, NULL);\n"               
            //~ + "    CVDense(cvode_mem, dFdy, NULL);\n\n");
            
      bw.write("    cvode_mem = CVodeCreate(CV_BDF,CV_NEWTON);\n"
            + "    CVodeSetUserData(cvode_mem, &condition);\n"
            + "    CVodeSetMaxNumSteps(cvode_mem, 10000);\n"
            + "    CVodeInit(cvode_mem,__F__,t0,y_c_" + currentCondition.intValue() + ");\n"
            + "    CVodeSStolerances(cvode_mem,RTOL,ATOL);\n"
            + "    CVDense(cvode_mem,neq);\n"
            + "    CVDlsSetDenseJacFn(cvode_mem, __Jac__);\n");

      bw.write("    if (!isproper(neq, y_c_" + currentCondition.intValue() + ")) { std::cerr << \"ODE simulator"
            + " cannot continue [isproper returned false] (condition \\\"\" << " + lookUpTable.lookUpString(currentCondition) 
            + " << \"\\\", parameter set \" << currentParameterSet+1 << \", time \" << t << \")\" << std::endl; rv = 1; goto end; }\n\n");
      
      //before entering simulation loop, check if we need to store initial conditions
      //this in case data time point at time 0 have been specified
      Iterator<DataNodeReplicates> ite7 = dataNodesSet.iterator();
      while (ite7.hasNext()){
        DataNodeReplicates dnr = ite7.next();       
        
        if (dnr.getConditions().contains(currentCondition)){
          String uniqueID = QueryModelAndData.buildUniqueIDFrom(lookUpTable,(DataNode)dnr);
          //up to here we have identified which data structures are relevant
          //for this condition
          
          bw.write("    if (" + uniqueID + "data[0][currentTimePoint" + uniqueID + "].time <= t0){\n"
                + "      " + uniqueID + "sims[currentParameterSet][currentTimePoint" + uniqueID + "].c_" + currentCondition.intValue() + " = ");
          
          
          int type = QueryModelAndData.getIDType(entryLists,dnr.getIdentifier());
          if(type==ALGEBRAIC){
            //type is algebraic, need to make a call
            
            bw.write(lookUpTable.lookUpString(dnr.getIdentifier()) + "(t0,condition");
      
            Set<Integer> s = signatures.get(dnr.getIdentifier());
            if (s != null){
              Iterator<Integer> ite8 = s.iterator();
              while (ite8.hasNext()){
                Integer par = ite8.next();
                bw.write(",NV_Ith_S(y_c_" + currentCondition.intValue() + "," + lookUpTable.lookUpString(par) + ")"); 
              }
            } 
            bw.write(");\n");
            
          }else if (type==DIFFERENTIAL){
            bw.write("NV_Ith_S(y_c_" + currentCondition.intValue() + "," + lookUpTable.lookUpString(dnr.getIdentifier()) + ");\n");
          }else{
            throw new Exception("[error] there is no Algebraic nor Differential entry for the fitting of " + lookUpTable.lookUpString(dnr.getIdentifier())
                        + " condition " + lookUpTable.lookUpString(currentCondition));
          }
          
          bw.write("      currentTimePoint" + uniqueID + "++;\n"
                + "    }\n");
          
        }
      }      
      
      bw.write("\n");
      
      //------- check for normalisation points of normalisations of simulations
      
      itenorm = simulateOptions.getListOfNormalisations().iterator();
      while (itenorm.hasNext()){
        NormSimulationsNode tmp = itenorm.next();
        if (tmp instanceof NormSimulationsWithoutData){
          NormSimulationsWithoutData nswd = (NormSimulationsWithoutData) tmp;
          String uniqueID = QueryModelAndData.buildUniqueIDFrom(lookUpTable,nswd.getIdentifier(),nswd.getConditions());
          //~ bw.write("  realtype scaling" + uniqueID + "[numberOfFits];\n");
          //~ bw.write("  for(int i=0;i<numberOfFits;i++) scaling" + uniqueID + "[i] = 0;\n");
          //~ bw.write("  bool scalingAcquired" + uniqueID + "[numberOfFits];\n");
          //~ bw.write("  for(int i=0;i<numberOfFits;i++) scalingAcquired" + uniqueID + "[i] = false;\n");
          if (nswd.getNormCondition().equals(currentCondition)){ //we are at the condition to be used for normalisation
            bw.write("    if (" + nswd.getNormTime() + " <= t0 && !scalingAcquired" + uniqueID + "[currentParameterSet]){\n"
                  + "      scaling" + uniqueID + "[currentParameterSet] = ");
            
            
            int type = QueryModelAndData.getIDType(entryLists,nswd.getIdentifier());
            if(type==ALGEBRAIC){
              //type is algebraic, need to make a call
              
              bw.write(lookUpTable.lookUpString(nswd.getIdentifier()) + "(t0,condition");
        
              Set<Integer> s = signatures.get(nswd.getIdentifier());
              if (s != null){
                Iterator<Integer> ite8 = s.iterator();
                while (ite8.hasNext()){
                  Integer par = ite8.next();
                  bw.write(",NV_Ith_S(y_c_" + currentCondition.intValue() + "," + lookUpTable.lookUpString(par) + ")"); 
                }
              } 
              bw.write(");\n");
              
            }else if (type==DIFFERENTIAL){
              bw.write("NV_Ith_S(y_c_" + currentCondition.intValue() + "," + lookUpTable.lookUpString(nswd.getIdentifier()) + ");\n");
            }else{
              throw new Exception("[error] there is no Algebraic nor Differential entry for " + lookUpTable.lookUpString(nswd.getIdentifier())
                          + " condition " + lookUpTable.lookUpString(currentCondition));
            }
            
            bw.write("      scalingAcquired" + uniqueID + "[currentParameterSet]=true;\n"
                  + "    }\n");
          }
        } 
      }
        
      //------- DONE check for normalisation points of normalisations of simulations
      
      //------- Store simulations of time point zero
      
      bw.write("    simulationMatrix[c_sims_" + currentCondition.intValue() + "][currentParameterSet][0][0] = t0; // time\n");
      
      int n = 1;
      Iterator<GenericEntry> iteTP0 = entryLists.getDiffList().iterator();
      while (iteTP0.hasNext()){
        GenericEntry tmp = iteTP0.next();
        if (! (tmp instanceof DifferentialEquationNode)) throw new Exception("[error] Probably a "
          + "coding error (GLSDCTool.printMainODEsimulator()). The GenericEntry object is not a DifferentialEquationNode object");
        DifferentialEquationNode den = (DifferentialEquationNode) tmp;
        
        bw.write("    simulationMatrix[c_sims_" + currentCondition.intValue() + "][currentParameterSet][s_" + den.getIdentifier() + "+1][0] = ");
        bw.write("NV_Ith_S(y_c_" + currentCondition.intValue() + "," + lookUpTable.lookUpString(den.getIdentifier()) + ");\n");
        
        n++;
      }

      iteTP0 = entryLists.getAlgList().iterator();
      while (iteTP0.hasNext()){
        GenericEntry tmp = iteTP0.next();
        if (! (tmp instanceof AlgebraicEquationNode)) throw new Exception("[error] Probably a "
          + "coding error (GLSDCTool.printMainODEsimulator()). The GenericEntry object is not a AlgebraicEquationNode object");
        AlgebraicEquationNode den = (AlgebraicEquationNode) tmp;
        
        bw.write("    simulationMatrix[c_sims_" + currentCondition.intValue() + "][currentParameterSet][s_" + den.getIdentifier() + "+1][0] = ");
        bw.write(lookUpTable.lookUpString(den.getIdentifier()) + "(t0,condition");
      
        Set<Integer> s = signatures.get(den.getIdentifier());
        if (s != null){
          Iterator<Integer> ite8 = s.iterator();
          while (ite8.hasNext()){
            Integer par = ite8.next();
            bw.write(",NV_Ith_S(y_c_" + currentCondition.intValue() + "," + lookUpTable.lookUpString(par) + ")"); 
          }
        } 
        bw.write(");\n");
                
        n++;
      }
      
      bw.write("\n");
      
      //------- DONE Storing simulations of time point zero
      
      //---------- enter main simulation loop
      
      bw.write("    for (i = 1; i <= TIMESTEPS; i++) {\n"
            + "      int flag = CVode(cvode_mem, t0 + i*(FINALTIME - t0)/TIMESTEPS, y_c_" + currentCondition.intValue() + ", &t, CV_NORMAL);\n");
      //~ bw.write("      if (flag != CV_SUCCESS) { std::cerr << \"ODE simulator"
            //~ + " cannot continue [flag returned INSUCCESS] (condition \\\"\" << " + lookUpTable.lookUpString(currentCondition) 
            //~ + " << \"\\\", parameter set \" << currentParameterSet+1 << \", time \" << t << \")\" << std::endl; rv = 1; goto end; }\n\n");
    
             
      bw.write("    if (flag != CV_SUCCESS) { \n"
        + "      std::cerr << \"ODE simulator"
        + " cannot continue [flag returned INSUCCESS] (condition \\\"\" << " + lookUpTable.lookUpString(currentCondition) 
        + " << \"\\\", parameter set \" << currentParameterSet+1 << \", time \" << t << \")\" << std::endl;\n"
        + "      if (flag == CV_TSTOP_RETURN) { \n"
        + "        fprintf(stderr,\"CV_TSTOP_RETURN\\n\");\n"
        + "      }else if (flag == CV_ROOT_RETURN) { \n"
        + "        fprintf(stderr,\"CV_ROOT_RETURN\\n\");\n"
        + "      }else if (flag == CV_MEM_NULL) { \n"
        + "        fprintf(stderr,\"CV_MEM_NULL\\n\");\n"
        + "      }else if (flag == CV_NO_MALLOC) { \n"
        + "        fprintf(stderr,\"CV_NO_MALLOC\\n\");\n"
        + "      }else if (flag == CV_ILL_INPUT) { \n"
        + "        fprintf(stderr,\"CV_ILL_INPUT\\n\");\n"
        + "      }else if (flag == CV_TOO_CLOSE) { \n"
        + "        fprintf(stderr,\"CV_TOO_CLOSE\\n\");\n"
        + "      }else if (flag == CV_TOO_MUCH_WORK) { \n"
        + "        fprintf(stderr,\"CV_TOO_MUCH_WORK\\n\");\n"
        + "      }else if (flag == CV_TOO_MUCH_ACC) { \n"
        + "        fprintf(stderr,\"CV_TOO_MUCH_ACC\\n\");\n"
        + "      }else if (flag == CV_ERR_FAILURE) { \n"
        + "        fprintf(stderr,\"CV_ERR_FAILURE\\n\");\n"
        + "      }else if (flag == CV_CONV_FAILURE) { \n"
        + "        fprintf(stderr,\"CV_CONV_FAILURE\\n\");\n"
        + "      }else if (flag == CV_LINIT_FAIL) { \n"
        + "        fprintf(stderr,\"CV_LINIT_FAIL\\n\");\n"
        + "      }else if (flag == CV_LSETUP_FAIL) { \n"
        + "        fprintf(stderr,\"CV_LSETUP_FAIL\\n\");\n"
        + "      }else if (flag == CV_LSOLVE_FAIL) { \n"
        + "        fprintf(stderr,\"CV_LSOLVE_FAIL\\n\");\n"
        + "      }else if (flag == CV_RHSFUNC_FAIL) { \n"
        + "        fprintf(stderr,\"CV_RHSFUNC_FAIL\\n\");\n"
        + "      }else if (flag == CV_FIRST_RHSFUNC_ERR) { \n"
        + "        fprintf(stderr,\"CV_FIRST_RHSFUNC_ERR\\n\");\n"
        + "      }else if (flag == CV_REPTD_RHSFUNC_ERR) { \n"
        + "        fprintf(stderr,\"CV_REPTD_RHSFUNC_ERR\\n\");\n"
        + "      }else if (flag == CV_UNREC_RHSFUNC_ERR) { \n"
        + "        fprintf(stderr,\"CV_UNREC_RHSFUNC_ERR\\n\");\n"
        + "      }\n"
        + "      rv = 1; goto end; \n"
        + "    }\n\n");

    
      //-------- Store simulations similarly as above (t instead of t0) and more indentation
    
      Iterator<DataNodeReplicates> ite9 = dataNodesSet.iterator();
      while (ite9.hasNext()){
        DataNodeReplicates dnr = ite9.next();       
        
        if (dnr.getConditions().contains(currentCondition)){
          String uniqueID = QueryModelAndData.buildUniqueIDFrom(lookUpTable,(DataNode)dnr);
          //up to here we have identified which data structures are relevant
          //for this condition
          
          bw.write("      if (" + uniqueID + "data[0][currentTimePoint" + uniqueID + "].time <= t && currentTimePoint" + uniqueID + " < " + uniqueID + "ntimepoints){\n"
                + "        " + uniqueID + "sims[currentParameterSet][currentTimePoint" + uniqueID + "].c_" + currentCondition.intValue() + " = ");
          
          
          int type = QueryModelAndData.getIDType(entryLists,dnr.getIdentifier());
          if(type==ALGEBRAIC){
            //type is algebraic, need to make a call
            
            bw.write(lookUpTable.lookUpString(dnr.getIdentifier()) + "(t,condition");
      
            Set<Integer> s = signatures.get(dnr.getIdentifier());
            if (s != null){
              Iterator<Integer> ite10 = s.iterator();
              while (ite10.hasNext()){
                Integer par = ite10.next();
                bw.write(",NV_Ith_S(y_c_" + currentCondition.intValue() + "," + lookUpTable.lookUpString(par) + ")"); 
              }
            } 
            bw.write(");\n");
            
          }else if (type==DIFFERENTIAL){
            bw.write("NV_Ith_S(y_c_" + currentCondition.intValue() + "," + lookUpTable.lookUpString(dnr.getIdentifier()) + ");\n");
          }else{
            throw new Exception("[error] there is no Algebraic nor Differential entry for the fitting of " + lookUpTable.lookUpString(dnr.getIdentifier())
                        + " condition " + lookUpTable.lookUpString(currentCondition));
          }
          
          bw.write("        currentTimePoint" + uniqueID + "++;\n"
                + "      }\n");
          
        }
      }      
      
      bw.write("\n");
      
      //------- check for normalisation points of normalisations of simulations
      
      itenorm = simulateOptions.getListOfNormalisations().iterator();
      while (itenorm.hasNext()){
        NormSimulationsNode tmp = itenorm.next();
        if (tmp instanceof NormSimulationsWithoutData){
          NormSimulationsWithoutData nswd = (NormSimulationsWithoutData) tmp;
          String uniqueID = QueryModelAndData.buildUniqueIDFrom(lookUpTable,nswd.getIdentifier(),nswd.getConditions());
          //~ bw.write("  realtype scaling" + uniqueID + "[numberOfFits];\n");
          //~ bw.write("  for(int i=0;i<numberOfFits;i++) scaling" + uniqueID + "[i] = 0;\n");
          //~ bw.write("  bool scalingAcquired" + uniqueID + "[numberOfFits];\n");
          //~ bw.write("  for(int i=0;i<numberOfFits;i++) scalingAcquired" + uniqueID + "[i] = false;\n");
          if (nswd.getNormCondition().equals(currentCondition)){ //we are at the condition to be used for normalisation
            bw.write("      if (" + nswd.getNormTime() + " <= t && !scalingAcquired" + uniqueID + "[currentParameterSet]){\n"
                  + "        scaling" + uniqueID + "[currentParameterSet] = ");
            
            
            int type = QueryModelAndData.getIDType(entryLists,nswd.getIdentifier());
            if(type==ALGEBRAIC){
              //type is algebraic, need to make a call
              
              bw.write(lookUpTable.lookUpString(nswd.getIdentifier()) + "(t,condition");
        
              Set<Integer> s = signatures.get(nswd.getIdentifier());
              if (s != null){
                Iterator<Integer> ite8 = s.iterator();
                while (ite8.hasNext()){
                  Integer par = ite8.next();
                  bw.write(",NV_Ith_S(y_c_" + currentCondition.intValue() + "," + lookUpTable.lookUpString(par) + ")"); 
                }
              } 
              bw.write(");\n");
              
            }else if (type==DIFFERENTIAL){
              bw.write("NV_Ith_S(y_c_" + currentCondition.intValue() + "," + lookUpTable.lookUpString(nswd.getIdentifier()) + ");\n");
            }else{
              throw new Exception("[error] there is no Algebraic nor Differential entry for " + lookUpTable.lookUpString(nswd.getIdentifier())
                          + " condition " + lookUpTable.lookUpString(currentCondition));
            }
            
            bw.write("        scalingAcquired" + uniqueID + "[currentParameterSet]=true;\n"
                  + "      }\n");
          }
        } 
      }
        
      //------- DONE check for normalisation points of normalisations of simulations


      //------- Store simulations of time point i
      
      bw.write("      simulationMatrix[c_sims_" + currentCondition.intValue() + "][currentParameterSet][0][i] = t; // time\n");
      
      n = 1;
      iteTP0 = entryLists.getDiffList().iterator();
      while (iteTP0.hasNext()){
        GenericEntry tmp = iteTP0.next();
        if (! (tmp instanceof DifferentialEquationNode)) throw new Exception("[error] Probably a "
          + "coding error (GLSDCTool.printMainODEsimulator()). The GenericEntry object is not a DifferentialEquationNode object");
        DifferentialEquationNode den = (DifferentialEquationNode) tmp;
        
        bw.write("      simulationMatrix[c_sims_" + currentCondition.intValue() + "][currentParameterSet][s_" + den.getIdentifier() + "+1][i] = ");
        bw.write("NV_Ith_S(y_c_" + currentCondition.intValue() + "," + lookUpTable.lookUpString(den.getIdentifier()) + ");\n");
        
        n++;
      }

      iteTP0 = entryLists.getAlgList().iterator();
      while (iteTP0.hasNext()){
        GenericEntry tmp = iteTP0.next();
        if (! (tmp instanceof AlgebraicEquationNode)) throw new Exception("[error] Probably a "
          + "coding error (GLSDCTool.printMainODEsimulator()). The GenericEntry object is not a AlgebraicEquationNode object");
        AlgebraicEquationNode den = (AlgebraicEquationNode) tmp;
        
        bw.write("      simulationMatrix[c_sims_" + currentCondition.intValue() + "][currentParameterSet][s_" + den.getIdentifier() + "+1][i] = ");
        bw.write(lookUpTable.lookUpString(den.getIdentifier()) + "(t,condition");
      
        Set<Integer> s = signatures.get(den.getIdentifier());
        if (s != null){
          Iterator<Integer> ite8 = s.iterator();
          while (ite8.hasNext()){
            Integer par = ite8.next();
            bw.write(",NV_Ith_S(y_c_" + currentCondition.intValue() + "," + lookUpTable.lookUpString(par) + ")"); 
          }
        } 
        bw.write(");\n");
                
        n++;
      }
      
      bw.write("\n");
      
      //------- DONE Storing simulations 
      
      
      //-----------events
      printEvents(false,bw,currentCondition,signatures);
    
      //----------- closing the loop
    
      bw.write("      if (!isproper(neq, y_c_" + currentCondition.intValue() + ")) { std::cerr << \"ODE simulator"
            + " cannot continue [isproper returned false] (condition \\\"\" << " + lookUpTable.lookUpString(currentCondition) 
            + " << \"\\\", parameter set \" << currentParameterSet+1 << \", time \" << t << \")\" << std::endl; rv = 1; goto end; }\n"
            + "    }\n");
      
      bw.write("    if (cvode_mem != NULL) CVodeFree(&cvode_mem);\n"
            + "    cvode_mem = NULL;\n"
            + "    N_VDestroy_Serial(y_c_" + currentCondition.intValue() + ");\n"
            + "    y_c_" + currentCondition.intValue() + " = NULL;\n"
            + "  }\n\n");
            
      //bw.write("  condIter++;\n\n");
          

    }
    
    
    bw.write("\n");
    
    bw.write("  delete[] X;\n\n");
    //----------- DONE SIMULATING

    
    //----------- Normalisation of the Simulations    
 
    
    bw.write("  //--- Done with the simulations, now normalise simulations if necessary\n\n"); 
    bw.write("  for (currentParameterSet = 0;currentParameterSet<numberOfFits;currentParameterSet++){\n");    
    Iterator<DataNodeReplicates> ite11 = dataNodesSet.iterator();
    while (ite11.hasNext()){
      DataNodeReplicates dnr = ite11.next();
      
      //check if dnr has an associated normalisation
      NormalisationNode nnode = getNormalisationNodeOf(dnr);
        
      if (nnode != null){ //there is a normalisation for the DataNodeReplicates, so also for the simulations
        //apply nnode normalisation to the corresponding simulations
        //get unique id
        String uniqueID = QueryModelAndData.buildUniqueIDFrom(lookUpTable,(DataNode)dnr);
        
        
        if (nnode instanceof NormalisationNodeFixedPoint){
          NormalisationNodeFixedPoint nfp = (NormalisationNodeFixedPoint)nnode;
          
          bw.write("    //compute scaling" + uniqueID + " (fixedpoint normalisation)\n");
          bw.write("    scaling" + uniqueID + "[currentParameterSet] = " + uniqueID + "sims[currentParameterSet][" + nfp.getTimePointPosition() + "].c_" 
                + nfp.getCondition().intValue() + "; // " + lookUpTable.lookUpString(nfp.getCondition()) 
                + " at time " + dnr.getListOfDataMatrices().getFirst()[nfp.getTimePointPosition()][0] + "\n");
          bw.write("\n");
          
        }else if (nnode instanceof NormalisationNodeAverage){
          NormalisationNodeAverage ns = (NormalisationNodeAverage)nnode;
                  
          bw.write("    //compute scaling" + uniqueID + " (sum normalisation)\n");  
          
          
          bw.write("    for(i=0;i<" + uniqueID + "ntimepoints;i++){\n");    
          Iterator<Integer> iteConds = ns.getConditions().iterator();
          while (iteConds.hasNext()){
            Integer cond = iteConds.next();
            bw.write("      if (toSum" + lookUpTable.lookUpString(dnr.getIdentifier()) + "c_" + cond.intValue() + "[i]) scaling" 
                + uniqueID + "[currentParameterSet] += " + uniqueID + "sims[currentParameterSet][i].c_" + cond.intValue() + ";// " + lookUpTable.lookUpString(cond) + "\n");
          
          }
             
          bw.write("    }\n\n");
          bw.write("    scaling" + uniqueID + "[currentParameterSet] /= " + ns.getNumberOfDataPointsInTheSum() +".0;// divide by number of points in the sum\n\n");
          
          
        }else{
          throw new Exception("[error] probably a coding error: Unknown type of NormalisationNode in method GLSDC.printMainODEsimulator()");
        }
        
        //write normalisation procedure for the current simulations (normalise a condition only if it is a simulated condition
        //i.e. it belongs to conditionsToSimulate)
        //I also need to normalise simulations that don't have corresponding data
        bw.write("    for(i=0;i<=TIMESTEPS;i++){\n");
        
        Iterator<Integer> ite12 = dnr.getConditions().iterator();
        if (ite12.hasNext()) ite12.next(); //skip time
        while (ite12.hasNext()){
          Integer cond = ite12.next();
          if (conditionsToSimulate.contains(cond)){
            bw.write("      simulationMatrix[c_sims_"+ cond.intValue() +"][currentParameterSet][s_" + dnr.getIdentifier() + "+1][i] /= scaling" + uniqueID + "[currentParameterSet]; // " + lookUpTable.lookUpString(cond) + "\n");
          }          
        }
        
        //normalise additional simulations by current data-related scaling factor
        //we know all conditions requested exist, because we checked above the consistency of the NormSimulationsNode objects
        
        Iterator<NormSimulationsNode> iteNS = simulateOptions.getListOfNormalisations().iterator();
        while (iteNS.hasNext()){
          NormSimulationsNode tmpNS = iteNS.next();
          
          if (tmpNS instanceof NormSimulationsWithData){
            NormSimulationsWithData ns = (NormSimulationsWithData) tmpNS;
            if (dnr.getIdentifier().equals(ns.getDataID()) && dnr.getConditions().equals(ns.getDataConditions())){
              Iterator<Integer> itec = ns.getConditions().iterator();
              while (itec.hasNext()){
                Integer tmpc = itec.next();
                bw.write("      simulationMatrix[c_sims_"+ tmpc.intValue() +"][currentParameterSet][s_" + ns.getIdentifier() + "+1][i] /= scaling" + uniqueID + "[currentParameterSet]; // " + lookUpTable.lookUpString(ns.getIdentifier()) + " " + lookUpTable.lookUpString(tmpc) + "\n");
              }
              
            }
            
          }else if(tmpNS instanceof NormSimulationsWithoutData){
            //ignore
          }else{
            throw new Exception("[error] probably a coding error: Unknown type of NormSimulationsNode in method GLSDC.getAdditionalSimulatedConditionsToNormaliseBy()");
          }
        }
        
        
        
        
        bw.write("    }\n\n");
        
      }
            
    }
    
    //----------- done with Normalisation of the Simulations
    
    //----------- additional Normalisation of the Simulations
    
    itenorm = simulateOptions.getListOfNormalisations().iterator();
    while (itenorm.hasNext()){
      NormSimulationsNode tmp = itenorm.next();
      if (tmp instanceof NormSimulationsWithoutData){
        NormSimulationsWithoutData nswd = (NormSimulationsWithoutData) tmp;
        String uniqueID = QueryModelAndData.buildUniqueIDFrom(lookUpTable,nswd.getIdentifier(),nswd.getConditions());
        //~ bw.write("  realtype scaling" + uniqueID + "[numberOfFits];\n");
        //~ bw.write("  for(int i=0;i<numberOfFits;i++) scaling" + uniqueID + "[i] = 0;\n");
        //~ bw.write("  bool scalingAcquired" + uniqueID + "[numberOfFits];\n");
        //~ bw.write("  for(int i=0;i<numberOfFits;i++) scalingAcquired" + uniqueID + "[i] = false;\n");
        bw.write("    //normalise by " + lookUpTable.lookUpString(nswd.getIdentifier()) + ", " + lookUpTable.lookUpString(nswd.getNormCondition()) + " at time " + nswd.getNormTime() + "\n");
        bw.write("    for(i=0;i<=TIMESTEPS;i++){\n");
        
        Iterator<Integer> ite12 = nswd.getConditions().iterator();
        //~ if (ite12.hasNext()) ite12.next(); //skip time
        while (ite12.hasNext()){
          Integer cond = ite12.next();
          //~ if (conditionsToSimulate.contains(cond)){
          bw.write("      simulationMatrix[c_sims_"+ cond.intValue() +"][currentParameterSet][s_" + nswd.getIdentifier() + "+1][i] /= scaling" + uniqueID + "[currentParameterSet]; // " + lookUpTable.lookUpString(cond) + "\n");
          //~ }          
        }
        bw.write("    }\n\n");
      } 
    }
    bw.write("  }\n\n");

    //----------- save simulations and ave sd to files
    
    
    bw.write("  //save simulations, averages and standard deviation to files\n\n");
    
    Iterator<Integer> ite00 = conditionsToSimulate.iterator();
    while (ite00.hasNext()){
      Integer currentCondition = ite00.next();
      bw.write("  // Condition " + lookUpTable.lookUpString(currentCondition) + "\n");
      bw.write("  printSimsToFileAndComputeAverageOfSims(simulationMatrix[c_sims_" + currentCondition.intValue() 
            + "],averageMatrix[c_sims_" + currentCondition.intValue() + "],\"sims/c_" + currentCondition.intValue() + "sims.txt\");\n"
            + "  computeStd(simulationMatrix[c_sims_" + currentCondition.intValue() + "],averageMatrix[c_sims_" + currentCondition.intValue() + "],stdMatrix[c_sims_" + currentCondition.intValue() + "]);\n"
            + "  printAveStdToFile(averageMatrix[c_sims_" + currentCondition.intValue() + "],stdMatrix[c_sims_" + currentCondition.intValue() + "],\"sims/c_" + currentCondition.intValue() + "simsAveStd.txt\");\n\n");
      
    }
    
    //------------- save dose response simulations
    
    printDoseResponseSimulationsFileWriting(conditionsToSimulate,bw);
    
    //------------- delete simulation data structures

    bw.write("  //matrix conditions x fits x (time + species) x timepoints\n"
            + "  for(int c=0;c<" + conditionsToSimulate.size() + ";c++){\n"
            + "    for(int i=0;i<numberOfFits;i++){\n"
            + "      for(int j=0;j<S_n+1;j++){\n"
            + "        delete[] simulationMatrix[c][i][j];\n"
            + "      }\n"
            + "      delete[] simulationMatrix[c][i];\n"
            + "    }\n"
            + "    delete[] simulationMatrix[c];\n"
            + "  }\n"
            + "  delete[] simulationMatrix;\n\n");

    bw.write("  //average matrix\n"
            + "  for(int c=0;c<" + conditionsToSimulate.size() + ";c++){\n"
            + "    for(int j=0;j<S_n+1;j++){\n"
            + "      delete[] averageMatrix[c][j];\n"
            + "    }\n"
            + "    delete[] averageMatrix[c];\n"
            + "  }\n"
            + "  delete[] averageMatrix;\n\n");

    bw.write("  //sample std matrix\n"
            + "  for(int c=0;c<" + conditionsToSimulate.size() + ";c++){\n"
            + "    for(int j=0;j<S_n+1;j++){\n"
            + "      delete[] stdMatrix[c][j];\n"
            + "    }\n"
            + "    delete[] stdMatrix[c];\n"
            + "  }\n"
            + "  delete[] stdMatrix;\n\n");
    
    
    //---------------- last lines below
    
    bw.write("  //**************************************************END\n");
    bw.write("  end:\n");
    
    Iterator<Integer> iteEnd = conditionsToSimulate.iterator();
    while (iteEnd.hasNext()){
      Integer tmp = iteEnd.next();
      bw.write("  if (y_c_" + tmp.intValue() + " != NULL) N_VDestroy_Serial(y_c_" + tmp.intValue() + "); // " + lookUpTable.lookUpString(tmp) + "\n"
            + "  y_c_" + tmp.intValue() + " = NULL;\n");
    }
    bw.write("\n");
    
    
    bw.write("  if (abstol != NULL) N_VDestroy_Serial(abstol);\n"
          + "  abstol = NULL;\n"
          + "  if (cvode_mem != NULL) CVodeFree(&cvode_mem);\n"
          + "  cvode_mem = NULL;\n\n");
    bw.write("  return rv;\n}\n\n");
  }
  
  
  private void generateDataFiles_ODEsimulator(BufferedWriter log) throws Exception{
    Iterator<GenericEntry> ite = entryLists.getDataList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof DataNodeReplicates)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.generateDataFiles_ODEsimulator()). The GenericEntry object is not a DataNodeReplicates object");
      DataNodeReplicates dnr = (DataNodeReplicates) tmp;
      
      String uniqueID = QueryModelAndData.buildUniqueIDFrom(lookUpTable,(DataNode) dnr);
      
      BufferedWriter bw = null; 
      String fileName = "KimuraODEsim/data/" + uniqueID + "_data.txt";
      
      try{
        //Output log file
        File outputFile = new File(fileName);        
        if (!outputFile.exists()) {
          outputFile.createNewFile();
        }
        FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
        bw = new BufferedWriter(fw);
        
        //--------------- WRITE FILE!
        
        Iterator<Double[][]> ite2 = dnr.getListOfDataMatrices().iterator();
        while (ite2.hasNext()){
          Double[][] matrix = ite2.next();
          
          for (int i=0;i<matrix.length;i++){
            for (int j=0;j<matrix[i].length;j++){
              bw.write(matrix[i][j].doubleValue() + " ");
            }
            bw.write("\n");
          }
          
        }
        
        //--------------- DONE WRITING!
        
      }catch (IOException e){
        throw new IOException("[error] IOException while generating " + fileName + "!\n");
      }finally{ //this is always executed, even if a return is called
        bw.flush();
        bw.close();
      }
    }
        log.write("[info] done writing data files in KimuraODEsim/data\n");
  }
  
  private void generatePlots_ODEsimulator(BufferedWriter log) throws Exception{
    
    LinkedList<String> listOfFiles = new LinkedList<String>();
    
    //--------------------------------------------------------------
    //----------------------- all simulations plots
    //--------------------------------------------------------------    
    
    Iterator<HashMap<Integer, LinkedList<Integer>>> ite = simulateOptions.getListOfIdCondList().iterator();
    while (ite.hasNext()){
      HashMap<Integer, LinkedList<Integer>> map = ite.next();
      String plotID = buildPlotID(map);
      
      BufferedWriter bw = null; 
      String fileName = "KimuraODEsim/plots/" + plotID + "plot.plt";
      listOfFiles.add(plotID + "plot.plt");
      
      try{
        //Output log file
        File outputFile = new File(fileName);        
        if (!outputFile.exists()) {
          outputFile.createNewFile();
        }
        FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
        bw = new BufferedWriter(fw);
        
        //--------------- WRITE FILE!
        
        bw.write("#!/usr/bin/gnuplot -persist\n\n"
              + "set term pngcairo dashed dl 5 enhanced size 2800, 3400 font \"Arial, 64\"\n\n"
              + "set lmargin at screen 0.15\n"
              + "set bmargin at screen 0.45\n"
              + "set rmargin at screen 0.9\n"
              + "set tmargin at screen 0.9\n"
              + "set size 1,1 \n"
              + "set key bmargin \n\n"
              + "set output \"" + plotID + "plot.png\"\n"
              + "set border lw 16\n\n"
              + "set title \"Time courses of \".numberOfBestFits.\" best parameter sets\"\n"
              + "set style line 1 linetype 1 lw 16 lc 1\n"
              + "set style line 2 linetype 2 lw 16 lc 2\n"
              + "set style line 3 linetype 3 lw 16 lc 3\n"
              + "set style line 4 linetype 4 lw 16 lc 4\n"
              + "set style line 5 linetype 5 lw 16 lc 5\n"
              + "set style line 6 linetype 6 lw 16 lc 7\n"
              + "set style line 11 linetype 1 lw 10 pt 2 ps 16 lc 1\n"
              + "set style line 12 linetype 2 lw 10 pt 3 ps 16 lc 2\n"
              + "set style line 13 linetype 3 lw 10 pt 4 ps 16 lc 3\n"
              + "set style line 14 linetype 4 lw 10 pt 6 ps 16 lc 4\n"
              + "set style line 15 linetype 5 lw 10 pt 8 ps 16 lc 5\n"
              + "set style line 16 linetype 6 lw 10 pt 1 ps 16 lc 7\n\n"
              + "set xrange [" + simulateOptions.getInitialTime() + ":" + simulateOptions.getFinalTime() + "]\n"
              + "set yrange [0:]\n"
              + "set xlabel \"time\"\n"
              + "set ylabel \"normalised level\"\n\n");
              
        //first write data if it is present then add the simultions
        String simsLines = "";
        boolean dataPresent = false;
        int lineStyle = 1;
        Iterator<Integer> ite00 = map.keySet().iterator();
        while (ite00.hasNext()){
          Integer key = ite00.next();
          LinkedList<Integer> list = map.get(key);
          lookUpTable.lookUpString(key);
          
          Iterator<Integer> ite2 = list.iterator();
          while (ite2.hasNext()){
            Integer cond = ite2.next();
            
            simsLines += "\"../sims/c_" + cond.intValue() + "sims.txt\" using 1:"
                  + (getPositionInSimulationMatrixOf(key)+1) + " with lines ls " + lineStyle + " t \'" 
                  + lookUpTable.lookUpString(key).replace("_","\\_") + ", " + lookUpTable.lookUpString(cond).replace("\"","").replace("_","\\_") + "\'";
            
            DataNodeReplicates dnr = QueryModelAndData.getDataNodeReplicatesOf(entryLists,key,cond);
            if (dnr != null) {
              if (dataPresent == false){
                bw.write("plot NaN lw 0 title \"Data\", \\\n");
                dataPresent = true;
              }
              String uniqueID = QueryModelAndData.buildUniqueIDFrom(lookUpTable,(DataNode)dnr);
              int pos = dnr.getConditions().indexOf(cond) + 1; //we know it is there, because dnr is not null
              bw.write("\"../data/" + uniqueID + "_data.txt\" using 1:" + pos + " ls 1" + lineStyle + " w p t \'" 
                  + lookUpTable.lookUpString(key).replace("_","\\_") + ", " + lookUpTable.lookUpString(cond).replace("\"","").replace("_","\\_") + "\', \\\n");
            }
            
            if (!ite00.hasNext() && !ite2.hasNext()){ //this was the last line to be plotted
              simsLines += "\n\n";
              //so conclude by plotting simulations
              if (dataPresent == true){ //data present
                bw.write("NaN lw 0 title \" \", \\\n"
                      + "NaN lw 0 title \"Simulations\", \\\n"
                      + simsLines);
              } else { //data not present
                bw.write("plot NaN lw 0 title \"Simulations\", \\\n"
                      + simsLines);              
              }
            } else { //we haven't finished plotting lines
              simsLines += ", \\\n";
               
            }
            
            lineStyle++;
          }

        }        
        
        //--------------- DONE WRITING!
        
      }catch (IOException e){
        throw new IOException("[error] IOException while generating " + fileName + "!\n");
      }finally{ //this is always executed, even if a return is called
        bw.flush();
        bw.close();
      }
    }

    //--------------------------------------------------------------
    //----------------------- ave sd simulations plots
    //--------------------------------------------------------------    
    
    ite = simulateOptions.getListOfIdCondList().iterator();
    while (ite.hasNext()){
      HashMap<Integer, LinkedList<Integer>> map = ite.next();
      String plotID = buildPlotID(map);
      
      BufferedWriter bw = null; 
      String fileName = "KimuraODEsim/plots/" + plotID + "plot_AveSD.plt";
      listOfFiles.add(plotID + "plot_AveSD.plt");
      
      try{
        //Output log file
        File outputFile = new File(fileName);        
        if (!outputFile.exists()) {
          outputFile.createNewFile();
        }
        FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
        bw = new BufferedWriter(fw);
        
        //--------------- WRITE FILE!
        
        bw.write("#!/usr/bin/gnuplot -persist\n\n"
              + "set term pngcairo dashed dl 5 enhanced size 2800, 3400 font \"Arial, 64\"\n\n"
              + "set lmargin at screen 0.15\n"
              + "set bmargin at screen 0.45\n"
              + "set rmargin at screen 0.9\n"
              + "set tmargin at screen 0.9\n"
              + "set size 1,1 \n"
              + "set key bmargin \n\n"
              + "set output \"" + plotID + "plot_AveSD.png\"\n"
              + "set border lw 16\n\n"
              + "set title \"Average and standard deviation of \".numberOfBestFits.\" best parameter sets\"\n"
              + "set style line 1 linetype 1 lw 16 lc 1\n"
              + "set style line 2 linetype 2 lw 16 lc 2\n"
              + "set style line 3 linetype 3 lw 16 lc 3\n"
              + "set style line 4 linetype 4 lw 16 lc 4\n"
              + "set style line 5 linetype 5 lw 16 lc 5\n"
              + "set style line 6 linetype 6 lw 16 lc 7\n"
              + "set style line 11 linetype 1 lw 10 pt 2 ps 16 lc 1\n"
              + "set style line 12 linetype 2 lw 10 pt 3 ps 16 lc 2\n"
              + "set style line 13 linetype 3 lw 10 pt 4 ps 16 lc 3\n"
              + "set style line 14 linetype 4 lw 10 pt 6 ps 16 lc 4\n"
              + "set style line 15 linetype 5 lw 10 pt 8 ps 16 lc 5\n"
              + "set style line 16 linetype 6 lw 10 pt 1 ps 16 lc 7\n\n"
              + "set xrange [" + simulateOptions.getInitialTime() + ":" + simulateOptions.getFinalTime() + "]\n"
              + "set yrange [0:]\n"
              + "set xlabel \"time\"\n"
              + "set ylabel \"normalised level\"\n\n");
              
        //first write data if it is present then add the simultions
        String simsLines = "";
        boolean dataPresent = false;
        int lineStyle = 1;
        Iterator<Integer> ite00 = map.keySet().iterator();
        while (ite00.hasNext()){
          Integer key = ite00.next();
          LinkedList<Integer> list = map.get(key);
          lookUpTable.lookUpString(key);
          
          Iterator<Integer> ite2 = list.iterator();
          while (ite2.hasNext()){
            Integer cond = ite2.next();
            
            //average
            int filePos = getPositionInSimulationMatrixOf(key)*2+1; // position of average in average/sd file
            simsLines += "\"../sims/c_" + cond.intValue() + "simsAveStd.txt\" using 1:"
                  + filePos + " with lines ls " + lineStyle + " t \'" 
                  + lookUpTable.lookUpString(key).replace("_","\\_") + ", " + lookUpTable.lookUpString(cond).replace("\"","").replace("_","\\_") + "\', \\\n";
            simsLines += "\"../sims/c_" + cond.intValue() + "simsAveStd.txt\" using 1:($"
                  + filePos + "+$" + (filePos+1) + ") with lines ls " + lineStyle + " notitle, \\\n";
            simsLines += "\"../sims/c_" + cond.intValue() + "simsAveStd.txt\" using 1:($"
                  + filePos + "-$" + (filePos+1) + ") with lines ls " + lineStyle + " notitle";
            
            DataNodeReplicates dnr = QueryModelAndData.getDataNodeReplicatesOf(entryLists,key,cond);
            if (dnr != null) {
              if (dataPresent == false){
                bw.write("plot NaN lw 0 title \"Data\", \\\n");
                dataPresent = true;
              }
              String uniqueID = QueryModelAndData.buildUniqueIDFrom(lookUpTable,(DataNode)dnr);
              int pos = dnr.getConditions().indexOf(cond) + 1; //we know it is there, because dnr is not null
              bw.write("\"../data/" + uniqueID + "_data.txt\" using 1:" + pos + " ls 1" + lineStyle + " w p t \'" 
                  + lookUpTable.lookUpString(key).replace("_","\\_") + ", " + lookUpTable.lookUpString(cond).replace("\"","").replace("_","\\_") + "\', \\\n");
            }
            
            if (!ite00.hasNext() && !ite2.hasNext()){ //this was the last line to be plotted
              simsLines += "\n\n";
              //so conclude by plotting simulations
              if (dataPresent == true){ //data present
                bw.write("NaN lw 0 title \" \", \\\n"
                      + "NaN lw 0 title \"Simulations\", \\\n"
                      + simsLines);
              } else { //data not present
                bw.write("plot NaN lw 0 title \"Simulations\", \\\n"
                      + simsLines);              
              }
            } else{ //we haven't finished plotting lines
              simsLines += ", \\\n";
               
            }
            
            lineStyle++;
          }

        }        
        
        //--------------- DONE WRITING!
        
      }catch (IOException e){
        throw new IOException("[error] IOException while generating " + fileName + "!\n");
      }finally{ //this is always executed, even if a return is called
        bw.flush();
        bw.close();
      }
    }

    //--------------------------------------------------------------
    //----------------------- all Dose Response plots
    //--------------------------------------------------------------    
    
    Integer speciesID = null;
    String speciesIDstring = null;
    Double time = null;
    String xlabel = null;
    TreeMap<Double,Integer> doses = null;
    String plotID = null;
    String fileName = null;
    
    Iterator<DoseResponsePlot> iteDRP = simulateOptions.getDoseResponsePlots().iterator();
    while (iteDRP.hasNext()){
      DoseResponsePlot drp = iteDRP.next();
      speciesID = drp.getSpeciesID();
      speciesIDstring = lookUpTable.lookUpString(speciesID).replace("_","\\_");
      time = drp.getTime();
      xlabel = drp.getXlabel();
      doses = drp.getDoses();
      plotID = buildDoseResponsePlotID(drp);
      fileName = "KimuraODEsim/plots/" + plotID + "_DR.plt";
      
      listOfFiles.add(plotID + "_DR.plt");
      
      BufferedWriter bw = null; 
      
      try{
        //Output log file
        File outputFile = new File(fileName);        
        if (!outputFile.exists()) {
          outputFile.createNewFile();
        }
        FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
        bw = new BufferedWriter(fw);
        
        //--------------- WRITE FILE!
        
        bw.write("#!/usr/bin/gnuplot -persist\n\n"
              + "set term pngcairo dashed dl 5 enhanced size 2800, 3400 font \"Arial, 64\"\n\n"
              + "set lmargin at screen 0.15\n"
              + "set bmargin at screen 0.45\n"
              + "set rmargin at screen 0.9\n"
              + "set tmargin at screen 0.9\n"
              + "set size 1,1 \n"
              + "set key bmargin \n\n"
              + "set output \"" + plotID + "_DR.png\"\n"
              + "set border lw 16\n\n"
              + "set title \"Dose Response at time " + time + " of \".numberOfBestFits.\" best parameter sets\"\n"
              + "set style line 1 linetype 1 lw 16 lc 1\n"
              + "set style line 2 linetype 2 lw 16 lc 2\n"
              + "set style line 3 linetype 3 lw 16 lc 3\n"
              + "set style line 4 linetype 4 lw 16 lc 4\n"
              + "set style line 5 linetype 5 lw 16 lc 5\n"
              + "set style line 6 linetype 6 lw 16 lc 7\n"
              + "set style line 11 linetype 1 lw 10 pt 2 ps 16 lc 1\n"
              + "set style line 12 linetype 2 lw 10 pt 3 ps 16 lc 2\n"
              + "set style line 13 linetype 3 lw 10 pt 4 ps 16 lc 3\n"
              + "set style line 14 linetype 4 lw 10 pt 6 ps 16 lc 4\n"
              + "set style line 15 linetype 5 lw 10 pt 8 ps 16 lc 5\n"
              + "set style line 16 linetype 6 lw 10 pt 1 ps 16 lc 7\n\n"
              + "set yrange [0:]\n"
              + "set xlabel \"" + xlabel.replace("\"","") + "\"\n"
              + "set ylabel \"normalised level\"\n\n");
        
        bw.write("plot  ");
              
        boolean dataPresent = false;
        String firstDataLine = "";
        String dataLines = "";
        Iterator<Double> iteD = doses.keySet().iterator();
        while (iteD.hasNext()){
          Double dose = iteD.next();
          Integer cond = doses.get(dose);
          DataNodeReplicates dnr = QueryModelAndData.getDataNodeReplicatesOf(entryLists,speciesID,cond);
          if (dnr != null){
            // I do have data for this species in this condition
            
            // need to find out the index of a certain time point and condition
            int timePos = getPositionOfTimePointInDataMatrix(dnr,time);
            int condPos = getPositionOfConditionInDataMatrix(dnr,cond);
            
            if (timePos != -1 && condPos != -1){
              // ok, se have data, one or more replicates!
              Iterator<Double[][]> ite2 = dnr.getListOfDataMatrices().iterator();
              while (ite2.hasNext()){
                Double[][] matrix = ite2.next();
                if (!dataPresent){
                  firstDataLine += "\'-\' using 1:2 ls 11 w p t \'" + speciesIDstring + "\', \\\n";
                  dataLines += "" + dose + " " + matrix[timePos][condPos] + "\n";
                  dataPresent = true;
                }else{
                  dataLines += "" + dose + " " + matrix[timePos][condPos] + "\n";
                }
              }          
              
            }
          }
        }
        
        if (dataPresent){
          bw.write("NaN lw 0 title \"Data\", \\\n"
                + firstDataLine + "NaN lw 0 title \" \", \\\n");
        }
        
        bw.write("NaN lw 0 title \"Simulations\", \\\n"
              + "\"../sims/" + plotID + "_DR.txt\" using 1:2 with lines ls 1 t \'" + speciesIDstring + "\'\n");
              
        if (dataPresent){
          bw.write(dataLines + "EOF\n");
        }
              
        //--------------- DONE WRITING!
        
      }catch (IOException e){
        throw new IOException("[error] IOException while generating " + fileName + "!\n");
      }finally{ //this is always executed, even if a return is called
        bw.flush();
        bw.close();
      }
    }

    //--------------------------------------------------------------
    //----------------------- Ave/SD Dose Response plots
    //--------------------------------------------------------------    
    
    
    iteDRP = simulateOptions.getDoseResponsePlots().iterator();
    while (iteDRP.hasNext()){
      DoseResponsePlot drp = iteDRP.next();
      speciesID = drp.getSpeciesID();
      speciesIDstring = lookUpTable.lookUpString(speciesID).replace("_","\\_");
      time = drp.getTime();
      xlabel = drp.getXlabel();
      doses = drp.getDoses();
      plotID = buildDoseResponsePlotID(drp);
      fileName = "KimuraODEsim/plots/" + plotID + "_DR_AveSD.plt";
      
      listOfFiles.add(plotID + "_DR_AveSD.plt");
      
      BufferedWriter bw = null; 
      
      try{
        //Output log file
        File outputFile = new File(fileName);        
        if (!outputFile.exists()) {
          outputFile.createNewFile();
        }
        FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
        bw = new BufferedWriter(fw);
        
        //--------------- WRITE FILE!
        
        bw.write("#!/usr/bin/gnuplot -persist\n\n"
              + "set term pngcairo dashed dl 5 enhanced size 2800, 3400 font \"Arial, 64\"\n\n"
              + "set lmargin at screen 0.15\n"
              + "set bmargin at screen 0.45\n"
              + "set rmargin at screen 0.9\n"
              + "set tmargin at screen 0.9\n"
              + "set size 1,1 \n"
              + "set key bmargin \n\n"
              + "set output \"" + plotID + "_DR_AveSD.png\"\n"
              + "set border lw 16\n\n"
              + "set title \"Dose Response at time " + time + " of \".numberOfBestFits.\" best parameter sets (ave/sd)\"\n"
              + "set style line 1 linetype 1 lw 16 lc 1\n"
              + "set style line 2 linetype 2 lw 16 lc 2\n"
              + "set style line 3 linetype 3 lw 16 lc 3\n"
              + "set style line 4 linetype 4 lw 16 lc 4\n"
              + "set style line 5 linetype 5 lw 16 lc 5\n"
              + "set style line 6 linetype 6 lw 16 lc 7\n"
              + "set style line 11 linetype 1 lw 10 pt 2 ps 16 lc 1\n"
              + "set style line 12 linetype 2 lw 10 pt 3 ps 16 lc 2\n"
              + "set style line 13 linetype 3 lw 10 pt 4 ps 16 lc 3\n"
              + "set style line 14 linetype 4 lw 10 pt 6 ps 16 lc 4\n"
              + "set style line 15 linetype 5 lw 10 pt 8 ps 16 lc 5\n"
              + "set style line 16 linetype 6 lw 10 pt 1 ps 16 lc 7\n\n"
              + "set yrange [0:]\n"
              + "set xlabel \"" + xlabel.replace("\"","") + "\"\n"
              + "set ylabel \"normalised level\"\n\n");
        
        bw.write("plot  ");
              
        boolean dataPresent = false;
        String firstDataLine = "";
        String dataLines = "";
        Iterator<Double> iteD = doses.keySet().iterator();
        while (iteD.hasNext()){
          Double dose = iteD.next();
          Integer cond = doses.get(dose);
          DataNodeReplicates dnr = QueryModelAndData.getDataNodeReplicatesOf(entryLists,speciesID,cond);
          if (dnr != null){
            // I do have data for this species in this condition
            
            // need to find out the index of a certain time point and condition
            int timePos = getPositionOfTimePointInDataMatrix(dnr,time);
            int condPos = getPositionOfConditionInDataMatrix(dnr,cond);
            
            if (timePos != -1 && condPos != -1){
              // ok, se have data, one or more replicates!
              Iterator<Double[][]> ite2 = dnr.getListOfDataMatrices().iterator();
              while (ite2.hasNext()){
                Double[][] matrix = ite2.next();
                if (!dataPresent){
                  firstDataLine += "\'-\' using 1:2 ls 11 w p t \'" + speciesIDstring + "\', \\\n";
                  dataLines += "" + dose + " " + matrix[timePos][condPos] + "\n";
                  dataPresent = true;
                }else{
                  dataLines += "" + dose + " " + matrix[timePos][condPos] + "\n";
                }
              }          
              
            }
          }
        }
        
        if (dataPresent){
          bw.write("NaN lw 0 title \"Data\", \\\n"
                + firstDataLine + "NaN lw 0 title \" \", \\\n");
        }
        
        bw.write("NaN lw 0 title \"Simulations\", \\\n"
              + "\"../sims/" + plotID + "_DR_AveSD.txt\" using 1:2 with lines ls 1 t \'" + speciesIDstring + "\', \\\n"
              + "\"../sims/" + plotID + "_DR_AveSD.txt\" using 1:($2+$3) with lines ls 1 notitle, \\\n"
              + "\"../sims/" + plotID + "_DR_AveSD.txt\" using 1:($2-$3) with lines ls 1 notitle\n");
        
        if (dataPresent){
          bw.write(dataLines + "EOF\n");
        }
        
        //--------------- DONE WRITING!
        
      }catch (IOException e){
        throw new IOException("[error] IOException while generating " + fileName + "!\n");
      }finally{ //this is always executed, even if a return is called
        bw.flush();
        bw.close();
      }
    }



    //--------------------------------------------------------------
    //----------------------- all TwoVariables plots
    //--------------------------------------------------------------    
    
    Iterator<TwoVariablesPlot> iteTVP = simulateOptions.getTwoVariablesPlots().iterator();
    while (iteTVP.hasNext()){
      TwoVariablesPlot tvp = iteTVP.next();
      Integer xID = tvp.getSpeciesID();
      String xVar = lookUpTable.lookUpString(xID);
      HashMap<Integer, LinkedList<Integer>> map = tvp.getIdCondList();
      plotID = xVar + "_" + buildPlotID(map);
      
      BufferedWriter bw = null; 
      fileName = "KimuraODEsim/plots/" + plotID + "TwoVarsPlot.plt";
      listOfFiles.add(plotID + "TwoVarsPlot.plt");
      
      try{
        //Output log file
        File outputFile = new File(fileName);        
        if (!outputFile.exists()) {
          outputFile.createNewFile();
        }
        FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
        bw = new BufferedWriter(fw);
        
        //--------------- WRITE FILE!
        
        bw.write("#!/usr/bin/gnuplot -persist\n\n"
              + "set term pngcairo dashed dl 5 enhanced size 2800, 3400 font \"Arial, 64\"\n\n"
              + "set lmargin at screen 0.15\n"
              + "set bmargin at screen 0.45\n"
              + "set rmargin at screen 0.9\n"
              + "set tmargin at screen 0.9\n"
              + "set size 1,1 \n"
              + "set key bmargin \n\n"
              + "set output \"" + plotID + "TwoVarsPlot.png\"\n"
              + "set border lw 16\n\n"
              + "set title \"Time courses of \".numberOfBestFits.\" best parameter sets\"\n"
              + "set style line 1 linetype 1 lw 16 lc 1\n"
              + "set style line 2 linetype 2 lw 16 lc 2\n"
              + "set style line 3 linetype 3 lw 16 lc 3\n"
              + "set style line 4 linetype 4 lw 16 lc 4\n"
              + "set style line 5 linetype 5 lw 16 lc 5\n"
              + "set style line 6 linetype 6 lw 16 lc 7\n"
              + "set style line 11 linetype 1 lw 10 pt 2 ps 16 lc 1\n"
              + "set style line 12 linetype 2 lw 10 pt 3 ps 16 lc 2\n"
              + "set style line 13 linetype 3 lw 10 pt 4 ps 16 lc 3\n"
              + "set style line 14 linetype 4 lw 10 pt 6 ps 16 lc 4\n"
              + "set style line 15 linetype 5 lw 10 pt 8 ps 16 lc 5\n"
              + "set style line 16 linetype 6 lw 10 pt 1 ps 16 lc 7\n\n"
              + "set xrange [0:]\n"
              + "set yrange [0:]\n"
              + "set xlabel \"" + xVar + "\"\n"
              + "set ylabel \"normalised level\"\n\n");
              
        //Do not write data or look for data, only simulations
        String simsLines = "";
        int lineStyle = 1;
        Iterator<Integer> ite00 = map.keySet().iterator();
        while (ite00.hasNext()){
          Integer key = ite00.next();
          LinkedList<Integer> list = map.get(key);
          lookUpTable.lookUpString(key);
          
          Iterator<Integer> ite2 = list.iterator();
          while (ite2.hasNext()){
            Integer cond = ite2.next();
            
            simsLines += "\"../sims/c_" + cond.intValue() + "sims.txt\" using "
                  + (getPositionInSimulationMatrixOf(xID)+1) + ":"
                  + (getPositionInSimulationMatrixOf(key)+1) + " with lines ls " + lineStyle + " t \'" 
                  + lookUpTable.lookUpString(key).replace("_","\\_") + ", " + lookUpTable.lookUpString(cond).replace("\"","").replace("_","\\_") + "\'";
            
            if (!ite00.hasNext() && !ite2.hasNext()){ //this was the last line to be plotted
              simsLines += "\n\n";
              //so conclude by plotting simulations
                bw.write("plot NaN lw 0 title \"Simulations\", \\\n"
                      + simsLines);              
            } else { //we haven't finished plotting lines
              simsLines += ", \\\n";
            }
            lineStyle++;
          }

        }        
        
        //--------------- DONE WRITING!
        
      }catch (IOException e){
        throw new IOException("[error] IOException while generating " + fileName + "!\n");
      }finally{ //this is always executed, even if a return is called
        bw.flush();
        bw.close();
      }
    }




    
    //--------------------------------------------------------------
    //----------------------- now the final plots.sh script
    //--------------------------------------------------------------
    
    BufferedWriter bw = null; 
    fileName = "KimuraODEsim/plots/plots.sh";
    
    try{
      //Output log file
      File outputFile = new File(fileName);        
      if (!outputFile.exists()) {
        outputFile.createNewFile();
      }
      FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
      bw = new BufferedWriter(fw);
      
      //--------------- WRITE FILE!
      
      bw.write("#!/bin/bash\n\n");
      
      bw.write("INPUT1=$1\n"
         + "if [ \"$INPUT1\" = \'\' ]; then\n"
         + "  let INPUT1=100\n"
         + "fi\n\n");
      
      
      Iterator<String> itel = listOfFiles.iterator();
      while (itel.hasNext()){
        String tmp = itel.next();
        bw.write("gnuplot -e \"numberOfBestFits=${INPUT1}\" " + tmp + "\n");
      }
      bw.write("\n");
      
      //--------------- DONE WRITING!
      
    }catch (IOException e){
      throw new IOException("[error] IOException while generating " + fileName + "!\n");
    }finally{ //this is always executed, even if a return is called
      bw.flush();
      bw.close();
    }
    
    log.write("[info] done writing plot files for KimuraODEsim/ODEsimulator\n");
  }  
  
  /**
   * generate an ID for the simulation plot identified by 
   * the map speciesID -> list of condID
   * */
  
  private String buildPlotID(HashMap<Integer, LinkedList<Integer>> map) throws Exception{
    
    int n = 0;
    String id = "";
    Iterator<Integer> ite = map.keySet().iterator();
    while (ite.hasNext()){
      Integer key = ite.next();
      LinkedList<Integer> list = map.get(key);
      id += lookUpTable.lookUpString(key);
      
      Iterator<Integer> ite2 = list.iterator();
      while (ite2.hasNext()){
        Integer cond = ite2.next();
        id += "c_" + cond.intValue();
        n++;
      }
      id += "_";
    }
    if (n > 6) throw new Exception("[error] the maximum number of species/condition allowed by simulateOutput option is 6, but " + n + " were specified.");
    return id;
  }
  
  /**
   * generate an ID for a dose response plot, using the information in a DoseResponsePlot object
   * */
  
  private String buildDoseResponsePlotID(DoseResponsePlot drp) throws Exception{
    
    int n = 0;
    String id = lookUpTable.lookUpString(drp.getSpeciesID()) + "_t" + drp.getTime();
    Iterator<Integer> ite =drp.getDoses().values().iterator();
    while (ite.hasNext()){
      Integer v = ite.next();
      id += "_c_" + v.intValue();
    }
    return id;
  }
  
  /**
   * retrieves the position of a species in the simulationMatrix of ODEsimulator.c
   * position 0 is never returned (it is always time, in the simulationMatrix).
   * If the speciesID is not found throws an Exception
   * */
  
  private int getPositionInSimulationMatrixOf(Integer speciesID) throws Exception{
    int n = 1;
    int position = -1;
    Iterator<GenericEntry> ite = entryLists.getDiffList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof DifferentialEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.getPositionInSimulationMatrixOf()). The GenericEntry object is not a DifferentialEquationNode object");
      DifferentialEquationNode den = (DifferentialEquationNode) tmp;
      if(speciesID.equals(den.getIdentifier())) return n;
      n++;
    }

    ite = entryLists.getAlgList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof AlgebraicEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.getPositionInSimulationMatrixOf()). The GenericEntry object is not a AlgebraicEquationNode object");
      AlgebraicEquationNode den = (AlgebraicEquationNode) tmp;
      if(speciesID.equals(den.getIdentifier())) return n;
      n++;
    }
    
    if (position == -1) throw new Exception("[error] The object requested for plotting (" + lookUpTable.lookUpString(speciesID) + ") is not a differential nor algebraic species");
    return position;
  }  
  
  /**
   * return data node with the exact ID and list of conditions (includes time)
   * there must be only one
   * returns null if exact match is not found
   * */
  
  private DataNode getDataNodeWith(Integer id, LinkedList<Integer> conditions) throws Exception{
    DataNode node = null;
    Iterator<GenericEntry> ite = entryLists.getDataList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof DataNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.existsDataNodeWith()). The GenericEntry object is not a DataNode object");
      DataNode dn = (DataNode) tmp;
      if (dn.getIdentifier().equals(id) && dn.getConditions().equals(conditions)) return dn;
    }
    
    return node;
  }
  
  /**
   * Print C++ function signatures from SBC function Definitions (FunctionNode list)
   * */
  private void printCFunctionSignatures(boolean writeDer,BufferedWriter bw) throws Exception{
    Iterator<GenericEntry> ite = entryLists.getFunList().iterator();
    while (ite.hasNext()){
      GenericEntry ge = ite.next();
      if (! (ge instanceof FunctionNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printCFunctionSignatures()). The GenericEntry object is not a FunctionNode object");
      FunctionNode tmp = (FunctionNode) ge;
      bw.write("static realtype fun_" + tmp.getIdentifier() + "(");      
      Iterator<String> ite2 = tmp.getArguments().iterator();
      while (ite2.hasNext()){
        String tmp2 = ite2.next();
        bw.write("realtype " + tmp2);
        if (ite2.hasNext()) bw.write(",");
      }
      bw.write("); // function " + lookUpTable.lookUpString(tmp.getIdentifier()) + "\n");
    
      //if requested, write the derivative functions
      if (writeDer){
          //for all parameters to fit
          LinkedList<Integer> paramsToFit = QueryModelAndData.getParametersToFit(entryLists);
          LinkedList<Integer> differentials = QueryModelAndData.getDifferentialVariablesIDs(entryLists);
          LinkedList<Integer> fullDerivativesList = new LinkedList<Integer>();
          fullDerivativesList.addAll(paramsToFit);
          //fullDerivativesList.addAll(differentials);
          fullDerivativesList.add(differentials.getFirst()); //trick: I just need to write this derivative once for all differentials
          Iterator<Integer> ite3 = fullDerivativesList.iterator();
          while (ite3.hasNext()){
            Integer paramID = ite3.next();
            //--------copy from above and adapt
            int type = QueryModelAndData.getIDType(entryLists,paramID);
            if (type == GLSDCTool.PARAMETER){
              bw.write("static realtype fun_" + tmp.getIdentifier() + "__der__" + lookUpTable.lookUpString(paramID) + "("); 
            }else if (type == GLSDCTool.DIFFERENTIAL){
              bw.write("static realtype fun_" + tmp.getIdentifier() + "__der__("); 
            }
            ite2 = tmp.getArguments().iterator();
            while (ite2.hasNext()){
              String tmp2 = ite2.next();
              bw.write("realtype " + tmp2);
              bw.write(",");
              if (type == GLSDCTool.PARAMETER){
                bw.write("realtype " + tmp2 + "__der__" + lookUpTable.lookUpString(paramID));
              }else if (type == GLSDCTool.DIFFERENTIAL){
                bw.write("realtype " + tmp2 + "__der__");
              }
              if (ite2.hasNext()) bw.write(",");
            }
            bw.write(");\n");
            //--------
          }
      }
    }
    bw.write("\n");
  }
   
   
    /**
   * Print C++ function definitions from SBC function Definitions (FunctionNode list)
   * */
  private void printCFunctionDefinitions(boolean writeDer,BufferedWriter bw) throws Exception{
    Iterator<GenericEntry> ite = entryLists.getFunList().iterator();
    while (ite.hasNext()){
      GenericEntry ge = ite.next();
      if (! (ge instanceof FunctionNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printCFunctionDefinitions()). The GenericEntry object is not a FunctionNode object");
      FunctionNode tmp = (FunctionNode) ge;
      bw.write("static realtype fun_" + tmp.getIdentifier() + "(");      
      Iterator<String> ite2 = tmp.getArguments().iterator();
      while (ite2.hasNext()){
        String tmp2 = ite2.next();
        bw.write("realtype " + tmp2);
        if (ite2.hasNext()) bw.write(",");
      }
      bw.write("){ // function " + lookUpTable.lookUpString(tmp.getIdentifier()) + "\n");
      bw.write("  return ");
      bw.write(writeExpression(tmp.getExpression(),bw,2,null));
      bw.write(";\n}\n\n");
      
      //if requested, write the derivative functions
      if (writeDer){
          //for all parameters to fit
          LinkedList<Integer> paramsToFit = QueryModelAndData.getParametersToFit(entryLists);
          LinkedList<Integer> differentials = QueryModelAndData.getDifferentialVariablesIDs(entryLists);
          LinkedList<Integer> fullDerivativesList = new LinkedList<Integer>();
          fullDerivativesList.addAll(paramsToFit);
          //fullDerivativesList.addAll(differentials);
          fullDerivativesList.add(differentials.getFirst()); //trick: I just need to write this derivative once for all differentials
          Iterator<Integer> ite3 = fullDerivativesList.iterator();
          while (ite3.hasNext()){
            Integer paramID = ite3.next();
            //--------copy from above and adapt
            int type = QueryModelAndData.getIDType(entryLists,paramID);
            if (type == GLSDCTool.PARAMETER){
              bw.write("static realtype fun_" + tmp.getIdentifier() + "__der__" + lookUpTable.lookUpString(paramID) + "("); 
            }else if (type == GLSDCTool.DIFFERENTIAL){
              bw.write("static realtype fun_" + tmp.getIdentifier() + "__der__(");      
            }     
            ite2 = tmp.getArguments().iterator();
            while (ite2.hasNext()){
              String tmp2 = ite2.next();
              bw.write("realtype " + tmp2);
              bw.write(",");
              if (type == GLSDCTool.PARAMETER){
                bw.write("realtype " + tmp2 + "__der__" + lookUpTable.lookUpString(paramID)); 
              }else if (type == GLSDCTool.DIFFERENTIAL){
                bw.write("realtype " + tmp2 + "__der__");
              }  
              if (ite2.hasNext()) bw.write(",");
            }
            bw.write("){\n");
            bw.write("  return ");
            
            try{
							bw.write(writeDerivative(tmp.getExpression(),paramID,2,bw));
            }catch(DerivativeZeroException dze){
							bw.write(" 0.0 ");
						}
            
            bw.write(";\n}\n\n");
            //--------
          }
      }
    }
    
  }
  
  /**
   * According to SBML definition, functions can contain only local variables
   * (the ones in the signature of the function) and parameters.
   * Return false if a function definition contains an ID that is not
   * a parameter ID.
   * */
  
  private void checkFunctionDefinitions() throws Exception{
    Iterator<GenericEntry> ite = entryLists.getFunList().iterator();
    while (ite.hasNext()){
      GenericEntry ge = ite.next();
      if (! (ge instanceof FunctionNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.checkFunctionDefinitions()). The GenericEntry object is not a FunctionNode object");
      FunctionNode tmp = (FunctionNode) ge;
      Set<Integer> set = new HashSet<Integer>();
      getAlgEqAndDiffEqIDInExpression(tmp.getExpression(), set);
      if (!set.isEmpty()) {
        String msg = "[error] Function definitions cannot contain global variables (SBML specifications). "
        + "In function definition for " + lookUpTable.lookUpString(tmp.getIdentifier()) + " the following "
        + "global variables are not allowed: ";
        Iterator<Integer> ite2 = set.iterator();
        while (ite2.hasNext()){
          Integer tmp2 = ite2.next();
          msg += lookUpTable.lookUpString(tmp2);
          if (ite2.hasNext()) msg += ", ";
        }
        msg += ".\n";
        
        throw new Exception(msg);
      }
    }
  }
  
  
  private void getAlgEqAndDiffEqIDInExpression(Expression expr, Set<Integer> set) throws Exception{
    
    if (expr instanceof ExprID){
      ExprID e = (ExprID)expr; //downcast
      Integer id = e.getID();
      int type = QueryModelAndData.getIDType(entryLists,id);
      //add id to appropriate set if it is an id of an algebraic or differential or parameter entry
      if (type == ALGEBRAIC){
        set.add(id);
      }else if (type == DIFFERENTIAL){
        set.add(id);    
      }else if (type == PARAMETER){
         //do nothing      
      }else {
        throw new Exception("[error] Unknown type of ID in Expression. Cannot find: " + lookUpTable.lookUpString(id));        
      }
    }else if(expr instanceof ExprCap){
      ExprCap e = (ExprCap)expr; //downcast
      getAlgEqAndDiffEqIDInExpression(e.getExpression1(),set);
      getAlgEqAndDiffEqIDInExpression(e.getExpression2(),set);
    }else if(expr instanceof ExprCos){
      ExprCos e = (ExprCos)expr; //downcast
      getAlgEqAndDiffEqIDInExpression(e.getExpression(),set);
    }else if(expr instanceof ExprDiv){
      ExprDiv e = (ExprDiv)expr; //downcast
      getAlgEqAndDiffEqIDInExpression(e.getExpression1(),set);
      getAlgEqAndDiffEqIDInExpression(e.getExpression2(),set);
    }else if(expr instanceof ExprDOUBLE){
      ExprDOUBLE e = (ExprDOUBLE)expr; //downcast
      //do nothing
    }else if(expr instanceof ExprExp){
      ExprExp e = (ExprExp)expr; //downcast
      getAlgEqAndDiffEqIDInExpression(e.getExpression(),set);
    }else if(expr instanceof ExprIfThenElse){
      ExprIfThenElse e = (ExprIfThenElse)expr; //downcast
      getAlgEqAndDiffEqIDInBExpression(e.getBExpression(),set);
      getAlgEqAndDiffEqIDInExpression(e.getExpression1(),set);
      getAlgEqAndDiffEqIDInExpression(e.getExpression2(),set);
    }else if(expr instanceof ExprINT){
      ExprINT e = (ExprINT)expr; //downcast
      //do nothing
    }else if(expr instanceof ExprTime){
      //do nothing
    }else if(expr instanceof ExprLn){
      ExprLn e = (ExprLn)expr; //downcast
      getAlgEqAndDiffEqIDInExpression(e.getExpression(),set);
    }else if(expr instanceof ExprNegation){
      ExprNegation e = (ExprNegation)expr; //downcast
      getAlgEqAndDiffEqIDInExpression(e.getExpression(),set);
    }else if(expr instanceof ExprPar){
      ExprPar e = (ExprPar)expr; //downcast
      getAlgEqAndDiffEqIDInExpression(e.getExpression(),set);
    }else if(expr instanceof ExprSin){
      ExprSin e = (ExprSin)expr; //downcast
      getAlgEqAndDiffEqIDInExpression(e.getExpression(),set);
    }else if(expr instanceof ExprSqrt){
      ExprSqrt e = (ExprSqrt)expr; //downcast
      getAlgEqAndDiffEqIDInExpression(e.getExpression(),set);
    }else if(expr instanceof ExprMinus){
      ExprMinus e = (ExprMinus)expr; //downcast
      getAlgEqAndDiffEqIDInExpression(e.getExpression1(),set);
      getAlgEqAndDiffEqIDInExpression(e.getExpression2(),set);
    }else if(expr instanceof ExprMul){
      ExprMul e = (ExprMul)expr; //downcast
      getAlgEqAndDiffEqIDInExpression(e.getExpression1(),set);
      getAlgEqAndDiffEqIDInExpression(e.getExpression2(),set);
    }else if(expr instanceof ExprPlus){
      ExprPlus e = (ExprPlus)expr; //downcast
      getAlgEqAndDiffEqIDInExpression(e.getExpression1(),set);
      getAlgEqAndDiffEqIDInExpression(e.getExpression2(),set);
    }else if(expr instanceof ExprFunCall){
      ExprFunCall e = (ExprFunCall)expr; //downcast
      Iterator<Expression> ite = e.getArguments().iterator();
      while (ite.hasNext()){
        Expression tmp = ite.next();      
        getAlgEqAndDiffEqIDInExpression(tmp,set);
      }
    }else if(expr instanceof ExprLocalPar){
      ExprLocalPar e = (ExprLocalPar)expr; //downcast
      // do nothing
    }else{
      throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.getAlgEqAndDiffEqIDInExpression()). Unknown type of Expression.");
    }
  }
  
  private void getAlgEqAndDiffEqIDInBExpression(BExpression bexp, Set<Integer> set) throws Exception{
    
    if (bexp instanceof BExprAND){
      BExprAND b = (BExprAND)bexp; //downcast
      getAlgEqAndDiffEqIDInBExpression(b.getBExpression1(),set);
      getAlgEqAndDiffEqIDInBExpression(b.getBExpression2(),set);
    } else if (bexp instanceof BExprDiff){
      BExprDiff b = (BExprDiff)bexp; //downcast
      getAlgEqAndDiffEqIDInExpression(b.getExpression1(),set);
      getAlgEqAndDiffEqIDInExpression(b.getExpression2(),set);
    }else if (bexp instanceof BExprEqual){
      BExprEqual b = (BExprEqual)bexp; //downcast
      getAlgEqAndDiffEqIDInExpression(b.getExpression1(),set);
      getAlgEqAndDiffEqIDInExpression(b.getExpression2(),set);
    }else if (bexp instanceof BExprGE){
      BExprGE b = (BExprGE)bexp; //downcast
      getAlgEqAndDiffEqIDInExpression(b.getExpression1(),set);
      getAlgEqAndDiffEqIDInExpression(b.getExpression2(),set);
    }else if (bexp instanceof BExprGT){
      BExprGT b = (BExprGT)bexp; //downcast
      getAlgEqAndDiffEqIDInExpression(b.getExpression1(),set);
      getAlgEqAndDiffEqIDInExpression(b.getExpression2(),set);
    }else if (bexp instanceof BExprLE){
      BExprLE b = (BExprLE)bexp; //downcast
      getAlgEqAndDiffEqIDInExpression(b.getExpression1(),set);
      getAlgEqAndDiffEqIDInExpression(b.getExpression2(),set);
    }else if (bexp instanceof BExprLT){
      BExprLT b = (BExprLT)bexp; //downcast
      getAlgEqAndDiffEqIDInExpression(b.getExpression1(),set);
      getAlgEqAndDiffEqIDInExpression(b.getExpression2(),set);
    }else if (bexp instanceof BExprOR){
      BExprOR b = (BExprOR)bexp; //downcast
      getAlgEqAndDiffEqIDInBExpression(b.getBExpression1(),set);
      getAlgEqAndDiffEqIDInBExpression(b.getBExpression2(),set);
    }else if (bexp instanceof BExprPar){
      BExprPar b = (BExprPar)bexp; //downcast
      getAlgEqAndDiffEqIDInBExpression(b.getBExpression(),set);
    }else if (bexp instanceof BExprNot){
      BExprNot b = (BExprNot)bexp; //downcast
      getAlgEqAndDiffEqIDInBExpression(b.getBExpression(),set);
    }else if (bexp instanceof BExprFALSE){
      //do nothing
    }else if (bexp instanceof BExprTRUE){
      //do nothing
    }else{
      throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.getAlgEqAndDiffEqIDInBExpression()). Unknown type of BExpression.");
    }

  }  
  
  private void printEventTriggerBooleanVariables(BufferedWriter bw) throws Exception{
    Iterator<GenericEntry> ite = entryLists.getEventList().iterator();
    bw.write("bool reinit = false;\n");
    while (ite.hasNext()){
      GenericEntry ge = ite.next();
      if (! (ge instanceof EventNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printEventTriggerBooleanVariables()). The GenericEntry object is not an EventNode object");
      EventNode en = (EventNode) ge;
      bw.write("bool event_" + en.getIdentifier() + "_triggered = false; // event " + lookUpTable.lookUpString(en.getIdentifier()) + "\n");
    }
    bw.write("\n");
  }
  
  private void printEventTriggeredReset(BufferedWriter bw) throws Exception{
    Iterator<GenericEntry> ite = entryLists.getEventList().iterator();
    bw.write("\n");
    while (ite.hasNext()){
      GenericEntry ge = ite.next();
      if (! (ge instanceof EventNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printEventTriggerBooleanVariables()). The GenericEntry object is not an EventNode object");
      EventNode en = (EventNode) ge;
      bw.write("  event_" + en.getIdentifier() + "_triggered = false; // event " + lookUpTable.lookUpString(en.getIdentifier()) + "\n");
    }
    bw.write("\n");
  }
  
  private void printEvents(boolean sensitivitiesON,BufferedWriter bw, Integer condID, Map<Integer,Set<Integer>> signatures) throws Exception{
    Set<Integer> algebraicEquationIDs = new HashSet<Integer>();  
        //---------- add calls from events
    Iterator<GenericEntry> iteEvents = entryLists.getEventList().iterator();
    while (iteEvents.hasNext()){
      GenericEntry ge = iteEvents.next();
      if (! (ge instanceof EventNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printEvents()). The GenericEntry object is not an EventNode object");
      EventNode en = (EventNode) ge;
      getAlgEqIDInBExpression(en.getBExpression(),algebraicEquationIDs);
      Iterator<Expression> iteExpr = en.getListOfAssignments().values().iterator();
      while (iteExpr.hasNext()){
        Expression tmp = iteExpr.next();
        getAlgEqIDInExpression(tmp,algebraicEquationIDs);
      }
    }
    
    //--------- call algebraic functions
    Iterator<GenericEntry> ite = entryLists.getAlgList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof AlgebraicEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printEvents()). The GenericEntry object is not a AlgebraicEquationNode object");
      AlgebraicEquationNode aen = (AlgebraicEquationNode) tmp;
      if (algebraicEquationIDs.contains(aen.getIdentifier())){
        bw.write("  realtype var_" + lookUpTable.lookUpString(aen.getIdentifier()) + " = " + lookUpTable.lookUpString(aen.getIdentifier()) + "(t,condition");
        
        Set<Integer> s = signatures.get(aen.getIdentifier());
        if (s != null){
          Iterator<Integer> ite2 = s.iterator();
          while (ite2.hasNext()){
            Integer tmp2 = ite2.next();
            bw.write(",NV_Ith_S(y_c_" + condID + "," + lookUpTable.lookUpString(tmp2) + ")"); 
          }
        } 
        bw.write(");\n");
      }
    }
    bw.write("\n");
    
    //--------- done calling algebraic functions
    
    
    //--------- write events
    Iterator<GenericEntry> iteEvents2 = entryLists.getEventList().iterator();
    while (iteEvents2.hasNext()){
      GenericEntry ge = iteEvents2.next();
      if (! (ge instanceof EventNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.printEvents()). The GenericEntry object is not an EventNode object");
      EventNode en = (EventNode) ge;
      bw.write("  bool event_" + en.getIdentifier() + "_trigger = ");
      writeBExpression(en.getBExpression(),bw,3,condID);
      bw.write("; // event " + lookUpTable.lookUpString(en.getIdentifier()) + "\n");
      bw.write("  if (");
      bw.write("event_" + en.getIdentifier() + "_trigger && !event_" + en.getIdentifier() + "_triggered){\n");
      bw.write("    event_" + en.getIdentifier() + "_triggered = true;\n");
      bw.write("    reinit = true;\n");
      HashMap<Integer,Expression> listOfAssignments = en.getListOfAssignments();
      Iterator<Integer> iteKey = listOfAssignments.keySet().iterator();
      while (iteKey.hasNext()){
        Integer key = iteKey.next();
        Expression expr = listOfAssignments.get(key);
        //find out what type of ID it is
        int type = QueryModelAndData.getIDType(entryLists,key);
        if (type == ALGEBRAIC){
          // events cannot change algebraic equations as they are
          // already assignments evaluated at each step
          throw new Exception("[error] events cannot change algebraic equations (" + lookUpTable.lookUpString(key) 
                    + "), because they are already assignments evaluated at each step.");        
        }else if (type == DIFFERENTIAL){
          bw.write("    NV_Ith_S(y_c_" + condID + "," + lookUpTable.lookUpString(key) + ")");
        }else if (type == PARAMETER){
          bw.write("    X[" + lookUpTable.lookUpString(key) + "]");    
        }else {
          throw new Exception("[error] Unknown type of ID in Expression. Cannot find: " + lookUpTable.lookUpString(key));        
        }
        
        bw.write(" = ");
        bw.write(writeExpression(expr,bw,3,condID));
        bw.write(";\n");
      }
      bw.write("  }\n");
      bw.write("  if (");
      bw.write("!event_" + en.getIdentifier() + "_trigger && event_" + en.getIdentifier() + "_triggered){\n");
      bw.write("    event_" + en.getIdentifier() + "_triggered = false;\n");
      bw.write("  }\n");
    }
    if (entryLists.getEventList().size()>0){ //write the following only if events exist
      bw.write("\n");
      bw.write("  if (reinit){\n"
             //+ "    CVodeFree(&cvode_mem);\n"
             //+ "    cvode_mem = CVodeMalloc(neq, F, t, y_c_" + condID + ", BDF, NEWTON, SV, &reltol, abstol,\n"
             //+ "             &condition, stderr, TRUE, iopt, ropt, NULL);\n"
             //+ "    CVDense(cvode_mem, dFdy, NULL);\n"
             + "    CVodeReInit(cvode_mem,t,y_c_" + condID + ");\n");
      //also reinit sensitivities if necessary       
      if (sensitivitiesON){
        bw.write("    CVodeSensReInit(cvode_mem,CV_SIMULTANEOUS,uS_c_" + condID + ");\n");
      }
      bw.write("    reinit = false;\n"
             + "  }\n\n");
    }
  }
  
  /**
   * code to be inserted in ODEsimulator.c in order to save simulations that
   * will be plotted as dose response curves
   * */
  
  private void printDoseResponseSimulationsFileWriting(Set<Integer> conditionsToSimulate, BufferedWriter bw) throws Exception{
    LinkedList<DoseResponsePlot> drpList = simulateOptions.getDoseResponsePlots();
    Iterator<DoseResponsePlot> ite = drpList.iterator();
    
    Integer speciesID = null;
    String speciesIDstring = null;
    Double time = null;
    String xlabel = null;
    TreeMap<Double,Integer> doses = null;
    String plotID = null;
    String fileName = null;
        
    if (ite.hasNext()) bw.write("  FILE *pFile;\n\n");
    
    while (ite.hasNext()){
      DoseResponsePlot drp = ite.next();
      //check that the species ID has been simulated
      if(!getSpeciesVariables().contains(drp.getSpeciesID())) throw new Exception("[error] Dose Response plot requested for " 
      + lookUpTable.lookUpString(drp.getSpeciesID()) + ", but this species is not simulated (should be Differential of Algebraic variable).");  
      //check that the time point is not less than 0 and not more than final time
      if(drp.getTime() < 0) throw new Exception("[error] Dose Response plot requested for " 
      + lookUpTable.lookUpString(drp.getSpeciesID()) + ", but time point requested is less than the initial time (" + drp.getTime() + " < 0)");  
      if(drp.getTime() > simulateOptions.getFinalTime()) throw new Exception("[error] Dose Response plot requested for " 
      + lookUpTable.lookUpString(drp.getSpeciesID()) + ", but time point requested is larger than the final time (" + drp.getTime() + " > " + simulateOptions.getFinalTime() + ")");  
      if(drp.getTime() < simulateOptions.getInitialTime()) throw new Exception("[error] Dose Response plot requested for " 
      + lookUpTable.lookUpString(drp.getSpeciesID()) + ", but time point requested is smaller than the initial time (" + drp.getTime() + " > " + simulateOptions.getInitialTime() + ")");  
      //check that all requested conditions for the dose response are in the conditionsToSimulate
      if(!conditionsToSimulate.containsAll(drp.getDoses().values())) throw new Exception("[error] Dose Response plot requested for " 
      + lookUpTable.lookUpString(drp.getSpeciesID()) + ", but some of the requested conditions are not simulated.");  
      
      //And now the actual code
      
      speciesID = drp.getSpeciesID();
      speciesIDstring = lookUpTable.lookUpString(speciesID);
      time = drp.getTime();
      doses = drp.getDoses();
      plotID = buildDoseResponsePlotID(drp);
      
      fileName = "sims/" + plotID + "_DR.txt";
      
      bw.write("  // save dose response data for " + speciesIDstring + " at time " + time + " and with xlabel " + xlabel + "\n");
      bw.write("  pFile = fopen(\"" + fileName + "\",\"w\");\n");
      
      bw.write("  for (currentParameterSet = 0;currentParameterSet<numberOfFits;currentParameterSet++){\n");
      
      Iterator<Double> ite2 = doses.keySet().iterator();
      while (ite2.hasNext()){
        Double dose = ite2.next();  //current dose
        Integer cond = doses.get(dose);  //condition corresponding to this dose
        bw.write("    fprintf(pFile,\"%.8f %.16f\\n\"," + dose + ",simulationMatrix[c_sims_" + cond + "][currentParameterSet][s_" + speciesID + "+1][(int)ceil((" + time + " - INITIALTIME)*TIMESTEPS/(FINALTIME - INITIALTIME))]);\n");
      }
      bw.write("    fprintf(pFile,\"\\n\\n\");\n");      
      bw.write("  }\n");      
      bw.write("  fclose(pFile);\n\n");
      
      //----------------- also ave/sd
      
      fileName = "sims/" + plotID + "_DR_AveSD.txt";    

      bw.write("  // save dose response Average and Standard Deviation\n");
      bw.write("  pFile = fopen(\"" + fileName + "\",\"w\");\n");
      
      ite2 = doses.keySet().iterator();
      while (ite2.hasNext()){
        Double dose = ite2.next();  //current dose
        Integer cond = doses.get(dose);  //condition corresponding to this dose
        bw.write("  fprintf(pFile,\"%.8f %.16f %.16f\\n\"," + dose + ",averageMatrix[c_sims_" + cond + "][s_" + speciesID + "+1][(int)ceil((" + time + " - INITIALTIME)*TIMESTEPS/(FINALTIME - INITIALTIME))],stdMatrix[c_sims_" + cond + "][s_" + speciesID + "+1][(int)ceil((" + time + " - INITIALTIME)*TIMESTEPS/(FINALTIME - INITIALTIME))]);\n");
      }
      bw.write("  fprintf(pFile,\"\\n\\n\");\n");        
      bw.write("  fclose(pFile);\n\n");
    }
    
  }
  
  /**
   * return the index of the data matrix in the DataNodeReplicate where
   * there is the given condition
   * If the condition is not present return -1
   */
  
  private int getPositionOfConditionInDataMatrix(DataNodeReplicates dnr, Integer condition){
    int i=0;
    Iterator<Integer> ite = dnr.getConditions().iterator();
    while (ite.hasNext()){
      Integer cond = ite.next();
      if (cond.equals(condition)) return i;
      i++;
    }
    return -1;
  }
  
  /**
   * return the index of the data matrix in the DataNodeReplicate where
   * there is the given time point
   * If the time point is not present return -1
   */
  
  private int getPositionOfTimePointInDataMatrix(DataNodeReplicates dnr, Double timePoint){
    Double[][] matrix = dnr.getListOfDataMatrices().getFirst();
    for (int i=0;i<matrix.length;i++){
      // j=0 is position of time
      if (matrix[i][0].equals(timePoint)) return i;
      i++;
    }
    return -1;
  }
  
    
  public Set<Integer> getConditionsRequestedForPlots() throws Exception{
    Set<Integer> returnSet = new HashSet<Integer>();
    
    //simulateOutput maps
    Iterator<HashMap<Integer, LinkedList<Integer>>> ite = simulateOptions.getListOfIdCondList().iterator();
    while (ite.hasNext()){
      HashMap<Integer, LinkedList<Integer>> map = ite.next();
      Iterator<Integer> ite2 = map.keySet().iterator();
      while (ite2.hasNext()){
        Integer key = ite2.next();
        returnSet.addAll(map.get(key));
        
        //find if other conditions are necessary for normalisation
        Iterator<Integer> itei = map.get(key).iterator();
        while (itei.hasNext()){
          Integer cond = itei.next();
          //~ returnSet.add(cond);
          
          DataNodeReplicates dn = QueryModelAndData.getDataNodeReplicatesOf(entryLists,key,cond);
          NormalisationNode nnode = null;
          if (dn != null) nnode = getNormalisationNodeOf(dn);
          if (nnode != null){  //only if there is a corresponding normalisation node
            if (nnode instanceof NormalisationNodeFixedPoint){
              NormalisationNodeFixedPoint nn = (NormalisationNodeFixedPoint) nnode; //downcast

              returnSet.add(nn.getCondition());
             
            }else if (nnode instanceof NormalisationNodeAverage){   
              NormalisationNodeAverage nn = (NormalisationNodeAverage) nnode; //downcast   
              
              returnSet.addAll(nn.getConditions());
           
            }else {
              throw new Exception("[error] Probably a "
                + "coding error (GLSDCTool.getConditionsRequestedForPlots()). Unknown NormalisationNode object");  
            }
          }

        }
        
      }
    }
    
    //NormSimulationsNodes
    Iterator<NormSimulationsNode> ite3 = simulateOptions.getListOfNormalisations().iterator();
    while (ite3.hasNext()){
      NormSimulationsNode nsn = ite3.next();
      if (nsn instanceof NormSimulationsWithData){
        NormSimulationsWithData ns = (NormSimulationsWithData) nsn;
        returnSet.addAll(ns.getConditions());
        
        DataNode dn = getDataNodeWith(ns.getDataID(),ns.getDataConditions());
        NormalisationNode nnode = null;
        if (dn != null) nnode = getNormalisationNodeOf((DataNodeReplicates)dn);
        if (nnode != null){  //only if there is a corresponding normalisation node
          if (nnode instanceof NormalisationNodeFixedPoint){
            NormalisationNodeFixedPoint nn = (NormalisationNodeFixedPoint) nnode; //downcast

            returnSet.add(nn.getCondition());
           
          }else if (nnode instanceof NormalisationNodeAverage){   
            NormalisationNodeAverage nn = (NormalisationNodeAverage) nnode; //downcast   
            
            returnSet.addAll(nn.getConditions());
         
          }else {
            throw new Exception("[error] Probably a "
              + "coding error (GLSDCTool.getConditionsRequestedForPlots()). Unknown NormalisationNode object");  
          }
        }
        
      } else if (nsn instanceof NormSimulationsWithoutData){
        NormSimulationsWithoutData ns = (NormSimulationsWithoutData) nsn;
        
        returnSet.addAll(ns.getConditions());
        returnSet.add(ns.getNormCondition());
        
      }else{
        //if you reach here it is a coding error (that is there is a NormSimulationsNode
        //that I forgot about
        throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.getConditionsRequestedForPlots()). The NormSimulationsNode object is not a known class.");

      }
    }
    
    //listOfDoseResponsePlot
    Iterator<DoseResponsePlot> ite5 = simulateOptions.getDoseResponsePlots().iterator();
    while (ite5.hasNext()){
      DoseResponsePlot tmp = ite5.next();
      returnSet.addAll(tmp.getDoses().values());
      
      //find if other conditions are necessary for normalisation
      Iterator<Integer> itei = tmp.getDoses().values().iterator();
      while (itei.hasNext()){
        Integer cond = itei.next();
        //~ returnSet.add(cond);
        
        DataNodeReplicates dn = QueryModelAndData.getDataNodeReplicatesOf(entryLists,tmp.getSpeciesID(),cond);
        NormalisationNode nnode = null;
        if (dn != null) nnode = getNormalisationNodeOf(dn);
        if (nnode != null){  //only if there is a corresponding normalisation node
          if (nnode instanceof NormalisationNodeFixedPoint){
            NormalisationNodeFixedPoint nn = (NormalisationNodeFixedPoint) nnode; //downcast

            returnSet.add(nn.getCondition());
           
          }else if (nnode instanceof NormalisationNodeAverage){   
            NormalisationNodeAverage nn = (NormalisationNodeAverage) nnode; //downcast   
            
            returnSet.addAll(nn.getConditions());
         
          }else {
            throw new Exception("[error] Probably a "
              + "coding error (GLSDCTool.getConditionsRequestedForPlots()). Unknown NormalisationNode object");  
          }
        }

      }
    }
    
    //listOfTwoVariablesPlots
    Iterator<TwoVariablesPlot> ite6 = simulateOptions.getTwoVariablesPlots().iterator();
    while (ite6.hasNext()){
      TwoVariablesPlot tmp = ite6.next();
      
      Iterator<Integer> ite7 = tmp.getIdCondList().keySet().iterator();
      while (ite7.hasNext()){
        Integer key = ite7.next();
        returnSet.addAll(tmp.getIdCondList().get(key));
        
        //find if other conditions are necessary for normalisation
        Iterator<Integer> itei = tmp.getIdCondList().get(key).iterator();
        while (itei.hasNext()){
          Integer cond = itei.next();
          //~ returnSet.add(cond);
          
          DataNodeReplicates dn = QueryModelAndData.getDataNodeReplicatesOf(entryLists,key,cond);
          NormalisationNode nnode = null;
          if (dn != null) nnode = getNormalisationNodeOf(dn);
          if (nnode != null){  //only if there is a corresponding normalisation node
            if (nnode instanceof NormalisationNodeFixedPoint){
              NormalisationNodeFixedPoint nn = (NormalisationNodeFixedPoint) nnode; //downcast

              returnSet.add(nn.getCondition());
             
            }else if (nnode instanceof NormalisationNodeAverage){   
              NormalisationNodeAverage nn = (NormalisationNodeAverage) nnode; //downcast   
              
              returnSet.addAll(nn.getConditions());
           
            }else {
              throw new Exception("[error] Probably a "
                + "coding error (GLSDCTool.getConditionsRequestedForPlots()). Unknown NormalisationNode object");  
            }
          }

        }
        
        
        //find if other conditions are necessary for normalisation
        //also for species at the x axis
        itei = tmp.getIdCondList().get(key).iterator();
        while (itei.hasNext()){
          Integer cond = itei.next();
          //~ returnSet.add(cond);
          
          DataNodeReplicates dn = QueryModelAndData.getDataNodeReplicatesOf(entryLists,tmp.getSpeciesID(),cond);
          NormalisationNode nnode = null;
          if (dn != null) nnode = getNormalisationNodeOf(dn);
          if (nnode != null){  //only if there is a corresponding normalisation node
            if (nnode instanceof NormalisationNodeFixedPoint){
              NormalisationNodeFixedPoint nn = (NormalisationNodeFixedPoint) nnode; //downcast

              returnSet.add(nn.getCondition());
             
            }else if (nnode instanceof NormalisationNodeAverage){   
              NormalisationNodeAverage nn = (NormalisationNodeAverage) nnode; //downcast   
              
              returnSet.addAll(nn.getConditions());
           
            }else {
              throw new Exception("[error] Probably a "
                + "coding error (GLSDCTool.getConditionsRequestedForPlots()). Unknown NormalisationNode object");  
            }
          }

        }
      }
       
    }    
    
    return returnSet;
  }
  
}
