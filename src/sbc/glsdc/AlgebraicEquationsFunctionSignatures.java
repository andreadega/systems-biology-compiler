
package sbc.glsdc;
import java.util.*;
import sbc.*;
import sbc.expr.*;
import sbc.util.*;

import java.io.*;

public class AlgebraicEquationsFunctionSignatures {
  
  
  static private int ALGEBRAIC = 0;
  static private int DIFFERENTIAL = 1;
  static private int PARAMETER = 2;
/**
 * Retrieve map: algebraicEqID -> Set of DiffEqID
 * The Set of DiffEqID is obtained by recursively exploring
 * the algeraic equations nested in the algebraic equations 
 * */
  static public Map<Integer,Set<Integer>> retrieveSignatures(EntryLists entryLists,LookUpTable lookUpTable) throws Exception {
    
    Map<Integer,Pair<Set<Integer>,Set<Integer>>> nextMap = retriveInitialSets(entryLists,lookUpTable);
    Map<Integer,Pair<Set<Integer>,Set<Integer>>> baseMap = null;
    
    do{
      //System.out.println("hey");
      
      //replace base with next
      baseMap = mapCopy(nextMap);
      //if updating the nextMap does not alter it, then we have converged
      
      Iterator<Integer> ite = baseMap.keySet().iterator();
      //for all algebraic id
      while (ite.hasNext()){
        Integer key = ite.next();
        
        Pair<Set<Integer>,Set<Integer>> pair = baseMap.get(key);
        
        //for all id in the algebraic id set
        Iterator<Integer> ite2 = pair.first.iterator();
        while (ite2.hasNext()){
          Integer tmp2 = ite2.next();
          //add algebraic id and differential id
          
          addToSet(nextMap,key,getAlgebraicIDSet(baseMap,tmp2),ALGEBRAIC);
          addToSet(nextMap,key,getDifferentialIDSet(baseMap,tmp2),DIFFERENTIAL);
          
        }

      }

    }while(!baseMap.equals(nextMap));
    //this algorithm should certainly converge because there is a finite
    //number of algebraic and differential equations
    
    Map<Integer,Set<Integer>> returnMap = new HashMap<Integer,Set<Integer>>();
    
    //convert to return type
    Iterator<Integer> ite = nextMap.keySet().iterator();
    while (ite.hasNext()){
      Integer key = ite.next();
      
      Pair<Set<Integer>,Set<Integer>> pair = baseMap.get(key);
      
      Set<Integer> s = new HashSet<Integer>();
      //s.addAll(pair.first);
      s.addAll(pair.second); // only the differential id go in the signature
      
      returnMap.put(key,s);
      
    }
    
    
    
    return returnMap;
  }
  
/**
 * Retrieve map: algebraicEqID -> Set of algebraicEqID
 * The Set of algebraicEqID is obtained by recursively exploring
 * the algeraic equations nested in the algebraic equations 
 * Important: in the map sID -> Set, Set may not include sID 
 * */
  static public Map<Integer,Set<Integer>> retrieveNestedAlgEq(EntryLists entryLists,LookUpTable lookUpTable) throws Exception {
    
    Map<Integer,Pair<Set<Integer>,Set<Integer>>> nextMap = retriveInitialSets(entryLists,lookUpTable);
    Map<Integer,Pair<Set<Integer>,Set<Integer>>> baseMap = null;
    
    do{
      //System.out.println("hey");
      
      //replace base with next
      baseMap = mapCopy(nextMap);
      //if updating the nextMap does not alter it, then we have converged
      
      Iterator<Integer> ite = baseMap.keySet().iterator();
      //for all algebraic id
      while (ite.hasNext()){
        Integer key = ite.next();
        
        Pair<Set<Integer>,Set<Integer>> pair = baseMap.get(key);
        
        //for all id in the algebraic id set
        Iterator<Integer> ite2 = pair.first.iterator();
        while (ite2.hasNext()){
          Integer tmp2 = ite2.next();
          //add algebraic id and differential id
          
          addToSet(nextMap,key,getAlgebraicIDSet(baseMap,tmp2),ALGEBRAIC);
          addToSet(nextMap,key,getDifferentialIDSet(baseMap,tmp2),DIFFERENTIAL);
          
        }

      }

    }while(!baseMap.equals(nextMap));
    //this algorithm should certainly converge because there is a finite
    //number of algebraic and differential equations
    
    Map<Integer,Set<Integer>> returnMap = new HashMap<Integer,Set<Integer>>();
    
    //convert to return type
    Iterator<Integer> ite = nextMap.keySet().iterator();
    while (ite.hasNext()){
      Integer key = ite.next();
      
      Pair<Set<Integer>,Set<Integer>> pair = baseMap.get(key);
      
      Set<Integer> s = new HashSet<Integer>();
      s.addAll(pair.first);  // only the algebraic id are returned
      //s.addAll(pair.second); 
      
      returnMap.put(key,s);
      
    }
    
    return returnMap;
  }
  
  static private Set<Integer> getAlgebraicIDSet(Map<Integer,Pair<Set<Integer>,Set<Integer>>> map, Integer key){
    //should check for null map
    
    //may have empty signatures
    Pair<Set<Integer>,Set<Integer>> pair = map.get(key);
    if (pair == null) return new HashSet<Integer>();
    return pair.first;
  }
  static private Set<Integer> getDifferentialIDSet(Map<Integer,Pair<Set<Integer>,Set<Integer>>> map, Integer key){
    //should check for null map
    
    //may have empty signatures
    Pair<Set<Integer>,Set<Integer>> pair = map.get(key);
    if (pair == null) return new HashSet<Integer>();
    return pair.second;
  }
  
  /**
   * this function retrieves initial sets of signatures for each algebraic equation
   * in particular, it finds two sets: one for the ID of algebraic equations found
   * and another for the ID of differentil equations found
   * */
  
  static private Map<Integer,Pair<Set<Integer>,Set<Integer>>> retriveInitialSets(EntryLists entryLists,LookUpTable lookUpTable) throws Exception{
    
    Map<Integer,Pair<Set<Integer>,Set<Integer>>> map = new HashMap<Integer,Pair<Set<Integer>,Set<Integer>>>();
    
    Iterator<GenericEntry> ite = entryLists.getAlgList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof AlgebraicEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (AlgebraicEquationsFunctionSignatures.retriveInitialSets()). The GenericEntry object is not a AlgebraicEquationNode object");
      
      //find out what AlgebraicEquationNode this is
      if (tmp instanceof AlgebraicEquationExpr){
        AlgebraicEquationExpr aee = (AlgebraicEquationExpr) tmp;
        exploreExpressionAndAdd(aee.getExpression(),map,aee.getIdentifier(),entryLists,lookUpTable);
        
      }else if (tmp instanceof AlgebraicEquationInterpolation){
        
        //nothing to do here
        
      }else if (tmp instanceof AlgebraicEquationCondList){
        
        AlgebraicEquationCondList aecl = (AlgebraicEquationCondList) tmp; //downcast
        
        Iterator<AlgebraicEquationCond> ite2 = aecl.getAlgebraicEquationCondList().iterator();
        while (ite2.hasNext()){
          AlgebraicEquationCond tmp2 = ite2.next();
          
          //find out what AlgebraicEquationCond this is
          if (tmp2 instanceof AlgebraicEquationCondExpr){
            AlgebraicEquationCondExpr aee = (AlgebraicEquationCondExpr) tmp2;
            exploreExpressionAndAdd(aee.getExpression(),map,aecl.getIdentifier(),entryLists,lookUpTable);
            
          }else if (tmp2 instanceof AlgebraicEquationCondInterpolation){
            
            //nothing to do here
            
          }else{
            throw new Exception("[error] Probably a "
              + "coding error (AlgebraicEquationsFunctionSignatures.retriveInitialSets()). Unknown type of AlgebraicEquationCond.");
          }
          
        }
        
      }else{
        throw new Exception("[error] Probably a "
          + "coding error (AlgebraicEquationsFunctionSignatures.retriveInitialSets()). Unknown type of AlgebraicEquationNode.");
      }
      
      
      
    }
    
    return map;
  }
  
  static private void exploreExpressionAndAdd(Expression expr, Map<Integer,Pair<Set<Integer>,Set<Integer>>> map, 
                        Integer key,EntryLists entryLists,LookUpTable lookUpTable)throws Exception{
    
    if (expr instanceof ExprID){
      
      ExprID e = (ExprID)expr; //downcast
      Integer id = e.getID();
      int type = getIDType(id,entryLists);
      //add id to appropriate set if it is an id of an algebraic or differential entry
      if (type == ALGEBRAIC || type == DIFFERENTIAL) addToSet(map,key,id,type);
      
      if (type == -1) throw new Exception("[error] Unknown type of ID in Expression. Cannot find " + lookUpTable.lookUpString(id)
                                      + " in Expression for " + lookUpTable.lookUpString(key));   
      
    }else if(expr instanceof ExprCap){
      ExprCap e = (ExprCap)expr; //downcast
      exploreExpressionAndAdd(e.getExpression1(),map,key,entryLists,lookUpTable);
      exploreExpressionAndAdd(e.getExpression2(),map,key,entryLists,lookUpTable);
    }else if(expr instanceof ExprCos){
      ExprCos e = (ExprCos)expr; //downcast
      exploreExpressionAndAdd(e.getExpression(),map,key,entryLists,lookUpTable);
    }else if(expr instanceof ExprDiv){
      ExprDiv e = (ExprDiv)expr; //downcast
      exploreExpressionAndAdd(e.getExpression1(),map,key,entryLists,lookUpTable);
      exploreExpressionAndAdd(e.getExpression2(),map,key,entryLists,lookUpTable);
    }else if(expr instanceof ExprDOUBLE){
      //do nothing
    }else if(expr instanceof ExprExp){
      ExprExp e = (ExprExp)expr; //downcast
      exploreExpressionAndAdd(e.getExpression(),map,key,entryLists,lookUpTable);
    }else if(expr instanceof ExprIfThenElse){
      ExprIfThenElse e = (ExprIfThenElse)expr; //downcast
      exploreExpressionAndAdd(e.getBExpression(),map,key,entryLists,lookUpTable);
      exploreExpressionAndAdd(e.getExpression1(),map,key,entryLists,lookUpTable);
      exploreExpressionAndAdd(e.getExpression2(),map,key,entryLists,lookUpTable);
    }else if(expr instanceof ExprINT){
      //do nothing
    }else if(expr instanceof ExprTime){
      //do nothing
    }else if(expr instanceof ExprLn){
      ExprLn e = (ExprLn)expr; //downcast
      exploreExpressionAndAdd(e.getExpression(),map,key,entryLists,lookUpTable);
    }else if(expr instanceof ExprNegation){
      ExprNegation e = (ExprNegation)expr; //downcast
      exploreExpressionAndAdd(e.getExpression(),map,key,entryLists,lookUpTable);
    }else if(expr instanceof ExprPar){
      ExprPar e = (ExprPar)expr; //downcast
      exploreExpressionAndAdd(e.getExpression(),map,key,entryLists,lookUpTable);
    }else if(expr instanceof ExprSin){
      ExprSin e = (ExprSin)expr; //downcast
      exploreExpressionAndAdd(e.getExpression(),map,key,entryLists,lookUpTable);
    }else if(expr instanceof ExprSqrt){
      ExprSqrt e = (ExprSqrt)expr; //downcast
      exploreExpressionAndAdd(e.getExpression(),map,key,entryLists,lookUpTable);
    }else if(expr instanceof ExprMinus){
      ExprMinus e = (ExprMinus)expr; //downcast
      exploreExpressionAndAdd(e.getExpression1(),map,key,entryLists,lookUpTable);
      exploreExpressionAndAdd(e.getExpression2(),map,key,entryLists,lookUpTable);
    }else if(expr instanceof ExprMul){
      ExprMul e = (ExprMul)expr; //downcast
      exploreExpressionAndAdd(e.getExpression1(),map,key,entryLists,lookUpTable);
      exploreExpressionAndAdd(e.getExpression2(),map,key,entryLists,lookUpTable);
    }else if(expr instanceof ExprPlus){
      ExprPlus e = (ExprPlus)expr; //downcast
      exploreExpressionAndAdd(e.getExpression1(),map,key,entryLists,lookUpTable);
      exploreExpressionAndAdd(e.getExpression2(),map,key,entryLists,lookUpTable);
    }else if(expr instanceof ExprFunCall){
      ExprFunCall e = (ExprFunCall)expr; //downcast
      Iterator<Expression> ite = e.getArguments().iterator();
      while (ite.hasNext()){
        Expression tmp = ite.next();      
        exploreExpressionAndAdd(tmp,map,key,entryLists,lookUpTable);
      }
    }else if(expr instanceof ExprLocalPar){
      ExprLocalPar e = (ExprLocalPar)expr; //downcast
      // do nothing
    }else{
      throw new Exception("[error] Probably a "
        + "coding error (AlgebraicEquationsFunctionSignatures.exploreExpressionAndAdd()). Unknown type of Expression.");
    }

  }
  
  static private void exploreExpressionAndAdd(BExpression bexp, Map<Integer,Pair<Set<Integer>,Set<Integer>>> map, 
                        Integer key, EntryLists entryLists,LookUpTable lookUpTable)throws Exception{
                          
    if (bexp instanceof BExprAND){
      BExprAND b = (BExprAND)bexp; //downcast
      exploreExpressionAndAdd(b.getBExpression1(),map,key,entryLists,lookUpTable);
      exploreExpressionAndAdd(b.getBExpression2(),map,key,entryLists,lookUpTable);
    } else if (bexp instanceof BExprDiff){
      BExprDiff b = (BExprDiff)bexp; //downcast
      exploreExpressionAndAdd(b.getExpression1(),map,key,entryLists,lookUpTable);
      exploreExpressionAndAdd(b.getExpression2(),map,key,entryLists,lookUpTable);
    }else if (bexp instanceof BExprEqual){
      BExprEqual b = (BExprEqual)bexp; //downcast
      exploreExpressionAndAdd(b.getExpression1(),map,key,entryLists,lookUpTable);
      exploreExpressionAndAdd(b.getExpression2(),map,key,entryLists,lookUpTable);
    }else if (bexp instanceof BExprGE){
      BExprGE b = (BExprGE)bexp; //downcast
      exploreExpressionAndAdd(b.getExpression1(),map,key,entryLists,lookUpTable);
      exploreExpressionAndAdd(b.getExpression2(),map,key,entryLists,lookUpTable);
    }else if (bexp instanceof BExprGT){
      BExprGT b = (BExprGT)bexp; //downcast
      exploreExpressionAndAdd(b.getExpression1(),map,key,entryLists,lookUpTable);
      exploreExpressionAndAdd(b.getExpression2(),map,key,entryLists,lookUpTable);
    }else if (bexp instanceof BExprLE){
      BExprLE b = (BExprLE)bexp; //downcast
      exploreExpressionAndAdd(b.getExpression1(),map,key,entryLists,lookUpTable);
      exploreExpressionAndAdd(b.getExpression2(),map,key,entryLists,lookUpTable);
    }else if (bexp instanceof BExprLT){
      BExprLT b = (BExprLT)bexp; //downcast
      exploreExpressionAndAdd(b.getExpression1(),map,key,entryLists,lookUpTable);
      exploreExpressionAndAdd(b.getExpression2(),map,key,entryLists,lookUpTable);
    }else if (bexp instanceof BExprOR){
      BExprOR b = (BExprOR)bexp; //downcast
      exploreExpressionAndAdd(b.getBExpression1(),map,key,entryLists,lookUpTable);
      exploreExpressionAndAdd(b.getBExpression2(),map,key,entryLists,lookUpTable);
    }else if (bexp instanceof BExprPar){
      BExprPar b = (BExprPar)bexp; //downcast
      exploreExpressionAndAdd(b.getBExpression(),map,key,entryLists,lookUpTable);
    }else if (bexp instanceof BExprNot){
      BExprNot b = (BExprNot)bexp; //downcast
      exploreExpressionAndAdd(b.getBExpression(),map,key,entryLists,lookUpTable);
    }else if (bexp instanceof BExprFALSE){
      //do nothing
    }else if (bexp instanceof BExprTRUE){
      //do nothing
    }else{
      throw new Exception("[error] Probably a "
        + "coding error (AlgebraicEquationsFunctionSignatures.exploreExpressionAndAdd()). Unknown type of BExpression.");
    }

  }
  
  /**
   * return ALGEBRAIC if the Integer id is the id of an algebraic entry
   * or return DIFFERENTIAL if the id is the id of a differential entry
   * or return -1 if it is not one of the above
   * */
  
  static private int getIDType(Integer id, EntryLists entryLists)throws Exception{
    
    //check algebraic entries first
    Iterator<GenericEntry> ite = entryLists.getAlgList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof AlgebraicEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (AlgebraicEquationsFunctionSignatures.getIDType()). The GenericEntry object is not a AlgebraicEquationNode object");
      AlgebraicEquationNode aen = (AlgebraicEquationNode) tmp;
      
      if (aen.getIdentifier().equals(id)) return ALGEBRAIC;
    }

    //check differential entries now in case it was not an algebraic entry
    Iterator<GenericEntry> ite2 = entryLists.getDiffList().iterator();
    while (ite2.hasNext()){
      GenericEntry tmp = ite2.next();
      if (! (tmp instanceof DifferentialEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (AlgebraicEquationsFunctionSignatures.getIDType()). The GenericEntry object is not a DifferentialEquationNode object");
      DifferentialEquationNode den = (DifferentialEquationNode) tmp;
      
      if (den.getIdentifier().equals(id)) return DIFFERENTIAL;
    }    
    
    //check paramter entry in case it was none of the above
    Iterator<GenericEntry> ite3 = entryLists.getParamList().iterator();
    while (ite3.hasNext()){
      GenericEntry tmp = ite3.next();
      if (! (tmp instanceof ParameterNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.getIDType()). The GenericEntry object is not a ParameterNode object");
      ParameterNode pn = (ParameterNode) tmp;
      
      if (pn.getIdentifier().equals(id)) return PARAMETER;
    } 
    
    //if not found return -1 
    return -1;
  }
  
  
  static private void addToSet(Map<Integer,Pair<Set<Integer>,Set<Integer>>> map, Integer key, Integer toAdd, int type) throws Exception{
    
    Pair<Set<Integer>,Set<Integer>> pair = map.get(key);
    
    if (pair==null){
      //Entry not present, add new mapping
      
      //algebraic id set
      HashSet<Integer> algIDset = new HashSet<Integer>();
      //differential id set
      HashSet<Integer> diffIDset = new HashSet<Integer>();
      if (type == ALGEBRAIC){
        algIDset.add(toAdd);
      }else if (type == DIFFERENTIAL){
        diffIDset.add(toAdd);
      }else{
        throw new Exception("[error] Coding error in AlgebraicEquationsFunctionSignatures.addToSet(): unknown type.");
      }
      
      pair = new Pair<Set<Integer>,Set<Integer>>(algIDset,diffIDset);
      
      map.put(key,pair);
    }else{
      //add to appropriate set
      if (type == ALGEBRAIC){
        pair.first.add(toAdd);
      }else if (type == DIFFERENTIAL){
        pair.second.add(toAdd);
      }else{
        throw new Exception("[error] Coding error in AlgebraicEquationsFunctionSignatures.addToSet(): unknown type.");
      }
      
    }
  }

  static private void addToSet(Map<Integer,Pair<Set<Integer>,Set<Integer>>> map, Integer key, Set<Integer> toAdd, int type) throws Exception{
    
    Pair<Set<Integer>,Set<Integer>> pair = map.get(key);
    
    if (pair==null){
      //Entry not present, add new mapping
      
      //algebraic id set
      HashSet<Integer> algIDset = new HashSet<Integer>();
      //differential id set
      HashSet<Integer> diffIDset = new HashSet<Integer>();
      if (type == ALGEBRAIC){
        algIDset.addAll(toAdd);
      }else if (type == DIFFERENTIAL){
        diffIDset.addAll(toAdd);
      }else{
        throw new Exception("[error] Coding error in AlgebraicEquationsFunctionSignatures.addToSet(): unknown type.");
      }
      
      pair = new Pair<Set<Integer>,Set<Integer>>(algIDset,diffIDset);
      
      map.put(key,pair);
    }else{
      //add to appropriate set
      if (type == ALGEBRAIC){
        pair.first.addAll(toAdd);
      }else if (type == DIFFERENTIAL){
        pair.second.addAll(toAdd);
      }else{
        throw new Exception("[error] Coding error in AlgebraicEquationsFunctionSignatures.addToSet(): unknown type.");
      }
      
    }
  }

  static private Map<Integer,Pair<Set<Integer>,Set<Integer>>> mapCopy(Map<Integer,Pair<Set<Integer>,Set<Integer>>> map){
    
    Map<Integer,Pair<Set<Integer>,Set<Integer>>> copy = new HashMap<Integer,Pair<Set<Integer>,Set<Integer>>>();
    
    Set<Integer> keys = map.keySet();
    
    Iterator<Integer> ite = keys.iterator();
    while (ite.hasNext()){
      Integer key = ite.next();
      
      Pair<Set<Integer>,Set<Integer>> pairToBeCopied = map.get(key);
      
      Pair<Set<Integer>,Set<Integer>> tmpPair = new Pair<Set<Integer>,Set<Integer>>(new HashSet<Integer>(pairToBeCopied.first),new HashSet<Integer>(pairToBeCopied.second));
      copy.put(key,tmpPair);
      
    }
    
    return copy;
  }
  

}
