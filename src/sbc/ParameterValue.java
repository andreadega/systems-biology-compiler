package sbc;

import java.util.*;

public class ParameterValue extends ParameterNode {
  
  private Double value;

  public ParameterValue(Integer identifier, Double value){
    super(identifier);
    this.value = value;
  }
  
  public Double getValue(){
    return value;
  }
  
}
