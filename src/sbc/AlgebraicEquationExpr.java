package sbc;

import java.util.*;
import sbc.expr.Expression;

public class AlgebraicEquationExpr extends AlgebraicEquationNode {
  
  private Expression expr;

  public AlgebraicEquationExpr(Integer identifier, Expression expr){
    super(identifier);
    this.expr = expr;
  }
  
  public Expression getExpression(){
    return expr;
  }
  
}
