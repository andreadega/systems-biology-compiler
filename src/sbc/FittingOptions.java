
package sbc;

import java.util.*;

//probably need a funtion to run some checks on the Fitting Options

public class FittingOptions {
  
  //map Integer species ID to a list of Integer condition IDs
  private HashMap<Integer, LinkedList<Integer>> idCondList = null;
  
  private int numberOfNodes = 1;
  private int numberOfProcessorsPerNode = 1;
  private int numberOfJobsPerProcessor = 1;
  
  /* email to send notification of start/end PBS jobs*/
  private String email = null;
  
  /* PBS -A ...*/
  private String minusA = null;
  
  /* PBS  -l walltime= number of hours*/
  private int wallTime = 48;
  
  /* Lsqnonlin Options*/
  private int lsqnonlin_maxiter = 50;
  private int lsqnonlin_numOfRestarts = 100;
  
  private String clusterDirectory = "/home/people/adegaspe/build/";
  
  /* Use LogLikelihood in place of least squares? */
  // 0 use least squares
  // 1 use logLikelihood with Independent Noise
  // 2 use logLikelihood with Linear Model Noise
  private int logLikelihood = 0;
  
  public FittingOptions(){
    
  }

  public void setIDCondList(HashMap<Integer, LinkedList<Integer>> idCondList){
    this.idCondList = idCondList;
  }

  public HashMap<Integer, LinkedList<Integer>> getIDCondList(){
    return idCondList;
  }
  
  public void setNumberOfNodes(int numberOfNodes){
    this.numberOfNodes = numberOfNodes;
  }
  
  public int getNumberOfNodes(){
    return numberOfNodes;
  }
  
  public void setNumberOfProcessorsPerNode(int numberOfProcessorsPerNode){
    this.numberOfProcessorsPerNode = numberOfProcessorsPerNode;
  }
  
  public int getNumberOfProcessorsPerNode(){
    return numberOfProcessorsPerNode;
  }

  public void setNumberOfJobsPerProcessor(int numberOfJobsPerProcessor){
    this.numberOfJobsPerProcessor = numberOfJobsPerProcessor;
  }
  
  public int getNumberOfJobsPerProcessor(){
    return numberOfJobsPerProcessor;
  }

  public void setEmail(String email){
    this.email = email;
  }
  
  public String getEmail(){
    return email;
  }
  
  public void setMinusA(String minusA){
    this.minusA = minusA;
  }
  
  public String getMinusA(){
    return minusA;
  }
  
  public void setClusterDirectory(String clusterDirectory){
    this.clusterDirectory = clusterDirectory;
  }
  
  public String getClusterDirectory(){
    return clusterDirectory;
  }
  
  public void setLogLikelihood(int ll){
    this.logLikelihood = ll;
  }
  
  public int getLogLikelihood(){
    return this.logLikelihood;
  }
  
  /**
   * Set wall time for PBS scripts
   * */
  public void setWallTime(int wallTime){
    this.wallTime = wallTime;
  }
    
  /**
   * Get wall time for PBS scripts
   * */
  public int getWallTime(){
    return wallTime;
  }
  
  /**
   * Set Max iterations of Lsqnonlin
   * */
  public void setLsqnonlin_maxiter(int lsqnonlin_maxiter){
    this.lsqnonlin_maxiter = lsqnonlin_maxiter;
  }
    
  /**
   * Get Max iterations of Lsqnonlin
   * */
  public int getLsqnonlin_maxiter(){
    return lsqnonlin_maxiter;
  }
  
 /**
   * Set the number of resampling/restarts of Lsqnonlin
   * */
  public void setLsqnonlin_numOfRestarts(int lsqnonlin_numOfRestarts){
    this.lsqnonlin_numOfRestarts = lsqnonlin_numOfRestarts;
  }
    
  /**
   * Get the number of resampling/restarts of Lsqnonlin
   * */
  public int getLsqnonlin_numOfRestarts(){
    return lsqnonlin_numOfRestarts;
  }
}
