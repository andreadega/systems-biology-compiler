
package sbc;

import java.util.*;

public abstract class NormSimulationsNode {
  
  private Integer identifier;
  private LinkedList<Integer> conditions;
  
  protected NormSimulationsNode(Integer identifier, LinkedList<Integer> conditions){
    this.identifier = identifier;
    this.conditions = conditions;
  }
  
  public LinkedList<Integer> getConditions(){
    return this.conditions;
  }
  
  public Integer getIdentifier(){
    return this.identifier;
  }
  
}
