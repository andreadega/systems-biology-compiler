package sbc;

import java.util.*;

public abstract class AlgebraicEquationCond {
  
  private Integer condition;

  protected AlgebraicEquationCond(Integer condition){
    this.condition = condition;
  }
  
  public Integer getCondition(){
    return condition;
  }
  
}
