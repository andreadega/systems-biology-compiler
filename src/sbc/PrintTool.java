
package sbc;
import java.util.*;
import java.io.*;

public class PrintTool{
  public static void printDataList(LinkedList<GenericEntry> listOfDataNodes, LookUpTable lookUpTable, BufferedWriter bw)throws Exception{
    Iterator<GenericEntry> ite = listOfDataNodes.iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
       
      if (! (tmp instanceof DataNodeReplicates)) throw new Exception("[error] Probably a "
      + "coding error (NormalisationTool class). The GenericEntry object is not a DataNodeReplicates object.");
      DataNodeReplicates dnr = (DataNodeReplicates) tmp; //downcast
      
      bw.write("Data entry for: " + lookUpTable.lookUpString(dnr.getIdentifier()) + "\n");
      
      Iterator<Integer> ite2 = dnr.getConditions().iterator();
      while (ite2.hasNext()){
        Integer tmp2 = ite2.next();
        bw.write(lookUpTable.lookUpString(tmp2) + "\t");
      }
      bw.write("\n");
      
      LinkedList<Double[][]> matrixList = dnr.getListOfDataMatrices();
      Iterator<Double[][]> ite3 = matrixList.iterator();
      while (ite3.hasNext()){
        Double[][] matrix = ite3.next();
        
        String s = "";
        for (int i = 0; i< matrix.length; i++){
          for (int j = 0; j< matrix[i].length; j++){
            if (j==0) s += String.format("%.2f", matrix[i][j].doubleValue()) + "\t";
            else s += String.format("%.4f", matrix[i][j].doubleValue()) + "\t";
          }
          s += "\n";
        }
        
        bw.write(s + "-\n");
      }
      
      
    }
    
  }
}
