package sbc;

import java.util.*;

public class EntryLists {
  //list of data nodes
  private LinkedList<GenericEntry> dataList;
  //list of parameter nodes
  private LinkedList<GenericEntry> paramList;
  //list of initial condition nodes
  private LinkedList<GenericEntry> initList;
  //list of normalisation nodes
  private LinkedList<GenericEntry> normList;
  //list of differential equations nodes
  private LinkedList<GenericEntry> diffList;
  //list of algebraic equation nodes
  private LinkedList<GenericEntry> algList;
  //list of function definitions
  private LinkedList<GenericEntry> funList;
  //list of event definitions
  private LinkedList<GenericEntry> eventList;
  
  
  public EntryLists(){
    dataList = new LinkedList<GenericEntry>();
    paramList = new LinkedList<GenericEntry>();
    initList = new LinkedList<GenericEntry>();
    normList = new LinkedList<GenericEntry>();
    diffList = new LinkedList<GenericEntry>();
    algList = new LinkedList<GenericEntry>();
    funList = new LinkedList<GenericEntry>();
    eventList = new LinkedList<GenericEntry>();
  }
  
  /**
   * Check the lists for compatibility and add to appropriate list depending
   * on the type of generic entry
   * */
  public void add(GenericEntry ge) throws Exception{
    if (ge == null) throw new Exception("[error] Attempting to add a null entry to EntryLists");
    
    //check compatibilities
    checkCompatibilities(ge,dataList);
    checkCompatibilities(ge,paramList);
    checkCompatibilities(ge,initList);
    checkCompatibilities(ge,normList);
    checkCompatibilities(ge,diffList);
    checkCompatibilities(ge,algList);
    checkCompatibilities(ge,funList);
    checkCompatibilities(ge,eventList);
    
    //if no exception thrown so far, add to appropriate list
    if (ge instanceof DataNode){
      dataList.add(ge);
    } else if (ge instanceof ParameterNode){
      paramList.add(ge);
    } else if (ge instanceof InitialConditionNode){
      initList.add(ge);
    } else if (ge instanceof NormalisationNode){
      normList.add(ge);
    } else if (ge instanceof DifferentialEquationNode){
      diffList.add(ge);
    } else if (ge instanceof AlgebraicEquationNode){
      algList.add(ge);
    } else if (ge instanceof FunctionNode){
      funList.add(ge);
    } else if (ge instanceof EventNode){
      eventList.add(ge);
    }
  }
  
  /**
   * check compatibilities, if an incompatibility is detected, an exception is
   * thrown with a message, depending on GenericEntry class implementations
   * */ 
  private void checkCompatibilities(GenericEntry ge, LinkedList<GenericEntry> list) throws Exception{
    Iterator<GenericEntry> ite = list.iterator();
    while (ite.hasNext()){
       GenericEntry tmp = ite.next();
       ge.checkCompatibleWith(tmp);
    }
    
  }
  
  public LinkedList<GenericEntry> getDataList(){
    return dataList;
  }
  
  public LinkedList<GenericEntry> getNormList(){
    return normList;
  }
  
  public LinkedList<GenericEntry> getParamList(){
    return paramList;
  }
  
  public LinkedList<GenericEntry> getInitList(){
    return initList;
  }
  
  public LinkedList<GenericEntry> getAlgList(){
    return algList;
  }
  
  public LinkedList<GenericEntry> getDiffList(){
    return diffList;
  }
  
  public LinkedList<GenericEntry> getFunList(){
    return funList;
  }
  
  public LinkedList<GenericEntry> getEventList(){
    return eventList;
  }
}
