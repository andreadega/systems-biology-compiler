
package sbc;

import java.util.*;

public abstract class NormalisationNode implements GenericEntry {
  
  private Integer identifier;
  
  protected NormalisationNode(Integer identifier){
    this.identifier = identifier;
  }
  
  public Integer getIdentifier(){
    return this.identifier;
  }
  
  public void checkCompatibleWith(GenericEntry entry) throws Exception{
    //no need to check, will check everything is fine when applying the
    //normalisation
  
    //no known problems with other types implementing GenericEntry
    return;
  }
}
