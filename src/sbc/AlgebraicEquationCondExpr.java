package sbc;

import java.util.*;
import sbc.expr.Expression;

public class AlgebraicEquationCondExpr extends AlgebraicEquationCond {
  
  private Expression expr;

  public AlgebraicEquationCondExpr(Integer condition, Expression expr){
    super(condition);
    this.expr = expr;
  }
  
  public Expression getExpression(){
    return expr;
  }
  
}
