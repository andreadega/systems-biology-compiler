
package sbc;

import java.util.*;
import sbc.expr.*;

public class FunctionNode implements GenericEntry {
  
  private Integer identifier;
  private LinkedList<String> args;
  private Expression expr;
  
  public FunctionNode(Integer identifier, LinkedList<String> args, Expression expr){
    this.identifier = identifier;
    this.args = args;
    this.expr = expr;
  }
  
  public Integer getIdentifier(){
    return this.identifier;
  }

  public LinkedList<String> getArguments(){
    return this.args;
  }

  public Expression getExpression(){
    return this.expr;
  }
  
  
  public void checkCompatibleWith(GenericEntry entry) throws Exception{
    if (entry==null) return;
    //really something wrong if you check compatibility with yourself (this)
    //I don't see any reason why this should happen
    if (entry==this) throw new Exception("[error] Probably programming error,"+
                          " as two FunctionNode objects tested for compatibility" + 
                          " (isCompatibleWith) are the same pointer."); 
    if (entry instanceof AlgebraicEquationNode){
      //if the identifier is different then return
      
      AlgebraicEquationNode dn = (AlgebraicEquationNode)entry;  //downcast
      
      if (dn.getIdentifier().equals(this.getIdentifier())){
        throw new Exception("[error] A function entry and an algebraic equation entry "+
                      "have the same identifier.");
      }
      return;
    } else if (entry instanceof ParameterNode){
      ParameterNode p = (ParameterNode)entry;  //downcast
      if (p.getIdentifier().equals(this.getIdentifier())){
        throw new Exception("[error] A function entry and a parameter entry "+
                      "have the same identifier.");
      }
      return;
    } else if (entry instanceof DifferentialEquationNode){
      //if the identifier is different then return 
      
      DifferentialEquationNode dn = (DifferentialEquationNode)entry;  //downcast
      
      if (dn.getIdentifier().equals(this.getIdentifier())){
        throw new Exception("[error] A function entry and a differential equation entry "+
                      "have the same identifier.");
      }
      return;
    } else if (entry instanceof FunctionNode){
      //if the identifier is different then return 
      
      FunctionNode dn = (FunctionNode)entry;  //downcast
      
      if (dn.getIdentifier().equals(this.getIdentifier())){
        throw new Exception("[error] Two function entries "+
                      "have the same identifier.");
      }
      return;
    } else if (entry instanceof DataNode){
      //if the identifier is different then return 
      
      DataNode dn = (DataNode)entry;  //downcast
      
      if (dn.getIdentifier().equals(this.getIdentifier())){
        throw new Exception("[error] A data entry and a function entry "+
                      "have the same identifier.");
      }
      return;
    } else if (entry instanceof InitialConditionNode){
      //if the identifier is different then return 
      
      InitialConditionNode dn = (InitialConditionNode)entry;  //downcast
      
      if (dn.getIdentifier().equals(this.getIdentifier())){
        throw new Exception("[error] An initial condition entry and a function entry "+
                      "have the same identifier.");
      }
      return;
    }else if (entry instanceof EventNode){
      //if the identifier is different then return 
      
      EventNode dn = (EventNode)entry;  //downcast
      
      if (dn.getIdentifier().equals(this.getIdentifier())){
        throw new Exception("[error] A function entry and an event entry "+
                      "have the same identifier.");
      }
      return;
    }
    //no known problems with other types implementing GenericEntry
    return;
  }
}
