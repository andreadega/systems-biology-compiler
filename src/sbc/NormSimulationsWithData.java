
package sbc;

import java.util.*;

public class NormSimulationsWithData extends NormSimulationsNode {
  
  private Integer dataID;
  private LinkedList<Integer> dataConditions;
  
  public NormSimulationsWithData(Integer identifier, LinkedList<Integer> conditions, Integer dataID, LinkedList<Integer> dataConditions){
    super(identifier,conditions);
    this.dataID = dataID;
    this.dataConditions = dataConditions;
  }
  
  public LinkedList<Integer> getDataConditions(){
    return this.dataConditions;
  }
  
  public Integer getDataID(){
    return this.dataID;
  }
}
