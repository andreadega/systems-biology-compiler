
package sbc.util;

import java.util.*;
import sbc.*;
import sbc.expr.*;
import sbc.glsdc.*;
import java.io.*;

/**
 * Class with only staic methods that can only be used in a static way
 * and not be instantiated as object. Use this class to query models and
 * data as loaded in the internal datastructure
 * */

public final class QueryModelAndData  {
  
  private QueryModelAndData(){}
  
  /**
   * retrive the DataNodeReplicates with the 
   * given speciesID and conditionID. Only one DataNode can have this combination
   * return null if not found
   * */
  
  public static DataNodeReplicates getDataNodeReplicatesOf(EntryLists entryLists,Integer speciesID,Integer conditionID) throws Exception{
    
    Iterator<GenericEntry> ite = entryLists.getDataList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof DataNodeReplicates)) throw new Exception("[error] Probably a "
        + "coding error (QueryModelAndData.getDataNodeReplicatesOf()). The GenericEntry object is not a DataNodeReplicates object");
      DataNodeReplicates dnr = (DataNodeReplicates) tmp;
      if (dnr.getIdentifier().equals(speciesID) && dnr.getConditions().contains(conditionID)){
        return dnr;
      }
    }
    
    return null;
  }
  
  /**
   * generate a unique ID from a DataNode using species ID and condition IDs
   * */
  
  public static String buildUniqueIDFrom(LookUpTable lookUpTable, DataNode dn){
    String uniqueID = lookUpTable.lookUpString(dn.getIdentifier()); //initialise
    
    Iterator<Integer> ite0 = dn.getConditions().iterator();
    if (ite0.hasNext()) ite0.next(); //skip time
    while (ite0.hasNext()){
      Integer tmp0 = ite0.next();
      uniqueID += "c_" + tmp0.intValue();
    }
    return uniqueID;
  }
  
  /**
   * generate a unique ID from a speciesID and a list of condition IDs
   * */
  
  public static String buildUniqueIDFrom(LookUpTable lookUpTable, Integer id,LinkedList<Integer> list){
    String uniqueID = lookUpTable.lookUpString(id); //initialise
    
    Iterator<Integer> ite0 = list.iterator();
    if (ite0.hasNext()) ite0.next(); //skip time
    while (ite0.hasNext()){
      Integer tmp0 = ite0.next();
      uniqueID += "c_" + tmp0.intValue();
    }
    return uniqueID;
  }
  
    
    /**
   * return ALGEBRAIC if the Integer id is the id of an algebraic entry
   * or return DIFFERENTIAL if the id is the id of a differential entry
   * or return PARAMETER if the id is the id of a parameter entry
   * or return -1 if it is not one of the above
   * */
  
  public static int getIDType(EntryLists entryLists, Integer id)throws Exception{
    
    //check algebraic entries first
    Iterator<GenericEntry> ite = entryLists.getAlgList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof AlgebraicEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (QueryModelAndData.getIDType()). The GenericEntry object is not a AlgebraicEquationNode object");
      AlgebraicEquationNode aen = (AlgebraicEquationNode) tmp;
      
      if (aen.getIdentifier().equals(id)) return GLSDCTool.ALGEBRAIC;
    }

    //check differential entries now in case it was not an algebraic entry
    Iterator<GenericEntry> ite2 = entryLists.getDiffList().iterator();
    while (ite2.hasNext()){
      GenericEntry tmp = ite2.next();
      if (! (tmp instanceof DifferentialEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (QueryModelAndData.getIDType()). The GenericEntry object is not a DifferentialEquationNode object");
      DifferentialEquationNode den = (DifferentialEquationNode) tmp;
      
      if (den.getIdentifier().equals(id)) return GLSDCTool.DIFFERENTIAL;
    }    

    //check paramter entry in case it was none of the above
    Iterator<GenericEntry> ite3 = entryLists.getParamList().iterator();
    while (ite3.hasNext()){
      GenericEntry tmp = ite3.next();
      if (! (tmp instanceof ParameterNode)) throw new Exception("[error] Probably a "
        + "coding error (QueryModelAndData.getIDType()). The GenericEntry object is not a ParameterNode object");
      ParameterNode pn = (ParameterNode) tmp;
      
      if (pn.getIdentifier().equals(id)) return GLSDCTool.PARAMETER;
    } 
    
    //if not found return -1 
    return -1;
  }


  /**
   * return all conditions
   * */
  
  public static Set<Integer> getAllConditions(EntryLists entryLists,LookUpTable lookUpTable) throws Exception{
    Set<Integer> set = new HashSet<Integer>();
    
    Iterator<GenericEntry> ite = entryLists.getDataList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof DataNodeReplicates)) throw new Exception("[error] Probably a "
        + "coding error (QueryModelAndData.getAllConditions()). The GenericEntry object is not a DataNodeReplicates object");
      DataNodeReplicates dnr = (DataNodeReplicates) tmp;
      Iterator<Integer> ite2 = dnr.getConditions().iterator();
      if (ite2.hasNext()) ite2.next(); //skip time
      while (ite2.hasNext()){
        Integer cond = ite2.next();
        set.add(cond);
      }
    }

    ite = entryLists.getInitList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof InitialConditionNode)) throw new Exception("[error] Probably a "
        + "coding error (QueryModelAndData.getAllConditions()). The GenericEntry object is not a InitialConditionNode object");
      if (tmp instanceof InitialConditionParam){
        
      } else if (tmp instanceof InitialConditionCondList){
        InitialConditionCondList iccl = (InitialConditionCondList) tmp;
        Iterator<Integer> ite2 = iccl.getMapConditionToParamID().keySet().iterator();
        while (ite2.hasNext()){
          Integer cond = ite2.next();
          set.add(cond);
        }
      } else {
        throw new Exception("[error] Probably a "
        + "coding error (QueryModelAndData.getAllConditions()). Unknown type of InitialConditionNode");
      }

    }    
    
    ite = entryLists.getAlgList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof AlgebraicEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (QueryModelAndData.getAllConditions()). The GenericEntry object is not a AlgebraicEquationNode object");
      if (tmp instanceof AlgebraicEquationExpr){
        
      } else if (tmp instanceof AlgebraicEquationInterpolation){
        
      } else if (tmp instanceof AlgebraicEquationCondList){
        AlgebraicEquationCondList aecl = (AlgebraicEquationCondList) tmp;
        set.addAll(aecl.getConditions());
        
      } else {
        throw new Exception("[error] Probably a "
        + "coding error (QueryModelAndData.getAllConditions()). Unknown type of AlgebraicEquationNode");
      }

    }       

    ite = entryLists.getDiffList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof DifferentialEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (QueryModelAndData.getAllConditions()). The GenericEntry object is not a DifferentialEquationNode object");
      if (tmp instanceof DifferentialEquationExpr){
        
      } else if (tmp instanceof DifferentialEquationCondList){
        DifferentialEquationCondList decl = (DifferentialEquationCondList) tmp;
        set.addAll(decl.getMapConditionToExpression().keySet());
        
      } else {
        throw new Exception("[error] Probably a "
        + "coding error (QueryModelAndData.getAllConditions()). Unknown type of DifferentialEquationNode");
      }

    } 
    // if no conditions have been specified you can still simulate the default condition of the equations
    if (set.isEmpty()) set.add(lookUpTable.getKey("\"default\""));
    return set;
  }

  /**
   * get IDs of parameters to fit
   * */
  public static LinkedList<Integer> getParametersToFit(EntryLists entryLists) throws Exception{
    LinkedList<Integer> params = new LinkedList<Integer>();
    Iterator<GenericEntry> ite = entryLists.getParamList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof ParameterNode)) throw new Exception("[error] coding error! I should have ParameterNode objects in QueryModelAndData.getParametersToFit()");
      if (tmp instanceof ParameterRange){
        ParameterRange pr = (ParameterRange) tmp; //downcast
        params.addLast(pr.getIdentifier());
      }
    }    
    return params;
  }
  
  
  /**
   * procedure to print the initial conditions for a specified condition
   * 
   * If initSensitivities==true then also initialise the sensitivities
   * Given a condition, a differential variable (species S) and a parameter p,
   * find the initial value of the sensitivity dS/dp
   * This requires to find the initial value of S, which has the form const*k.
   * If k != p then dS/dp = 0, otherwise dS/dp = const
   * */
  
  //~ public static void printInitialConditionsForCondition(EntryLists entryLists,LookUpTable lookUpTable,
                      //~ FittingOptions fittingOptions,boolean initSensitivities,LogLikelihoodTool logLikelihoodTool,
                      //~ Integer condition, BufferedWriter bw) throws Exception{
  public static void printInitialConditionsForCondition(EntryLists entryLists,LookUpTable lookUpTable,
                      boolean initSensitivities,Integer condition, BufferedWriter bw) throws Exception{
    //int i = 0;
    Iterator<GenericEntry> ite = entryLists.getDiffList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof DifferentialEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (QueryModelAndData.printInitialConditionsForCondition()). The GenericEntry object is not a DifferentialEquationNode object");
      DifferentialEquationNode den = (DifferentialEquationNode) tmp;
      
      //find initialisation of speciesID of the DifferentialEquationNode
      //and retrieve appropriate paramID
      Integer paramID = null;
      Double factor = null;
      
      Iterator<GenericEntry> ite2 = entryLists.getInitList().iterator();
      while (ite2.hasNext()){
        GenericEntry tmp2 = ite2.next();
        if (! (tmp2 instanceof InitialConditionNode)) throw new Exception("[error] Probably a "
            + "coding error (QueryModelAndData.printInitialConditionsForCondition()). The GenericEntry object is not a InitialConditionNode object");
        //if correct initialisation node
        InitialConditionNode n = (InitialConditionNode)tmp2;
        if (den.getIdentifier().equals(n.getIdentifier())){
          if (tmp2 instanceof InitialConditionParam){
            InitialConditionParam icp = (InitialConditionParam)tmp2;
            paramID = icp.getParamID();
            factor = icp.getFactor();
   
          }else if (tmp2 instanceof InitialConditionCondList){
            InitialConditionCondList iccl = (InitialConditionCondList)tmp2;
            HashMap<Integer,Pair<Integer,Double>> map = iccl.getMapConditionToParamID();
            Pair<Integer,Double> p = map.get(condition);//null if not found
            if (p!=null){
              paramID = p.first;  //null if not found
              factor = p.second;
            }
          }else{
            throw new Exception("[error] Probably a "
              + "coding error (QueryModelAndData.printInitialConditionsForCondition()). Unknown InitialConditionNode object");
          }
        }
      }
      
      if (paramID == null){
        //initial condition not found!
        throw new Exception("[error] Initial condition for " + lookUpTable.lookUpString(den.getIdentifier())
                      + " and condition " + lookUpTable.lookUpString(condition) + " not found.");
      }else{
        //need to check what this paramID is, because it may not be a param at all!
        if (GLSDCTool.PARAMETER != QueryModelAndData.getIDType(entryLists,paramID)){
          throw new Exception("[error] Initial condition for " + lookUpTable.lookUpString(den.getIdentifier())
                      + " and condition " + lookUpTable.lookUpString(condition) + ": ID " + lookUpTable.lookUpString(paramID) 
                      + " is not a parameter! Only parameters can be used in initial conditions.");
        }else{
          String factorS = "";
          double factorD = factor.doubleValue();
          if (factorD != 1.0) factorS = factor + "*";
          bw.write("  NV_Ith_S(y_c_" + condition.intValue() + "," + lookUpTable.lookUpString(den.getIdentifier()) 
                + ") = " + factorS + "X[" + lookUpTable.lookUpString(paramID) + "];\n");
          
          if (initSensitivities){
            //I should probably just write the intialisation of the sensitivity here!
            //-------------------
            // when I write for(int is=0;is<NP;is++), I need to know what paramID
            // corresponds to the iterator "is".
            //get IDs of parameters to fit
            LinkedList<Integer> params = QueryModelAndData.getParametersToFit(entryLists);

            int n = 0;
            Iterator<Integer> ite99 = params.iterator();
            while (ite99.hasNext()){
              Integer pr = ite99.next();
              bw.write("  NV_Ith_S(uS_c_" + condition.intValue() + "[" + n + "]," + lookUpTable.lookUpString(den.getIdentifier()) 
                + ") = ");
              if (pr.equals(paramID)){
                bw.write(factor.toString());
              }else{
                bw.write("0.0");
              }
              bw.write(";//derivative w.r.t. " + lookUpTable.lookUpString(pr) + "\n");
              n++; 
            }
            //no need to compute these derivatives!
            //~ //if LogLikelihood is requested, I need to fit some additional parameters as well
            //~ int typeOfLL = fittingOptions.getLogLikelihood();
            //~ if (typeOfLL == LogLikelihoodTool.IndependentNoise || typeOfLL == LogLikelihoodTool.LinearModelOfNoise){
              //~ Iterator<ParameterRangeOfAdditionalParam> ite_sigma = logLikelihoodTool.getListOfAdditionalParams().iterator();
              //~ while (ite_sigma.hasNext()){
                //~ Integer pr = ite_sigma.next().getIdentifier();
                //~ bw.write("  NV_Ith_S(uS_c_" + condition.intValue() + "[" + n + "]," + lookUpTable.lookUpString(den.getIdentifier()) 
                  //~ + ") = ");
                //~ if (pr.equals(paramID)){
                  //~ bw.write(factor.toString());
                //~ }else{
                  //~ bw.write("0.0");
                //~ }
                //~ bw.write(";  //derivative w.r.t. " + lookUpTable.lookUpString(pr) + "\n");
                //~ n++;
              //~ }
            //~ }
          }
          //-------------------
        }
      }
      
      //i++;
    }
    
  }
  
    /**
   * retrieve the set of Algebraic equations that require their derivative
   * versions to compute the sensitivities for parameter estimation
   * 
   * Start from the list of species to fit, and for each species,
   * if it is an algebraic equation, then dig to find recursively
   * what algebraic equations are used and thus require derivative versions
   * */
  public static Set<Integer> requiredAlgebraicEqDerivatives(EntryLists entryLists,
                  LookUpTable lookUpTable,FittingOptions fittingOptions) throws Exception{
    //initialise return set
    Set<Integer> returnSet = new HashSet<Integer>();
    //retrieve map from algEqID to set of the nested algEqID
    Map<Integer,Set<Integer>> nestedAE_Map = AlgebraicEquationsFunctionSignatures.retrieveNestedAlgEq(entryLists,lookUpTable);
    //retrieve species to fit with associated conditions 
    //(could be null if no fitting data was specified)
    HashMap<Integer, LinkedList<Integer>> speciesToCond = fittingOptions.getIDCondList();
    if (speciesToCond != null){
      //iterate the species that need to be fitted
      //if they are algebraic eq then add their nested alg eq to the ret set
      Iterator<Integer> iteSpecies = speciesToCond.keySet().iterator();
      while (iteSpecies.hasNext()){
        Integer speciesID = iteSpecies.next();
        if (getIDType(entryLists,speciesID) == GLSDCTool.ALGEBRAIC){
          //add all nested algebraic eq ID to the return Set
          //also add speciesID as it may not be in the nested set
          returnSet.add(speciesID);
          returnSet.addAll(nestedAE_Map.get(speciesID));
        }
      }
    }
    return returnSet;
  }
  
  /**
   * return the List of Ids of the differential variables dx/dt
   * */
  
  public static LinkedList<Integer> getDifferentialVariablesIDs(EntryLists entryLists) throws Exception{
    LinkedList<Integer> speciesVariables = new LinkedList<Integer>();
    
    Iterator<GenericEntry> ite = entryLists.getDiffList().iterator();
    while (ite.hasNext()){
      GenericEntry tmp = ite.next();
      if (! (tmp instanceof DifferentialEquationNode)) throw new Exception("[error] Probably a "
        + "coding error (GLSDCTool.speciesVariables()). The GenericEntry object is not a DifferentialEquationNode object");
      DifferentialEquationNode den = (DifferentialEquationNode) tmp;
      
      speciesVariables.add(den.getIdentifier());

    }
    return speciesVariables;
  }
}
