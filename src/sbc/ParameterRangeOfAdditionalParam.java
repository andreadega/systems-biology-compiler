package sbc;

import java.util.*;

/**
 * Additional parameters used for example in the loglikelihood objective 
 * function and that need to be estimated
 * */
public class ParameterRangeOfAdditionalParam extends ParameterRange {
  
  //variable name used in C++ file to store the sigma value
  private String variable;

  public ParameterRangeOfAdditionalParam(Integer identifier, Double bottom, Double top, String variable){
    super(identifier,bottom,top);
    this.variable = variable;
  }
  
  public String getVariable(){
    return this.variable;
  }

  
}
