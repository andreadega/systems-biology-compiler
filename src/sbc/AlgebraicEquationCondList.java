package sbc;

import java.util.*;

public class AlgebraicEquationCondList extends AlgebraicEquationNode {
  
  private LinkedList<AlgebraicEquationCond> list;

  public AlgebraicEquationCondList(Integer identifier, LinkedList<AlgebraicEquationCond> list){
    super(identifier);
    this.list = list;
  }
  
  public LinkedList<AlgebraicEquationCond> getAlgebraicEquationCondList(){
    return list;
  }
  
    /**
   * extract condition IDs from an AlgebraicEquationCondList object
   * */
  
  public Set<Integer> getConditions(){
    Set<Integer> s = new HashSet<Integer>();
    
    Iterator<AlgebraicEquationCond> ite = list.iterator();
    while (ite.hasNext()){
      AlgebraicEquationCond tmp = ite.next();
      s.add(tmp.getCondition());
    }
    
    return s;
  }
  
      /**
   * return the AlgebraicEquationCond object with the requested condition ID
   * return null if not found
   * */
  
  public AlgebraicEquationCond getNodeWithConditionID(Integer id){
    
    Iterator<AlgebraicEquationCond> ite = list.iterator();
    while (ite.hasNext()){
      AlgebraicEquationCond tmp = ite.next();
      if (tmp.getCondition().equals(id)) return tmp;
    }
    
    return null;
  }
  
}
