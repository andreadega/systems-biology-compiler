
package sbc;
import java.util.*;


public class LookUpTable {
  Map<Integer,String> table;
  Map<String,Integer> tableInverse;
  
  public LookUpTable(){
    table = new HashMap<Integer,String>();
    tableInverse = new HashMap<String,Integer>();
  }
  
  /**
   * if the string id is present in the table, return the corresponding
   * key, while if the string id is not present then return a new key
   * and add the new map (key, id) to the table
   * Keys cannot be removed, so the Integer key is taken from size() +1
   * of the table
   * */
  
  public Integer getKey(String id){    
    if (table.containsValue(id)){ //id already present
      return tableInverse.get(id);
    }
    //id not present, add
    int newKey = table.size() + 1;
    Integer newKeyObj = new Integer(newKey);
    table.put(newKeyObj,id);
    tableInverse.put(id,newKeyObj);
    return newKeyObj;
  }
  
  /**
   * return the string associated the given Integer key, or null if the
   * key is not present
   * */
  
  public String lookUpString(Integer key){
    return table.get(key);
  }
  
}
