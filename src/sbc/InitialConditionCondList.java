package sbc;

import java.util.*;
import sbc.util.Pair;

public class InitialConditionCondList extends InitialConditionNode {
  
  //maps conditions to factors and paramIDs
  private HashMap<Integer,Pair<Integer,Double>> mapConditionToParamID;

  public InitialConditionCondList(Integer identifier, HashMap<Integer,Pair<Integer,Double>> mapConditionToParamID){
    super(identifier);
    this.mapConditionToParamID = mapConditionToParamID;
  }
  
  public HashMap<Integer,Pair<Integer,Double>> getMapConditionToParamID(){
    return mapConditionToParamID;
  }
  
}
