
package sbc;

import java.util.*;


public class TwoVariablesPlot {
  
  Integer speciesID = null;
  // maps a speciesID to a list of conditions
  HashMap<Integer,LinkedList<Integer>> idCondList = null;
  
  public TwoVariablesPlot(Integer speciesID, HashMap<Integer,LinkedList<Integer>> idCondList){
    this.idCondList = idCondList;
    this.speciesID = speciesID;
  }

  
  public Integer getSpeciesID(){
    return speciesID;
  }  
  

  public HashMap<Integer,LinkedList<Integer>> getIdCondList(){
    return idCondList;
  } 
}
