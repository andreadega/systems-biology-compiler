
package sbc;

import java.util.*;


public class DoseResponsePlot {
  
  String xlabel = null;
  Integer speciesID = null;
  Double time = null;
  // maps double (dose value) to integer (id of corresponding condition).
  // Keys are sorted (so increasing order of the key value)
  // TreeMap implements SortedMap interface
  TreeMap<Double,Integer> doses = null;
  
  public DoseResponsePlot(String xlabel, Integer speciesID, Double time, TreeMap<Double,Integer> doses){
    this.xlabel = xlabel;
    this.speciesID = speciesID;
    this.time = time;
    this.doses = doses;
  }

  public String getXlabel(){
    return xlabel;
  }
  
  public Integer getSpeciesID(){
    return speciesID;
  }  
  
  public Double getTime(){
    return time;
  }

  public TreeMap<Double,Integer> getDoses(){
    return doses;
  } 
}
