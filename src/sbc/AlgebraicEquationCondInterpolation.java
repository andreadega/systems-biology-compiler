package sbc;

import java.util.*;

public class AlgebraicEquationCondInterpolation extends AlgebraicEquationCond {
  
  private int typeOfInterpolation;

  public AlgebraicEquationCondInterpolation(Integer condition, int typeOfInterpolation){
    super(condition);
    this.typeOfInterpolation = typeOfInterpolation;
  }
  
  public int getTypeOfInterpolation(){
    return typeOfInterpolation;
  }
  
}
