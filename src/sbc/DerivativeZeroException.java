package sbc.glsdc;

import java.util.*;

public class DerivativeZeroException extends Exception {
	private static final long serialVersionUID = -1113582265865921786L;
  public DerivativeZeroException() { super(); }
  public DerivativeZeroException(String message) { super(message); }
  public DerivativeZeroException(String message, Throwable cause) { super(message, cause); }
  public DerivativeZeroException(Throwable cause) { super(cause); }
}
