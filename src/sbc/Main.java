
package sbc;

import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import antlr.*;
import java.util.*;
import java.text.*;
import java.io.*;
import sbc.*;
import sbc.glsdc.*;
import javax.swing.JFileChooser; 

public class Main {
  public static void main(String[] args) throws Exception {
    
    
    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    //get current date time with Date()
    Date date = new Date();
    //System.out.println(dateFormat.format(date));

    //get current date time with Calendar()
    Calendar cal = Calendar.getInstance();
    //System.out.println(dateFormat.format(cal.getTime()));
        
    String inputFileName = null;
    BufferedWriter bw = null; 
      
    try{
      //Output log file
      File outputFile = new File("results.log");        
      if (!outputFile.exists()) {
        outputFile.createNewFile();
      }
      FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
      bw = new BufferedWriter(fw);
      
      //bw.write("[info] number of arguments: " + args.length + "\n");
      bw.write("[info] start program at: " + dateFormat.format(cal.getTime()) + "\n");
      
      if (args.length == 0){
        
        JFileChooser chooser = new JFileChooser(new File("."));
        int returnVal = chooser.showOpenDialog(null);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
          inputFileName = chooser.getSelectedFile().getAbsolutePath();
          bw.write("[info] file selected from input file dialog: "+ inputFileName +"\n");
        } else {
          bw.write("[info] input file dialog cancelled by user. Quitting now.\n");
          
          return;
          
          //Probably better to cancel execution if input file dialog is cancelled
          //instead of loading a default file
          
          //~ File f = new File("modules.txt");
          //~ if (f.exists()){
            //~ bw.write("[info] no input file specified, using \"modules.txt\".\n");
          //~ }else{
            //~ bw.write("[error] no input file specified, trying to use \"modules.txt\", but file not found!\n");
            //~ //bw.close(); //finally below takes care of this
            //~ return;
          //~ }  
          
        }
        
      } else if (args.length == 1){
        inputFileName = args[0];
        File f = new File(inputFileName);
        if (f.exists()){
          bw.write("[info] input file given by user, using \""+ inputFileName +"\".\n");
        }else{
          bw.write("[error] input file given by user, namely \""+ inputFileName +"\", but file not found!\n");
          //bw.close(); //finally below takes care of this
          return;
        }
      } else {
        
        bw.write("[error] wrong number of arguments specified! Either run with no arguments (you can choose the input file in the program) or with input file as argument.\n");
        //bw.close(); //finally below takes care of this
        return;
      }
        
        
      SBCLexer lexer = new SBCLexer(new ANTLRFileStream(inputFileName));
      SBCParser parser = new SBCParser(new CommonTokenStream(lexer));
      
      parser.removeErrorListeners(); // remove ConsoleErrorListener
      parser.addErrorListener(new OutputFileListener(bw)); // add ours
      
      //Data Structures
      EntryLists entryLists = new EntryLists();
      FittingOptions fittingOptions = new FittingOptions();
      SimulateOptions simulateOptions = new SimulateOptions();      
      LookUpTable lookUpTable = new LookUpTable();
      
      ParseTree tree = parser.code();
      
      SBCVisitorFinal visitor = new SBCVisitorFinal(entryLists,fittingOptions,simulateOptions,lookUpTable);
              
      visitor.visit(tree);
      
      //apply data normalisations
      NormalisationTool.apply(entryLists.getDataList(),entryLists.getNormList(),lookUpTable);
      
      //print data
      //~ PrintTool.printDataList(entryLists.getDataList(),lookUpTable,bw);
      //check if the logLikelihood function has been requested
      LogLikelihoodTool logLikelihoodTool = null;
      int typeOfLL = fittingOptions.getLogLikelihood();
      if (typeOfLL == LogLikelihoodTool.IndependentNoise || typeOfLL == LogLikelihoodTool.LinearModelOfNoise){ 
        logLikelihoodTool = new LogLikelihoodTool(entryLists,fittingOptions,lookUpTable);
      }
      
      //generate files for fitting
      GLSDCTool glsdc = new GLSDCTool(entryLists,fittingOptions,simulateOptions,logLikelihoodTool,lookUpTable);
      glsdc.generateFiles(bw);
      
      
      //moved to finally
      //bw.flush();
      //bw.close();
    } catch (IOException e){
      bw.write("[error] IOException!\n");
      e.printStackTrace();
    } catch (NullPointerException npe){
      bw.write("[error] NullPointerException!\n");
      npe.printStackTrace();
    } catch (Exception e){
      //bw.write("Found Exception:\n");
      bw.write(e.getMessage() + "\n");
      //e.printStackTrace();
    }finally { //this is always executed, even if a return is called
      bw.flush();
      bw.close();
    }
  }
}
