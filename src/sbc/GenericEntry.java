
package sbc;

public interface GenericEntry {
  
  /**
   * Checks whether this GenericEntry is compatible with the GenericEntry
   * entry. The purpose of this method is
   * to check compatibilities between different types of entry. For example,
   * if an identifier is used for the name of a parameter, we should check
   * that such identifier is not already the name of a variable of a
   * differential equation. It throws an Exception with a message that
   * explains why it was not compatible. If no Exception is thrown,
   * compatibility is granted.
   * */
  public void checkCompatibleWith(GenericEntry entry) throws Exception;
}
