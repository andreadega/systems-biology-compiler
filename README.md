
### Parameter Estimation Pipeline for Systems and Synthetic Biology

The Parameter Estimation Pipeline for Systems and Synthetic Biology (PEPSSBI) is a computer program for the automation of the parameter estimation work flow. It includes:

- data normalisation;
- model definition in ordinary differential equations with multi-condition experiments support;
- construction of objective functions based on data-driven normalisation of the simulations;
- generation of code compatible with High Performance Computing (HPC) infrastructures with Portable Batch System (PBS) job scheduler;
- plotting of the results with gnuplot

PEPSSBI's main application filed is the dynamic modelling of cellular signalling pathways using Ordinary Differential Equations (ODEs). Available parameter estimation algorithms are:

- GLSDC, genetic local search algorithm with distance independent diversity control *(Kimura and Konagaya, 2003)*;
- LSQNONLIN SE, Levenberg-Marquardt optimization algorithm for non linear least squares with Latin Hypercube sampling starting points. Gradient computed using sensitivity equations in CVODES (SE) *(Raue et al. 2013)*;
- LSQNONLIN FD, Levenberg-Marquardt optimization algorithm for non linear least squares with Latin Hypercube sampling starting points. Gradient computed using finite difference approximation in CVODES (FD).

Our implementation of LSQNONLIN SE and LSQNONLIN FD uses an open source version of the Levenberg-Marquardt nonlinear least squares algorithm *(Lourakis 2009)*.

A central component of PEPSSBI is the Systems Biology Compiler (SBC). A compiler is a program that converts a computer language to a lower level computer language. For example, a C compiler, converts a human readable C code into machine readable instructions. The SBC converts an input file written in the SBC/PEPSSBI input language described in the manual in the bitbucket wiki page [here](https://bitbucket.org/andreadega/systems-biology-compiler/wiki/Home) into C code and scripts that compose the PEPSSBI program. 

The compile/code generation step using SBC allows the definition of a simplified and minimal input language, which in turns allows the user to reduce its interactions with the software, thus saving time and reducing possible sources of erros. For example, the user can copy/paste the raw data from an excel file and with a single line of SBC input language instruct the compiler to normalise the biological replicates with respect to the average (normalisation by average) of their data points or with respect to a specific control data point (normalisation by fixed point). For more details about data normalisation see the dedicated sections in the manual in the bitbucket wiki page [here](https://bitbucket.org/andreadega/systems-biology-compiler/wiki/Home) or *(Degasperi et al. 2014)*. The use of SBC also allows for several errors to be identified *at compilation time*, that is before running the optimisation algorithms. Some examples are missing variables or conditions for simulation, normalisation or parameter estimation or consistency tests for the data format.

One of the most important features in PEPSSBI is the data-driven normalisation of the simulations (DNS). In order to compare the data, which are in arbitrary units, with the model simulations, which are in concentration or number of molecules, we apply to the simulations the same normalisation that was applied to the data. The alternative, would be to estimate scaling factors, thus incresing the dimensionality of the parameter search space and increasing the work necessary by the user to edit the model. In PEPSSBI, one can simply write the model, add the data and specify how to normalise the data. **By giving the data the same identifier as the corresponding model variable, the SBC is instructed to apply the same normalisation to the simulation of that variable.** For more details about DNS and the objective function definitions see *(Degasperi et al. 2017)*.

To use the program download a zipped build from the ```download``` page of the bitbucket website (https://bitbucket.org/andreadega/systems-biology-compiler/downloads).
A manual can be found in the bitbucket wiki page (https://bitbucket.org/andreadega/systems-biology-compiler/wiki/Home). 

#### References

*(Kimura and Konagaya, 2003)* Kimura, S. and A. Konagaya, **High dimensional function optimization using a new genetic local search suitable for parallel computers.** *2003 Ieee International Conference on Systems, Man and Cybernetics*, Vols 1-5, Conference Proceedings, 2003: p. 335-342.

*(Raue et al. 2013)* Raue A., et al. **Lessons Learned from Quantitative Dynamical Modeling in Systems Biology.** *PLOS ONE*, 8(9), e74335, 2013.

*(Lourakis 2009)* Lourakis, M. (2009). **Levmar: Levenberg-marquardt nonlinear least squares algorithms in C/C++(2004).** Software available at http://www.ics.forth.gr/~lourakis/levmar.

*(Degasperi et al. 2014)* A. Degasperi, M. R. Birtwistle, N. Volinsky, J. Rauch, W. Kolch, B. N. Kholodenko. **Evaluating Strategies to Normalise Biological Replicates of Western Blot Data.** *PLoS ONE*, 9(1): e87293. doi:10.1371/journal.pone.0087293, 2014.

*(Degasperi et al. 2017)* A. Degasperi, D. Fey, B. N. Kholodenko. **Performance of objective functions and optimisation procedures for parameter estimation in system biology models.** npj Systems Biology and Applications, doi:10.1038/s41540-017-0023-2, 2017.

### SBML to SBC converter

A small java program that converts SBML to the SBC input language is in development here: 

https://bitbucket.org/andreadega/sbml2sbc/

### Geany syntax highlighter

A simple SBC syntax highlighter for the Geany IDE is available here (filetypes.sbc.conf): 

https://bitbucket.org/andreadega/systems-biology-compiler/downloads

### Versions

- Version 2.2
    - This build version comes with pre-compiled PEPSSBI libraries for Linux 64 bits
    - added summaryTable.txt which is a more human readable table of the optimal parameters obtained from the parameter estimation
- Version 2.1
    - Recovery of best fits reached even if the algorithm stops unexpectedly (e.g. PBS wall time)
    - optimisation of Automated Differentiation
    - tested on OSX (using Homebrew) and Windows (using MSYS2 and MinGW-w64)
- Version 2.0
    - New parameter estimation algorithms added: LSQNONLIN SE and LSQNONLIN FD
    - New objective function added: LogLikelihood with linear model of noise
    - Reorganisation of libraries. CVODES 2.8.2 added to substitute old CVODE library, GSL added once again, Lapack and Levmar libraries also added
    - Added tracking of function evaluation values versus time and number of function evaluations. This is useful to examine convergence of algorithms and to compare their performance
    - Automated differentiation of df/dx Jacobian matrix (all ODE simulations use it now) and df/dp sensitivity right hand side (LSQNONLIN SE)
    - Normalisation by sum changed to normalisation by average
- Version 1.3
    - files reorganisation
    - initialisation of differential variable (init) now supports multiplication by a factor
- Version 1.2:
    - removed gsl (Gnu Scientific Library) from this build, gsl is now a
      system requirement
    - tested on OSX 10.8.5 (Mountain Lion)
- Version 1.1:
    - added function definitions and events
    - added dose response plots and two variables plots
    - small bug fixes and improvements
- Version 1.0:
    - SBC generates files for parameter estimation with GLSDC (genetic local search algorithm with distance independent diversity control) in the folder KimuraGLSDC
    - SBC generates files for the optimal parameter sets analysis in the folder paramterAnalysis.
    - SBC generates files for simulating and plotting in the KimuraODE folder

### Acknowledgements: 

The research leading to these results has received funding from the European Union Seventh Framework Programme (FP7/2007-2013) under grant agreement no 613879.
