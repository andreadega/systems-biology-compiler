#!/bin/bash

./clean.sh

cd src/antlr
java -jar ../../lib/antlr-4.2.2-complete.jar -no-listener -visitor -package antlr SBC.g4
cd ../..

#option -p to do nothing if directory already exists
mkdir -p classes

javac -Xlint:none -d classes -cp lib/antlr-4.2.2-complete.jar src/sbc/*.java src/antlr/*.java src/sbc/expr/*.java src/sbc/glsdc/*.java src/sbc/util/*.java 

#make the jar

cd classes
jar -xf ../lib/antlr-4.2.2-complete.jar org
cd ..
jar cfm sbc.jar Manifest.txt -C classes/ .
cd classes
rm -r org
rm -r sbc
rm -r antlr
cd ..
mv sbc.jar build/

##java -cp classes:lib/antlr-4.1-complete.jar motifGenerator.Main


