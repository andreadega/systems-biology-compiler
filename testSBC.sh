#!/bin/bash

LSQNONLINSERESULT=""
GLSDCRESULT=""

./compile.sh
mkdir testdir
cp -r build/ testdir/

./clean.sh
cd testdir/build/

./runSBC.sh ../../examples_from_manual/kinaseExample.sbc
./runParameterEstimationOnLinuxDesktop.sh LSQNONLIN_SE
wait

if [ -f "KimuraODEsim/plots/aKc_3c_2_plot.png" ]; then
  LSQNONLINSERESULT="OK"
fi

./clean.sh

./runSBC.sh ../../examples_from_manual/kinaseExample.sbc
./runParameterEstimationOnLinuxDesktop.sh GLSDC
wait

if [ -f "KimuraODEsim/plots/aKc_3c_2_plot.png" ]; then
  GLSDCRESULT="OK"
fi

./clean.sh

echo "[Test results below] ---------------------------------"

if [ "$LSQNONLINSERESULT" = 'OK' ]; then
  echo "[Test Successful] Seems that LSQNONLIN_SE (LevMar SE) is working properly because it produced plots at the end"
else
  echo "[Test Failed] Seems that LSQNONLIN_SE (LevMar SE) is NOT working properly because it did NOT produce plots at the end"
fi

if [ "$GLSDCRESULT" = 'OK' ]; then
  echo "[Test Successful] Seems that GLSDC is working properly because it produced plots at the end"
else
  echo "[Test Failed] Seems that GLSDC is NOT working properly because it did NOT produce plots at the end"
fi

echo "[Test Cleaning up]"
cd ../..
rm -r testdir
echo "[Test done.]"

