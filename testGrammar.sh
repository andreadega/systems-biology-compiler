#!/bin/bash

cd src/antlr
java -jar ../../lib/antlr-4.2.2-complete.jar -no-listener -visitor -package antlr SBC.g4
cd ../..

#option -p to do nothing if directory already exists
mkdir -p classes

javac -Xlint -d classes -cp lib/antlr-4.2.2-complete.jar src/sbc/*.java src/antlr/*.java src/sbc/expr/*.java src/sbc/glsdc/*.java src/sbc/util/*.java 

java -cp classes:lib/antlr-4.2.2-complete.jar org.antlr.v4.runtime.misc.TestRig antlr.SBC code -gui test.sbc 
