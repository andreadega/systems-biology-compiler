#!/bin/bash

BUILDLIBRARIES=false

if [ ! -f "common/lib/libsundials_cvodes.a" ]; then
  BUILDLIBRARIES=true
fi

if [ ! -f "common/lib/libga.a" ]; then
  BUILDLIBRARIES=true
fi

if [ ! -f "common/lib/libgsl.a" ]; then
  BUILDLIBRARIES=true
fi

if [ ! -f "common/lib/liblapack.a" ]; then  
  BUILDLIBRARIES=true
fi

if [ ! -f "common/lib/liblevmar.a" ]; then
  BUILDLIBRARIES=true
fi


if [ "$BUILDLIBRARIES" = true ]; then
  echo "###############################################################"
  echo "Third party libraries need to be compiled. This may take a few"
  echo "minutes but you have to do it only once."
  echo " "
  echo "Press any key to continue"
  echo " "
  read -n 1 -s
  #unpack src
  echo " "
  echo "Unpacking source code..."
  echo " "  
  cd common/
  tar -xzf src.tar.gz
  cd ..
else
  echo "Third party libraries already compiled and ready."
fi

if [ ! -f "common/lib/libsundials_cvodes.a" ]; then
  echo "###############################################################"
  echo "### Building Sundials CVODES 2.8.2                          ###"
  echo "### Ordinary Differential Equations solver                  ###"
  echo "###############################################################"
  cd common/src/cvodes-2.8.2
  ./confmake.sh
  cd ../../..
  echo "###############################################################"
  echo "### Done building CVODES                                    ###"
  echo "###############################################################"
fi

if [ ! -f "common/lib/libga.a" ]; then
  echo "###############################################################"
  echo "### Building libga                                          ###"
  echo "### Genetic Algorithm library required by GLSDC             ###"
  echo "###############################################################"
  cd common/src/ga
  make
  make clean
  cd ../../..
  echo "###############################################################"
  echo "### Done building libga                                     ###"
  echo "###############################################################"
fi

if [ ! -f "common/lib/libgsl.a" ]; then
  echo "###############################################################"
  echo "### Building libgsl                                         ###"
  echo "### GNU Scientific Library                                  ###"
  echo "###############################################################"
  cd common/src/gsl-1.16
  ./confmake.sh
  cd ../../..
  echo "###############################################################"
  echo "### Done building libgsl                                    ###"
  echo "###############################################################"
fi

if [ ! -f "common/lib/liblapack.a" ]; then
  echo "###############################################################"
  echo "### Building liblapack                                      ###"
  echo "### Linear Algebra Package required by lsqnonlin (levmar)   ###"
  echo "###############################################################"
  cd common/src/lapack-3.6.0
  ./confmake.sh
  cd ../../..
  echo "###############################################################"
  echo "### Done building lapack                                    ###"
  echo "###############################################################"
fi

if [ ! -f "common/lib/liblevmar.a" ]; then
  echo "###############################################################"
  echo "### Building liblevmar                                      ###"
  echo "### Levenberg-Marquardt non-linear least squares algorithm  ###"
  echo "###############################################################"
  cd common/src/levmar-2.6
  ./confmake.sh
  cd ../../..
  echo "###############################################################"
  echo "### Done building liblevmar                                 ###"
  echo "###############################################################"
fi

SUCCESSFUL=true

if [ ! -f "common/lib/libsundials_cvodes.a" ]; then
  SUCCESSFUL=false
  MISSINGLIB="${MISSINGLIB}\ncommon/lib/libsundials_cvodes.a"
fi

if [ ! -f "common/lib/libga.a" ]; then
  SUCCESSFUL=false
  MISSINGLIB="${MISSINGLIB}\ncommon/lib/libga.a"
fi

if [ ! -f "common/lib/libgsl.a" ]; then
  SUCCESSFUL=false
  MISSINGLIB="${MISSINGLIB}\ncommon/lib/libgsl.a"
fi

if [ ! -f "common/lib/liblapack.a" ]; then  
  SUCCESSFUL=false
  MISSINGLIB="${MISSINGLIB}\ncommon/lib/liblapack.a"
fi

if [ ! -f "common/lib/liblevmar.a" ]; then
  SUCCESSFUL=false
  MISSINGLIB="${MISSINGLIB}\ncommon/lib/liblevmar.a"
fi

if [ "$BUILDLIBRARIES" = true ] && [ "$SUCCESSFUL" = true ]; then
  cd common
  rm -r src
  cd ..
  echo "###############################################################"
  echo "Done building third party libraries"
  echo "Compiled libraries should be in common/lib/"
  echo "To rebuild the libraries, run:" 
  echo "  ./cleanLibraries.sh"
  echo "which removes the libraries, followed by:"
  echo "  ./buildLibraries.sh"
  echo "which compiles the libraries again."
  echo "###############################################################"
fi

if [ "$SUCCESSFUL" = false ]; then
  echo "###############################################################"
  echo "Error: not all libraries were compiled correcly"
  echo "Missing libraries that should be in common/lib/"
  echo -e "$MISSINGLIB\n" 
  echo "###############################################################"
  exit 1
fi
