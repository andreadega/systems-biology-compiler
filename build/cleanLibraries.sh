#!/bin/bash

cd common/lib
rm -f libgsl*
rm -f libga*
rm -f libsundials*
rm -rf pkgconfig
rm -f librefblas.a
rm -f liblapack.a
rm -f liblevmar.a
cd ..
cd include
rm -rf gsl
rm -rf cvodes
rm -rf nvector
rm -rf sundials
cd ..
