#ifndef __UNDX__
#define __UNDX__

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include "xover.h"
#include "rand.h"

class UNDX : public XOVER
{
  public:
    UNDX(Rand*,double,double);
    ~UNDX();
    int SetParent(INDIVIDUAL*);
    int GetChild(INDIVIDUAL*);

  private:
    double Alpha,Beta;
    double *vec_d,*vec_e,*vec_0;
    double vecdlen,D,maxdcmp;
    int Dimension;
};

#endif
