#ifndef __INDIVIDUAL__
#define __INDIVIDUAL__

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>

#define _INDIVIDUAL_FEASIBLE_   0
#define _INDIVIDUAL_INFEASIBLE_ 1

class INDIVIDUAL
{
  public:
#ifndef GCC_COMPILER_
    void copy_(const INDIVIDUAL&);
#endif /* GCC_COMPILER_ */

    INDIVIDUAL();
    INDIVIDUAL(int,int);
    INDIVIDUAL(const INDIVIDUAL&);
    ~INDIVIDUAL();
    int ResetIndividual(int,int);
    int GetDimension();
    int GetFitnessDimension();
    double GetFitness(int);
    double SetFitness(int,double);
    double GetData(int);
    double SetData(int,double);
    double AddData(int,double);
    int GetInfeasible();
    int SetInfeasible(int);
    
    INDIVIDUAL& operator = (const INDIVIDUAL& src);
    double operator [] (int index) const;
    double& operator [] (int index);
    int operator < (const INDIVIDUAL& src);
    int operator <= (const INDIVIDUAL& src);
    int operator == (const INDIVIDUAL& src);
    
    double *Data;
    double *FitnessData;
    
  protected:
    int Dimension;
    int FitnessDimension;
    int InfeasibleFlag;
    
    void error();
};

#endif
