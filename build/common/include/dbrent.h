//************************Brent's Method
#ifndef __dBRENT__
#define __dBRENT__

#include "nrutil.h"
#include "individual.h"
#include "linemin.h"

class dBRENT : public LINEMIN
{
  public:
    dBRENT(void (*)(INDIVIDUAL*),void (*)(INDIVIDUAL*,double*));
    ~dBRENT();
    int Search(INDIVIDUAL*,INDIVIDUAL*,double);
    
  protected:
    int diff(double,double*);
    void error();

    void (*Difference)(INDIVIDUAL*,double*);
    int Dimension;
    double *workvector;
};

#endif

