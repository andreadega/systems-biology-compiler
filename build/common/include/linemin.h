//************************Line Search Algorithm
#ifndef __LINEMIN__
#define __LINEMIN__

#include "nrutil.h"
#include "individual.h"

class LINEMIN
{
  public:
#ifndef GCC_COMPILER_
    LINEMIN() { }
    void copy_(const LINEMIN&);
#endif /* GCC_COMPILER_ */

    LINEMIN(void (*)(INDIVIDUAL*));
    ~LINEMIN();
    int SetMaxNumOfEval(int);
    virtual int Search(INDIVIDUAL*,INDIVIDUAL*,double);
    
  protected:
    void initfunc(INDIVIDUAL*,INDIVIDUAL*,double);
    void func(double,double*,int*);
    void error();
    
    int MaxNumOfEval;
    void (*FitnessFunction)(INDIVIDUAL*);
    INDIVIDUAL *StartPoint,*Direction,*NewPoint;
    double Alpha;
};

#endif

