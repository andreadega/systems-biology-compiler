//**************************Simplex Method for Linear Programming
#ifndef __LPSIMPLEX__
#define __LPSIMPLEX__

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include "vector.h"

class LPSIMPLEX
{
  public:
    LPSIMPLEX(VECTOR*,int,int,int);
    ~LPSIMPLEX();
    int DoIt(double*);
    
    double **Tableau;
    VECTOR *ResultVector;
    
  protected:
    int Dimension,M1,M2,M3,M;
    
    void error();

  private:
    int *l1,*l2,*l3;
    int *izrov,*iposv;
    
    void simplx(double**,int,int,int,int,int,int*);
    void simp1(double**,int,int[],int,int,int*,double*);
    void simp2(double**,int,int[],int,int*,int,double*);
    void simp3(double**,int,int,int,int);
};

#endif

