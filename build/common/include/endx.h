#ifndef __ENDX__
#define __ENDX__

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include "xover.h"
#include "rand.h"

class ENDX : public XOVER
{
  public:
    ENDX(Rand*,double,double,int);
    ~ENDX();
    int SetParentNumber(int);
    int SetParent(INDIVIDUAL*);
    int GetChild(INDIVIDUAL*);

    double Alpha,Beta;

  protected:
    //double Alpha,Beta;
    double *vec_d,*vec_0,**vec_p;
    int Dimension;
};

#endif
