#ifndef __BUFFER__
#define __BUFFER__

#define list_eltype  double

struct List_Node
{
  list_eltype data;

  List_Node *before;
  List_Node *next;
};

class List
{
  public:
    List();
    ~List();
    int GetTotalNumber();
    List_Node *construct_Node();
    void delete_Node(List_Node*);
    void goto_Top(List_Node*);
    void goto_Bottom(List_Node*);
    List_Node *search(int);
    void deleteAll();
    void error();

  private:
    List_Node *Top,*Bottom;
    int number;
};


#define default_buffer_size 100
#define buffer_el_type double

class Buffer
{
  public:
    Buffer();
    Buffer(int);
    ~Buffer();
    void init();
    int counter();
    int put(buffer_el_type);
    int stack_get(buffer_el_type*);
    int queue_get(buffer_el_type*);

protected:
    void error();

  private:
    buffer_el_type *data;
    int buffer_size;
    int In,Out,Count;
};

#endif
