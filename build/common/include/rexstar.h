#ifndef __REXstar__
#define __REXstar__

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include "xover.h"
#include "rand.h"

class REXstar : public XOVER
{
  public:
    REXstar(Rand*,double,double,void (*)(INDIVIDUAL*),int);
    ~REXstar();
    int SetParentNumber(int);
    int SetParent(INDIVIDUAL*);
    int GetChild(INDIVIDUAL*);

  protected:
    double StepSize,URange;
    double *vec_xg,*vec_xb;
    INDIVIDUAL *Parents;
    int Dimension,Counter;
    int *Ranking;

    void (*FitnessFunction)(INDIVIDUAL*);
};

#endif
