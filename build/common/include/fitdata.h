#ifndef __FITDATA__
#define __FITDATA__

class fitData
{
  public:
    double Fitness;
    int Infeasible;
    
    int operator > (const fitData&);
    int operator < (const fitData&);
    int operator >= (const fitData&);
    int operator <= (const fitData&);
    int operator == (const fitData&);
    fitData& operator = (const fitData&);
};

#endif
