#ifndef __ALTERNATION__
#define __ALTERNATION__

#include "rand.h"
#include "individual.h"
#include "xover.h"
#include "setting.h"

class ALTERNATION
{
  public:
    ALTERNATION(XOVER*,Rand*,void(*)(INDIVIDUAL*),int,INDIVIDUAL*);
    ~ALTERNATION();
    int SetProblemSetting(PROBLEMSETTING*);
    virtual int SetChildNumber(int);
    virtual int SelectReproduction();
    virtual int DoXover();
    virtual int SelectSurvival();
    virtual int Alternation();
    
    int *SelectedParents;
    int *SelectedChildren;
    INDIVIDUAL *Children,*Population;
    
  protected:
    void error();
    
    XOVER *xover;
    Rand *ran;
    void (*FitnessFunction)(INDIVIDUAL*);
    int ChildSize,PopSize,ParentsNumber;
    PROBLEMSETTING *ProblemSetting;
};

#endif
