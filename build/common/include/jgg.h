//***********************************JGG
#ifndef __JGG__
#define __JGG__

#include "alter.h"

class JGG : public ALTERNATION
{
  public:
    JGG(XOVER*,Rand*,void(*)(INDIVIDUAL*),int,INDIVIDUAL*);
    ~JGG();
    int SetChildNumber(int);
    int SelectReproduction();
    int SelectSurvival();
    int Alternation();

  protected:
    int *ranking;
};


#endif

