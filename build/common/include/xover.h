#ifndef __XOVER__
#define __XOVER__

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include "individual.h"
#include "rand.h"

class XOVER
{
  public:
    XOVER(int,Rand*);
    ~XOVER();
    virtual int GetParentNumber();
    virtual int SetParentNumber(int);
    virtual int SetParent(INDIVIDUAL*);
    virtual int GetChild(INDIVIDUAL*);
    
    virtual double GetData();
    
  protected:
    void error();

    int ParentNumber;
    double Data;
    Rand *ran;
};

#endif
