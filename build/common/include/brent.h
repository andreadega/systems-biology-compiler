//************************Brent's Method
#ifndef __BRENT__
#define __BRENT__

#include "nrutil.h"
#include "individual.h"
#include "linemin.h"

class BRENT : public LINEMIN
{
  public:
#ifndef GCC_COMPILER_
    BRENT() { }
#endif /* GCC_COMPILER_ */
    
    BRENT(void (*)(INDIVIDUAL*));
    int Search(INDIVIDUAL*,INDIVIDUAL*,double);
    
  protected:
    void error();
};

#endif

