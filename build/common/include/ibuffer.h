#ifndef __iBUFFER__
#define __iBUFFER__

#define ilist_eltype  int

struct iList_Node
{
  ilist_eltype data;

  iList_Node *before;
  iList_Node *next;
};

class iList
{
  public:
    iList();
    ~iList();
    iList_Node *construct_Node();
    void delete_Node(iList_Node*);
    void goto_Top(iList_Node*);
    void goto_Bottom(iList_Node*);
    iList_Node *search(int);
    void deleteAll();
    int GetNumOfNode();
    void error();

  private:
    iList_Node *Top,*Bottom;
    int number;
};


#define default_buffer_size 100
#define ibuffer_el_type int

class iBuffer
{
  public:
    iBuffer();
    iBuffer(int);
    ~iBuffer();
    void init();
    int counter();
    int put(ibuffer_el_type);
    int stack_get(ibuffer_el_type*);
    int queue_get(ibuffer_el_type*);

protected:
    void error();

  private:
    ibuffer_el_type *data;
    int buffer_size;
    int In,Out,Count;
};

#endif
