//*************************Conjugate Gradient Method
#ifndef __CGM__
#define __CGM__

#include <math.h>
#include "setting.h"
#include "linemin.h"
#include "individual.h"

class CGM
{
  public:
    CGM(void(*)(INDIVIDUAL*),void(*)(INDIVIDUAL*,double*),INDIVIDUAL*,LINEMIN*);
    ~CGM();
    int SetProblemSetting(PROBLEMSETTING*);
    int SetMaxEval(int);
    int SetMaxIteration(int);
    int DoIt();
    
    double *DiffValue;
    
  protected:
    void error();
    
    int Dimension,MaxNumOfEval,MaxIterationNum;
    INDIVIDUAL *Individual,*G,*H,*XiT;
    double FTOL;
    PROBLEMSETTING *ProblemSetting;
    LINEMIN *LineMin;
    void (*FitnessFunction)(INDIVIDUAL*);
    void (*Difference)(INDIVIDUAL*,double*);
};

#endif
