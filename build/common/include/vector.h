#ifndef __VECTOR__
#define __VECTOR__

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>

class VECTOR
{
  public:
#ifndef GCC_COMPILER_
    void copy_(const VECTOR&);
#endif /* GCC_COMPILER_ */

    VECTOR();
    VECTOR(int);
    VECTOR(const VECTOR&);
    ~VECTOR();
    int ReSize(int);
    int GetDimension();
    double& operator [] (int);
    double operator * (const VECTOR&);
    VECTOR operator + (const VECTOR&);
    VECTOR operator - (const VECTOR&);
    VECTOR& operator += (const VECTOR&);
    VECTOR& operator -= (const VECTOR&);
    VECTOR& operator = (const VECTOR&);
    friend VECTOR operator * (const VECTOR&,double);
    friend VECTOR operator * (double,const VECTOR&);

    double *Data;
    
  protected:
    int Dimension;
    int arraysize;
    void error();
};

#endif

