#ifndef __UNDXn__
#define __UNDXn__

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include "xover.h"
#include "rand.h"

class UNDXn : public XOVER
{
  public:
    UNDXn(Rand*,double,int);
    ~UNDXn();
    int SetParent(INDIVIDUAL*);
    int GetChild(INDIVIDUAL*);

  private:
    double Alpha;
    double *vec_p;
    int Dimension;
    INDIVIDUAL *Parent;
};

#endif
