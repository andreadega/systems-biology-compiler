//************************************MGG(best+roulette)
#ifndef __MGG_Best_Roulette__
#define __MGG_Best_Roulette__

#include "alter.h"

class MGGBROU : public ALTERNATION
{
  public:
    MGGBROU(XOVER*,Rand*,void(*)(INDIVIDUAL*),int,INDIVIDUAL*);
    ~MGGBROU();
    int SetChildNumber(int);
    int SelectReproduction();
    int SelectSurvival();
    int Alternation();

  protected:
    int *ranking;
    
    void swap(int&,int&);
};


#endif

