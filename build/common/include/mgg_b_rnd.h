//***********************************MGG(best+random)
#ifndef __MGG_Best_Rnd__
#define __MGG_Best_Rnd__

#include "alter.h"

class MGGBRND : public ALTERNATION
{
  public:
    MGGBRND(XOVER*,Rand*,void(*)(INDIVIDUAL*),int,INDIVIDUAL*);
    ~MGGBRND();
    int SetChildNumber(int);
    int SetParents(int,int);
    int SelectReproduction();
    int SelectSurvival();
    int Alternation();

  protected:
    int *ranking;
    
    void swap(int&,int&);
};


#endif

