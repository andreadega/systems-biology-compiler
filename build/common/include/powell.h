//*************************Powell's Method
#ifndef __POWELL__
#define __POWELL__

#include <math.h>
#include "nrutil.h"
#include "setting.h"
#include "linemin.h"
#include "brent.h"
#include "individual.h"

class POWELL
{
  public:
    POWELL(void(*)(INDIVIDUAL*),INDIVIDUAL*,LINEMIN*);
    ~POWELL();
    int SetProblemSetting(PROBLEMSETTING*);
    int SetMaxEval(int);
    int SetMaxIteration(int);
    int DoIt();
    
    double **Xi;
    
  protected:
    void error();
    
    int Dimension,MaxNumOfEval,MaxIterationNum;
    INDIVIDUAL *Individual,*PT,*PTT,*XiT;
    double FTOL;
    PROBLEMSETTING *ProblemSetting;
    LINEMIN *LineMin;
    void (*FitnessFunction)(INDIVIDUAL*);
};

#endif
