#ifndef __RAND__
#define __RAND__

/* Period parameters */  
#define N_RAND 624
#define M_RAND 397
#define MATRIX_A 0x9908b0df   /* constant vector a */
#define UPPER_MASK 0x80000000 /* most significant w-r bits */
#define LOWER_MASK 0x7fffffff /* least significant r bits */

/* Tempering parameters */   
#define TEMPERING_MASK_B 0x9d2c5680
#define TEMPERING_MASK_C 0xefc60000
#define TEMPERING_SHIFT_U(y)  (y >> 11)
#define TEMPERING_SHIFT_S(y)  (y << 7)
#define TEMPERING_SHIFT_T(y)  (y << 15)
#define TEMPERING_SHIFT_L(y)  (y >> 18)

class Rand
{
 public:
  Rand();
  //void initrand(int);
  void initrand(unsigned long);

  int randi(int);
  int erandi(int,double);
  int frandi(int);
  double randd();
  double Nrandd();
  double NranddBM();
  double gamma_rnd1(int); 
  double gamma_rnd(double);

 private:
  unsigned long mt[N_RAND]; /* the array for the state vector  */
  int mti;//=N_RAND+1; /* mti==N_RAND+1 means mt[N_RAND] is not initialized */

  unsigned long rand0();
  //int rand0();
  //int irand;
};

#endif
