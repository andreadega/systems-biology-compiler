#ifndef __SETTING__
#define __SETTING__

#include "individual.h"

#define _NORANGE_SEARCH_ 0
#define _RANGE_SEARCH_   1
#define _TORUS_SEARCH_   2

class PROBLEMSETTING
{
  public:
    PROBLEMSETTING(int);
    PROBLEMSETTING();
    ~PROBLEMSETTING();
    void SetProblemSize(int);
    int GetProblemSize();
    int GetSearchOption();
    int AlternationConversion(INDIVIDUAL*);
    int FeasibleCheck(INDIVIDUAL*);
    double *SearchArea[2];
    double *StartSearchArea[2];
    int SearchOption;

  private:
    int Dimension;
    void init();
    void mallocall();
    void deleteall();
    double toregular(double,double,double);
    void error();
};

#endif

