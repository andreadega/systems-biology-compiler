#ifndef __SPX__
#define __SPX__

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include "xover.h"
#include "rand.h"

class SPX : public XOVER
{
  public:
    SPX(Rand*,double,int);
    ~SPX();
    int SetParentNumber(int);
    int SetParent(INDIVIDUAL*);
    int GetChild(INDIVIDUAL*);

  private:
    INDIVIDUAL *Parent;
    double Alpha;
    double **data;    
    int Dimension;
};

#endif
