//**************************MATRIX OPERATION
#ifndef __MATRIX__
#define __MATRIX__

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include "vector.h"

class MATRIX
{
  public:
#ifndef GCC_COMPILER_
    void copy_(const MATRIX&);
#endif /* GCC_COMPILER_ */

    MATRIX();
    MATRIX(int,int);
    ~MATRIX();
    int ReSize(int,int);
    int GetRow();
    int GetColumn();
    int CHOLESKIdcmp(MATRIX*);
    int LUdcmp(int*,double*);
    int LUbksb(int*,VECTOR*);
    int SVdcmp(VECTOR*,MATRIX*);
    int SVbksb(VECTOR*,MATRIX*,VECTOR*);

    double **Element;
    
  protected:
    void error();
    double pythag(double,double);
    
    int NumOfColumn,NumOfRow;
    int arrayc,arrayr;
    VECTOR *workvector;
};

#endif

