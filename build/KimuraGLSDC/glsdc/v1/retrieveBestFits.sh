#!/bin/bash

BESTFITS=$1

if [ "$BESTFITS" = '' ]; then
  let BESTFITS=50
fi

cd work/sbcmodel_all/1/

#remove if it is already there
rm -f 000_bestValues.txt

#extract list "ranSeed best"
START=1
END=${BESTFITS}
for ((i=START; i<=END; i++))
do
  if [ -f "$i.best" ]; then
    #check not empty
    LINES=`wc -l $i.best | awk {'print $1'}`
    if [ "$LINES" -gt 0 ]; then
      head -n 1 $i.best >> 000_bestValues.txt
    fi
  fi
done

g++ -o extractBestFits extractBestFits.cpp
chmod 755 extractBestFits
#grep "infeasible= 0" 000_bestValues.txt | cut -d' ' -f1,9 > tmp2
FITSFOUND=`wc -l 000_bestValues.txt | awk {'print $1'}`
./extractBestFits ${FITSFOUND} 000_bestValues.txt
#construct the summary table
echo -n "objFun rSeed " > summaryTable.txt
cat ../../../../../../listOfUnknownParameters.txt >> summaryTable.txt
paste -d' ' bestsAndSeeds.txt bestFits.txt >> summaryTable.txt
#Copy to main directory
cp bestFits.txt ../../../../../../
cp summaryTable.txt ../../../../../../
echo "Found ${FITSFOUND} best fits out of ${BESTFITS} attempts"
