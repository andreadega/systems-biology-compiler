#! /bin/bash

#PBS -l nodes=1:ppn=1
#PBS -l walltime=2:00:00
#PBS -N sbcfitting
#PBS -m bea
#PBS -M andrea.degasperi@ucd.ie

cd /home/people/adegaspe/KimuraGLSDC/glsdc/v1
./test.sh
