#!/bin/bash

X=$1
cd work/sbcmodel_all/1/


for i in `seq 1 $3`;
do
  ./prepareSettings.sh $X $2
  sh bat $2 < /dev/null > $X.res 2> $X.log
  ./appendResult.sh $X
  COUNTING=`wc -l 000_bestValues.txt | awk {'print $1'}`
  echo ${COUNTING} > tmp
  echo done with ${COUNTING} optimisations
  let X=X+1
done

