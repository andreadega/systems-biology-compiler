#!/bin/bash

BESTFITS=$1

if [ "$BESTFITS" = '' ]; then
  let BESTFITS=50
fi

chmod 755 retrieveBestFits.sh
chmod 755 retrieveNEvalVsBest.sh
./retrieveBestFits.sh ${BESTFITS}
./retrieveNEvalVsBest.sh ${BESTFITS}

cd ../../..

if [ -f "nEvalVsBest/nEvalVsBest.txt" ]; then
  cd nEvalVsBest
  ./plotNEvalVsBest.sh
  cd ..
fi

if [ ! -f "bestFits.txt" ]; then
  echo "bestFits.txt not found!"
  exit
fi

./runParameterAnalysis.sh bestFits.txt

if [ -f "runSimulations.sh" ]; then
  ./runSimulations.sh bestFits.txt
fi
