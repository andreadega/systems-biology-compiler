//**************************Read Parameters
#ifndef __ReadParams__
#define __ReadParams__

#include <stdio.h>
#include <string.h>
#include "setting.h"
#include "localsetting_glsdc.h"

int ReadGASetting(FILE*,LOCALSETTING*);
int WriteGASetting(LOCALSETTING*);

#endif
