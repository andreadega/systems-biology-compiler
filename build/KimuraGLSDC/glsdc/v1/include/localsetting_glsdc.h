#ifndef __LOCALSETTING__
#define __LOCALSETTING__

struct LOCALSETTING {
  int Dimension;
  int MaxGeneration;
  int MaxNumOfEval;
  int PopSize;
  int NumOfParents;
  int NumOfChildren;
  double PopSizeRatio;
  int Step_normal;
  int Step_backup;
  int Step_debug;
  int RndSeed;
  int search_option;
  char IndivFileName[1024];
  char NormalOut[1024];
  char BackupOut[1024];
  char DebugOut[1024];

  int MaxIterNum_LineMin;
  int MaxIterNum_Powell;
  int MaxIterNumPrecision_LineMin;
  int MaxIterNumPrecision_Powell;
  double ConvergingRatio;
};

struct RANGE {
  double Upper;
  double Lower;
};

struct COUNTER {
  int Generation;
  int NumOfEvalUpper;
  int NumOfEvalLower;
};

#endif
