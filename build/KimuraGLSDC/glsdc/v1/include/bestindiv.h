#ifndef __BESTINDIV__
#define __BESTINDIV__

#include "individual.h"

class BESTINDIV
{
  public:
    BESTINDIV();
    BESTINDIV(int,double);
    ~BESTINDIV();
    int ChangeStackSize(int);
    double ChangeDistinguishLength(double);
    int NumOfData();
    int SearchAll(INDIVIDUAL*,int);
    int RenewStack(INDIVIDUAL*);
    int RenewBest(INDIVIDUAL*);
    INDIVIDUAL *GetIndividual(int);
    INDIVIDUAL *Best;
    
  protected:
    int InsertIndiv(int,INDIVIDUAL*);
    int DeleteIndiv(int);
    double length(INDIVIDUAL*,INDIVIDUAL*);
    void error();
    INDIVIDUAL *StackIndiv;
    int *Stack;
    int NumOfStack,MaxNumOfStack;
    double DistinguishLength;
};

#endif
