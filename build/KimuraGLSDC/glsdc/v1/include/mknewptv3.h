//***********************************MKNEWPT
#ifndef __MKNEWPT__
#define __MKNEWPT__

#include "alter.h"

class MKNEWPT : public ALTERNATION
{
  public:
    MKNEWPT(XOVER*,Rand*,void(*)(INDIVIDUAL*),int,INDIVIDUAL*);
    ~MKNEWPT();
    int SetChildNumber(int);
    int SetParents(int,int);
    int SelectReproduction();
    int SelectReproduction(int);
    int DoXover();
    int SelectSurvival();
    int Alternation();

    int Init();

  protected:
    int *ranking;
    int *workvector_mknewpt;
    void swap(int&,int&);
};


#endif

