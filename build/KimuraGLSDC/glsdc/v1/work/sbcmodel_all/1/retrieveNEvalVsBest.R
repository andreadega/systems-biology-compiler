#!/usr/bin/Rscript

args <- commandArgs(TRUE)
nfits <- as.numeric(args[1])
if (is.na(nfits)) nfits <- 96

#load tables from i.res and take care if i.res does not exist

maxtime <- 0
maxnevals <- 0
availableTables <- 0

#first, check how many available tables we have
for (i in 1:nfits){
  currentfile <- paste(i,".res",sep='')
  if (file.exists(currentfile)){
    x <- try(read.table(currentfile,sep=","))
    if(inherits(x, "try-error")){
      
    } else {
      #we have the file and it is not empty
      availableTables <- availableTables + 1
    }
  }
}

#vector of tables
vectorOfTables <- vector(mode = "list", length = availableTables)

currentTable <- 0
llIsOn <- FALSE #loglikelihood is on?

for (i in 1:nfits){
  currentfile <- paste(i,".res",sep='')
  if (file.exists(currentfile)){
    x <- try(read.table(currentfile,sep=","))
    if(inherits(x, "try-error")){
      
    } else {
      #we have the file and it is not empty
      currentTable <- currentTable + 1
      #check if the table is of width 6 (least squares only)
      #                            or 8 (log likelihood and least squares)
      if (dim(x)[2]==6) {
        #get only interesting columns: neval, best, time
        y <- x[c(2,4,6)]
      } else if (dim(x)[2]==8) {
        #get only interesting columns: neval, best, time, best2
        y <- x[c(2,4,6,8)]
        llIsOn <- TRUE
      }
      #change time to relative to initial time
      y[3] <- y[3] - y[1,3]
      vectorOfTables[[currentTable]] <- y
      #save some info for later
      if (maxtime < tail(y[3],n = 1)[[1]]){
        maxtime <- tail(y[3],n = 1)[[1]]
      }
      if (maxnevals < tail(y[1],n = 1)[[1]]){
        maxnevals <- tail(y[1],n = 1)[[1]]
      }
    }
  }
}

#now build and populate the new tables nEvalVsBest and timeVsBest
nEvalVsBestMatrix <- matrix(,nrow=(maxnevals/10),ncol=(availableTables+1)) #remember one row every 10 fevals
timeVsBestMatrix <- matrix(,nrow=(maxtime+1),ncol=(availableTables+1)) #remember time 0
if (llIsOn){
  nEvalVsBestMatrix2 <- matrix(,nrow=(maxnevals/10),ncol=(availableTables+1)) #remember one row every 10 fevals
  timeVsBestMatrix2 <- matrix(,nrow=(maxtime+1),ncol=(availableTables+1)) #remember time 0
}

for (i in 1:(maxnevals/10)){
  nEvalVsBestMatrix[i,1] <- i*10
  if (llIsOn) nEvalVsBestMatrix2[i,1] <- i*10
}

for (i in 1:(maxtime+1)){
  timeVsBestMatrix[i,1] <- (i-1)
  if (llIsOn) timeVsBestMatrix2[i,1] <- (i-1)
}

#retrieve feval vs best
for (i in 1:availableTables){
  tableLength <- dim(vectorOfTables[[i]])[1]
  for (j in 1:tableLength){
    nEvalVsBestMatrix[j,i+1] <- vectorOfTables[[i]][j,2]
    if (llIsOn) nEvalVsBestMatrix2[j,i+1] <- vectorOfTables[[i]][j,4]
  }
}

#reconstruct time vs best
for (j in 1:availableTables){
  time_table_n <- vectorOfTables[[j]][3]
  tableLength <- dim(vectorOfTables[[j]])[1]
  tableMaxTime <- tail(vectorOfTables[[j]][3],n = 1)[[1]]
  i <- 1
  p <- 1
  while (i<=(tableMaxTime+1)){
    if (time_table_n[p,1]<=timeVsBestMatrix[i,1]){
      timeVsBestMatrix[i,j+1] <- vectorOfTables[[j]][p,2]
      if (llIsOn) timeVsBestMatrix2[i,j+1] <- vectorOfTables[[j]][p,4]
    }else{
      
    }
    i <- i+1;
    if (i<=(tableMaxTime+1)){
      while (time_table_n[p,1]<=timeVsBestMatrix[i,1] && p<=tableLength){
        p <- p+1;
      }
      p <- p-1;
    }
  }
}

write.table(nEvalVsBestMatrix,"nEvalVsBest.txt",col.names = FALSE,row.names = FALSE,quote = FALSE)
write.table(timeVsBestMatrix,"timeVsBest.txt",col.names = FALSE,row.names = FALSE,quote = FALSE)
if (llIsOn){
  write.table(nEvalVsBestMatrix2,"nEvalVsBestLS.txt",col.names = FALSE,row.names = FALSE,quote = FALSE)
  write.table(timeVsBestMatrix2,"timeVsBestLS.txt",col.names = FALSE,row.names = FALSE,quote = FALSE)
}
