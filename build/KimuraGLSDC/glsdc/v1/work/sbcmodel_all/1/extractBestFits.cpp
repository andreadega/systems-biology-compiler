//Andrea Degasperi, Systems Biology Ireland, 10/07/2012
//Program to identify first N best fits
//The program reads a list of random seeds and associated fit values
//where the seeds indicate the files where the identified parameters are
//The program returns the list of N seeds with the N best fits

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string>
#include <fstream>
#include <sstream>

//use with ./extractBestFits N filename
//where N is the number of best fits to return and filename is a file 
//with two columns: seed fit


struct linkedList{
  int seed;
  double fit;
  linkedList* next;
};

//add elements to the list at increasing order w.r.t. fit, and remove
//last element if max is reached
void linkedListAdd(linkedList **plist,int seed,double fit,int max);

//deletes the linked list
void deleteLinkedList(linkedList **plist);

//prints the linked list
void printLinkedList(linkedList **plist);

//extracts parameter sets from files
void extractParameters(linkedList **plist, double**matrixOfParameters, int &numberOfParameters);

//print the sets of parameters
void printParametersGnuplotTransposed(double**matrixOfParameters, int numberOfFits, int numberOfParameters);

//delete the matrix of parameters
void deleteMatrixOfParameters(double**matrixOfParameters, int numberOfFits);

//print the sets of parameters for Gnuplot visualisation
void printParametersGnuplot(double**matrixOfParameters, int numberOfFits, int numberOfParameters);

//compute average of parameters
double* computeAverageOfParameters(double**matrixOfParameters, int numberOfFits, int numberOfParameters);

//compute sample standard deviation of parameters
double* computeStdOfParameters(double**matrixOfParameters, double *average, int numberOfFits, int numberOfParameters);

//print parameter averages and std to be used in Gnuplot
void printParametersAveStdGnuplot(double*average, double*std, int numberOfFits, int numberOfParameters);


int main(int argc,char **argv){
  
  /***** Initialisation *****/
  int numberOfFits = atoi(argv[1]);  
  std::ifstream infile;
  linkedList *listOfBestFits = NULL;
  double **matrixOfParameters = new double*[numberOfFits];
  int numberOfParameters;

  /***** Get the seeds of the best N fits in order *****/  
  int tmpSeed;
  std::string tmpFit;

  infile.open(argv[2]);
  while(infile >> tmpSeed >> tmpFit){
    if (tmpFit != "nan"){
      linkedListAdd(&listOfBestFits,tmpSeed,strtod(tmpFit.c_str(),NULL),numberOfFits);
    }
  }
  infile.close();
  
  /***** Get the actual parameters of the N best fits *****/
  extractParameters(&listOfBestFits,matrixOfParameters,numberOfParameters);
  
  /***** Compute average and standard deviation *****/
  double * sampleAverage = computeAverageOfParameters(matrixOfParameters,numberOfFits,numberOfParameters);
  double * sampleStd = computeStdOfParameters(matrixOfParameters,sampleAverage,numberOfFits,numberOfParameters);
  
  /***** Output *****/
  printParametersGnuplotTransposed(matrixOfParameters,numberOfFits,numberOfParameters);
  //below commented, from now on R script should take care of it, Andrea 2012/09/05
  //printParametersGnuplot(matrixOfParameters,numberOfFits,numberOfParameters);
  //printParametersAveStdGnuplot(sampleAverage,sampleStd,numberOfFits,numberOfParameters);
  printLinkedList(&listOfBestFits);
  
  /***** Free memory before exit *****/
  deleteLinkedList(&listOfBestFits);
  deleteMatrixOfParameters(matrixOfParameters,numberOfFits);
  delete []sampleAverage;
  delete []sampleStd;
  
  return 0;
}


void linkedListAdd(linkedList **plist,int seed,double fit,int max){
  
  if(*plist == NULL){ //head is NULL, add as first
    linkedList *tmp = new linkedList;
    tmp->seed = seed;
    tmp->fit = fit;
    tmp->next = NULL;
    *plist = tmp;    
  }else{  //head is not NULL
    linkedList *iterator = *plist;
    if (iterator->fit > fit){ //new element is better than head
      linkedList *tmp = new linkedList;
      tmp->seed = seed;
      tmp->fit = fit;
      tmp->next = iterator;
      *plist = tmp;
    }else{  //new item is worse than head
      int depth = 2; //depth of iterator->next
      bool inserted = false;
      while(iterator->next != NULL){
        if (inserted == false && (iterator->next)->fit > fit){
          linkedList *tmp = new linkedList;
          tmp->seed = seed;
          tmp->fit = fit;
          tmp->next = iterator->next;
          iterator->next = tmp;
          inserted = true;
        }
        if (depth > max){
          delete iterator->next;
          iterator->next = NULL;
        }else{
          iterator=iterator->next;
          depth++;
        }
      }
      if (inserted == false && depth <= max){
          linkedList *tmp = new linkedList;
          tmp->seed = seed;
          tmp->fit = fit;
          tmp->next = NULL;
          iterator->next = tmp;
          inserted = true;        
      }
    }
  }
}

  
void deleteLinkedList(linkedList **plist){
  linkedList *iterator;
  if (*plist == NULL) return;
  while(*plist != NULL){
      iterator = (*plist)->next;
      delete *plist;
      *plist = iterator;
  }
}


void printLinkedList(linkedList **plist){
  std::stringstream filename;
  std::string sfilename;
  filename << "bestsAndSeeds.txt ";
  filename >> sfilename; 
  FILE * pFile = fopen(sfilename.c_str(),"w");;

  linkedList *iterator = *plist;
  while(iterator != NULL){
    //std::cout << iterator->seed << " " <<iterator->fit << std::endl;
    //std::cout << iterator->seed << std::endl;
    fprintf(pFile,"%.16f %d\n",iterator->fit,iterator->seed);
    iterator = iterator->next;
  }
  fclose(pFile);
}


void extractParameters(linkedList **plist,double**matrixOfParameters, int &numberOfParameters){
  linkedList *iterator = *plist;
  std::stringstream filename;
  std::string sfilename;
  std::ifstream infile;
  int currentParameterSet = 0;
  numberOfParameters = 0;
  
  while(iterator != NULL){
    filename << iterator->seed << ".best ";
    filename >> sfilename;
    //std::cout << sfilename << std::endl;
    
    //First open log file to count how many parameters are there
    infile.open(sfilename.c_str()); //open current log file
    while(!infile.eof()){
      std::string line;
      std::getline(infile,line);
      if(line == "----Normal----"){ //parameters reached, count how many
        std::getline(infile,line);
        numberOfParameters = 0;
        while(line != ""){
          numberOfParameters++;
          std::getline(infile,line);
        }
      }
    }
    infile.close(); //close current log file
    
    //Second open log file to read the parameters and store them
    infile.open(sfilename.c_str()); //open current log file
    while(!infile.eof()){
      std::string line;
      std::getline(infile,line);
      if(line == "----Normal----"){ //parameters reached, read and store
        matrixOfParameters[currentParameterSet] = new double[numberOfParameters];
        std::getline(infile,line);
        int i = 0;
        while(line != ""){
          std::stringstream streamline(line);
          streamline >> matrixOfParameters[currentParameterSet][i];
          streamline >> matrixOfParameters[currentParameterSet][i];
          std::getline(infile,line);
          i++;
        }
      }
    }
    infile.close(); //close current log file
    currentParameterSet++;
    iterator = iterator->next;
  }

}

void printParametersGnuplotTransposed(double**matrixOfParameters, int numberOfFits, int numberOfParameters){
  std::stringstream filename;
  std::string sfilename;
  filename << "bestFits.txt "; //transposed data
  filename >> sfilename; 
  FILE * pFile = fopen(sfilename.c_str(),"w");;

  for (int i=0;i<numberOfFits;i++){
    for (int j=0;j<numberOfParameters;j++){
      //std::cout << matrixOfParameters[i][j] << " ";
      fprintf(pFile,"%.64f ",matrixOfParameters[i][j]);
    }
    //std::cout << "\n";
    fprintf(pFile,"\n");
  }
  fclose(pFile);
}

void deleteMatrixOfParameters(double**matrixOfParameters, int numberOfFits){
    for (int i=0;i<numberOfFits;i++){
      delete []matrixOfParameters[i];
  }
  delete []matrixOfParameters;
}

void printParametersGnuplot(double**matrixOfParameters, int numberOfFits, int numberOfParameters){
  //first find maximum of each parameter
  double maximum[numberOfParameters];
  for (int j=0;j<numberOfParameters;j++) maximum[j] = 0;
  
  for (int i=0;i<numberOfFits;i++){
    for (int j=0;j<numberOfParameters;j++){
      if (matrixOfParameters[i][j] > maximum[j]) maximum[j] = matrixOfParameters[i][j];
    }
  }
  
  std::stringstream filename;
  std::string sfilename;
  filename << "bestFits.txtT";
  filename >> sfilename; 
  FILE * pFile = fopen(sfilename.c_str(),"w");;
  
  for (int i=0;i<numberOfParameters;i++){
    //printf("%d ",i);
    for (int j=0;j<numberOfFits;j++){
      //std::cout << matrixOfParameters[i][j] << " ";
      //printf("%.64f ",log(matrixOfParameters[j][i])/log(maximum[i]));
      fprintf(pFile,"%.64f ",matrixOfParameters[j][i]);
    }
    //std::cout << "\n";
    fprintf(pFile,"\n");
  }
  fclose(pFile);
}

double* computeAverageOfParameters(double**matrixOfParameters, int numberOfFits, int numberOfParameters){
  double * average = new double[numberOfParameters];
  for (int j=0;j<numberOfParameters;j++) average[j] = 0;  

  for (int i=0;i<numberOfParameters;i++){
    for (int j=0;j<numberOfFits;j++){
      average[i] += matrixOfParameters[j][i];
    }
    average[i] /= numberOfFits;
  }
  return average;
}

double* computeStdOfParameters(double**matrixOfParameters, double *average, int numberOfFits, int numberOfParameters){
  double * sampleStd = new double[numberOfParameters];
  for (int j=0;j<numberOfParameters;j++) sampleStd[j] = 0;  
  
  for (int i=0;i<numberOfParameters;i++){
    for (int j=0;j<numberOfFits;j++){
      sampleStd[i] += pow(matrixOfParameters[j][i]-average[i],2);
    }
    sampleStd[i] /= (numberOfFits - 1); //sample standard deviation
    sampleStd[i] = sqrt(sampleStd[i]);
  }  
  return sampleStd;
}

void printParametersAveStdGnuplot(double*average, double*std, int numberOfFits, int numberOfParameters){
  std::stringstream filename;
  std::string sfilename;
  filename << "bestFitsAveStd.txtT";
  filename >> sfilename; 
  FILE * pFile = fopen(sfilename.c_str(),"w");;
  
  for (int i=0;i<numberOfParameters;i++){
    //fprintf(pFile,"%.64f %.64f\n",log(average[i]),log(std[i]));
    fprintf(pFile,"%.64f %.64f\n",average[i],std[i]);
  }
  fclose(pFile);
}
