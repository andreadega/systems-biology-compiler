#!/bin/bash

X=$1
JACOBIANMETHOD=$2

if [ "$X" = '' ]; then
  X=0
fi

if [ "$JACOBIANMETHOD" = '' ]; then
  JACOBIANMETHOD="SE"
fi

#make

LD_LIBRARY_PATH=../common/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH
DYLD_LIBRARY_PATH=../common/lib:${DYLD_LIBRARY_PATH}
export DYLD_LIBRARY_PATH


if [ "$JACOBIANMETHOD" = 'SE' ]; then
  ./levmarOptimiser_se $X > results/$X.res 2> results/$X.log
elif [ "$JACOBIANMETHOD" = 'FD' ]; then
  ./levmarOptimiser_fd $X > results/$X.res 2> results/$X.log
fi
