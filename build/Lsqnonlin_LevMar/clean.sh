#!/bin/bash

make clean
cd results
rm -f 000_bestValues.txt
rm -f summaryTable.txt
rm -f *.res
rm -f *.log
rm -f *.best
rm -f extractBestFits
rm -f bestFits.txt
