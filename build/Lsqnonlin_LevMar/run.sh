#!/bin/bash

X=$1
JACOBIANMETHOD=$4
END=$3

if [ "$X" = '' ]; then
  X=1
fi

if [ "$END" = '' ]; then
  END=2
fi

if [ "$JACOBIANMETHOD" = '' ]; then
  JACOBIANMETHOD="SE"
fi

for i in `seq 1 $END`;
do
  ./runLevMar.sh $X $JACOBIANMETHOD
  cd results
  ./appendResult.sh $X
  COUNTING=`wc -l 000_bestValues.txt | awk {'print $1'}`
  #echo ${COUNTING} > tmp
  echo done with ${COUNTING} optimisations
  let X=X+1
  cd ..
done

