#!/bin/bash

BESTFITS=$1

if [ "$BESTFITS" = '' ]; then
  let BESTFITS=96
fi

Rscript retrieveNEvalVsBest.R $BESTFITS

mv nEvalVsBest.txt ../../nEvalVsBest/
mv timeVsBest.txt ../../nEvalVsBest/

if [ -f "nEvalVsBestLS.txt" ]; then
  mv nEvalVsBestLS.txt ../../nEvalVsBest/
fi

if [ -f "timeVsBestLS.txt" ]; then
  mv timeVsBestLS.txt ../../nEvalVsBest/
fi
