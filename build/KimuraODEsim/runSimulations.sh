#!/bin/bash

INPUT1=$1
PARSETS=1
RV=1

cd ..
./buildLibraries.sh
if [ $? != 0 ]; then
  echo "Failed to compile all libraries, quitting the pipeline"
  exit 1
fi
cd KimuraODEsim

make
chmod 755 ODEsimulator

LD_LIBRARY_PATH=../common/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH
DYLD_LIBRARY_PATH=../common/lib:${DYLD_LIBRARY_PATH}
export DYLD_LIBRARY_PATH

if [ "$INPUT1" = '' -o "$INPUT1" = '../' ]; then
  ./ODEsimulator 1
  RV=$?
else
  PARSETS=`wc -l ${INPUT1} | awk {'print $1'}`
  #cp ../KimuraGLSDC/glsdc/v1/work/sbcmodel_all/1/bestFits${INPUT1}T.txt .
  ./ODEsimulator ${PARSETS} < ${INPUT1}
  RV=$?
fi

#echo "Return value is: $?"
if [ "$RV" = '0' ]; then
  echo "Simulator successful (rv=${RV})"
  cd plots
  chmod 755 plots.sh
  ./plots.sh ${PARSETS}
else
  echo "Simulator could not continue (rv=${RV})"
fi  
  

