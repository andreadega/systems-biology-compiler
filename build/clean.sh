#!/bin/bash

rm -f runSimulations.sh
rm -f runParameterEstimationOnUnixCluster.sh
rm -f runParameterEstimationOnLinuxDesktop.sh
rm -f runParameterAnalysis.sh
rm -f retrieveBestFits.sh

#cd  build
rm -f results.log
#rm -f sbc.jar

cd Lsqnonlin_LevMar
./clean.sh
rm -f runall.sh
rm -f pbspostprocess.sh
rm -f pbsrunallnodes.sh
rm -f pbsrunall.sh
rm -f levmarOptimiser.c
cd ..

cd KimuraGLSDC/glsdc/v1/
rm -f bin/a.out
make clean
rm -f src/fitness_sbcmodel_org.C
rm -f pbspostprocess.sh
rm -f pbsrunallnodes.sh
#rm -f postprocess.sh
rm -f runall.sh
rm -f pbsrunall.sh
cd work/sbcmodel_all/
rm -f sbcmodel
rm -f pset_sbcmodel_org.dat
cd 1/
rm -f exp_sbcmodel_org_1.dat_top
rm -f extractBestFits
rm -f summaryTable.txt
rm -f *.txt
rm -f *.log
rm -f *.res
rm -f *.best
cd ../../../../../..
cd parameterAnalysis
rm -f *.txt
rm -f *.png
rm -f *.jpg
rm -f pset.dat
rm -f correlationMatrix.R
rm -f visualiseBestFits.plt
rm -f visualiseBestFitsClusters.plt
cd ..
cd KimuraODEsim
rm -rf sims
rm -rf data
rm -rf plots
rm -f *.txt
rm -f ODEsimulator*
cd ../..
