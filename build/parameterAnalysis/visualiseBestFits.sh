#!/bin/bash

INPUT1=$1

if [ "$INPUT1" = '' -o "$INPUT1" = '../' ]; then
  echo "file name not provided. Exit."
  exit
fi

PARSETS=`wc -l ${INPUT1} | awk {'print $1'}`

gnuplot -e "numberOfBestFits=${PARSETS};fileWithPars='${INPUT1}'" visualiseBestFits.plt
