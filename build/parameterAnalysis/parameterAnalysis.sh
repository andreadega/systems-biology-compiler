#!/bin/bash

#filename
INPUT1=$1

if [ "$INPUT1" = '' -o "$INPUT1" = '../' ]; then
  echo "file name not provided. Exit."
  exit
fi

#cp ../KimuraGLSDC/glsdc/v1/work/sbcmodel_all/1/bestFits${INPUT1}T.txt .
cp ../KimuraGLSDC/glsdc/v1/work/sbcmodel_all/pset_sbcmodel_org.dat pset.dat

mkdir tmp
chmod 777 tmp
export TMPDIR=tmp

Rscript aveStdAndRotated.R ${INPUT1}
Rscript correlationMatrix.R ${INPUT1}
./visualiseBestFits.sh ${INPUT1}
#uncomment below for clustering parameter sets, number of clusters is the second parameters (7 is max)
#./visualiseBestFitsClusters.sh ${INPUT1} 2

rm -r tmp
