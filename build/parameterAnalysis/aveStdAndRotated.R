#!/usr/bin/Rscript

args <- commandArgs(TRUE)
bestfits <- args[1]
#cluster <- args[2]
if (is.na(bestfits)) bestfits <- "../bestFits.txt"
#if (is.na(cluster)) cluster <- ""

readfrom <- paste(bestfits,sep='')
writetoT <- paste(bestfits,"T",sep="")
writetoAveStd <- paste(bestfits,"AveStdT",sep="")

#read matrix
matrixOfParams <- read.table(readfrom)
matrixTr <- t(matrixOfParams)

#write transposed matrix
write.table(matrix(sprintf("%.64f",matrixTr),nrow=dim(matrixOfParams)[2]),writetoT,col.names = FALSE,row.names = FALSE,quote = FALSE)

#compute mean/standard deviation/coefficient of variation matrix
matrixTrMean <- apply(matrixTr,1,mean)
matrixTrSd <- apply(matrixTr,1,sd)
matrixTrCV <- matrixTrSd/matrixTrMean
matrixTrMSC <- cbind(matrixTrMean,matrixTrSd,matrixTrCV)

write.table(matrix(sprintf("%.64f",matrixTrMSC),nrow=dim(matrixOfParams)[2]),writetoAveStd,col.names = FALSE,row.names = FALSE,quote = FALSE)
