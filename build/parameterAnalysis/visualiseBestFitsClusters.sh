#!/bin/bash

INPUT1=$1
NOFCLUSTERS=$2

if [ "$INPUT1" = '' -o "$INPUT1" = '../' ]; then
  echo "file name not provided. Exit."
  exit
fi

if [ "$NOFCLUSTERS" = '' ]; then
  let NOFCLUSTERS=2
fi

PARSETS=`wc -l ${INPUT1} | awk {'print $1'}`

CLUSTERNAMES[1]='A'
CLUSTERNAMES[2]='B'
CLUSTERNAMES[3]='C'
CLUSTERNAMES[4]='D'
CLUSTERNAMES[5]='E'
CLUSTERNAMES[6]='F'
CLUSTERNAMES[7]='G'

Rscript classifyParameters.R ${INPUT1} ${NOFCLUSTERS}

for i in `seq ${NOFCLUSTERS}`;
do
  Rscript aveStdAndRotated.R ${INPUT1}${CLUSTERNAMES[$i]}
  LINES=$(wc -l ${INPUT1}${CLUSTERNAMES[$i]} | awk {'print $1'})
  cp ${INPUT1}${CLUSTERNAMES[$i]} ../
  gnuplot -e "numberOfBestFits=${PARSETS};numberOfBestFitsCluster=${LINES};cluster='${CLUSTERNAMES[$i]}';fileWithPars='${INPUT1}'" visualiseBestFitsClusters.plt
done


#LINESA=$(wc -l bestFits100TA.txt | cut -f1 -d' ')
#LINESB=$(wc -l bestFits100TB.txt | cut -f1 -d' ')

#gnuplot -e "numberOfBestFits=${INPUT1};numberOfBestFitsCluster=${LINESA};cluster='A'" visualiseBestFitsClusters.plt
#gnuplot -e "numberOfBestFits=${INPUT1};numberOfBestFitsCluster=${LINESB};cluster='B'" visualiseBestFitsClusters.plt
