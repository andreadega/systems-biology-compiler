#!/bin/bash

COLUMNS=`awk -F' ' '{print NF; exit}' nEvalVsBest.txt`
#`wc -l $i.res | awk {'print $1'}`
let COLUMNS=COLUMNS-1

#fill NA
Rscript fillNA.R nEvalVsBest.txt
Rscript fillNA.R timeVsBest.txt


#average and standard deviation
Rscript aveStd.R nEvalVsBest.txt
Rscript aveStd.R timeVsBest.txt
Rscript percentiles.R nEvalVsBest.txt
Rscript percentiles.R timeVsBest.txt

#plots
gnuplot -e "numberOfBestFits=${COLUMNS}" plotNEvalVsBest.plt
gnuplot -e "numberOfBestFits=${COLUMNS}" plotNEvalVsBest_AveSD.plt
gnuplot -e "numberOfBestFits=${COLUMNS}" plotNEvalVsBest_Perc.plt
gnuplot -e "numberOfBestFits=${COLUMNS}" plotTimeVsBest.plt
gnuplot -e "numberOfBestFits=${COLUMNS}" plotTimeVsBest_AveSD.plt
gnuplot -e "numberOfBestFits=${COLUMNS}" plotTimeVsBest_Perc.plt
