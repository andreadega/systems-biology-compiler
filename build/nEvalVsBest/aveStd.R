#!/usr/bin/Rscript

args <- commandArgs(TRUE)
fileInput <- args[1]
if (is.na(fileInput)) fileInput <- "nEvalVsBest.txt"

writetoAveStd <- paste(strsplit(fileInput,'.',fixed=TRUE)[[1]][1],"_AveSD.txt",sep='')

#read matrix
nevalVSbest_table <- read.table(fileInput)

#compute mean/standard deviation, skipping first column
matrixMean <- apply(nevalVSbest_table[,-1],1,mean,na.rm = TRUE)
matrixSd <- apply(nevalVSbest_table[,-1],1,sd,na.rm = TRUE)

#write.table(matrix(sprintf("%.64f",cbind(nevalVSbest_table[,1],matrixMean,matrixSd)),nrow=dim(matrixOfParams)[2]),writetoAveStd,col.names = FALSE,row.names = FALSE,quote = FALSE)
write.table(cbind(nevalVSbest_table[,1],matrixMean,matrixSd),writetoAveStd,col.names = FALSE,row.names = FALSE,quote = FALSE)
