#!/usr/bin/gnuplot -persist

set term pngcairo dashed dl 5 enhanced size 2800, 2000 font "Arial, 64"

set lmargin at screen 0.15
set bmargin at screen 0.15
set rmargin at screen 0.9
set tmargin at screen 0.9
set size 1,1 
set key bmargin 

set output "timeVsBest_AveSD.png"
set border lw 16

set title "Average and Standard Dev. Time vs best (n=".numberOfBestFits.")"
set style line 1 linetype 1 lw 16 lc 1
set style line 2 linetype 2 lw 16 lc 2
set style line 3 linetype 3 lw 16 lc 3
set style line 4 linetype 4 lw 16 lc 4
set style line 5 linetype 5 lw 16 lc 5
set style line 6 linetype 6 lw 16 lc 7
set style line 11 linetype 1 lw 10 pt 2 ps 16 lc 1
set style line 12 linetype 2 lw 10 pt 3 ps 16 lc 2
set style line 13 linetype 3 lw 10 pt 4 ps 16 lc 3
set style line 14 linetype 4 lw 10 pt 6 ps 16 lc 4
set style line 15 linetype 5 lw 10 pt 8 ps 16 lc 5
set style line 16 linetype 6 lw 10 pt 1 ps 16 lc 7

set log x
set xrange [:]
set yrange [:]
set xlabel "Time (s)"
set ylabel "Objective function value"

#plot "timeVsBest_AveSD.txt" using 1:2 with lines ls 1 notitle, \
#  "timeVsBest_AveSD.txt" using 1:($2+$3) with lines ls 1 notitle, \
#  "timeVsBest_AveSD.txt" using 1:($2-$3) with lines ls 1 notitle

plot "timeVsBest_AveSD.txt" using 1:2:3 with errorbars ls 1 notitle
