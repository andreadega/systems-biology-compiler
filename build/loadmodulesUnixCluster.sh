#!/bin/bash -l

if hash module 2>/dev/null; then
  module load cmake
  module load R
  module load gnuplot
fi
